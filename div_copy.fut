let extractIndex [n] (a: [n]i32): i32 =
    let aIdx_tmp = zip a (iota n)
    in #2 (filter (\(x,_) -> x != 0) aIdx_tmp)[0]

let checkZero (x: i32) (y:i32): i32 =
    if bool(x) || bool(y)
        then if bool(x) then x else y
    else 0

let checkLessDegree (x: []i32) (y: []i32): bool =
    if reduce (+) 0 x == 0 then true
    else extractIndex x < extractIndex y


let div [degreeB] [n][degreeA] (b: [degreeB]i32, ass: [n][degreeA]i32): ([][]i32, [][]i32, bool, bool) =
    let bsInit = replicate n b
    let remain = loop (as, bs, isZero, isValid) = (ass, bsInit, false, false) for loopIdx < degreeB do
        if !isZero && !isValid then
            let polIdx = iota n
            let bsIdx = map(\b -> extractIndex b) bs
            let results = map (\idx -> bs[idx, bsIdx[idx]] / as[idx, 0]) polIdx
            let ends = map (\i ->
                let endTmp = map(\j ->
                    let res = as[i, j] * results[i]
                    in bs[i, j + bsIdx[i]] - res
                ) (iota degreeA)

                let kage =  degreeA +  bsIdx[i]
                in concat (bs[i, :bsIdx[i]]) endTmp (bs[i, kage:])
            ) polIdx

            let bsAs = zip ends as
            let bsAsTmp = filter (\(b,a) -> checkLessDegree b a) bsAs
            let newBsAs = unzip bsAsTmp

            let isZeros = map (\end ->
                reduce checkZero 0 end
            ) ends
            let isZero = reduce (+) 0 isZeros

            in (#2 newBsAs, #1 newBsAs, isZero == 0, null #1 newBsAs)
         else (as, bs, isZero, isValid)
    in
    remain



let main() : ([][]i32, [][]i32, bool, bool) = div([0,1,1], [[1,0]])