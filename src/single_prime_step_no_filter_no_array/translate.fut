-- Tests if the translate function handles polynomials                                           
-- without padding                                                                               
-- ==                                                                                            
-- entry: translateTestTight                                                                     
-- compiled input { 1 2 0  } output { [0,0] }                                                    
-- compiled input { 1 2 1  } output { [0,1] }                                                    
-- compiled input { 1 2 2  } output { [1,0] }                                                    
-- compiled input { 1 2 3  } output { [1,1] }                                                    
-- compiled input { 1 2 4  } output { [0,0] }                                                    
-- compiled input { 1 2 5  } output { [0,1] }                                                    
-- compiled input { 2 5 0  } output { [0,0,0] }                                                  
-- compiled input { 2 5 1  } output { [0,0,1] }                                                  
-- compiled input { 2 5 2  } output { [0,0,2] }                                                  
-- compiled input { 2 5 3  } output { [0,0,3] }                                                  
-- compiled input { 2 5 4  } output { [0,0,4] }                                                  
-- compiled input { 2 5 5  } output { [0,1,0] }                                                  
-- compiled input { 2 5 6  } output { [0,1,1] }                                                  
-- compiled input { 2 5 7  } output { [0,1,2] }                                                  
-- compiled input { 2 5 8  } output { [0,1,3] }                                                  
-- compiled input { 2 5 9  } output { [0,1,4] }                                                  
-- compiled input { 2 5 10 } output { [0,2,0] }                                                  
-- compiled input { 2 5 11 } output { [0,2,1] }                                                  
-- compiled input { 2 5 12 } output { [0,2,2] }                                                  
-- compiled input { 2 5 13 } output { [0,2,3] }                                                  
-- compiled input { 2 5 14 } output { [0,2,4] }                                                  
-- compiled input { 2 5 15 } output { [0,3,0] } 

-- Tests if the translate function handles polynomials                                           
-- with padding                                                                                  
-- ==                                                                                            
-- entry: translateTestLoose                                                                     
-- compiled input { 1 2 0  } output { [0,0] }                                                    
-- compiled input { 1 2 1  } output { [0,1] }                                                    
-- compiled input { 1 2 2  } output { [1,0] }                                                    
-- compiled input { 1 2 3  } output { [1,1] }                                                    
-- compiled input { 1 2 4  } output { [0,0] }                                                    
-- compiled input { 1 2 5  } output { [0,1] }                                                    
-- compiled input { 2 5 0  } output { [0,0,0,0] }                                                
-- compiled input { 2 5 1  } output { [0,0,0,1] }                                                
-- compiled input { 2 5 2  } output { [0,0,0,2] }                                                
-- compiled input { 2 5 3  } output { [0,0,0,3] }                                                
-- compiled input { 2 5 4  } output { [0,0,0,4] }                                                
-- compiled input { 2 5 5  } output { [0,0,1,0] }                                                
-- compiled input { 2 5 6  } output { [0,0,1,1] }                                                
-- compiled input { 2 5 7  } output { [0,0,1,2] }                                                
-- compiled input { 2 5 8  } output { [0,0,1,3] }                                                
-- compiled input { 2 5 9  } output { [0,0,1,4] }                                                
-- compiled input { 2 5 10 } output { [0,0,2,0] }                                                
-- compiled input { 2 5 11 } output { [0,0,2,1] }                                                
-- compiled input { 2 5 12 } output { [0,0,2,2] }                                                
-- compiled input { 2 5 13 } output { [0,0,2,3] }                                                
-- compiled input { 2 5 14 } output { [0,0,2,4] }                                                
-- compiled input { 2 5 15 } output { [0,0,3,0] }             

import "/futlib/math"

let log_x (x: f32) (i: i32) : i32 =
    if x > 0f32 then i32((f32.log x) / (f32.log (f32(i)))) else 0

-- Translate a polynomial tuple into a array polynomial
entry translate (padding: i32) (d: i32, p: i32, x: i32) : [padding]i32 =
  map (\i ->
    if i < padding - d - 1 then 0
    else (x  / (p ** (padding - i - 1))) % p)
  (iota padding)

entry idx (d: i32, p: i32, x: i32) (i: i32): i32 = x / (p ** (d - i)) % p

entry mul (d: i32, p: i32, x: i32) (i: i32) (v: i32): (i32, i32, i32) =
    let curVal = idx (d, p, x) i
    in (d, p, x + ((curVal * v) % p - curVal) * (p ** (d - i)))

entry minus (d: i32, p: i32, x: i32) (i: i32) (v: i32): (i32, i32, i32) =
    let curVal = idx (d, p, x) i
    in (d, p, x + ((curVal - v) % p - curVal) * (p ** (d - i)))

entry isZeroPolynomial (d: i32, p: i32, x: i32) : bool =
    x % (p ** (d + 1)) == 0

entry truncate (d: i32, p: i32, x: i32) : (i32, i32, i32) =
    if x % p ** (d + 1) < p ** d then
        let dif = (log_x (f32((p ** d) / (x + 1))) p) + 1
        in if d - dif < 0 then (0, p, x)
           else (d - dif, p, x)
    else (d, p, x)

entry rotateLeft (d: i32, p: i32, x: i32) (i: i32) : (i32, i32, i32) =
    (d + i, p, x * (p ** i))


-- Following 2 functions are implemented to get around the problem with                          
-- tuples not being supported properly in Futharks testing environment                           
entry translateTestTight (d: i32, p: i32, x: i32) : []i32 =                                      
    translate (d + 1) (d: i32, p: i32, x: i32)                                                   
                                                                                                 
entry translateTestLoose (d: i32, p: i32, x: i32) : []i32 =                                      
    translate (d * 2) (d: i32, p: i32, x: i32)  















--let main (i : i32) : i32 =
  -- let pol = (2,5,146)
   --in translate 4 pol
  --   let mulPol = (idx pol i) * 4
    --in [#1 mulPol, #2 mulPol, #3 mulPol]
    -- in #1(truncate pol)



    --let pol = translate 3 (2,5,6)
    --in idx (2,5,6) i



