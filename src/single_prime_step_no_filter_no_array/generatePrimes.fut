import "futlib/monoid"

module segmented_scan(M: monoid): {
  val segmented_scan : []i32 -> []M.t -> []M.t
} = {
  let segmented_scan [n] (flags: [n]i32) (as: [n]M.t): [n]M.t =
    #2 (unzip (scan (\(x_flag,x) (y_flag,y) ->
                     if y_flag > 0
                     then (x_flag | y_flag, y)
                     else (x_flag | y_flag, M.op x y))
                    (0i32, M.ne)
                    (zip flags as)))
}

module SS = segmented_scan { type t = i32
                             let ne = 0i32
 		                     let op (x:i32) (y:i32) = x i32.+ y
                           }


-- Generates all primes less than n
entry generatePrimes (n : i32) : []i32 =
  if n <= 8 then [2,3,5,7]
  else let sq= i32( f64.sqrt (f64 n) )
       let sq_primes = generatePrimes sq
       let ms = map(\p -> n / p) sq_primes
       let mm1s = map(\m -> m - 1) ms
       let inds_tmp = scan (+) 0 mm1s
       let inds = map (\ i -> if i > 0 then inds_tmp[i-1] else 0) (iota (length sq_primes))
       let size = (last inds) + (last mm1s)
       let flag = scatter (replicate size 0) inds mm1s
       let tmp = replicate size 1
       let iots = SS.segmented_scan flag tmp
       let twoms = map (1+) iots
       let rps_size = (last inds) + (last mm1s)
       let vals = scatter (replicate size 0) inds sq_primes
       let rps = SS.segmented_scan flag vals
       let not_primes = map(\(j,p) -> j * p) (zip twoms rps)
       let zero_array = replicate rps_size 0
       let mostly_ones= map (\ x -> if x > 1 then 1 else 0) (iota (n+1))
       let prime_flags= scatter mostly_ones not_primes zero_array
       in filter (\i -> unsafe prime_flags[i] != 0) (iota (n+1))
