#ifdef _MSC_VER
#define inline __inline
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <getopt.h>
static int detail_memory = 0;
static int debugging = 0;
static int binary_output = 0;
/* Crash and burn. */

#include <stdarg.h>

static const char *fut_progname;

void panic(int eval, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
        fprintf(stderr, "%s: ", fut_progname);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
        exit(eval);
}

//// Text I/O

struct array_reader {
  char* elems;
  int64_t n_elems_space;
  int64_t elem_size;
  int64_t n_elems_used;
  int64_t *shape;
  int (*elem_reader)(void*);
};

static int peekc() {
  int c = getchar();
  if (c != EOF) {
    ungetc(c,stdin);
  }
  return c;
}

static int next_is_not_constituent() {
  int c = peekc();
  return c == EOF || !isalnum(c);
}

static void skipspaces() {
  int c = getchar();
  if (isspace(c)) {
    skipspaces();
  } else if (c == '-' && peekc() == '-') {
    // Skip to end of line.
    for (; c != '\n' && c != EOF; c = getchar());
    // Next line may have more spaces.
    skipspaces();
  } else if (c != EOF) {
    ungetc(c, stdin);
  }
}

static int read_str_elem(struct array_reader *reader) {
  int ret;
  if (reader->n_elems_used == reader->n_elems_space) {
    reader->n_elems_space *= 2;
    reader->elems = (char*) realloc(reader->elems,
                                    reader->n_elems_space * reader->elem_size);
  }

  ret = reader->elem_reader(reader->elems + reader->n_elems_used * reader->elem_size);

  if (ret == 0) {
    reader->n_elems_used++;
  }

  return ret;
}

static int read_str_array_elems(struct array_reader *reader, int dims) {
  int c;
  int ret;
  int first = 1;
  char *knows_dimsize = (char*) calloc(dims,sizeof(char));
  int cur_dim = dims-1;
  int64_t *elems_read_in_dim = (int64_t*) calloc(dims,sizeof(int64_t));
  while (1) {
    skipspaces();

    c = getchar();
    if (c == ']') {
      if (knows_dimsize[cur_dim]) {
        if (reader->shape[cur_dim] != elems_read_in_dim[cur_dim]) {
          ret = 1;
          break;
        }
      } else {
        knows_dimsize[cur_dim] = 1;
        reader->shape[cur_dim] = elems_read_in_dim[cur_dim];
      }
      if (cur_dim == 0) {
        ret = 0;
        break;
      } else {
        cur_dim--;
        elems_read_in_dim[cur_dim]++;
      }
    } else if (c == ',') {
      skipspaces();
      c = getchar();
      if (c == '[') {
        if (cur_dim == dims - 1) {
          ret = 1;
          break;
        }
        first = 1;
        cur_dim++;
        elems_read_in_dim[cur_dim] = 0;
      } else if (cur_dim == dims - 1) {
        ungetc(c, stdin);
        ret = read_str_elem(reader);
        if (ret != 0) {
          break;
        }
        elems_read_in_dim[cur_dim]++;
      } else {
        ret = 1;
        break;
      }
    } else if (c == EOF) {
      ret = 1;
      break;
    } else if (first) {
      if (c == '[') {
        if (cur_dim == dims - 1) {
          ret = 1;
          break;
        }
        cur_dim++;
        elems_read_in_dim[cur_dim] = 0;
      } else {
        ungetc(c, stdin);
        ret = read_str_elem(reader);
        if (ret != 0) {
          break;
        }
        elems_read_in_dim[cur_dim]++;
        first = 0;
      }
    } else {
      ret = 1;
      break;
    }
  }

  free(knows_dimsize);
  free(elems_read_in_dim);
  return ret;
}

static int read_str_empty_array(const char *type_name, int64_t *shape, int64_t dims) {
  char c;
  if (scanf("empty") == EOF) {
    return 1;
  }

  c = getchar();
  if (c != '(') {
    return 1;
  }

  for (int i = 0; i < dims-1; i++) {
    c = getchar();
    if (c != '[') {
      return 1;
    }
    c = getchar();
    if (c != ']') {
      return 1;
    }
  }

  int n = strlen(type_name);
  for (int i = 0; i < n; i++) {
    c = getchar();
    if (c != type_name[i]) {
      return 1;
    }
  }

  if (getchar() != ')') {
    return 1;
  }

  for (int i = 0; i < dims; i++) {
    shape[i] = 0;
  }

  return 0;
}

static int read_str_array(int64_t elem_size, int (*elem_reader)(void*),
                          const char *type_name,
                          void **data, int64_t *shape, int64_t dims) {
  int ret;
  struct array_reader reader;
  int64_t read_dims = 0;

  while (1) {
    int c;
    skipspaces();
    c = getchar();
    if (c=='[') {
      read_dims++;
    } else {
      if (c != EOF) {
        ungetc(c, stdin);
      }
      break;
    }
  }

  if (read_dims == 0) {
    return read_str_empty_array(type_name, shape, dims);
  }

  if (read_dims != dims) {
    return 1;
  }

  reader.shape = shape;
  reader.n_elems_used = 0;
  reader.elem_size = elem_size;
  reader.n_elems_space = 16;
  reader.elems = (char*) realloc(*data, elem_size*reader.n_elems_space);
  reader.elem_reader = elem_reader;

  ret = read_str_array_elems(&reader, dims);

  *data = reader.elems;

  return ret;
}

/* Makes a copy of numeric literal removing any underscores, and
   length of the literal. */
static int remove_underscores(char* buf) {
  int buf_index = 0;
  char c = getchar();
  while (isxdigit(c) || c == '.' || c == '+' || c == '-' ||
         c == 'x' || c == 'X' ||
         c == 'p' || c == 'P' || /* exponent for hex. floats */
         c == 'e' || c == 'E' || c == '_') {
    if (c == '_') {
      c = getchar();
      continue;
    }
    else {
      buf[buf_index++] = c;
      c = getchar();
    }
  }
  buf[buf_index] = 0;
  ungetc(c, stdin);             /* unget 'i' */
  return buf_index;
}

static int read_str_i8(void* dest) {
  skipspaces();
  /* Some platforms (WINDOWS) does not support scanf %hhd or its
     cousin, %SCNi8.  Read into int first to avoid corrupting
     memory.

     https://gcc.gnu.org/bugzilla/show_bug.cgi?id=63417  */
  int x;
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%i", &x) == 1) {
    *(int8_t*)dest = x;
    scanf("i8");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_u8(void* dest) {
  skipspaces();
  /* Some platforms (WINDOWS) does not support scanf %hhd or its
     cousin, %SCNu8.  Read into int first to avoid corrupting
     memory.

     https://gcc.gnu.org/bugzilla/show_bug.cgi?id=63417  */
  int x;
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%i", &x) == 1) {
    *(uint8_t*)dest = x;
    scanf("u8");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_i16(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi16, (int16_t*)dest) == 1) {
    scanf("i16");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    printf("fail\n");
    return 1;
  }
}

static int read_str_u16(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi16, (int16_t*)dest) == 1) {
    scanf("u16");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_i32(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi32, (int32_t*)dest) == 1) {
    scanf("i32");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_u32(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi32, (int32_t*)dest) == 1) {
    scanf("u32");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_i64(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi64, (int64_t*)dest) == 1) {
    scanf("i64");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_u64(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  // FIXME: This is not correct, as SCNu64 only permits decimal
  // literals.  However, SCNi64 does not handle very large numbers
  // correctly (it's really for signed numbers, so that's fair).
  if (sscanf(buf, "%"SCNu64, (int64_t*)dest) == 1) {
    scanf("u64");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_f32(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%f", (float*)dest) == 1) {
    scanf("f32");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_f64(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%lf", (double*)dest) == 1) {
    scanf("f64");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_bool(void* dest) {
  /* This is a monstrous hack.  Maybe we should get a proper lexer in here. */
  char b[4];
  skipspaces();
  if (scanf("%4c", b) == 1) {
    if (strncmp(b, "true", 4) == 0) {
      *(char*)dest = 1;
      return 0;
    } else if (strncmp(b, "fals", 4) == 0 && getchar() == 'e') {
      *(char*)dest = 0;
      return 0;
    } else {
      return 1;
    }
  } else {
    return 1;
  }
}

static int write_str_i8(FILE *out, int8_t *src) {
  return fprintf(out, "%hhdi8", *src);
}

static int write_str_u8(FILE *out, uint8_t *src) {
  return fprintf(out, "%hhuu8", *src);
}

static int write_str_i16(FILE *out, int16_t *src) {
  return fprintf(out, "%hdi16", *src);
}

static int write_str_u16(FILE *out, uint16_t *src) {
  return fprintf(out, "%huu16", *src);
}

static int write_str_i32(FILE *out, int32_t *src) {
  return fprintf(out, "%di32", *src);
}

static int write_str_u32(FILE *out, uint32_t *src) {
  return fprintf(out, "%uu32", *src);
}

static int write_str_i64(FILE *out, int64_t *src) {
  return fprintf(out, "%"PRIi64"i64", *src);
}

static int write_str_u64(FILE *out, uint64_t *src) {
  return fprintf(out, "%"PRIu64"u64", *src);
}

static int write_str_f32(FILE *out, float *src) {
  return fprintf(out, "%.6ff32", *src);
}

static int write_str_f64(FILE *out, double *src) {
  return fprintf(out, "%.6ff64", *src);
}

static int write_str_bool(FILE *out, void *src) {
  return fprintf(out, *(char*)src ? "true" : "false");
}

//// Binary I/O

#define BINARY_FORMAT_VERSION 2
#define IS_BIG_ENDIAN (!*(unsigned char *)&(uint16_t){1})

// Reading little-endian byte sequences.  On big-endian hosts, we flip
// the resulting bytes.

static int read_byte(void* dest) {
  int num_elems_read = fread(dest, 1, 1, stdin);
  return num_elems_read == 1 ? 0 : 1;
}

static int read_le_2byte(void* dest) {
  uint16_t x;
  int num_elems_read = fread(&x, 2, 1, stdin);
  if (IS_BIG_ENDIAN) {
    x = (x>>8) | (x<<8);
  }
  *(uint16_t*)dest = x;
  return num_elems_read == 1 ? 0 : 1;
}

static int read_le_4byte(void* dest) {
  uint32_t x;
  int num_elems_read = fread(&x, 4, 1, stdin);
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>24)&0xFF) |
      ((x>>8) &0xFF00) |
      ((x<<8) &0xFF0000) |
      ((x<<24)&0xFF000000);
  }
  *(uint32_t*)dest = x;
  return num_elems_read == 1 ? 0 : 1;
}

static int read_le_8byte(void* dest) {
  uint64_t x;
  int num_elems_read = fread(&x, 8, 1, stdin);
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>56)&0xFFull) |
      ((x>>40)&0xFF00ull) |
      ((x>>24)&0xFF0000ull) |
      ((x>>8) &0xFF000000ull) |
      ((x<<8) &0xFF00000000ull) |
      ((x<<24)&0xFF0000000000ull) |
      ((x<<40)&0xFF000000000000ull) |
      ((x<<56)&0xFF00000000000000ull);
  }
  *(uint64_t*)dest = x;
  return num_elems_read == 1 ? 0 : 1;
}

static int write_byte(void* dest) {
  int num_elems_written = fwrite(dest, 1, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

static int write_le_2byte(void* dest) {
  uint16_t x = *(uint16_t*)dest;
  if (IS_BIG_ENDIAN) {
    x = (x>>8) | (x<<8);
  }
  int num_elems_written = fwrite(&x, 2, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

static int write_le_4byte(void* dest) {
  uint32_t x = *(uint32_t*)dest;
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>24)&0xFF) |
      ((x>>8) &0xFF00) |
      ((x<<8) &0xFF0000) |
      ((x<<24)&0xFF000000);
  }
  int num_elems_written = fwrite(&x, 4, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

static int write_le_8byte(void* dest) {
  uint64_t x = *(uint64_t*)dest;
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>56)&0xFFull) |
      ((x>>40)&0xFF00ull) |
      ((x>>24)&0xFF0000ull) |
      ((x>>8) &0xFF000000ull) |
      ((x<<8) &0xFF00000000ull) |
      ((x<<24)&0xFF0000000000ull) |
      ((x<<40)&0xFF000000000000ull) |
      ((x<<56)&0xFF00000000000000ull);
  }
  int num_elems_written = fwrite(&x, 8, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

//// Types

typedef int (*writer)(FILE*, void*);
typedef int (*reader)(void*);

struct primtype_info_t {
  const char binname[4]; // Used for parsing binary data.
  const char* type_name; // Same name as in Futhark.
  const int size; // in bytes
  const writer write_str; // Write in text format.
  const reader read_str; // Read in text format.
  const writer write_bin; // Write in binary format.
  const reader read_bin; // Read in binary format.
};

const static struct primtype_info_t i8 =
  {.binname = "  i8", .type_name = "i8",   .size = 1,
   .write_str = (writer)write_str_i8, .read_str = (reader)read_str_i8,
   .write_bin = (writer)write_byte, .read_bin = (reader)read_byte};
const static struct primtype_info_t i16 =
  {.binname = " i16", .type_name = "i16",  .size = 2,
   .write_str = (writer)write_str_i16, .read_str = (reader)read_str_i16,
   .write_bin = (writer)write_le_2byte, .read_bin = (reader)read_le_2byte};
const static struct primtype_info_t i32 =
  {.binname = " i32", .type_name = "i32",  .size = 4,
   .write_str = (writer)write_str_i32, .read_str = (reader)read_str_i32,
   .write_bin = (writer)write_le_4byte, .read_bin = (reader)read_le_4byte};
const static struct primtype_info_t i64 =
  {.binname = " i64", .type_name = "i64",  .size = 8,
   .write_str = (writer)write_str_i64, .read_str = (reader)read_str_i64,
   .write_bin = (writer)write_le_8byte, .read_bin = (reader)read_le_8byte};
const static struct primtype_info_t u8 =
  {.binname = "  u8", .type_name = "u8",   .size = 1,
   .write_str = (writer)write_str_u8, .read_str = (reader)read_str_u8,
   .write_bin = (writer)write_byte, .read_bin = (reader)read_byte};
const static struct primtype_info_t u16 =
  {.binname = " u16", .type_name = "u16",  .size = 2,
   .write_str = (writer)write_str_u16, .read_str = (reader)read_str_u16,
   .write_bin = (writer)write_le_2byte, .read_bin = (reader)read_le_2byte};
const static struct primtype_info_t u32 =
  {.binname = " u32", .type_name = "u32",  .size = 4,
   .write_str = (writer)write_str_u32, .read_str = (reader)read_str_u32,
   .write_bin = (writer)write_le_4byte, .read_bin = (reader)read_le_4byte};
const static struct primtype_info_t u64 =
  {.binname = " u64", .type_name = "u64",  .size = 8,
   .write_str = (writer)write_str_u64, .read_str = (reader)read_str_u64,
   .write_bin = (writer)write_le_8byte, .read_bin = (reader)read_le_8byte};
const static struct primtype_info_t f32 =
  {.binname = " f32", .type_name = "f32",  .size = 4,
   .write_str = (writer)write_str_f32, .read_str = (reader)read_str_f32,
   .write_bin = (writer)write_le_4byte, .read_bin = (reader)read_le_4byte};
const static struct primtype_info_t f64 =
  {.binname = " f64", .type_name = "f64",  .size = 8,
   .write_str = (writer)write_str_f64, .read_str = (reader)read_str_f64,
   .write_bin = (writer)write_le_8byte, .read_bin = (reader)read_le_8byte};
const static struct primtype_info_t bool =
  {.binname = "bool", .type_name = "bool", .size = 1,
   .write_str = (writer)write_str_bool, .read_str = (reader)read_str_bool,
   .write_bin = (writer)write_byte, .read_bin = (reader)read_byte};

static const struct primtype_info_t* primtypes[] = {
  &i8, &i16, &i32, &i64,
  &u8, &u16, &u32, &u64,
  &f32, &f64,
  &bool,
  NULL // NULL-terminated
};

// General value interface.  All endian business taken care of at
// lower layers.

static int read_is_binary() {
  skipspaces();
  int c = getchar();
  if (c == 'b') {
    int8_t bin_version;
    int ret = read_byte(&bin_version);

    if (ret != 0) { panic(1, "binary-input: could not read version.\n"); }

    if (bin_version != BINARY_FORMAT_VERSION) {
      panic(1, "binary-input: File uses version %i, but I only understand version %i.\n",
            bin_version, BINARY_FORMAT_VERSION);
    }

    return 1;
  }
  ungetc(c, stdin);
  return 0;
}

static const struct primtype_info_t* read_bin_read_type_enum() {
  char read_binname[4];

  int num_matched = scanf("%4c", read_binname);
  if (num_matched != 1) { panic(1, "binary-input: Couldn't read element type.\n"); }

  const struct primtype_info_t **type = primtypes;

  for (; *type != NULL; type++) {
    // I compare the 4 characters manually instead of using strncmp because
    // this allows any value to be used, also NULL bytes
    if (memcmp(read_binname, (*type)->binname, 4) == 0) {
      return *type;
    }
  }
  panic(1, "binary-input: Did not recognize the type '%s'.\n", read_binname);
  return NULL;
}

static void read_bin_ensure_scalar(const struct primtype_info_t *expected_type) {
  int8_t bin_dims;
  int ret = read_byte(&bin_dims);
  if (ret != 0) { panic(1, "binary-input: Couldn't get dims.\n"); }

  if (bin_dims != 0) {
    panic(1, "binary-input: Expected scalar (0 dimensions), but got array with %i dimensions.\n",
          bin_dims);
  }

  const struct primtype_info_t *bin_type = read_bin_read_type_enum();
  if (bin_type != expected_type) {
    panic(1, "binary-input: Expected scalar of type %s but got scalar of type %s.\n",
          expected_type->type_name,
          bin_type->type_name);
  }
}

//// High-level interface

static int read_bin_array(const struct primtype_info_t *expected_type, void **data, int64_t *shape, int64_t dims) {
  int ret;

  int8_t bin_dims;
  ret = read_byte(&bin_dims);
  if (ret != 0) { panic(1, "binary-input: Couldn't get dims.\n"); }

  if (bin_dims != dims) {
    panic(1, "binary-input: Expected %i dimensions, but got array with %i dimensions.\n",
          dims, bin_dims);
  }

  const struct primtype_info_t *bin_primtype = read_bin_read_type_enum();
  if (expected_type != bin_primtype) {
    panic(1, "binary-input: Expected %iD-array with element type '%s' but got %iD-array with element type '%s'.\n",
          dims, expected_type->type_name, dims, bin_primtype->type_name);
  }

  uint64_t elem_count = 1;
  for (int i=0; i<dims; i++) {
    uint64_t bin_shape;
    ret = read_le_8byte(&bin_shape);
    if (ret != 0) { panic(1, "binary-input: Couldn't read size for dimension %i of array.\n", i); }
    elem_count *= bin_shape;
    shape[i] = (int64_t) bin_shape;
  }

  size_t elem_size = expected_type->size;
  void* tmp = realloc(*data, elem_count * elem_size);
  if (tmp == NULL) {
    panic(1, "binary-input: Failed to allocate array of size %i.\n",
          elem_count * elem_size);
  }
  *data = tmp;

  size_t num_elems_read = fread(*data, elem_size, elem_count, stdin);
  if (num_elems_read != elem_count) {
    panic(1, "binary-input: tried to read %i elements of an array, but only got %i elements.\n",
          elem_count, num_elems_read);
  }

  // If we're on big endian platform we must change all multibyte elements
  // from using little endian to big endian
  if (IS_BIG_ENDIAN && elem_size != 1) {
    char* elems = (char*) *data;
    for (uint64_t i=0; i<elem_count; i++) {
      char* elem = elems+(i*elem_size);
      for (int j=0; j<elem_size/2; j++) {
        char head = elem[j];
        int tail_index = elem_size-1-j;
        elem[j] = elem[tail_index];
        elem[tail_index] = head;
      }
    }
  }

  return 0;
}

static int read_array(const struct primtype_info_t *expected_type, void **data, int64_t *shape, int64_t dims) {
  if (!read_is_binary()) {
    return read_str_array(expected_type->size, (reader)expected_type->read_str, expected_type->type_name, data, shape, dims);
  } else {
    return read_bin_array(expected_type, data, shape, dims);
  }
}

static int write_str_array(FILE *out, const struct primtype_info_t *elem_type, unsigned char *data, int64_t *shape, int8_t rank) {
  if (rank==0) {
    elem_type->write_str(out, (void*)data);
  } else {
    int64_t len = shape[0];
    int64_t slice_size = 1;

    int64_t elem_size = elem_type->size;
    for (int64_t i = 1; i < rank; i++) {
      slice_size *= shape[i];
    }

    if (len*slice_size == 0) {
      printf("empty(");
      for (int64_t i = 1; i < rank; i++) {
        printf("[]");
      }
      printf("%s", elem_type->type_name);
      printf(")");
    } else if (rank==1) {
      putchar('[');
      for (int64_t i = 0; i < len; i++) {
        elem_type->write_str(out, (void*) (data + i * elem_size));
        if (i != len-1) {
          printf(", ");
        }
      }
      putchar(']');
    } else {
      putchar('[');
      for (int64_t i = 0; i < len; i++) {
        write_str_array(out, elem_type, data + i * slice_size * elem_size, shape+1, rank-1);
        if (i != len-1) {
          printf(", ");
        }
      }
      putchar(']');
    }
  }
  return 0;
}

static int write_bin_array(FILE *out, const struct primtype_info_t *elem_type, unsigned char *data, int64_t *shape, int8_t rank) {
  int64_t num_elems = 1;
  for (int64_t i = 0; i < rank; i++) {
    num_elems *= shape[i];
  }

  fputc('b', out);
  fputc((char)BINARY_FORMAT_VERSION, out);
  fwrite(&rank, sizeof(int8_t), 1, out);
  fputs(elem_type->binname, out);
  fwrite(shape, sizeof(int64_t), rank, out);

  if (IS_BIG_ENDIAN) {
    for (size_t i = 0; i < num_elems; i++) {
      unsigned char *elem = data+i*elem_type->size;
      for (size_t j = 0; j < elem_type->size; j++) {
        fwrite(&elem[elem_type->size-j], 1, 1, out);
      }
    }
  } else {
    fwrite(data, elem_type->size, num_elems, out);
  }

  return 0;
}

static int write_array(FILE *out, int write_binary,
                       const struct primtype_info_t *elem_type, void *data, int64_t *shape, int8_t rank) {
  if (write_binary) {
    return write_bin_array(out, elem_type, data, shape, rank);
  } else {
    return write_str_array(out, elem_type, data, shape, rank);
  }
}

static int read_scalar(const struct primtype_info_t *expected_type, void *dest) {
  if (!read_is_binary()) {
    return expected_type->read_str(dest);
  } else {
    read_bin_ensure_scalar(expected_type);
    return expected_type->read_bin(dest);
  }
}

static int write_scalar(FILE *out, int write_binary, const struct primtype_info_t *type, void *src) {
  if (write_binary) {
    return write_bin_array(out, type, src, NULL, 0);
  } else {
    return type->write_str(out, src);
  }
}

/* Some simple utilities for wall-clock timing.

   The function get_wall_time() returns the wall time in microseconds
   (with an unspecified offset).
*/

#ifdef _WIN32

#include <windows.h>

int64_t get_wall_time() {
  LARGE_INTEGER time,freq;
  assert(QueryPerformanceFrequency(&freq));
  assert(QueryPerformanceCounter(&time));
  return ((double)time.QuadPart / freq.QuadPart) * 1000000;
}

#else
/* Assuming POSIX */

#include <time.h>
#include <sys/time.h>

int64_t get_wall_time() {
  struct timeval time;
  assert(gettimeofday(&time,NULL) == 0);
  return time.tv_sec * 1000000 + time.tv_usec;
}

#endif

int64_t peak_mem_usage_default = 0;
int64_t cur_mem_usage_default = 0;
struct memblock {
    int *references;
    char *mem;
    int64_t size;
} ;
static void memblock_unref(struct memblock *block)
{
    if (block->references != NULL) {
        *block->references -= 1;
        if (detail_memory)
            fprintf(stderr,
                    "Unreferencing block in default space: %d references remaining.\n",
                    *block->references);
        if (*block->references == 0) {
            cur_mem_usage_default -= block->size;
            free(block->mem);
            free(block->references);
            block->references = NULL;
            if (detail_memory)
                fprintf(stderr, "%ld bytes freed (now allocated: %ld bytes)\n",
                        block->size, cur_mem_usage_default);
        }
    }
}
static void memblock_alloc(struct memblock *block, int32_t size)
{
    memblock_unref(block);
    block->mem = (char *) malloc(size);
    block->references = (int *) malloc(sizeof(int));
    *block->references = 1;
    block->size = size;
    cur_mem_usage_default += size;
    if (detail_memory)
        fprintf(stderr,
                "Allocated %d bytes in default space (now allocated: %ld bytes)",
                size, cur_mem_usage_default);
    if (cur_mem_usage_default > peak_mem_usage_default) {
        peak_mem_usage_default = cur_mem_usage_default;
        if (detail_memory)
            fprintf(stderr, " (new peak).\n", peak_mem_usage_default);
    } else if (detail_memory)
        fprintf(stderr, ".\n");
}
static void memblock_set(struct memblock *lhs, struct memblock *rhs)
{
    memblock_unref(lhs);
    (*rhs->references)++;
    *lhs = *rhs;
}
struct tuple_int32_t_int32_t_int32_t {
    int32_t elem_0;
    int32_t elem_1;
    int32_t elem_2;
} ;
struct tuple_int32_t_mem_int32_t {
    int32_t elem_0;
    struct memblock elem_1;
    int32_t elem_2;
} ;
static int32_t futhark_idx(int32_t d_2536, int32_t p_2537, int32_t x_2538,
                           int32_t i_2539);
static char futhark_isZZeroPolynomial(int32_t d_2549, int32_t p_2550,
                                      int32_t x_2551);
static struct tuple_int32_t_int32_t_int32_t futhark_rotateLeft(int32_t d_2560,
                                                               int32_t p_2561,
                                                               int32_t x_2562,
                                                               int32_t i_2563);
static struct tuple_int32_t_int32_t_int32_t futhark_mul(int32_t d_2574,
                                                        int32_t p_2575,
                                                        int32_t x_2576,
                                                        int32_t i_2577,
                                                        int32_t v_2578);
static struct tuple_int32_t_int32_t_int32_t futhark_minus(int32_t d_2603,
                                                          int32_t p_2604,
                                                          int32_t x_2605,
                                                          int32_t i_2606,
                                                          int32_t v_2607);
static struct tuple_int32_t_mem_int32_t futhark_translate(int32_t padding_2632,
                                                          int32_t d_2633,
                                                          int32_t p_2634,
                                                          int32_t x_2635);
static struct tuple_int32_t_int32_t_int32_t futhark_truncate(int32_t d_2663,
                                                             int32_t p_2664,
                                                             int32_t x_2665);
static struct tuple_int32_t_mem_int32_t
futhark_translateTestTight(int32_t d_2701, int32_t p_2702, int32_t x_2703);
static struct tuple_int32_t_mem_int32_t
futhark_translateTestLoose(int32_t d_2737, int32_t p_2738, int32_t x_2739);
static inline float futhark_log32(float x)
{
    return log(x);
}
static inline float futhark_sqrt32(float x)
{
    return sqrt(x);
}
static inline float futhark_exp32(float x)
{
    return exp(x);
}
static inline float futhark_cos32(float x)
{
    return cos(x);
}
static inline float futhark_sin32(float x)
{
    return sin(x);
}
static inline float futhark_acos32(float x)
{
    return acos(x);
}
static inline float futhark_asin32(float x)
{
    return asin(x);
}
static inline double futhark_atan32(float x)
{
    return atan(x);
}
static inline float futhark_atan2_32(float x, float y)
{
    return atan2(x, y);
}
static inline char futhark_isnan32(float x)
{
    return isnan(x);
}
static inline char futhark_isinf32(float x)
{
    return isinf(x);
}
static inline double futhark_log64(double x)
{
    return log(x);
}
static inline double futhark_sqrt64(double x)
{
    return sqrt(x);
}
static inline double futhark_exp64(double x)
{
    return exp(x);
}
static inline double futhark_cos64(double x)
{
    return cos(x);
}
static inline double futhark_sin64(double x)
{
    return sin(x);
}
static inline double futhark_acos64(double x)
{
    return acos(x);
}
static inline double futhark_asin64(double x)
{
    return asin(x);
}
static inline double futhark_atan64(double x)
{
    return atan(x);
}
static inline double futhark_atan2_64(double x, double y)
{
    return atan2(x, y);
}
static inline char futhark_isnan64(double x)
{
    return isnan(x);
}
static inline char futhark_isinf64(double x)
{
    return isinf(x);
}
static inline int8_t add8(int8_t x, int8_t y)
{
    return x + y;
}
static inline int16_t add16(int16_t x, int16_t y)
{
    return x + y;
}
static inline int32_t add32(int32_t x, int32_t y)
{
    return x + y;
}
static inline int64_t add64(int64_t x, int64_t y)
{
    return x + y;
}
static inline int8_t sub8(int8_t x, int8_t y)
{
    return x - y;
}
static inline int16_t sub16(int16_t x, int16_t y)
{
    return x - y;
}
static inline int32_t sub32(int32_t x, int32_t y)
{
    return x - y;
}
static inline int64_t sub64(int64_t x, int64_t y)
{
    return x - y;
}
static inline int8_t mul8(int8_t x, int8_t y)
{
    return x * y;
}
static inline int16_t mul16(int16_t x, int16_t y)
{
    return x * y;
}
static inline int32_t mul32(int32_t x, int32_t y)
{
    return x * y;
}
static inline int64_t mul64(int64_t x, int64_t y)
{
    return x * y;
}
static inline uint8_t udiv8(uint8_t x, uint8_t y)
{
    return x / y;
}
static inline uint16_t udiv16(uint16_t x, uint16_t y)
{
    return x / y;
}
static inline uint32_t udiv32(uint32_t x, uint32_t y)
{
    return x / y;
}
static inline uint64_t udiv64(uint64_t x, uint64_t y)
{
    return x / y;
}
static inline uint8_t umod8(uint8_t x, uint8_t y)
{
    return x % y;
}
static inline uint16_t umod16(uint16_t x, uint16_t y)
{
    return x % y;
}
static inline uint32_t umod32(uint32_t x, uint32_t y)
{
    return x % y;
}
static inline uint64_t umod64(uint64_t x, uint64_t y)
{
    return x % y;
}
static inline int8_t sdiv8(int8_t x, int8_t y)
{
    int8_t q = x / y;
    int8_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int16_t sdiv16(int16_t x, int16_t y)
{
    int16_t q = x / y;
    int16_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int32_t sdiv32(int32_t x, int32_t y)
{
    int32_t q = x / y;
    int32_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int64_t sdiv64(int64_t x, int64_t y)
{
    int64_t q = x / y;
    int64_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int8_t smod8(int8_t x, int8_t y)
{
    int8_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int16_t smod16(int16_t x, int16_t y)
{
    int16_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int32_t smod32(int32_t x, int32_t y)
{
    int32_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int64_t smod64(int64_t x, int64_t y)
{
    int64_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int8_t squot8(int8_t x, int8_t y)
{
    return x / y;
}
static inline int16_t squot16(int16_t x, int16_t y)
{
    return x / y;
}
static inline int32_t squot32(int32_t x, int32_t y)
{
    return x / y;
}
static inline int64_t squot64(int64_t x, int64_t y)
{
    return x / y;
}
static inline int8_t srem8(int8_t x, int8_t y)
{
    return x % y;
}
static inline int16_t srem16(int16_t x, int16_t y)
{
    return x % y;
}
static inline int32_t srem32(int32_t x, int32_t y)
{
    return x % y;
}
static inline int64_t srem64(int64_t x, int64_t y)
{
    return x % y;
}
static inline int8_t smin8(int8_t x, int8_t y)
{
    return x < y ? x : y;
}
static inline int16_t smin16(int16_t x, int16_t y)
{
    return x < y ? x : y;
}
static inline int32_t smin32(int32_t x, int32_t y)
{
    return x < y ? x : y;
}
static inline int64_t smin64(int64_t x, int64_t y)
{
    return x < y ? x : y;
}
static inline uint8_t umin8(uint8_t x, uint8_t y)
{
    return x < y ? x : y;
}
static inline uint16_t umin16(uint16_t x, uint16_t y)
{
    return x < y ? x : y;
}
static inline uint32_t umin32(uint32_t x, uint32_t y)
{
    return x < y ? x : y;
}
static inline uint64_t umin64(uint64_t x, uint64_t y)
{
    return x < y ? x : y;
}
static inline int8_t smax8(int8_t x, int8_t y)
{
    return x < y ? y : x;
}
static inline int16_t smax16(int16_t x, int16_t y)
{
    return x < y ? y : x;
}
static inline int32_t smax32(int32_t x, int32_t y)
{
    return x < y ? y : x;
}
static inline int64_t smax64(int64_t x, int64_t y)
{
    return x < y ? y : x;
}
static inline uint8_t umax8(uint8_t x, uint8_t y)
{
    return x < y ? y : x;
}
static inline uint16_t umax16(uint16_t x, uint16_t y)
{
    return x < y ? y : x;
}
static inline uint32_t umax32(uint32_t x, uint32_t y)
{
    return x < y ? y : x;
}
static inline uint64_t umax64(uint64_t x, uint64_t y)
{
    return x < y ? y : x;
}
static inline uint8_t shl8(uint8_t x, uint8_t y)
{
    return x << y;
}
static inline uint16_t shl16(uint16_t x, uint16_t y)
{
    return x << y;
}
static inline uint32_t shl32(uint32_t x, uint32_t y)
{
    return x << y;
}
static inline uint64_t shl64(uint64_t x, uint64_t y)
{
    return x << y;
}
static inline uint8_t lshr8(uint8_t x, uint8_t y)
{
    return x >> y;
}
static inline uint16_t lshr16(uint16_t x, uint16_t y)
{
    return x >> y;
}
static inline uint32_t lshr32(uint32_t x, uint32_t y)
{
    return x >> y;
}
static inline uint64_t lshr64(uint64_t x, uint64_t y)
{
    return x >> y;
}
static inline int8_t ashr8(int8_t x, int8_t y)
{
    return x >> y;
}
static inline int16_t ashr16(int16_t x, int16_t y)
{
    return x >> y;
}
static inline int32_t ashr32(int32_t x, int32_t y)
{
    return x >> y;
}
static inline int64_t ashr64(int64_t x, int64_t y)
{
    return x >> y;
}
static inline uint8_t and8(uint8_t x, uint8_t y)
{
    return x & y;
}
static inline uint16_t and16(uint16_t x, uint16_t y)
{
    return x & y;
}
static inline uint32_t and32(uint32_t x, uint32_t y)
{
    return x & y;
}
static inline uint64_t and64(uint64_t x, uint64_t y)
{
    return x & y;
}
static inline uint8_t or8(uint8_t x, uint8_t y)
{
    return x | y;
}
static inline uint16_t or16(uint16_t x, uint16_t y)
{
    return x | y;
}
static inline uint32_t or32(uint32_t x, uint32_t y)
{
    return x | y;
}
static inline uint64_t or64(uint64_t x, uint64_t y)
{
    return x | y;
}
static inline uint8_t xor8(uint8_t x, uint8_t y)
{
    return x ^ y;
}
static inline uint16_t xor16(uint16_t x, uint16_t y)
{
    return x ^ y;
}
static inline uint32_t xor32(uint32_t x, uint32_t y)
{
    return x ^ y;
}
static inline uint64_t xor64(uint64_t x, uint64_t y)
{
    return x ^ y;
}
static inline char ult8(uint8_t x, uint8_t y)
{
    return x < y;
}
static inline char ult16(uint16_t x, uint16_t y)
{
    return x < y;
}
static inline char ult32(uint32_t x, uint32_t y)
{
    return x < y;
}
static inline char ult64(uint64_t x, uint64_t y)
{
    return x < y;
}
static inline char ule8(uint8_t x, uint8_t y)
{
    return x <= y;
}
static inline char ule16(uint16_t x, uint16_t y)
{
    return x <= y;
}
static inline char ule32(uint32_t x, uint32_t y)
{
    return x <= y;
}
static inline char ule64(uint64_t x, uint64_t y)
{
    return x <= y;
}
static inline char slt8(int8_t x, int8_t y)
{
    return x < y;
}
static inline char slt16(int16_t x, int16_t y)
{
    return x < y;
}
static inline char slt32(int32_t x, int32_t y)
{
    return x < y;
}
static inline char slt64(int64_t x, int64_t y)
{
    return x < y;
}
static inline char sle8(int8_t x, int8_t y)
{
    return x <= y;
}
static inline char sle16(int16_t x, int16_t y)
{
    return x <= y;
}
static inline char sle32(int32_t x, int32_t y)
{
    return x <= y;
}
static inline char sle64(int64_t x, int64_t y)
{
    return x <= y;
}
static inline int8_t pow8(int8_t x, int8_t y)
{
    int8_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int16_t pow16(int16_t x, int16_t y)
{
    int16_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int32_t pow32(int32_t x, int32_t y)
{
    int32_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int64_t pow64(int64_t x, int64_t y)
{
    int64_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int8_t sext_i8_i8(int8_t x)
{
    return x;
}
static inline int16_t sext_i8_i16(int8_t x)
{
    return x;
}
static inline int32_t sext_i8_i32(int8_t x)
{
    return x;
}
static inline int64_t sext_i8_i64(int8_t x)
{
    return x;
}
static inline int8_t sext_i16_i8(int16_t x)
{
    return x;
}
static inline int16_t sext_i16_i16(int16_t x)
{
    return x;
}
static inline int32_t sext_i16_i32(int16_t x)
{
    return x;
}
static inline int64_t sext_i16_i64(int16_t x)
{
    return x;
}
static inline int8_t sext_i32_i8(int32_t x)
{
    return x;
}
static inline int16_t sext_i32_i16(int32_t x)
{
    return x;
}
static inline int32_t sext_i32_i32(int32_t x)
{
    return x;
}
static inline int64_t sext_i32_i64(int32_t x)
{
    return x;
}
static inline int8_t sext_i64_i8(int64_t x)
{
    return x;
}
static inline int16_t sext_i64_i16(int64_t x)
{
    return x;
}
static inline int32_t sext_i64_i32(int64_t x)
{
    return x;
}
static inline int64_t sext_i64_i64(int64_t x)
{
    return x;
}
static inline uint8_t zext_i8_i8(uint8_t x)
{
    return x;
}
static inline uint16_t zext_i8_i16(uint8_t x)
{
    return x;
}
static inline uint32_t zext_i8_i32(uint8_t x)
{
    return x;
}
static inline uint64_t zext_i8_i64(uint8_t x)
{
    return x;
}
static inline uint8_t zext_i16_i8(uint16_t x)
{
    return x;
}
static inline uint16_t zext_i16_i16(uint16_t x)
{
    return x;
}
static inline uint32_t zext_i16_i32(uint16_t x)
{
    return x;
}
static inline uint64_t zext_i16_i64(uint16_t x)
{
    return x;
}
static inline uint8_t zext_i32_i8(uint32_t x)
{
    return x;
}
static inline uint16_t zext_i32_i16(uint32_t x)
{
    return x;
}
static inline uint32_t zext_i32_i32(uint32_t x)
{
    return x;
}
static inline uint64_t zext_i32_i64(uint32_t x)
{
    return x;
}
static inline uint8_t zext_i64_i8(uint64_t x)
{
    return x;
}
static inline uint16_t zext_i64_i16(uint64_t x)
{
    return x;
}
static inline uint32_t zext_i64_i32(uint64_t x)
{
    return x;
}
static inline uint64_t zext_i64_i64(uint64_t x)
{
    return x;
}
static inline float fdiv32(float x, float y)
{
    return x / y;
}
static inline float fadd32(float x, float y)
{
    return x + y;
}
static inline float fsub32(float x, float y)
{
    return x - y;
}
static inline float fmul32(float x, float y)
{
    return x * y;
}
static inline float fmin32(float x, float y)
{
    return x < y ? x : y;
}
static inline float fmax32(float x, float y)
{
    return x < y ? y : x;
}
static inline float fpow32(float x, float y)
{
    return pow(x, y);
}
static inline char cmplt32(float x, float y)
{
    return x < y;
}
static inline char cmple32(float x, float y)
{
    return x <= y;
}
static inline float sitofp_i8_f32(int8_t x)
{
    return x;
}
static inline float sitofp_i16_f32(int16_t x)
{
    return x;
}
static inline float sitofp_i32_f32(int32_t x)
{
    return x;
}
static inline float sitofp_i64_f32(int64_t x)
{
    return x;
}
static inline float uitofp_i8_f32(uint8_t x)
{
    return x;
}
static inline float uitofp_i16_f32(uint16_t x)
{
    return x;
}
static inline float uitofp_i32_f32(uint32_t x)
{
    return x;
}
static inline float uitofp_i64_f32(uint64_t x)
{
    return x;
}
static inline int8_t fptosi_f32_i8(float x)
{
    return x;
}
static inline int16_t fptosi_f32_i16(float x)
{
    return x;
}
static inline int32_t fptosi_f32_i32(float x)
{
    return x;
}
static inline int64_t fptosi_f32_i64(float x)
{
    return x;
}
static inline uint8_t fptoui_f32_i8(float x)
{
    return x;
}
static inline uint16_t fptoui_f32_i16(float x)
{
    return x;
}
static inline uint32_t fptoui_f32_i32(float x)
{
    return x;
}
static inline uint64_t fptoui_f32_i64(float x)
{
    return x;
}
static inline double fdiv64(double x, double y)
{
    return x / y;
}
static inline double fadd64(double x, double y)
{
    return x + y;
}
static inline double fsub64(double x, double y)
{
    return x - y;
}
static inline double fmul64(double x, double y)
{
    return x * y;
}
static inline double fmin64(double x, double y)
{
    return x < y ? x : y;
}
static inline double fmax64(double x, double y)
{
    return x < y ? y : x;
}
static inline double fpow64(double x, double y)
{
    return pow(x, y);
}
static inline char cmplt64(double x, double y)
{
    return x < y;
}
static inline char cmple64(double x, double y)
{
    return x <= y;
}
static inline double sitofp_i8_f64(int8_t x)
{
    return x;
}
static inline double sitofp_i16_f64(int16_t x)
{
    return x;
}
static inline double sitofp_i32_f64(int32_t x)
{
    return x;
}
static inline double sitofp_i64_f64(int64_t x)
{
    return x;
}
static inline double uitofp_i8_f64(uint8_t x)
{
    return x;
}
static inline double uitofp_i16_f64(uint16_t x)
{
    return x;
}
static inline double uitofp_i32_f64(uint32_t x)
{
    return x;
}
static inline double uitofp_i64_f64(uint64_t x)
{
    return x;
}
static inline int8_t fptosi_f64_i8(double x)
{
    return x;
}
static inline int16_t fptosi_f64_i16(double x)
{
    return x;
}
static inline int32_t fptosi_f64_i32(double x)
{
    return x;
}
static inline int64_t fptosi_f64_i64(double x)
{
    return x;
}
static inline uint8_t fptoui_f64_i8(double x)
{
    return x;
}
static inline uint16_t fptoui_f64_i16(double x)
{
    return x;
}
static inline uint32_t fptoui_f64_i32(double x)
{
    return x;
}
static inline uint64_t fptoui_f64_i64(double x)
{
    return x;
}
static inline float fpconv_f32_f32(float x)
{
    return x;
}
static inline double fpconv_f32_f64(float x)
{
    return x;
}
static inline float fpconv_f64_f32(double x)
{
    return x;
}
static inline double fpconv_f64_f64(double x)
{
    return x;
}
static int detail_timing = 0;
static int32_t futhark_idx(int32_t d_2536, int32_t p_2537, int32_t x_2538,
                           int32_t i_2539)
{
    int32_t scalar_out_2792;
    int32_t y_2544 = d_2536 - i_2539;
    int32_t y_2545 = pow32(p_2537, y_2544);
    int32_t x_2546 = sdiv32(x_2538, y_2545);
    int32_t res_2547 = smod32(x_2546, p_2537);
    
    scalar_out_2792 = res_2547;
    
    int32_t retval_2818;
    
    retval_2818 = scalar_out_2792;
    return retval_2818;
}
static char futhark_isZZeroPolynomial(int32_t d_2549, int32_t p_2550,
                                      int32_t x_2551)
{
    char scalar_out_2793;
    int32_t y_2555 = d_2549 + 1;
    int32_t y_2556 = pow32(p_2550, y_2555);
    int32_t x_2557 = smod32(x_2551, y_2556);
    char res_2558 = x_2557 == 0;
    
    scalar_out_2793 = res_2558;
    
    char retval_2819;
    
    retval_2819 = scalar_out_2793;
    return retval_2819;
}
static struct tuple_int32_t_int32_t_int32_t futhark_rotateLeft(int32_t d_2560,
                                                               int32_t p_2561,
                                                               int32_t x_2562,
                                                               int32_t i_2563)
{
    int32_t scalar_out_2794;
    int32_t scalar_out_2795;
    int32_t scalar_out_2796;
    int32_t res_2568 = d_2560 + i_2563;
    int32_t y_2569 = pow32(p_2561, i_2563);
    int32_t res_2570 = x_2562 * y_2569;
    
    scalar_out_2794 = res_2568;
    scalar_out_2795 = p_2561;
    scalar_out_2796 = res_2570;
    
    struct tuple_int32_t_int32_t_int32_t retval_2820;
    
    retval_2820.elem_0 = scalar_out_2794;
    retval_2820.elem_1 = scalar_out_2795;
    retval_2820.elem_2 = scalar_out_2796;
    return retval_2820;
}
static struct tuple_int32_t_int32_t_int32_t futhark_mul(int32_t d_2574,
                                                        int32_t p_2575,
                                                        int32_t x_2576,
                                                        int32_t i_2577,
                                                        int32_t v_2578)
{
    int32_t scalar_out_2797;
    int32_t scalar_out_2798;
    int32_t scalar_out_2799;
    int32_t y_2588 = d_2574 - i_2577;
    int32_t y_2589 = pow32(p_2575, y_2588);
    int32_t x_2590 = sdiv32(x_2576, y_2589);
    int32_t res_2591 = smod32(x_2590, p_2575);
    int32_t x_2593 = res_2591 * v_2578;
    int32_t x_2594 = smod32(x_2593, p_2575);
    int32_t x_2595 = x_2594 - res_2591;
    int32_t y_2598 = x_2595 * y_2589;
    int32_t res_2599 = x_2576 + y_2598;
    
    scalar_out_2797 = d_2574;
    scalar_out_2798 = p_2575;
    scalar_out_2799 = res_2599;
    
    struct tuple_int32_t_int32_t_int32_t retval_2821;
    
    retval_2821.elem_0 = scalar_out_2797;
    retval_2821.elem_1 = scalar_out_2798;
    retval_2821.elem_2 = scalar_out_2799;
    return retval_2821;
}
static struct tuple_int32_t_int32_t_int32_t futhark_minus(int32_t d_2603,
                                                          int32_t p_2604,
                                                          int32_t x_2605,
                                                          int32_t i_2606,
                                                          int32_t v_2607)
{
    int32_t scalar_out_2800;
    int32_t scalar_out_2801;
    int32_t scalar_out_2802;
    int32_t y_2617 = d_2603 - i_2606;
    int32_t y_2618 = pow32(p_2604, y_2617);
    int32_t x_2619 = sdiv32(x_2605, y_2618);
    int32_t res_2620 = smod32(x_2619, p_2604);
    int32_t x_2622 = res_2620 - v_2607;
    int32_t x_2623 = smod32(x_2622, p_2604);
    int32_t x_2624 = x_2623 - res_2620;
    int32_t y_2627 = x_2624 * y_2618;
    int32_t res_2628 = x_2605 + y_2627;
    
    scalar_out_2800 = d_2603;
    scalar_out_2801 = p_2604;
    scalar_out_2802 = res_2628;
    
    struct tuple_int32_t_int32_t_int32_t retval_2822;
    
    retval_2822.elem_0 = scalar_out_2800;
    retval_2822.elem_1 = scalar_out_2801;
    retval_2822.elem_2 = scalar_out_2802;
    return retval_2822;
}
static struct tuple_int32_t_mem_int32_t futhark_translate(int32_t padding_2632,
                                                          int32_t d_2633,
                                                          int32_t p_2634,
                                                          int32_t x_2635)
{
    int32_t out_memsizze_2804;
    struct memblock out_mem_2803;
    
    out_mem_2803.references = NULL;
    
    int32_t out_arrsizze_2805;
    int32_t x_2651 = padding_2632 - d_2633;
    int32_t y_2652 = x_2651 - 1;
    int64_t binop_x_2790 = sext_i32_i64(padding_2632);
    int64_t bytes_2789 = binop_x_2790 * 4;
    struct memblock mem_2791;
    
    mem_2791.references = NULL;
    memblock_alloc(&mem_2791, bytes_2789);
    for (int32_t i_2784 = 0; i_2784 < padding_2632; i_2784++) {
        char cond_2655 = slt32(i_2784, y_2652);
        int32_t res_2656;
        
        if (cond_2655) {
            res_2656 = 0;
        } else {
            int32_t x_2657 = padding_2632 - i_2784;
            int32_t y_2658 = x_2657 - 1;
            int32_t y_2659 = pow32(p_2634, y_2658);
            int32_t x_2660 = sdiv32(x_2635, y_2659);
            int32_t res_2661 = smod32(x_2660, p_2634);
            
            res_2656 = res_2661;
        }
        *(int32_t *) &mem_2791.mem[i_2784 * 4] = res_2656;
    }
    memblock_set(&out_mem_2803, &mem_2791);
    out_arrsizze_2805 = padding_2632;
    out_memsizze_2804 = bytes_2789;
    
    struct tuple_int32_t_mem_int32_t retval_2823;
    
    retval_2823.elem_0 = out_memsizze_2804;
    retval_2823.elem_1.references = NULL;
    memblock_set(&retval_2823.elem_1, &out_mem_2803);
    retval_2823.elem_2 = out_arrsizze_2805;
    memblock_unref(&out_mem_2803);
    memblock_unref(&mem_2791);
    return retval_2823;
}
static struct tuple_int32_t_int32_t_int32_t futhark_truncate(int32_t d_2663,
                                                             int32_t p_2664,
                                                             int32_t x_2665)
{
    int32_t scalar_out_2807;
    int32_t scalar_out_2808;
    int32_t scalar_out_2809;
    int32_t y_2669 = d_2663 + 1;
    int32_t y_2670 = pow32(p_2664, y_2669);
    int32_t x_2671 = smod32(x_2665, y_2670);
    int32_t y_2672 = pow32(p_2664, d_2663);
    char cond_2673 = slt32(x_2671, y_2672);
    int32_t res_2674;
    
    if (cond_2673) {
        int32_t y_2676 = x_2665 + 1;
        int32_t tofloat_arg_2677 = sdiv32(y_2672, y_2676);
        float arg_2678 = sitofp_i32_f32(tofloat_arg_2677);
        char cond_2681 = 0.0F < arg_2678;
        int32_t res_2682;
        
        if (cond_2681) {
            float res_2684;
            
            res_2684 = futhark_log32(arg_2678);
            
            float arg_2686 = sitofp_i32_f32(p_2664);
            float res_2688;
            
            res_2688 = futhark_log32(arg_2686);
            
            float trunc_arg_2690 = res_2684 / res_2688;
            int32_t res_2691 = fptosi_f32_i32(trunc_arg_2690);
            
            res_2682 = res_2691;
        } else {
            res_2682 = 0;
        }
        
        int32_t res_2693 = res_2682 + 1;
        int32_t x_2694 = d_2663 - res_2693;
        char cond_2695 = slt32(x_2694, 0);
        int32_t res_2696;
        
        if (cond_2695) {
            res_2696 = 0;
        } else {
            res_2696 = x_2694;
        }
        res_2674 = res_2696;
    } else {
        res_2674 = d_2663;
    }
    scalar_out_2807 = res_2674;
    scalar_out_2808 = p_2664;
    scalar_out_2809 = x_2665;
    
    struct tuple_int32_t_int32_t_int32_t retval_2824;
    
    retval_2824.elem_0 = scalar_out_2807;
    retval_2824.elem_1 = scalar_out_2808;
    retval_2824.elem_2 = scalar_out_2809;
    return retval_2824;
}
static
struct tuple_int32_t_mem_int32_t futhark_translateTestTight(int32_t d_2701,
                                                            int32_t p_2702,
                                                            int32_t x_2703)
{
    int32_t out_memsizze_2811;
    struct memblock out_mem_2810;
    
    out_mem_2810.references = NULL;
    
    int32_t out_arrsizze_2812;
    int32_t arg_2707 = d_2701 + 1;
    int64_t binop_x_2790 = sext_i32_i64(arg_2707);
    int64_t bytes_2789 = binop_x_2790 * 4;
    struct memblock mem_2791;
    
    mem_2791.references = NULL;
    memblock_alloc(&mem_2791, bytes_2789);
    for (int32_t i_2784 = 0; i_2784 < arg_2707; i_2784++) {
        int32_t x_2729 = arg_2707 - i_2784;
        int32_t y_2730 = x_2729 - 1;
        int32_t y_2731 = pow32(p_2702, y_2730);
        int32_t x_2732 = sdiv32(x_2703, y_2731);
        int32_t res_2733 = smod32(x_2732, p_2702);
        
        *(int32_t *) &mem_2791.mem[i_2784 * 4] = res_2733;
    }
    memblock_set(&out_mem_2810, &mem_2791);
    out_arrsizze_2812 = arg_2707;
    out_memsizze_2811 = bytes_2789;
    
    struct tuple_int32_t_mem_int32_t retval_2825;
    
    retval_2825.elem_0 = out_memsizze_2811;
    retval_2825.elem_1.references = NULL;
    memblock_set(&retval_2825.elem_1, &out_mem_2810);
    retval_2825.elem_2 = out_arrsizze_2812;
    memblock_unref(&out_mem_2810);
    memblock_unref(&mem_2791);
    return retval_2825;
}
static
struct tuple_int32_t_mem_int32_t futhark_translateTestLoose(int32_t d_2737,
                                                            int32_t p_2738,
                                                            int32_t x_2739)
{
    int32_t out_memsizze_2815;
    struct memblock out_mem_2814;
    
    out_mem_2814.references = NULL;
    
    int32_t out_arrsizze_2816;
    int32_t arg_2743 = d_2737 * 2;
    int32_t y_2760 = d_2737 - 1;
    int64_t binop_x_2790 = sext_i32_i64(arg_2743);
    int64_t bytes_2789 = binop_x_2790 * 4;
    struct memblock mem_2791;
    
    mem_2791.references = NULL;
    memblock_alloc(&mem_2791, bytes_2789);
    for (int32_t i_2784 = 0; i_2784 < arg_2743; i_2784++) {
        char cond_2763 = slt32(i_2784, y_2760);
        int32_t res_2764;
        
        if (cond_2763) {
            res_2764 = 0;
        } else {
            int32_t x_2765 = arg_2743 - i_2784;
            int32_t y_2766 = x_2765 - 1;
            int32_t y_2767 = pow32(p_2738, y_2766);
            int32_t x_2768 = sdiv32(x_2739, y_2767);
            int32_t res_2769 = smod32(x_2768, p_2738);
            
            res_2764 = res_2769;
        }
        *(int32_t *) &mem_2791.mem[i_2784 * 4] = res_2764;
    }
    memblock_set(&out_mem_2814, &mem_2791);
    out_arrsizze_2816 = arg_2743;
    out_memsizze_2815 = bytes_2789;
    
    struct tuple_int32_t_mem_int32_t retval_2826;
    
    retval_2826.elem_0 = out_memsizze_2815;
    retval_2826.elem_1.references = NULL;
    memblock_set(&retval_2826.elem_1, &out_mem_2814);
    retval_2826.elem_2 = out_arrsizze_2816;
    memblock_unref(&out_mem_2814);
    memblock_unref(&mem_2791);
    return retval_2826;
}
static FILE *runtime_file;
static int perform_warmup = 0;
static int num_runs = 1;
static const char *entry_point = "main";
int parse_options(int argc, char *const argv[])
{
    int ch;
    static struct option long_options[] = {{"write-runtime-to",
                                            required_argument, NULL, 1},
                                           {"runs", required_argument, NULL, 2},
                                           {"memory-reporting", no_argument,
                                            NULL, 3}, {"entry-point",
                                                       required_argument, NULL,
                                                       4}, {"binary-output",
                                                            no_argument, NULL,
                                                            5}, {0, 0, 0, 0}};
    
    while ((ch = getopt_long(argc, argv, ":t:r:me:b", long_options, NULL)) !=
           -1) {
        if (ch == 1 || ch == 't') {
            runtime_file = fopen(optarg, "w");
            if (runtime_file == NULL)
                panic(1, "Cannot open %s: %s\n", optarg, strerror(errno));
        }
        if (ch == 2 || ch == 'r') {
            num_runs = atoi(optarg);
            perform_warmup = 1;
            if (num_runs <= 0)
                panic(1, "Need a positive number of runs, not %s\n", optarg);
        }
        if (ch == 3 || ch == 'm')
            detail_memory = 1;
        if (ch == 4 || ch == 'e')
            entry_point = optarg;
        if (ch == 5 || ch == 'b')
            binary_output = 1;
        if (ch == ':')
            panic(-1, "Missing argument for option %s\n", argv[optind - 1]);
        if (ch == '?')
            panic(-1, "Unknown option %s\n", argv[optind - 1]);
    }
    return optind;
}
void entry_idx()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2536;
    int32_t p_2537;
    int32_t x_2538;
    int32_t i_2539;
    int32_t main_ret_2827;
    
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    if (read_scalar(&i32, &i_2539) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t scalar_out_2792;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2827 = futhark_idx(d_2536, p_2537, x_2538, i_2539);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2827 = futhark_idx(d_2536, p_2537, x_2538, i_2539);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) { }
    }
    scalar_out_2792 = main_ret_2827;
    write_scalar(stdout, binary_output, &i32, &scalar_out_2792);
    printf("\n");
}
void entry_isZeroPolynomial()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2549;
    int32_t p_2550;
    int32_t x_2551;
    char main_ret_2828;
    
    if (read_scalar(&i32, &d_2549) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &p_2550) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &x_2551) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    char scalar_out_2793;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2828 = futhark_isZZeroPolynomial(d_2549, p_2550, x_2551);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2828 = futhark_isZZeroPolynomial(d_2549, p_2550, x_2551);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) { }
    }
    scalar_out_2793 = main_ret_2828;
    write_scalar(stdout, binary_output, &bool, &scalar_out_2793);
    printf("\n");
}
void entry_rotateLeft()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2560;
    int32_t p_2561;
    int32_t x_2562;
    int32_t i_2563;
    struct tuple_int32_t_int32_t_int32_t main_ret_2829;
    
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    if (read_scalar(&i32, &i_2563) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t scalar_out_2794;
    int32_t scalar_out_2795;
    int32_t scalar_out_2796;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2829 = futhark_rotateLeft(d_2560, p_2561, x_2562, i_2563);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2829 = futhark_rotateLeft(d_2560, p_2561, x_2562, i_2563);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) { }
    }
    scalar_out_2794 = main_ret_2829.elem_0;
    scalar_out_2795 = main_ret_2829.elem_1;
    scalar_out_2796 = main_ret_2829.elem_2;
    write_scalar(stdout, binary_output, &i32, &scalar_out_2794);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2795);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2796);
    printf("\n");
}
void entry_mul()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2574;
    int32_t p_2575;
    int32_t x_2576;
    int32_t i_2577;
    int32_t v_2578;
    struct tuple_int32_t_int32_t_int32_t main_ret_2830;
    
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    if (read_scalar(&i32, &i_2577) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &v_2578) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t scalar_out_2797;
    int32_t scalar_out_2798;
    int32_t scalar_out_2799;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2830 = futhark_mul(d_2574, p_2575, x_2576, i_2577, v_2578);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2830 = futhark_mul(d_2574, p_2575, x_2576, i_2577, v_2578);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) { }
    }
    scalar_out_2797 = main_ret_2830.elem_0;
    scalar_out_2798 = main_ret_2830.elem_1;
    scalar_out_2799 = main_ret_2830.elem_2;
    write_scalar(stdout, binary_output, &i32, &scalar_out_2797);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2798);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2799);
    printf("\n");
}
void entry_minus()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2603;
    int32_t p_2604;
    int32_t x_2605;
    int32_t i_2606;
    int32_t v_2607;
    struct tuple_int32_t_int32_t_int32_t main_ret_2831;
    
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    if (read_scalar(&i32, &i_2606) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &v_2607) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t scalar_out_2800;
    int32_t scalar_out_2801;
    int32_t scalar_out_2802;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2831 = futhark_minus(d_2603, p_2604, x_2605, i_2606, v_2607);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2831 = futhark_minus(d_2603, p_2604, x_2605, i_2606, v_2607);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) { }
    }
    scalar_out_2800 = main_ret_2831.elem_0;
    scalar_out_2801 = main_ret_2831.elem_1;
    scalar_out_2802 = main_ret_2831.elem_2;
    write_scalar(stdout, binary_output, &i32, &scalar_out_2800);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2801);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2802);
    printf("\n");
}
void entry_translate()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t padding_2632;
    int32_t d_2633;
    int32_t p_2634;
    int32_t x_2635;
    struct tuple_int32_t_mem_int32_t main_ret_2832;
    
    if (read_scalar(&i32, &padding_2632) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    
    int32_t out_memsizze_2804;
    struct memblock out_mem_2803;
    
    out_mem_2803.references = NULL;
    
    int32_t out_arrsizze_2805;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2832 = futhark_translate(padding_2632, d_2633, p_2634, x_2635);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref(&main_ret_2832.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2832 = futhark_translate(padding_2632, d_2633, p_2634, x_2635);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref(&main_ret_2832.elem_1);
        }
    }
    out_memsizze_2804 = main_ret_2832.elem_0;
    out_mem_2803 = main_ret_2832.elem_1;
    out_arrsizze_2805 = main_ret_2832.elem_2;
    {
        int64_t shape[] = {out_arrsizze_2805};
        
        write_array(stdout, binary_output, &i32, out_mem_2803.mem, shape, 1);
    }
    printf("\n");
    memblock_unref(&main_ret_2832.elem_1);
}
void entry_truncate()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2663;
    int32_t p_2664;
    int32_t x_2665;
    struct tuple_int32_t_int32_t_int32_t main_ret_2833;
    
    if (read_scalar(&i32, &d_2663) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &p_2664) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &x_2665) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t scalar_out_2807;
    int32_t scalar_out_2808;
    int32_t scalar_out_2809;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2833 = futhark_truncate(d_2663, p_2664, x_2665);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2833 = futhark_truncate(d_2663, p_2664, x_2665);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) { }
    }
    scalar_out_2807 = main_ret_2833.elem_0;
    scalar_out_2808 = main_ret_2833.elem_1;
    scalar_out_2809 = main_ret_2833.elem_2;
    write_scalar(stdout, binary_output, &i32, &scalar_out_2807);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2808);
    printf("\n");
    write_scalar(stdout, binary_output, &i32, &scalar_out_2809);
    printf("\n");
}
void entry_translateTestTight()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2701;
    int32_t p_2702;
    int32_t x_2703;
    struct tuple_int32_t_mem_int32_t main_ret_2834;
    
    if (read_scalar(&i32, &d_2701) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &p_2702) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &x_2703) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t out_memsizze_2811;
    struct memblock out_mem_2810;
    
    out_mem_2810.references = NULL;
    
    int32_t out_arrsizze_2812;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2834 = futhark_translateTestTight(d_2701, p_2702, x_2703);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref(&main_ret_2834.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2834 = futhark_translateTestTight(d_2701, p_2702, x_2703);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref(&main_ret_2834.elem_1);
        }
    }
    out_memsizze_2811 = main_ret_2834.elem_0;
    out_mem_2810 = main_ret_2834.elem_1;
    out_arrsizze_2812 = main_ret_2834.elem_2;
    {
        int64_t shape[] = {out_arrsizze_2812};
        
        write_array(stdout, binary_output, &i32, out_mem_2810.mem, shape, 1);
    }
    printf("\n");
    memblock_unref(&main_ret_2834.elem_1);
}
void entry_translateTestLoose()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_2737;
    int32_t p_2738;
    int32_t x_2739;
    struct tuple_int32_t_mem_int32_t main_ret_2835;
    
    if (read_scalar(&i32, &d_2737) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &p_2738) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &x_2739) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t out_memsizze_2815;
    struct memblock out_mem_2814;
    
    out_mem_2814.references = NULL;
    
    int32_t out_arrsizze_2816;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_2835 = futhark_translateTestLoose(d_2737, p_2738, x_2739);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref(&main_ret_2835.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_2835 = futhark_translateTestLoose(d_2737, p_2738, x_2739);
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref(&main_ret_2835.elem_1);
        }
    }
    out_memsizze_2815 = main_ret_2835.elem_0;
    out_mem_2814 = main_ret_2835.elem_1;
    out_arrsizze_2816 = main_ret_2835.elem_2;
    {
        int64_t shape[] = {out_arrsizze_2816};
        
        write_array(stdout, binary_output, &i32, out_mem_2814.mem, shape, 1);
    }
    printf("\n");
    memblock_unref(&main_ret_2835.elem_1);
}
typedef void entry_point_fun();
struct entry_point_entry {
    const char *name;
    entry_point_fun *fun;
} ;
int main(int argc, char **argv)
{
    fut_progname = argv[0];
    
    struct entry_point_entry entry_points[] = {{.name ="idx", .fun =entry_idx},
                                               {.name ="isZeroPolynomial",
                                                .fun =entry_isZeroPolynomial},
                                               {.name ="rotateLeft", .fun =
                                                entry_rotateLeft}, {.name =
                                                                    "mul",
                                                                    .fun =
                                                                    entry_mul},
                                               {.name ="minus", .fun =
                                                entry_minus}, {.name =
                                                               "translate",
                                                               .fun =
                                                               entry_translate},
                                               {.name ="truncate", .fun =
                                                entry_truncate}, {.name =
                                                                  "translateTestTight",
                                                                  .fun =
                                                                  entry_translateTestTight},
                                               {.name ="translateTestLoose",
                                                .fun =
                                                entry_translateTestLoose}};
    int parsed_options = parse_options(argc, argv);
    
    argc -= parsed_options;
    argv += parsed_options;
    
    int num_entry_points = sizeof(entry_points) / sizeof(entry_points[0]);
    entry_point_fun *entry_point_fun = NULL;
    
    for (int i = 0; i < num_entry_points; i++) {
        if (strcmp(entry_points[i].name, entry_point) == 0) {
            entry_point_fun = entry_points[i].fun;
            break;
        }
    }
    if (entry_point_fun == NULL) {
        fprintf(stderr,
                "No entry point '%s'.  Select another with --entry-point.  Options are:\n",
                entry_point);
        for (int i = 0; i < num_entry_points; i++)
            fprintf(stderr, "%s\n", entry_points[i].name);
        return 1;
    }
    entry_point_fun();
    if (runtime_file != NULL)
        fclose(runtime_file);
    if (detail_memory) {
        fprintf(stderr, "Peak memory usage for default space: %ld bytes.\n",
                peak_mem_usage_default);
    }
    return 0;
}
