-- Tests
-- ==
-- entry: isZeroable
-- compiled input { [1,2,3] 5 } output { false }
-- compiled input { [1,3,5] 1 } output { true }
-- compiled input { [4,2] 5 } output { true }
-- compiled input { [4,2] 1 } output { true }

entry isZeroable [degree] (pol: [degree]i32) (p: i32) : bool =
    let args = iota p
    let results = map (\arg ->
        let res = map (\i ->
            if i == degree - 1 then pol[i]
            else pol[i] * (arg ** (degree - 1 - i))) (iota degree)
        in reduce (\x y -> (x + y) % p) 0 res
    ) args
    in reduce (\x y -> if y == 0 then 0 else x) 1 results == 0



