-- Tests                                                                                         
-- ==                                                                                            
-- entry: isIrreducibleTest                                                                      
-- compiled input { [2,3,9] [1,3,3] } output { false }                                       
-- compiled input { [2,3,9] [1,3,4] } output { true }                                        
-- compiled input { [2,5,245] [2,5,145] } output { false }                                   
-- compiled input { [2,5,246] [2,5,145] } output { true }
-- compiled input { [3,5,2460] [2,5,1450] 5 4 } output { true }

import "translate"

entry isIrreducible (b: (i32,i32,i32)) (a: (i32,i32,i32)) : bool =

    let remain = loop (b, a, _) = (b, a, true) for i < #1 b + 1 do
        let degreeB = #1 b
        let degreeA = #1 a
        let degreeDif = degreeB - degreeA
        let bIsLessDegree = degreeDif < 0
        let isZero = isZeroPolynomial b
        in
        if !bIsLessDegree && !isZero then
            let res = rotateLeft a degreeDif

            let bDividend = if isZero then 0 else idx b 0
            let resultTmp = bDividend / (idx a 0)
            let result = if bIsLessDegree then 0 else resultTmp

            let end = loop b = b for i < degreeB + 1  do
                let ss = (idx res i) * result
                in minus b i ss
            in
            (truncate end, a, bIsLessDegree && !isZero || (degreeB >= degreeA && result == 0))
        else (b, a, (bIsLessDegree && !isZero))
    in
    #3 remain

entry isIrreducibleTest (b: []i32) (a: []i32) : bool =               
    isIrreducible (b[0], b[1], b[2]) (a[0], a[1], a[2])  

