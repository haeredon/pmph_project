#ifdef _MSC_VER
#define inline __inline
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <getopt.h>
static int detail_memory = 0;
static int debugging = 0;
static int binary_output = 0;
/* Crash and burn. */

#include <stdarg.h>

static const char *fut_progname;

void panic(int eval, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
        fprintf(stderr, "%s: ", fut_progname);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
        exit(eval);
}

//// Text I/O

struct array_reader {
  char* elems;
  int64_t n_elems_space;
  int64_t elem_size;
  int64_t n_elems_used;
  int64_t *shape;
  int (*elem_reader)(void*);
};

static int peekc() {
  int c = getchar();
  if (c != EOF) {
    ungetc(c,stdin);
  }
  return c;
}

static int next_is_not_constituent() {
  int c = peekc();
  return c == EOF || !isalnum(c);
}

static void skipspaces() {
  int c = getchar();
  if (isspace(c)) {
    skipspaces();
  } else if (c == '-' && peekc() == '-') {
    // Skip to end of line.
    for (; c != '\n' && c != EOF; c = getchar());
    // Next line may have more spaces.
    skipspaces();
  } else if (c != EOF) {
    ungetc(c, stdin);
  }
}

static int read_str_elem(struct array_reader *reader) {
  int ret;
  if (reader->n_elems_used == reader->n_elems_space) {
    reader->n_elems_space *= 2;
    reader->elems = (char*) realloc(reader->elems,
                                    reader->n_elems_space * reader->elem_size);
  }

  ret = reader->elem_reader(reader->elems + reader->n_elems_used * reader->elem_size);

  if (ret == 0) {
    reader->n_elems_used++;
  }

  return ret;
}

static int read_str_array_elems(struct array_reader *reader, int dims) {
  int c;
  int ret;
  int first = 1;
  char *knows_dimsize = (char*) calloc(dims,sizeof(char));
  int cur_dim = dims-1;
  int64_t *elems_read_in_dim = (int64_t*) calloc(dims,sizeof(int64_t));
  while (1) {
    skipspaces();

    c = getchar();
    if (c == ']') {
      if (knows_dimsize[cur_dim]) {
        if (reader->shape[cur_dim] != elems_read_in_dim[cur_dim]) {
          ret = 1;
          break;
        }
      } else {
        knows_dimsize[cur_dim] = 1;
        reader->shape[cur_dim] = elems_read_in_dim[cur_dim];
      }
      if (cur_dim == 0) {
        ret = 0;
        break;
      } else {
        cur_dim--;
        elems_read_in_dim[cur_dim]++;
      }
    } else if (c == ',') {
      skipspaces();
      c = getchar();
      if (c == '[') {
        if (cur_dim == dims - 1) {
          ret = 1;
          break;
        }
        first = 1;
        cur_dim++;
        elems_read_in_dim[cur_dim] = 0;
      } else if (cur_dim == dims - 1) {
        ungetc(c, stdin);
        ret = read_str_elem(reader);
        if (ret != 0) {
          break;
        }
        elems_read_in_dim[cur_dim]++;
      } else {
        ret = 1;
        break;
      }
    } else if (c == EOF) {
      ret = 1;
      break;
    } else if (first) {
      if (c == '[') {
        if (cur_dim == dims - 1) {
          ret = 1;
          break;
        }
        cur_dim++;
        elems_read_in_dim[cur_dim] = 0;
      } else {
        ungetc(c, stdin);
        ret = read_str_elem(reader);
        if (ret != 0) {
          break;
        }
        elems_read_in_dim[cur_dim]++;
        first = 0;
      }
    } else {
      ret = 1;
      break;
    }
  }

  free(knows_dimsize);
  free(elems_read_in_dim);
  return ret;
}

static int read_str_empty_array(const char *type_name, int64_t *shape, int64_t dims) {
  char c;
  if (scanf("empty") == EOF) {
    return 1;
  }

  c = getchar();
  if (c != '(') {
    return 1;
  }

  for (int i = 0; i < dims-1; i++) {
    c = getchar();
    if (c != '[') {
      return 1;
    }
    c = getchar();
    if (c != ']') {
      return 1;
    }
  }

  int n = strlen(type_name);
  for (int i = 0; i < n; i++) {
    c = getchar();
    if (c != type_name[i]) {
      return 1;
    }
  }

  if (getchar() != ')') {
    return 1;
  }

  for (int i = 0; i < dims; i++) {
    shape[i] = 0;
  }

  return 0;
}

static int read_str_array(int64_t elem_size, int (*elem_reader)(void*),
                          const char *type_name,
                          void **data, int64_t *shape, int64_t dims) {
  int ret;
  struct array_reader reader;
  int64_t read_dims = 0;

  while (1) {
    int c;
    skipspaces();
    c = getchar();
    if (c=='[') {
      read_dims++;
    } else {
      if (c != EOF) {
        ungetc(c, stdin);
      }
      break;
    }
  }

  if (read_dims == 0) {
    return read_str_empty_array(type_name, shape, dims);
  }

  if (read_dims != dims) {
    return 1;
  }

  reader.shape = shape;
  reader.n_elems_used = 0;
  reader.elem_size = elem_size;
  reader.n_elems_space = 16;
  reader.elems = (char*) realloc(*data, elem_size*reader.n_elems_space);
  reader.elem_reader = elem_reader;

  ret = read_str_array_elems(&reader, dims);

  *data = reader.elems;

  return ret;
}

/* Makes a copy of numeric literal removing any underscores, and
   length of the literal. */
static int remove_underscores(char* buf) {
  int buf_index = 0;
  char c = getchar();
  while (isxdigit(c) || c == '.' || c == '+' || c == '-' ||
         c == 'x' || c == 'X' ||
         c == 'p' || c == 'P' || /* exponent for hex. floats */
         c == 'e' || c == 'E' || c == '_') {
    if (c == '_') {
      c = getchar();
      continue;
    }
    else {
      buf[buf_index++] = c;
      c = getchar();
    }
  }
  buf[buf_index] = 0;
  ungetc(c, stdin);             /* unget 'i' */
  return buf_index;
}

static int read_str_i8(void* dest) {
  skipspaces();
  /* Some platforms (WINDOWS) does not support scanf %hhd or its
     cousin, %SCNi8.  Read into int first to avoid corrupting
     memory.

     https://gcc.gnu.org/bugzilla/show_bug.cgi?id=63417  */
  int x;
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%i", &x) == 1) {
    *(int8_t*)dest = x;
    scanf("i8");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_u8(void* dest) {
  skipspaces();
  /* Some platforms (WINDOWS) does not support scanf %hhd or its
     cousin, %SCNu8.  Read into int first to avoid corrupting
     memory.

     https://gcc.gnu.org/bugzilla/show_bug.cgi?id=63417  */
  int x;
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%i", &x) == 1) {
    *(uint8_t*)dest = x;
    scanf("u8");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_i16(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi16, (int16_t*)dest) == 1) {
    scanf("i16");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    printf("fail\n");
    return 1;
  }
}

static int read_str_u16(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi16, (int16_t*)dest) == 1) {
    scanf("u16");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_i32(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi32, (int32_t*)dest) == 1) {
    scanf("i32");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_u32(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi32, (int32_t*)dest) == 1) {
    scanf("u32");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_i64(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%"SCNi64, (int64_t*)dest) == 1) {
    scanf("i64");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_u64(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  // FIXME: This is not correct, as SCNu64 only permits decimal
  // literals.  However, SCNi64 does not handle very large numbers
  // correctly (it's really for signed numbers, so that's fair).
  if (sscanf(buf, "%"SCNu64, (int64_t*)dest) == 1) {
    scanf("u64");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_f32(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%f", (float*)dest) == 1) {
    scanf("f32");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_f64(void* dest) {
  skipspaces();
  char buf[128];
  remove_underscores(buf);
  if (sscanf(buf, "%lf", (double*)dest) == 1) {
    scanf("f64");
    return next_is_not_constituent() ? 0 : 1;
  } else {
    return 1;
  }
}

static int read_str_bool(void* dest) {
  /* This is a monstrous hack.  Maybe we should get a proper lexer in here. */
  char b[4];
  skipspaces();
  if (scanf("%4c", b) == 1) {
    if (strncmp(b, "true", 4) == 0) {
      *(char*)dest = 1;
      return 0;
    } else if (strncmp(b, "fals", 4) == 0 && getchar() == 'e') {
      *(char*)dest = 0;
      return 0;
    } else {
      return 1;
    }
  } else {
    return 1;
  }
}

static int write_str_i8(FILE *out, int8_t *src) {
  return fprintf(out, "%hhdi8", *src);
}

static int write_str_u8(FILE *out, uint8_t *src) {
  return fprintf(out, "%hhuu8", *src);
}

static int write_str_i16(FILE *out, int16_t *src) {
  return fprintf(out, "%hdi16", *src);
}

static int write_str_u16(FILE *out, uint16_t *src) {
  return fprintf(out, "%huu16", *src);
}

static int write_str_i32(FILE *out, int32_t *src) {
  return fprintf(out, "%di32", *src);
}

static int write_str_u32(FILE *out, uint32_t *src) {
  return fprintf(out, "%uu32", *src);
}

static int write_str_i64(FILE *out, int64_t *src) {
  return fprintf(out, "%"PRIi64"i64", *src);
}

static int write_str_u64(FILE *out, uint64_t *src) {
  return fprintf(out, "%"PRIu64"u64", *src);
}

static int write_str_f32(FILE *out, float *src) {
  return fprintf(out, "%.6ff32", *src);
}

static int write_str_f64(FILE *out, double *src) {
  return fprintf(out, "%.6ff64", *src);
}

static int write_str_bool(FILE *out, void *src) {
  return fprintf(out, *(char*)src ? "true" : "false");
}

//// Binary I/O

#define BINARY_FORMAT_VERSION 2
#define IS_BIG_ENDIAN (!*(unsigned char *)&(uint16_t){1})

// Reading little-endian byte sequences.  On big-endian hosts, we flip
// the resulting bytes.

static int read_byte(void* dest) {
  int num_elems_read = fread(dest, 1, 1, stdin);
  return num_elems_read == 1 ? 0 : 1;
}

static int read_le_2byte(void* dest) {
  uint16_t x;
  int num_elems_read = fread(&x, 2, 1, stdin);
  if (IS_BIG_ENDIAN) {
    x = (x>>8) | (x<<8);
  }
  *(uint16_t*)dest = x;
  return num_elems_read == 1 ? 0 : 1;
}

static int read_le_4byte(void* dest) {
  uint32_t x;
  int num_elems_read = fread(&x, 4, 1, stdin);
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>24)&0xFF) |
      ((x>>8) &0xFF00) |
      ((x<<8) &0xFF0000) |
      ((x<<24)&0xFF000000);
  }
  *(uint32_t*)dest = x;
  return num_elems_read == 1 ? 0 : 1;
}

static int read_le_8byte(void* dest) {
  uint64_t x;
  int num_elems_read = fread(&x, 8, 1, stdin);
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>56)&0xFFull) |
      ((x>>40)&0xFF00ull) |
      ((x>>24)&0xFF0000ull) |
      ((x>>8) &0xFF000000ull) |
      ((x<<8) &0xFF00000000ull) |
      ((x<<24)&0xFF0000000000ull) |
      ((x<<40)&0xFF000000000000ull) |
      ((x<<56)&0xFF00000000000000ull);
  }
  *(uint64_t*)dest = x;
  return num_elems_read == 1 ? 0 : 1;
}

static int write_byte(void* dest) {
  int num_elems_written = fwrite(dest, 1, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

static int write_le_2byte(void* dest) {
  uint16_t x = *(uint16_t*)dest;
  if (IS_BIG_ENDIAN) {
    x = (x>>8) | (x<<8);
  }
  int num_elems_written = fwrite(&x, 2, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

static int write_le_4byte(void* dest) {
  uint32_t x = *(uint32_t*)dest;
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>24)&0xFF) |
      ((x>>8) &0xFF00) |
      ((x<<8) &0xFF0000) |
      ((x<<24)&0xFF000000);
  }
  int num_elems_written = fwrite(&x, 4, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

static int write_le_8byte(void* dest) {
  uint64_t x = *(uint64_t*)dest;
  if (IS_BIG_ENDIAN) {
    x =
      ((x>>56)&0xFFull) |
      ((x>>40)&0xFF00ull) |
      ((x>>24)&0xFF0000ull) |
      ((x>>8) &0xFF000000ull) |
      ((x<<8) &0xFF00000000ull) |
      ((x<<24)&0xFF0000000000ull) |
      ((x<<40)&0xFF000000000000ull) |
      ((x<<56)&0xFF00000000000000ull);
  }
  int num_elems_written = fwrite(&x, 8, 1, stdin);
  return num_elems_written == 1 ? 0 : 1;
}

//// Types

typedef int (*writer)(FILE*, void*);
typedef int (*reader)(void*);

struct primtype_info_t {
  const char binname[4]; // Used for parsing binary data.
  const char* type_name; // Same name as in Futhark.
  const int size; // in bytes
  const writer write_str; // Write in text format.
  const reader read_str; // Read in text format.
  const writer write_bin; // Write in binary format.
  const reader read_bin; // Read in binary format.
};

const static struct primtype_info_t i8 =
  {.binname = "  i8", .type_name = "i8",   .size = 1,
   .write_str = (writer)write_str_i8, .read_str = (reader)read_str_i8,
   .write_bin = (writer)write_byte, .read_bin = (reader)read_byte};
const static struct primtype_info_t i16 =
  {.binname = " i16", .type_name = "i16",  .size = 2,
   .write_str = (writer)write_str_i16, .read_str = (reader)read_str_i16,
   .write_bin = (writer)write_le_2byte, .read_bin = (reader)read_le_2byte};
const static struct primtype_info_t i32 =
  {.binname = " i32", .type_name = "i32",  .size = 4,
   .write_str = (writer)write_str_i32, .read_str = (reader)read_str_i32,
   .write_bin = (writer)write_le_4byte, .read_bin = (reader)read_le_4byte};
const static struct primtype_info_t i64 =
  {.binname = " i64", .type_name = "i64",  .size = 8,
   .write_str = (writer)write_str_i64, .read_str = (reader)read_str_i64,
   .write_bin = (writer)write_le_8byte, .read_bin = (reader)read_le_8byte};
const static struct primtype_info_t u8 =
  {.binname = "  u8", .type_name = "u8",   .size = 1,
   .write_str = (writer)write_str_u8, .read_str = (reader)read_str_u8,
   .write_bin = (writer)write_byte, .read_bin = (reader)read_byte};
const static struct primtype_info_t u16 =
  {.binname = " u16", .type_name = "u16",  .size = 2,
   .write_str = (writer)write_str_u16, .read_str = (reader)read_str_u16,
   .write_bin = (writer)write_le_2byte, .read_bin = (reader)read_le_2byte};
const static struct primtype_info_t u32 =
  {.binname = " u32", .type_name = "u32",  .size = 4,
   .write_str = (writer)write_str_u32, .read_str = (reader)read_str_u32,
   .write_bin = (writer)write_le_4byte, .read_bin = (reader)read_le_4byte};
const static struct primtype_info_t u64 =
  {.binname = " u64", .type_name = "u64",  .size = 8,
   .write_str = (writer)write_str_u64, .read_str = (reader)read_str_u64,
   .write_bin = (writer)write_le_8byte, .read_bin = (reader)read_le_8byte};
const static struct primtype_info_t f32 =
  {.binname = " f32", .type_name = "f32",  .size = 4,
   .write_str = (writer)write_str_f32, .read_str = (reader)read_str_f32,
   .write_bin = (writer)write_le_4byte, .read_bin = (reader)read_le_4byte};
const static struct primtype_info_t f64 =
  {.binname = " f64", .type_name = "f64",  .size = 8,
   .write_str = (writer)write_str_f64, .read_str = (reader)read_str_f64,
   .write_bin = (writer)write_le_8byte, .read_bin = (reader)read_le_8byte};
const static struct primtype_info_t bool =
  {.binname = "bool", .type_name = "bool", .size = 1,
   .write_str = (writer)write_str_bool, .read_str = (reader)read_str_bool,
   .write_bin = (writer)write_byte, .read_bin = (reader)read_byte};

static const struct primtype_info_t* primtypes[] = {
  &i8, &i16, &i32, &i64,
  &u8, &u16, &u32, &u64,
  &f32, &f64,
  &bool,
  NULL // NULL-terminated
};

// General value interface.  All endian business taken care of at
// lower layers.

static int read_is_binary() {
  skipspaces();
  int c = getchar();
  if (c == 'b') {
    int8_t bin_version;
    int ret = read_byte(&bin_version);

    if (ret != 0) { panic(1, "binary-input: could not read version.\n"); }

    if (bin_version != BINARY_FORMAT_VERSION) {
      panic(1, "binary-input: File uses version %i, but I only understand version %i.\n",
            bin_version, BINARY_FORMAT_VERSION);
    }

    return 1;
  }
  ungetc(c, stdin);
  return 0;
}

static const struct primtype_info_t* read_bin_read_type_enum() {
  char read_binname[4];

  int num_matched = scanf("%4c", read_binname);
  if (num_matched != 1) { panic(1, "binary-input: Couldn't read element type.\n"); }

  const struct primtype_info_t **type = primtypes;

  for (; *type != NULL; type++) {
    // I compare the 4 characters manually instead of using strncmp because
    // this allows any value to be used, also NULL bytes
    if (memcmp(read_binname, (*type)->binname, 4) == 0) {
      return *type;
    }
  }
  panic(1, "binary-input: Did not recognize the type '%s'.\n", read_binname);
  return NULL;
}

static void read_bin_ensure_scalar(const struct primtype_info_t *expected_type) {
  int8_t bin_dims;
  int ret = read_byte(&bin_dims);
  if (ret != 0) { panic(1, "binary-input: Couldn't get dims.\n"); }

  if (bin_dims != 0) {
    panic(1, "binary-input: Expected scalar (0 dimensions), but got array with %i dimensions.\n",
          bin_dims);
  }

  const struct primtype_info_t *bin_type = read_bin_read_type_enum();
  if (bin_type != expected_type) {
    panic(1, "binary-input: Expected scalar of type %s but got scalar of type %s.\n",
          expected_type->type_name,
          bin_type->type_name);
  }
}

//// High-level interface

static int read_bin_array(const struct primtype_info_t *expected_type, void **data, int64_t *shape, int64_t dims) {
  int ret;

  int8_t bin_dims;
  ret = read_byte(&bin_dims);
  if (ret != 0) { panic(1, "binary-input: Couldn't get dims.\n"); }

  if (bin_dims != dims) {
    panic(1, "binary-input: Expected %i dimensions, but got array with %i dimensions.\n",
          dims, bin_dims);
  }

  const struct primtype_info_t *bin_primtype = read_bin_read_type_enum();
  if (expected_type != bin_primtype) {
    panic(1, "binary-input: Expected %iD-array with element type '%s' but got %iD-array with element type '%s'.\n",
          dims, expected_type->type_name, dims, bin_primtype->type_name);
  }

  uint64_t elem_count = 1;
  for (int i=0; i<dims; i++) {
    uint64_t bin_shape;
    ret = read_le_8byte(&bin_shape);
    if (ret != 0) { panic(1, "binary-input: Couldn't read size for dimension %i of array.\n", i); }
    elem_count *= bin_shape;
    shape[i] = (int64_t) bin_shape;
  }

  size_t elem_size = expected_type->size;
  void* tmp = realloc(*data, elem_count * elem_size);
  if (tmp == NULL) {
    panic(1, "binary-input: Failed to allocate array of size %i.\n",
          elem_count * elem_size);
  }
  *data = tmp;

  size_t num_elems_read = fread(*data, elem_size, elem_count, stdin);
  if (num_elems_read != elem_count) {
    panic(1, "binary-input: tried to read %i elements of an array, but only got %i elements.\n",
          elem_count, num_elems_read);
  }

  // If we're on big endian platform we must change all multibyte elements
  // from using little endian to big endian
  if (IS_BIG_ENDIAN && elem_size != 1) {
    char* elems = (char*) *data;
    for (uint64_t i=0; i<elem_count; i++) {
      char* elem = elems+(i*elem_size);
      for (int j=0; j<elem_size/2; j++) {
        char head = elem[j];
        int tail_index = elem_size-1-j;
        elem[j] = elem[tail_index];
        elem[tail_index] = head;
      }
    }
  }

  return 0;
}

static int read_array(const struct primtype_info_t *expected_type, void **data, int64_t *shape, int64_t dims) {
  if (!read_is_binary()) {
    return read_str_array(expected_type->size, (reader)expected_type->read_str, expected_type->type_name, data, shape, dims);
  } else {
    return read_bin_array(expected_type, data, shape, dims);
  }
}

static int write_str_array(FILE *out, const struct primtype_info_t *elem_type, unsigned char *data, int64_t *shape, int8_t rank) {
  if (rank==0) {
    elem_type->write_str(out, (void*)data);
  } else {
    int64_t len = shape[0];
    int64_t slice_size = 1;

    int64_t elem_size = elem_type->size;
    for (int64_t i = 1; i < rank; i++) {
      slice_size *= shape[i];
    }

    if (len*slice_size == 0) {
      printf("empty(");
      for (int64_t i = 1; i < rank; i++) {
        printf("[]");
      }
      printf("%s", elem_type->type_name);
      printf(")");
    } else if (rank==1) {
      putchar('[');
      for (int64_t i = 0; i < len; i++) {
        elem_type->write_str(out, (void*) (data + i * elem_size));
        if (i != len-1) {
          printf(", ");
        }
      }
      putchar(']');
    } else {
      putchar('[');
      for (int64_t i = 0; i < len; i++) {
        write_str_array(out, elem_type, data + i * slice_size * elem_size, shape+1, rank-1);
        if (i != len-1) {
          printf(", ");
        }
      }
      putchar(']');
    }
  }
  return 0;
}

static int write_bin_array(FILE *out, const struct primtype_info_t *elem_type, unsigned char *data, int64_t *shape, int8_t rank) {
  int64_t num_elems = 1;
  for (int64_t i = 0; i < rank; i++) {
    num_elems *= shape[i];
  }

  fputc('b', out);
  fputc((char)BINARY_FORMAT_VERSION, out);
  fwrite(&rank, sizeof(int8_t), 1, out);
  fputs(elem_type->binname, out);
  fwrite(shape, sizeof(int64_t), rank, out);

  if (IS_BIG_ENDIAN) {
    for (size_t i = 0; i < num_elems; i++) {
      unsigned char *elem = data+i*elem_type->size;
      for (size_t j = 0; j < elem_type->size; j++) {
        fwrite(&elem[elem_type->size-j], 1, 1, out);
      }
    }
  } else {
    fwrite(data, elem_type->size, num_elems, out);
  }

  return 0;
}

static int write_array(FILE *out, int write_binary,
                       const struct primtype_info_t *elem_type, void *data, int64_t *shape, int8_t rank) {
  if (write_binary) {
    return write_bin_array(out, elem_type, data, shape, rank);
  } else {
    return write_str_array(out, elem_type, data, shape, rank);
  }
}

static int read_scalar(const struct primtype_info_t *expected_type, void *dest) {
  if (!read_is_binary()) {
    return expected_type->read_str(dest);
  } else {
    read_bin_ensure_scalar(expected_type);
    return expected_type->read_bin(dest);
  }
}

static int write_scalar(FILE *out, int write_binary, const struct primtype_info_t *type, void *src) {
  if (write_binary) {
    return write_bin_array(out, type, src, NULL, 0);
  } else {
    return type->write_str(out, src);
  }
}

/* Some simple utilities for wall-clock timing.

   The function get_wall_time() returns the wall time in microseconds
   (with an unspecified offset).
*/

#ifdef _WIN32

#include <windows.h>

int64_t get_wall_time() {
  LARGE_INTEGER time,freq;
  assert(QueryPerformanceFrequency(&freq));
  assert(QueryPerformanceCounter(&time));
  return ((double)time.QuadPart / freq.QuadPart) * 1000000;
}

#else
/* Assuming POSIX */

#include <time.h>
#include <sys/time.h>

int64_t get_wall_time() {
  struct timeval time;
  assert(gettimeofday(&time,NULL) == 0);
  return time.tv_sec * 1000000 + time.tv_usec;
}

#endif

#define FUT_BLOCK_DIM 16
/* The simple OpenCL runtime framework used by Futhark. */

#ifdef __APPLE__
  #include <OpenCL/cl.h>
#else
  #include <CL/cl.h>
#endif

#define OPENCL_SUCCEED(e) opencl_succeed(e, #e, __FILE__, __LINE__)

static cl_context fut_cl_context;
static cl_command_queue fut_cl_queue;
static const char *cl_preferred_platform = "";
static const char *cl_preferred_device = "";
static int cl_preferred_device_num = 0;
static int cl_debug = 0;

static size_t cl_group_size = 256;
static size_t cl_num_groups = 128;
static size_t cl_tile_size = 32;
static size_t cl_lockstep_width = 1;
static const char* cl_dump_program_to = NULL;
static const char* cl_load_program_from = NULL;

struct opencl_device_option {
  cl_platform_id platform;
  cl_device_id device;
  cl_device_type device_type;
  char *platform_name;
  char *device_name;
};

/* This function must be defined by the user.  It is invoked by
   setup_opencl() after the platform and device has been found, but
   before the program is loaded.  Its intended use is to tune
   constants based on the selected platform and device. */
static void post_opencl_setup(struct opencl_device_option*);

static char *strclone(const char *str) {
  size_t size = strlen(str) + 1;
  char *copy = malloc(size);
  if (copy == NULL) {
    return NULL;
  }

  memcpy(copy, str, size);
  return copy;
}

static const char* opencl_error_string(unsigned int err)
{
    switch (err) {
        case CL_SUCCESS:                            return "Success!";
        case CL_DEVICE_NOT_FOUND:                   return "Device not found.";
        case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
        case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
        case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
        case CL_OUT_OF_RESOURCES:                   return "Out of resources";
        case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
        case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
        case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
        case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
        case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
        case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
        case CL_MAP_FAILURE:                        return "Map failure";
        case CL_INVALID_VALUE:                      return "Invalid value";
        case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
        case CL_INVALID_PLATFORM:                   return "Invalid platform";
        case CL_INVALID_DEVICE:                     return "Invalid device";
        case CL_INVALID_CONTEXT:                    return "Invalid context";
        case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
        case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
        case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
        case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
        case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
        case CL_INVALID_SAMPLER:                    return "Invalid sampler";
        case CL_INVALID_BINARY:                     return "Invalid binary";
        case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
        case CL_INVALID_PROGRAM:                    return "Invalid program";
        case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
        case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
        case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
        case CL_INVALID_KERNEL:                     return "Invalid kernel";
        case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
        case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
        case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
        case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
        case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
        case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
        case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
        case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
        case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
        case CL_INVALID_EVENT:                      return "Invalid event";
        case CL_INVALID_OPERATION:                  return "Invalid operation";
        case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
        case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
        case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
        default:                                    return "Unknown";
    }
}

static void opencl_succeed(unsigned int ret,
                    const char *call,
                    const char *file,
                    int line) {
  if (ret != CL_SUCCESS) {
    panic(-1, "%s:%d: OpenCL call\n  %s\nfailed with error code %d (%s)\n",
          file, line, call, ret, opencl_error_string(ret));
  }
}

void set_preferred_platform(const char *s) {
  cl_preferred_platform = s;
}

void set_preferred_device(const char *s) {
  int x = 0;
  if (*s == '#') {
    s++;
    while (isdigit(*s)) {
      x = x * 10 + (*s++)-'0';
    }
    // Skip trailing spaces.
    while (isspace(*s)) {
      s++;
    }
  }
  cl_preferred_device = s;
  cl_preferred_device_num = x;
}

static char* opencl_platform_info(cl_platform_id platform,
                                  cl_platform_info param) {
  size_t req_bytes;
  char *info;

  OPENCL_SUCCEED(clGetPlatformInfo(platform, param, 0, NULL, &req_bytes));

  info = malloc(req_bytes);

  OPENCL_SUCCEED(clGetPlatformInfo(platform, param, req_bytes, info, NULL));

  return info;
}

static char* opencl_device_info(cl_device_id device,
                                cl_device_info param) {
  size_t req_bytes;
  char *info;

  OPENCL_SUCCEED(clGetDeviceInfo(device, param, 0, NULL, &req_bytes));

  info = malloc(req_bytes);

  OPENCL_SUCCEED(clGetDeviceInfo(device, param, req_bytes, info, NULL));

  return info;
}

static void opencl_all_device_options(struct opencl_device_option **devices_out,
                                      size_t *num_devices_out) {
  size_t num_devices = 0, num_devices_added = 0;

  cl_platform_id *all_platforms;
  cl_uint *platform_num_devices;

  cl_uint num_platforms;

  // Find the number of platforms.
  OPENCL_SUCCEED(clGetPlatformIDs(0, NULL, &num_platforms));

  // Make room for them.
  all_platforms = calloc(num_platforms, sizeof(cl_platform_id));
  platform_num_devices = calloc(num_platforms, sizeof(cl_uint));

  // Fetch all the platforms.
  OPENCL_SUCCEED(clGetPlatformIDs(num_platforms, all_platforms, NULL));

  // Count the number of devices for each platform, as well as the
  // total number of devices.
  for (cl_uint i = 0; i < num_platforms; i++) {
    if (clGetDeviceIDs(all_platforms[i], CL_DEVICE_TYPE_ALL,
                       0, NULL, &platform_num_devices[i]) == CL_SUCCESS) {
      num_devices += platform_num_devices[i];
    } else {
      platform_num_devices[i] = 0;
    }
  }

  // Make room for all the device options.
  struct opencl_device_option *devices =
    calloc(num_devices, sizeof(struct opencl_device_option));

  // Loop through the platforms, getting information about their devices.
  for (cl_uint i = 0; i < num_platforms; i++) {
    cl_platform_id platform = all_platforms[i];
    cl_uint num_platform_devices = platform_num_devices[i];

    if (num_platform_devices == 0) {
      continue;
    }

    char *platform_name = opencl_platform_info(platform, CL_PLATFORM_NAME);
    cl_device_id *platform_devices =
      calloc(num_platform_devices, sizeof(cl_device_id));

    // Fetch all the devices.
    OPENCL_SUCCEED(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,
                                  num_platform_devices, platform_devices, NULL));

    // Loop through the devices, adding them to the devices array.
    for (cl_uint i = 0; i < num_platform_devices; i++) {
      char *device_name = opencl_device_info(platform_devices[i], CL_DEVICE_NAME);
      devices[num_devices_added].platform = platform;
      devices[num_devices_added].device = platform_devices[i];
      OPENCL_SUCCEED(clGetDeviceInfo(platform_devices[i], CL_DEVICE_TYPE,
                                     sizeof(cl_device_type),
                                     &devices[num_devices_added].device_type,
                                     NULL));
      // We don't want the structs to share memory, so copy the platform name.
      // Each device name is already unique.
      devices[num_devices_added].platform_name = strclone(platform_name);
      devices[num_devices_added].device_name = device_name;
      num_devices_added++;
    }
    free(platform_devices);
    free(platform_name);
  }
  free(all_platforms);
  free(platform_num_devices);

  *devices_out = devices;
  *num_devices_out = num_devices;
}

static struct opencl_device_option get_preferred_device() {
  struct opencl_device_option *devices;
  size_t num_devices;

  opencl_all_device_options(&devices, &num_devices);

  int num_platform_matches = 0;
  int num_device_matches = 0;

  for (size_t i = 0; i < num_devices; i++) {
    struct opencl_device_option device = devices[i];
    if (strstr(device.platform_name, cl_preferred_platform) != NULL &&
        strstr(device.device_name, cl_preferred_device) != NULL &&
        num_device_matches++ == cl_preferred_device_num) {
      // Free all the platform and device names, except the ones we have chosen.
      for (size_t j = 0; j < num_devices; j++) {
        if (j != i) {
          free(devices[j].platform_name);
          free(devices[j].device_name);
        }
      }
      free(devices);
      return device;
    }
  }

  panic(1, "Could not find acceptable OpenCL device.\n");
}

static void describe_device_option(struct opencl_device_option device) {
  fprintf(stderr, "Using platform: %s\n", device.platform_name);
  fprintf(stderr, "Using device: %s\n", device.device_name);
}

static cl_build_status build_opencl_program(cl_program program, cl_device_id device, const char* options) {
  cl_int ret_val = clBuildProgram(program, 1, &device, options, NULL, NULL);

  // Avoid termination due to CL_BUILD_PROGRAM_FAILURE
  if (ret_val != CL_SUCCESS && ret_val != CL_BUILD_PROGRAM_FAILURE) {
    assert(ret_val == 0);
  }

  cl_build_status build_status;
  ret_val = clGetProgramBuildInfo(program,
                                  device,
                                  CL_PROGRAM_BUILD_STATUS,
                                  sizeof(cl_build_status),
                                  &build_status,
                                  NULL);
  assert(ret_val == 0);

  if (build_status != CL_SUCCESS) {
    char *build_log;
    size_t ret_val_size;
    ret_val = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &ret_val_size);
    assert(ret_val == 0);

    build_log = malloc(ret_val_size+1);
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, ret_val_size, build_log, NULL);
    assert(ret_val == 0);

    // The spec technically does not say whether the build log is zero-terminated, so let's be careful.
    build_log[ret_val_size] = '\0';

    fprintf(stderr, "Build log:\n%s\n", build_log);

    free(build_log);
  }

  return build_status;
}

// We take as input several strings representing the program, because
// C does not guarantee that the compiler supports particularly large
// literals.  Notably, Visual C has a limit of 2048 characters.  The
// array must be NULL-terminated.
static cl_program setup_opencl(const char *srcs[]) {

  cl_int error;
  cl_platform_id platform;
  cl_device_id device;
  cl_uint platforms, devices;
  size_t max_group_size;

  struct opencl_device_option device_option = get_preferred_device();

  if (cl_debug) {
    describe_device_option(device_option);
  }

  device = device_option.device;
  platform = device_option.platform;

  OPENCL_SUCCEED(clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE,
                                 sizeof(size_t), &max_group_size, NULL));

  size_t max_tile_size = sqrt(max_group_size);

  if (max_group_size < cl_group_size) {
    fprintf(stderr, "Warning: Device limits group size to %zu (setting was %zu)\n",
            max_group_size, cl_group_size);
    cl_group_size = max_group_size;
  }

  if (max_tile_size < cl_tile_size) {
    fprintf(stderr, "Warning: Device limits tile size to %zu (setting was %zu)\n",
            max_tile_size, cl_tile_size);
    cl_tile_size = max_tile_size;
  }

  cl_context_properties properties[] = {
    CL_CONTEXT_PLATFORM,
    (cl_context_properties)platform,
    0
  };
  // Note that nVidia's OpenCL requires the platform property
  fut_cl_context = clCreateContext(properties, 1, &device, NULL, NULL, &error);
  assert(error == 0);

  fut_cl_queue = clCreateCommandQueue(fut_cl_context, device, 0, &error);
  assert(error == 0);

  // Make sure this function is defined.
  post_opencl_setup(&device_option);

  char *fut_opencl_src = NULL;
  size_t src_size = 0;

  // Maybe we have to read OpenCL source from somewhere else (used for debugging).
  if (cl_load_program_from) {
    FILE *f = fopen(cl_load_program_from, "r");
    assert(f != NULL);
    fseek(f, 0, SEEK_END);
    src_size = ftell(f);
    fseek(f, 0, SEEK_SET);
    fut_opencl_src = malloc(src_size);
    fread(fut_opencl_src, 1, src_size, f);
    fclose(f);
  } else {
    // Build the OpenCL program.  First we have to concatenate all the fragments.
    for (const char **src = srcs; *src; src++) {
      src_size += strlen(*src);
    }

    fut_opencl_src = malloc(src_size + 1);

    size_t n, i;
    for (i = 0, n = 0; srcs[i]; i++) {
      strncpy(fut_opencl_src+n, srcs[i], src_size-n);
      n += strlen(srcs[i]);
    }
    fut_opencl_src[src_size] = 0;

  }

  cl_program prog;
  error = 0;
  const char* src_ptr[] = {fut_opencl_src};

  if (cl_dump_program_to) {
    FILE *f = fopen(cl_dump_program_to, "w");
    assert(f != NULL);
    fputs(fut_opencl_src, f);
    fclose(f);
  }

  prog = clCreateProgramWithSource(fut_cl_context, 1, src_ptr, &src_size, &error);
  assert(error == 0);
  char compile_opts[1024];
  snprintf(compile_opts, sizeof(compile_opts), "-DFUT_BLOCK_DIM=%d -DLOCKSTEP_WIDTH=%d -DDEFAULT_GROUP_SIZE=%d -DDEFAULT_NUM_GROUPS=%d  -DDEFAULT_TILE_SIZE=%d", FUT_BLOCK_DIM, cl_lockstep_width, cl_group_size, cl_num_groups, cl_tile_size);
  OPENCL_SUCCEED(build_opencl_program(prog, device, compile_opts));
  free(fut_opencl_src);

  return prog;
}

static const char *fut_opencl_program[] =
                  {"__kernel void dummy_kernel(__global unsigned char *dummy, int n)\n{\n    const int thread_gid = get_global_id(0);\n    \n    if (thread_gid >= n)\n        return;\n}\ntypedef char int8_t;\ntypedef short int16_t;\ntypedef int int32_t;\ntypedef long int64_t;\ntypedef uchar uint8_t;\ntypedef ushort uint16_t;\ntypedef uint uint32_t;\ntypedef ulong uint64_t;\n#define ALIGNED_LOCAL_MEMORY(m,size) __local unsigned char m[size] __attribute__ ((align))\nstatic inline int8_t add8(int8_t x, int8_t y)\n{\n    return x + y;\n}\nstatic inline int16_t add16(int16_t x, int16_t y)\n{\n    return x + y;\n}\nstatic inline int32_t add32(int32_t x, int32_t y)\n{\n    return x + y;\n}\nstatic inline int64_t add64(int64_t x, int64_t y)\n{\n    return x + y;\n}\nstatic inline int8_t sub8(int8_t x, int8_t y)\n{\n    return x - y;\n}\nstatic inline int16_t sub16(int16_t x, int16_t y)\n{\n    return x - y;\n}\nstatic inline int32_t sub32(int32_t x, int32_t y)\n{\n    return x - y;\n}\nstatic inline int64_t sub64(int64_t x, int64_t y)\n{\n    return x - y;\n}\nstatic inline int8_t mul8(int8_t x, int8_t y)\n{\n    return x * y;\n}\nstatic inline int16_t mul16(int16_t x, int16_t y)\n{\n    return x * y;\n}\nstatic inline int32_t mul32(int32_t x, int32_t y)\n{\n    return x * y;\n}\nstatic inline int64_t mul64(int64_t x, int64_t y)\n{\n    return x * y;\n}\nstatic inline uint8_t udiv8(uint8_t x, uint8_t y)\n{\n    return x / y;\n}\nstatic inline uint16_t udiv16(uint16_t x, uint16_t y)\n{\n    return x / y;\n}\nstatic inline uint32_t udiv32(uint32_t x, uint32_t y)\n{\n    return x / y;\n}\nstatic inline uint64_t udiv64(uint64_t x, uint64_t y)\n{\n    return x / y;\n}\nstatic inline uint8_t umod8(uint8_t x, uint8_t y)\n{\n    return x % y;\n}\nstatic inline uint16_t umod16(uint16_t x, uint16_t y)\n{\n    return x % y;\n}\nstatic inline uint32_t umod32(uint32_t x, uint32_t y)\n{\n    return x % y;\n}\nstatic inline uint64_t umod64(uint64_t x, uint64_t y)\n{\n    return x % y;\n}\nstatic inline int8_t sdiv8(int8_t x, int8_t y)\n{\n    int8_t q = x / y;\n    int8_t r = x % y;\n    \n    return q - ((",
                   "r != 0 && r < 0 != y < 0) ? 1 : 0);\n}\nstatic inline int16_t sdiv16(int16_t x, int16_t y)\n{\n    int16_t q = x / y;\n    int16_t r = x % y;\n    \n    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);\n}\nstatic inline int32_t sdiv32(int32_t x, int32_t y)\n{\n    int32_t q = x / y;\n    int32_t r = x % y;\n    \n    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);\n}\nstatic inline int64_t sdiv64(int64_t x, int64_t y)\n{\n    int64_t q = x / y;\n    int64_t r = x % y;\n    \n    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);\n}\nstatic inline int8_t smod8(int8_t x, int8_t y)\n{\n    int8_t r = x % y;\n    \n    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);\n}\nstatic inline int16_t smod16(int16_t x, int16_t y)\n{\n    int16_t r = x % y;\n    \n    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);\n}\nstatic inline int32_t smod32(int32_t x, int32_t y)\n{\n    int32_t r = x % y;\n    \n    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);\n}\nstatic inline int64_t smod64(int64_t x, int64_t y)\n{\n    int64_t r = x % y;\n    \n    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);\n}\nstatic inline int8_t squot8(int8_t x, int8_t y)\n{\n    return x / y;\n}\nstatic inline int16_t squot16(int16_t x, int16_t y)\n{\n    return x / y;\n}\nstatic inline int32_t squot32(int32_t x, int32_t y)\n{\n    return x / y;\n}\nstatic inline int64_t squot64(int64_t x, int64_t y)\n{\n    return x / y;\n}\nstatic inline int8_t srem8(int8_t x, int8_t y)\n{\n    return x % y;\n}\nstatic inline int16_t srem16(int16_t x, int16_t y)\n{\n    return x % y;\n}\nstatic inline int32_t srem32(int32_t x, int32_t y)\n{\n    return x % y;\n}\nstatic inline int64_t srem64(int64_t x, int64_t y)\n{\n    return x % y;\n}\nstatic inline int8_t smin8(int8_t x, int8_t y)\n{\n    return x < y ? x : y;\n}\nstatic inline int16_t smin16(int16_t x, int16_t y)\n{\n    return x < y ? x : y;\n}\nstatic inline int32_t smin32(int32_t x, int32_t y)\n{\n    return x < y ? x : y;\n}\nstatic inline int64_t smin64(int64_t x, int64_t",
                   " y)\n{\n    return x < y ? x : y;\n}\nstatic inline uint8_t umin8(uint8_t x, uint8_t y)\n{\n    return x < y ? x : y;\n}\nstatic inline uint16_t umin16(uint16_t x, uint16_t y)\n{\n    return x < y ? x : y;\n}\nstatic inline uint32_t umin32(uint32_t x, uint32_t y)\n{\n    return x < y ? x : y;\n}\nstatic inline uint64_t umin64(uint64_t x, uint64_t y)\n{\n    return x < y ? x : y;\n}\nstatic inline int8_t smax8(int8_t x, int8_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline int16_t smax16(int16_t x, int16_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline int32_t smax32(int32_t x, int32_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline int64_t smax64(int64_t x, int64_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline uint8_t umax8(uint8_t x, uint8_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline uint16_t umax16(uint16_t x, uint16_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline uint32_t umax32(uint32_t x, uint32_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline uint64_t umax64(uint64_t x, uint64_t y)\n{\n    return x < y ? y : x;\n}\nstatic inline uint8_t shl8(uint8_t x, uint8_t y)\n{\n    return x << y;\n}\nstatic inline uint16_t shl16(uint16_t x, uint16_t y)\n{\n    return x << y;\n}\nstatic inline uint32_t shl32(uint32_t x, uint32_t y)\n{\n    return x << y;\n}\nstatic inline uint64_t shl64(uint64_t x, uint64_t y)\n{\n    return x << y;\n}\nstatic inline uint8_t lshr8(uint8_t x, uint8_t y)\n{\n    return x >> y;\n}\nstatic inline uint16_t lshr16(uint16_t x, uint16_t y)\n{\n    return x >> y;\n}\nstatic inline uint32_t lshr32(uint32_t x, uint32_t y)\n{\n    return x >> y;\n}\nstatic inline uint64_t lshr64(uint64_t x, uint64_t y)\n{\n    return x >> y;\n}\nstatic inline int8_t ashr8(int8_t x, int8_t y)\n{\n    return x >> y;\n}\nstatic inline int16_t ashr16(int16_t x, int16_t y)\n{\n    return x >> y;\n}\nstatic inline int32_t ashr32(int32_t x, int32_t y)\n{\n    return x >> y;\n}\nstatic inline int64_t ashr64(int64_t x, int64_t y)\n{\n    return x >> y;\n}\nstatic inline uint8_t and8(uint8_t x, uint8_t y)\n{\n    return x & y;\n}\nstatic inline u",
                   "int16_t and16(uint16_t x, uint16_t y)\n{\n    return x & y;\n}\nstatic inline uint32_t and32(uint32_t x, uint32_t y)\n{\n    return x & y;\n}\nstatic inline uint64_t and64(uint64_t x, uint64_t y)\n{\n    return x & y;\n}\nstatic inline uint8_t or8(uint8_t x, uint8_t y)\n{\n    return x | y;\n}\nstatic inline uint16_t or16(uint16_t x, uint16_t y)\n{\n    return x | y;\n}\nstatic inline uint32_t or32(uint32_t x, uint32_t y)\n{\n    return x | y;\n}\nstatic inline uint64_t or64(uint64_t x, uint64_t y)\n{\n    return x | y;\n}\nstatic inline uint8_t xor8(uint8_t x, uint8_t y)\n{\n    return x ^ y;\n}\nstatic inline uint16_t xor16(uint16_t x, uint16_t y)\n{\n    return x ^ y;\n}\nstatic inline uint32_t xor32(uint32_t x, uint32_t y)\n{\n    return x ^ y;\n}\nstatic inline uint64_t xor64(uint64_t x, uint64_t y)\n{\n    return x ^ y;\n}\nstatic inline char ult8(uint8_t x, uint8_t y)\n{\n    return x < y;\n}\nstatic inline char ult16(uint16_t x, uint16_t y)\n{\n    return x < y;\n}\nstatic inline char ult32(uint32_t x, uint32_t y)\n{\n    return x < y;\n}\nstatic inline char ult64(uint64_t x, uint64_t y)\n{\n    return x < y;\n}\nstatic inline char ule8(uint8_t x, uint8_t y)\n{\n    return x <= y;\n}\nstatic inline char ule16(uint16_t x, uint16_t y)\n{\n    return x <= y;\n}\nstatic inline char ule32(uint32_t x, uint32_t y)\n{\n    return x <= y;\n}\nstatic inline char ule64(uint64_t x, uint64_t y)\n{\n    return x <= y;\n}\nstatic inline char slt8(int8_t x, int8_t y)\n{\n    return x < y;\n}\nstatic inline char slt16(int16_t x, int16_t y)\n{\n    return x < y;\n}\nstatic inline char slt32(int32_t x, int32_t y)\n{\n    return x < y;\n}\nstatic inline char slt64(int64_t x, int64_t y)\n{\n    return x < y;\n}\nstatic inline char sle8(int8_t x, int8_t y)\n{\n    return x <= y;\n}\nstatic inline char sle16(int16_t x, int16_t y)\n{\n    return x <= y;\n}\nstatic inline char sle32(int32_t x, int32_t y)\n{\n    return x <= y;\n}\nstatic inline char sle64(int64_t x, int64_t y)\n{\n    return x <= y;\n}\nstatic inline int8_t pow8(int8_t x, int8_t y)\n{\n    int8_t res = 1, rem = y;\n    \n    ",
                   "while (rem != 0) {\n        if (rem & 1)\n            res *= x;\n        rem >>= 1;\n        x *= x;\n    }\n    return res;\n}\nstatic inline int16_t pow16(int16_t x, int16_t y)\n{\n    int16_t res = 1, rem = y;\n    \n    while (rem != 0) {\n        if (rem & 1)\n            res *= x;\n        rem >>= 1;\n        x *= x;\n    }\n    return res;\n}\nstatic inline int32_t pow32(int32_t x, int32_t y)\n{\n    int32_t res = 1, rem = y;\n    \n    while (rem != 0) {\n        if (rem & 1)\n            res *= x;\n        rem >>= 1;\n        x *= x;\n    }\n    return res;\n}\nstatic inline int64_t pow64(int64_t x, int64_t y)\n{\n    int64_t res = 1, rem = y;\n    \n    while (rem != 0) {\n        if (rem & 1)\n            res *= x;\n        rem >>= 1;\n        x *= x;\n    }\n    return res;\n}\nstatic inline int8_t sext_i8_i8(int8_t x)\n{\n    return x;\n}\nstatic inline int16_t sext_i8_i16(int8_t x)\n{\n    return x;\n}\nstatic inline int32_t sext_i8_i32(int8_t x)\n{\n    return x;\n}\nstatic inline int64_t sext_i8_i64(int8_t x)\n{\n    return x;\n}\nstatic inline int8_t sext_i16_i8(int16_t x)\n{\n    return x;\n}\nstatic inline int16_t sext_i16_i16(int16_t x)\n{\n    return x;\n}\nstatic inline int32_t sext_i16_i32(int16_t x)\n{\n    return x;\n}\nstatic inline int64_t sext_i16_i64(int16_t x)\n{\n    return x;\n}\nstatic inline int8_t sext_i32_i8(int32_t x)\n{\n    return x;\n}\nstatic inline int16_t sext_i32_i16(int32_t x)\n{\n    return x;\n}\nstatic inline int32_t sext_i32_i32(int32_t x)\n{\n    return x;\n}\nstatic inline int64_t sext_i32_i64(int32_t x)\n{\n    return x;\n}\nstatic inline int8_t sext_i64_i8(int64_t x)\n{\n    return x;\n}\nstatic inline int16_t sext_i64_i16(int64_t x)\n{\n    return x;\n}\nstatic inline int32_t sext_i64_i32(int64_t x)\n{\n    return x;\n}\nstatic inline int64_t sext_i64_i64(int64_t x)\n{\n    return x;\n}\nstatic inline uint8_t zext_i8_i8(uint8_t x)\n{\n    return x;\n}\nstatic inline uint16_t zext_i8_i16(uint8_t x)\n{\n    return x;\n}\nstatic inline uint32_t zext_i8_i32(uint8_t x)\n{\n    return x;\n}\nstatic inline uint64_t zext_i8_i64(uint8_t x)",
                   "\n{\n    return x;\n}\nstatic inline uint8_t zext_i16_i8(uint16_t x)\n{\n    return x;\n}\nstatic inline uint16_t zext_i16_i16(uint16_t x)\n{\n    return x;\n}\nstatic inline uint32_t zext_i16_i32(uint16_t x)\n{\n    return x;\n}\nstatic inline uint64_t zext_i16_i64(uint16_t x)\n{\n    return x;\n}\nstatic inline uint8_t zext_i32_i8(uint32_t x)\n{\n    return x;\n}\nstatic inline uint16_t zext_i32_i16(uint32_t x)\n{\n    return x;\n}\nstatic inline uint32_t zext_i32_i32(uint32_t x)\n{\n    return x;\n}\nstatic inline uint64_t zext_i32_i64(uint32_t x)\n{\n    return x;\n}\nstatic inline uint8_t zext_i64_i8(uint64_t x)\n{\n    return x;\n}\nstatic inline uint16_t zext_i64_i16(uint64_t x)\n{\n    return x;\n}\nstatic inline uint32_t zext_i64_i32(uint64_t x)\n{\n    return x;\n}\nstatic inline uint64_t zext_i64_i64(uint64_t x)\n{\n    return x;\n}\nstatic inline float fdiv32(float x, float y)\n{\n    return x / y;\n}\nstatic inline float fadd32(float x, float y)\n{\n    return x + y;\n}\nstatic inline float fsub32(float x, float y)\n{\n    return x - y;\n}\nstatic inline float fmul32(float x, float y)\n{\n    return x * y;\n}\nstatic inline float fmin32(float x, float y)\n{\n    return x < y ? x : y;\n}\nstatic inline float fmax32(float x, float y)\n{\n    return x < y ? y : x;\n}\nstatic inline float fpow32(float x, float y)\n{\n    return pow(x, y);\n}\nstatic inline char cmplt32(float x, float y)\n{\n    return x < y;\n}\nstatic inline char cmple32(float x, float y)\n{\n    return x <= y;\n}\nstatic inline float sitofp_i8_f32(int8_t x)\n{\n    return x;\n}\nstatic inline float sitofp_i16_f32(int16_t x)\n{\n    return x;\n}\nstatic inline float sitofp_i32_f32(int32_t x)\n{\n    return x;\n}\nstatic inline float sitofp_i64_f32(int64_t x)\n{\n    return x;\n}\nstatic inline float uitofp_i8_f32(uint8_t x)\n{\n    return x;\n}\nstatic inline float uitofp_i16_f32(uint16_t x)\n{\n    return x;\n}\nstatic inline float uitofp_i32_f32(uint32_t x)\n{\n    return x;\n}\nstatic inline float uitofp_i64_f32(uint64_t x)\n{\n    return x;\n}\nstatic inline int8_t fptosi_f32_i8(float x)\n{\n    return x;",
                   "\n}\nstatic inline int16_t fptosi_f32_i16(float x)\n{\n    return x;\n}\nstatic inline int32_t fptosi_f32_i32(float x)\n{\n    return x;\n}\nstatic inline int64_t fptosi_f32_i64(float x)\n{\n    return x;\n}\nstatic inline uint8_t fptoui_f32_i8(float x)\n{\n    return x;\n}\nstatic inline uint16_t fptoui_f32_i16(float x)\n{\n    return x;\n}\nstatic inline uint32_t fptoui_f32_i32(float x)\n{\n    return x;\n}\nstatic inline uint64_t fptoui_f32_i64(float x)\n{\n    return x;\n}\n#define group_sizze_8979 (DEFAULT_GROUP_SIZE)\n#define y_8981 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9101 (DEFAULT_GROUP_SIZE)\n#define group_sizze_9101 (DEFAULT_GROUP_SIZE)\n#define group_sizze_9195 (DEFAULT_GROUP_SIZE)\n#define y_9197 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9195 (DEFAULT_GROUP_SIZE)\n#define group_sizze_9351 (DEFAULT_GROUP_SIZE)\n#define y_9353 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9351 (DEFAULT_GROUP_SIZE)\n#define y_9353 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9351 (DEFAULT_GROUP_SIZE)\n#define y_9353 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9351 (DEFAULT_GROUP_SIZE)\n#define y_9353 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9351 (DEFAULT_GROUP_SIZE)\n#define group_sizze_9351 (DEFAULT_GROUP_SIZE)\n#define group_sizze_9871 (DEFAULT_GROUP_SIZE)\n#define y_9873 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9871 (DEFAULT_GROUP_SIZE)\n#define y_9873 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9871 (DEFAULT_GROUP_SIZE)\n#define y_9873 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9871 (DEFAULT_GROUP_SIZE)\n#define y_9873 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_9871 (DEFAULT_GROUP_SIZE)\n#define group_sizze_9871 (DEFAULT_GROUP_SIZE)\n#define group_sizze_10391 (DEFAULT_GROUP_SIZE)\n#define y_10393 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_10391 (DEFAULT_GROUP_SIZE)\n#define y_10393 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_10391 (DEFAULT_GROUP_SIZE)\n#define y_10393 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_10391 (DEFAULT_GROUP_SIZE)\n#define y_10393 (DEFAULT_GROUP_SIZE - 1)\n#define group",
                   "_sizze_10391 (DEFAULT_GROUP_SIZE)\n#define y_10393 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_10391 (DEFAULT_GROUP_SIZE)\n#define y_10393 (DEFAULT_GROUP_SIZE - 1)\n#define group_sizze_10391 (DEFAULT_GROUP_SIZE)\n#define y_10393 (DEFAULT_GROUP_SIZE - 1)\n__kernel void chunked_reduce_kernel_10338(__local volatile\n                                          int64_t *mem_aligned_0,\n                                          int32_t padding_8240,\n                                          int32_t num_threads_9878,\n                                          int32_t num_iterations_9921, __global\n                                          unsigned char *b_mem_11683, __global\n                                          unsigned char *mem_11725)\n{\n    __local volatile char *restrict mem_11722 = mem_aligned_0;\n    int32_t wave_sizze_12309;\n    int32_t group_sizze_12310;\n    char thread_active_12311;\n    int32_t global_tid_10338;\n    int32_t local_tid_10339;\n    int32_t group_id_10340;\n    \n    global_tid_10338 = get_global_id(0);\n    local_tid_10339 = get_local_id(0);\n    group_sizze_12310 = get_local_size(0);\n    wave_sizze_12309 = LOCKSTEP_WIDTH;\n    group_id_10340 = get_group_id(0);\n    thread_active_12311 = 1;\n    \n    int32_t chunk_sizze_10344 = smin32(num_iterations_9921,\n                                       squot32(padding_8240 - global_tid_10338 +\n                                               num_threads_9878 - 1,\n                                               num_threads_9878));\n    \n    if (thread_active_12311) { }\n    \n    int32_t x_10347;\n    int32_t final_result_10357;\n    int32_t acc_10350 = 0;\n    int32_t groupstream_mapaccum_dummy_chunk_sizze_10348 = 1;\n    \n    if (thread_active_12311) {\n        for (int32_t i_10349 = 0; i_10349 < chunk_sizze_10344; i_10349++) {\n            int32_t binop_param_y_10352 = *(__global\n                                            int32_t *) &b_mem_11683[(global_tid_10338 +\n                                                                    ",
                   " i_10349 *\n                                                                     num_threads_9878) *\n                                                                    4];\n            int32_t res_10354 = acc_10350 + binop_param_y_10352;\n            \n            acc_10350 = res_10354;\n        }\n    }\n    x_10347 = acc_10350;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10339, group_sizze_9871) && 1) {\n        *(__local int32_t *) &mem_11722[local_tid_10339 * 4] = x_10347;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t skip_waves_12312;\n    int32_t my_index_10358;\n    int32_t other_offset_10359;\n    int32_t binop_param_x_10360;\n    int32_t binop_param_y_10361;\n    \n    my_index_10358 = local_tid_10339;\n    other_offset_10359 = 0;\n    binop_param_x_10360 = *(__local int32_t *) &mem_11722[(local_tid_10339 +\n                                                           other_offset_10359) *\n                                                          4];\n    other_offset_10359 = 1;\n    while (slt32(other_offset_10359, wave_sizze_12309)) {\n        if (slt32(local_tid_10339 + other_offset_10359, group_sizze_9871) &&\n            ((local_tid_10339 - squot32(local_tid_10339, wave_sizze_12309) *\n              wave_sizze_12309) & (2 * other_offset_10359 - 1)) == 0) {\n            // read array element\n            {\n                binop_param_y_10361 = *(volatile __local\n                                        int32_t *) &mem_11722[(local_tid_10339 +\n                                                               other_offset_10359) *\n                                                              4];\n            }\n            \n            int32_t res_10362;\n            \n            if (thread_active_12311) {\n                res_10362 = binop_param_x_10360 + binop_param_y_10361;\n            }\n            binop_param_x_10360 = res_10362;\n            *(volatile __local int32_t *) &mem_11722[local_tid_10339 * 4] =\n                binop_param_x_10360;\n        }\n       ",
                   " other_offset_10359 *= 2;\n    }\n    skip_waves_12312 = 1;\n    while (slt32(skip_waves_12312, squot32(group_sizze_12310 +\n                                           wave_sizze_12309 - 1,\n                                           wave_sizze_12309))) {\n        barrier(CLK_LOCAL_MEM_FENCE);\n        other_offset_10359 = skip_waves_12312 * wave_sizze_12309;\n        if ((local_tid_10339 - squot32(local_tid_10339, wave_sizze_12309) *\n             wave_sizze_12309) == 0 && (squot32(local_tid_10339,\n                                                wave_sizze_12309) & (2 *\n                                                                     skip_waves_12312 -\n                                                                     1)) == 0) {\n            // read array element\n            {\n                binop_param_y_10361 = *(__local\n                                        int32_t *) &mem_11722[(local_tid_10339 +\n                                                               other_offset_10359) *\n                                                              4];\n            }\n            \n            int32_t res_10362;\n            \n            if (thread_active_12311) {\n                res_10362 = binop_param_x_10360 + binop_param_y_10361;\n            }\n            binop_param_x_10360 = res_10362;\n            *(__local int32_t *) &mem_11722[local_tid_10339 * 4] =\n                binop_param_x_10360;\n        }\n        skip_waves_12312 *= 2;\n    }\n    final_result_10357 = binop_param_x_10360;\n    if (local_tid_10339 == 0) {\n        *(__global int32_t *) &mem_11725[group_id_10340 * 4] =\n            final_result_10357;\n    }\n}\n__kernel void chunked_reduce_kernel_9116(__local volatile\n                                         int64_t *mem_aligned_0,\n                                         int32_t sizze_7966, int32_t p_7968,\n                                         int32_t y_7975,\n                                         int32_t per_thread_elements_9111,\n                              ",
                   "           __global unsigned char *pol_mem_11613,\n                                         __global unsigned char *mem_11619)\n{\n    __local volatile char *restrict mem_11616 = mem_aligned_0;\n    int32_t wave_sizze_12001;\n    int32_t group_sizze_12002;\n    char thread_active_12003;\n    int32_t global_tid_9116;\n    int32_t local_tid_9117;\n    int32_t group_id_9118;\n    \n    global_tid_9116 = get_global_id(0);\n    local_tid_9117 = get_local_id(0);\n    group_sizze_12002 = get_local_size(0);\n    wave_sizze_12001 = LOCKSTEP_WIDTH;\n    group_id_9118 = get_group_id(0);\n    thread_active_12003 = 1;\n    \n    int32_t chunk_sizze_9123;\n    int32_t starting_point_12004 = global_tid_9116 * per_thread_elements_9111;\n    int32_t remaining_elements_12005 = p_7968 - starting_point_12004;\n    \n    if (sle32(remaining_elements_12005, 0) || sle32(p_7968,\n                                                    starting_point_12004)) {\n        chunk_sizze_9123 = 0;\n    } else {\n        if (slt32(p_7968, (global_tid_9116 + 1) * per_thread_elements_9111)) {\n            chunk_sizze_9123 = p_7968 - global_tid_9116 *\n                per_thread_elements_9111;\n        } else {\n            chunk_sizze_9123 = per_thread_elements_9111;\n        }\n    }\n    \n    int32_t slice_offset_9124;\n    \n    if (thread_active_12003) {\n        slice_offset_9124 = global_tid_9116 * per_thread_elements_9111;\n    }\n    \n    int32_t x_9127;\n    int32_t final_result_9160;\n    int32_t acc_9130 = 1;\n    int32_t groupstream_mapaccum_dummy_chunk_sizze_9128;\n    int32_t i_9129 = 0;\n    \n    groupstream_mapaccum_dummy_chunk_sizze_9128 = chunk_sizze_9123;\n    for (int32_t i_9129 = 0; i_9129 < chunk_sizze_9123; i_9129++) {\n        int32_t convop_x_11566;\n        \n        if (thread_active_12003) {\n            convop_x_11566 = slice_offset_9124 + i_9129;\n        }\n        \n        int32_t res_9134;\n        int32_t x_9137 = 0;\n        int32_t chunk_sizze_9135;\n        int32_t chunk_offset_9136 = 0;\n        \n        chunk_sizze_913",
                   "5 = sizze_7966;\n        \n        int32_t res_9139;\n        int32_t acc_9142 = x_9137;\n        int32_t groupstream_mapaccum_dummy_chunk_sizze_9140 = 1;\n        \n        if (thread_active_12003) {\n            if (chunk_sizze_9135 == sizze_7966) {\n                for (int32_t i_9141 = 0; i_9141 < sizze_7966; i_9141++) {\n                    int32_t convop_x_11562 = chunk_offset_9136 + i_9141;\n                    char cond_9146 = convop_x_11562 == y_7975;\n                    int32_t res_9147;\n                    \n                    if (cond_9146) {\n                        int32_t res_9148 = *(__global\n                                             int32_t *) &pol_mem_11613[convop_x_11562 *\n                                                                       4];\n                        \n                        res_9147 = res_9148;\n                    } else {\n                        int32_t x_9149 = *(__global\n                                           int32_t *) &pol_mem_11613[convop_x_11562 *\n                                                                     4];\n                        int32_t x_9150 = sizze_7966 - 1;\n                        int32_t y_9151 = x_9150 - convop_x_11562;\n                        int32_t y_9152 = pow32(convop_x_11566, y_9151);\n                        int32_t res_9153 = x_9149 * y_9152;\n                        \n                        res_9147 = res_9153;\n                    }\n                    \n                    int32_t x_9154 = acc_9142 + res_9147;\n                    int32_t res_9155 = smod32(x_9154, p_7968);\n                    \n                    acc_9142 = res_9155;\n                }\n            } else {\n                for (int32_t i_9141 = 0; i_9141 < chunk_sizze_9135; i_9141++) {\n                    int32_t convop_x_11562 = chunk_offset_9136 + i_9141;\n                    char cond_9146 = convop_x_11562 == y_7975;\n                    int32_t res_9147;\n                    \n                    if (cond_9146) {\n                    ",
                   "    int32_t res_9148 = *(__global\n                                             int32_t *) &pol_mem_11613[convop_x_11562 *\n                                                                       4];\n                        \n                        res_9147 = res_9148;\n                    } else {\n                        int32_t x_9149 = *(__global\n                                           int32_t *) &pol_mem_11613[convop_x_11562 *\n                                                                     4];\n                        int32_t x_9150 = sizze_7966 - 1;\n                        int32_t y_9151 = x_9150 - convop_x_11562;\n                        int32_t y_9152 = pow32(convop_x_11566, y_9151);\n                        int32_t res_9153 = x_9149 * y_9152;\n                        \n                        res_9147 = res_9153;\n                    }\n                    \n                    int32_t x_9154 = acc_9142 + res_9147;\n                    int32_t res_9155 = smod32(x_9154, p_7968);\n                    \n                    acc_9142 = res_9155;\n                }\n            }\n        }\n        res_9139 = acc_9142;\n        x_9137 = res_9139;\n        res_9134 = x_9137;\n        \n        char cond_9156;\n        int32_t res_9157;\n        \n        if (thread_active_12003) {\n            cond_9156 = res_9134 == 0;\n            if (cond_9156) {\n                res_9157 = 0;\n            } else {\n                res_9157 = acc_9130;\n            }\n        }\n        acc_9130 = res_9157;\n    }\n    x_9127 = acc_9130;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9117, group_sizze_9101) && 1) {\n        *(__local int32_t *) &mem_11616[local_tid_9117 * 4] = x_9127;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t skip_waves_12006;\n    int32_t my_index_9161;\n    int32_t other_offset_9162;\n    int32_t x_9163;\n    int32_t y_9164;\n    \n    my_index_9161 = local_tid_9117;\n    other_offset_9162 = 0;\n    x_9163 = *(__local int32_t *) &mem_11616[(local_tid_9117 +\n            ",
                   "                                  other_offset_9162) * 4];\n    other_offset_9162 = 1;\n    while (slt32(other_offset_9162, wave_sizze_12001)) {\n        if (slt32(local_tid_9117 + other_offset_9162, group_sizze_9101) &&\n            ((local_tid_9117 - squot32(local_tid_9117, wave_sizze_12001) *\n              wave_sizze_12001) & (2 * other_offset_9162 - 1)) == 0) {\n            // read array element\n            {\n                y_9164 = *(volatile __local\n                           int32_t *) &mem_11616[(local_tid_9117 +\n                                                  other_offset_9162) * 4];\n            }\n            \n            char cond_9165;\n            int32_t res_9166;\n            \n            if (thread_active_12003) {\n                cond_9165 = y_9164 == 0;\n                if (cond_9165) {\n                    res_9166 = 0;\n                } else {\n                    res_9166 = x_9163;\n                }\n            }\n            x_9163 = res_9166;\n            *(volatile __local int32_t *) &mem_11616[local_tid_9117 * 4] =\n                x_9163;\n        }\n        other_offset_9162 *= 2;\n    }\n    skip_waves_12006 = 1;\n    while (slt32(skip_waves_12006, squot32(group_sizze_12002 +\n                                           wave_sizze_12001 - 1,\n                                           wave_sizze_12001))) {\n        barrier(CLK_LOCAL_MEM_FENCE);\n        other_offset_9162 = skip_waves_12006 * wave_sizze_12001;\n        if ((local_tid_9117 - squot32(local_tid_9117, wave_sizze_12001) *\n             wave_sizze_12001) == 0 && (squot32(local_tid_9117,\n                                                wave_sizze_12001) & (2 *\n                                                                     skip_waves_12006 -\n                                                                     1)) == 0) {\n            // read array element\n            {\n                y_9164 = *(__local int32_t *) &mem_11616[(local_tid_9117 +\n                                                          ",
                   "other_offset_9162) *\n                                                         4];\n            }\n            \n            char cond_9165;\n            int32_t res_9166;\n            \n            if (thread_active_12003) {\n                cond_9165 = y_9164 == 0;\n                if (cond_9165) {\n                    res_9166 = 0;\n                } else {\n                    res_9166 = x_9163;\n                }\n            }\n            x_9163 = res_9166;\n            *(__local int32_t *) &mem_11616[local_tid_9117 * 4] = x_9163;\n        }\n        skip_waves_12006 *= 2;\n    }\n    final_result_9160 = x_9163;\n    if (local_tid_9117 == 0) {\n        *(__global int32_t *) &mem_11619[group_id_9118 * 4] = final_result_9160;\n    }\n}\n__kernel void chunked_reduce_kernel_9818(__local volatile\n                                         int64_t *mem_aligned_0,\n                                         int32_t padding_8114,\n                                         int32_t num_threads_9358,\n                                         int32_t num_iterations_9401, __global\n                                         unsigned char *b_mem_11679, __global\n                                         unsigned char *mem_11721)\n{\n    __local volatile char *restrict mem_11718 = mem_aligned_0;\n    int32_t wave_sizze_12182;\n    int32_t group_sizze_12183;\n    char thread_active_12184;\n    int32_t global_tid_9818;\n    int32_t local_tid_9819;\n    int32_t group_id_9820;\n    \n    global_tid_9818 = get_global_id(0);\n    local_tid_9819 = get_local_id(0);\n    group_sizze_12183 = get_local_size(0);\n    wave_sizze_12182 = LOCKSTEP_WIDTH;\n    group_id_9820 = get_group_id(0);\n    thread_active_12184 = 1;\n    \n    int32_t chunk_sizze_9824 = smin32(num_iterations_9401,\n                                      squot32(padding_8114 - global_tid_9818 +\n                                              num_threads_9358 - 1,\n                                              num_threads_9358));\n    \n    if (thread_active_12184) { }\n    \n    i",
                   "nt32_t x_9827;\n    int32_t final_result_9837;\n    int32_t acc_9830 = 0;\n    int32_t groupstream_mapaccum_dummy_chunk_sizze_9828 = 1;\n    \n    if (thread_active_12184) {\n        for (int32_t i_9829 = 0; i_9829 < chunk_sizze_9824; i_9829++) {\n            int32_t binop_param_y_9832 = *(__global\n                                           int32_t *) &b_mem_11679[(global_tid_9818 +\n                                                                    i_9829 *\n                                                                    num_threads_9358) *\n                                                                   4];\n            int32_t res_9834 = acc_9830 + binop_param_y_9832;\n            \n            acc_9830 = res_9834;\n        }\n    }\n    x_9827 = acc_9830;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9819, group_sizze_9351) && 1) {\n        *(__local int32_t *) &mem_11718[local_tid_9819 * 4] = x_9827;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t skip_waves_12185;\n    int32_t my_index_9838;\n    int32_t other_offset_9839;\n    int32_t binop_param_x_9840;\n    int32_t binop_param_y_9841;\n    \n    my_index_9838 = local_tid_9819;\n    other_offset_9839 = 0;\n    binop_param_x_9840 = *(__local int32_t *) &mem_11718[(local_tid_9819 +\n                                                          other_offset_9839) *\n                                                         4];\n    other_offset_9839 = 1;\n    while (slt32(other_offset_9839, wave_sizze_12182)) {\n        if (slt32(local_tid_9819 + other_offset_9839, group_sizze_9351) &&\n            ((local_tid_9819 - squot32(local_tid_9819, wave_sizze_12182) *\n              wave_sizze_12182) & (2 * other_offset_9839 - 1)) == 0) {\n            // read array element\n            {\n                binop_param_y_9841 = *(volatile __local\n                                       int32_t *) &mem_11718[(local_tid_9819 +\n                                                              other_offset_9839) *\n                           ",
                   "                                  4];\n            }\n            \n            int32_t res_9842;\n            \n            if (thread_active_12184) {\n                res_9842 = binop_param_x_9840 + binop_param_y_9841;\n            }\n            binop_param_x_9840 = res_9842;\n            *(volatile __local int32_t *) &mem_11718[local_tid_9819 * 4] =\n                binop_param_x_9840;\n        }\n        other_offset_9839 *= 2;\n    }\n    skip_waves_12185 = 1;\n    while (slt32(skip_waves_12185, squot32(group_sizze_12183 +\n                                           wave_sizze_12182 - 1,\n                                           wave_sizze_12182))) {\n        barrier(CLK_LOCAL_MEM_FENCE);\n        other_offset_9839 = skip_waves_12185 * wave_sizze_12182;\n        if ((local_tid_9819 - squot32(local_tid_9819, wave_sizze_12182) *\n             wave_sizze_12182) == 0 && (squot32(local_tid_9819,\n                                                wave_sizze_12182) & (2 *\n                                                                     skip_waves_12185 -\n                                                                     1)) == 0) {\n            // read array element\n            {\n                binop_param_y_9841 = *(__local\n                                       int32_t *) &mem_11718[(local_tid_9819 +\n                                                              other_offset_9839) *\n                                                             4];\n            }\n            \n            int32_t res_9842;\n            \n            if (thread_active_12184) {\n                res_9842 = binop_param_x_9840 + binop_param_y_9841;\n            }\n            binop_param_x_9840 = res_9842;\n            *(__local int32_t *) &mem_11718[local_tid_9819 * 4] =\n                binop_param_x_9840;\n        }\n        skip_waves_12185 *= 2;\n    }\n    final_result_9837 = binop_param_x_9840;\n    if (local_tid_9819 == 0) {\n        *(__global int32_t *) &mem_11721[group_id_9820 * 4] = final_result_9837;\n    }\n",
                   "}\n__kernel void fut_kernel_map_transpose_i32(__global int32_t *odata,\n                                           uint odata_offset, __global\n                                           int32_t *idata, uint idata_offset,\n                                           uint width, uint height,\n                                           uint input_size, uint output_size,\n                                           __local int32_t *block)\n{\n    uint x_index;\n    uint y_index;\n    uint our_array_offset;\n    \n    // Adjust the input and output arrays with the basic offset.\n    odata += odata_offset / sizeof(int32_t);\n    idata += idata_offset / sizeof(int32_t);\n    // Adjust the input and output arrays for the third dimension.\n    our_array_offset = get_global_id(2) * width * height;\n    odata += our_array_offset;\n    idata += our_array_offset;\n    // read the matrix tile into shared memory\n    x_index = get_global_id(0);\n    y_index = get_global_id(1);\n    \n    uint index_in = y_index * width + x_index;\n    \n    if ((x_index < width && y_index < height) && index_in < input_size)\n        block[get_local_id(1) * (FUT_BLOCK_DIM + 1) + get_local_id(0)] =\n            idata[index_in];\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // Scatter the transposed matrix tile to global memory.\n    x_index = get_group_id(1) * FUT_BLOCK_DIM + get_local_id(0);\n    y_index = get_group_id(0) * FUT_BLOCK_DIM + get_local_id(1);\n    \n    uint index_out = y_index * height + x_index;\n    \n    if ((x_index < height && y_index < width) && index_out < output_size)\n        odata[index_out] = block[get_local_id(0) * (FUT_BLOCK_DIM + 1) +\n                                 get_local_id(1)];\n}\n__kernel void fut_kernel_map_transpose_lowheight_i32(__global int32_t *odata,\n                                                     uint odata_offset, __global\n                                                     int32_t *idata,\n                                                     uint idata_offset,\n                               ",
                   "                      uint width, uint height,\n                                                     uint input_size,\n                                                     uint output_size,\n                                                     uint mulx, __local\n                                                     int32_t *block)\n{\n    uint x_index;\n    uint y_index;\n    uint our_array_offset;\n    \n    // Adjust the input and output arrays with the basic offset.\n    odata += odata_offset / sizeof(int32_t);\n    idata += idata_offset / sizeof(int32_t);\n    // Adjust the input and output arrays for the third dimension.\n    our_array_offset = get_global_id(2) * width * height;\n    odata += our_array_offset;\n    idata += our_array_offset;\n    // read the matrix tile into shared memory\n    x_index = get_group_id(0) * FUT_BLOCK_DIM * mulx + get_local_id(0) +\n        get_local_id(1) % mulx * FUT_BLOCK_DIM;\n    y_index = get_group_id(1) * FUT_BLOCK_DIM + get_local_id(1) / mulx;\n    \n    uint index_in = y_index * width + x_index;\n    \n    if ((x_index < width && y_index < height) && index_in < input_size)\n        block[get_local_id(1) * (FUT_BLOCK_DIM + 1) + get_local_id(0)] =\n            idata[index_in];\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // Scatter the transposed matrix tile to global memory.\n    x_index = get_group_id(1) * FUT_BLOCK_DIM + get_local_id(0) / mulx;\n    y_index = get_group_id(0) * FUT_BLOCK_DIM * mulx + get_local_id(1) +\n        get_local_id(0) % mulx * FUT_BLOCK_DIM;\n    \n    uint index_out = y_index * height + x_index;\n    \n    if ((x_index < height && y_index < width) && index_out < output_size)\n        odata[index_out] = block[get_local_id(0) * (FUT_BLOCK_DIM + 1) +\n                                 get_local_id(1)];\n}\n__kernel void fut_kernel_map_transpose_lowwidth_i32(__global int32_t *odata,\n                                                    uint odata_offset, __global\n                                                    int32_t *idata,\n                 ",
                   "                                   uint idata_offset,\n                                                    uint width, uint height,\n                                                    uint input_size,\n                                                    uint output_size, uint muly,\n                                                    __local int32_t *block)\n{\n    uint x_index;\n    uint y_index;\n    uint our_array_offset;\n    \n    // Adjust the input and output arrays with the basic offset.\n    odata += odata_offset / sizeof(int32_t);\n    idata += idata_offset / sizeof(int32_t);\n    // Adjust the input and output arrays for the third dimension.\n    our_array_offset = get_global_id(2) * width * height;\n    odata += our_array_offset;\n    idata += our_array_offset;\n    // read the matrix tile into shared memory\n    x_index = get_group_id(0) * FUT_BLOCK_DIM + get_local_id(0) / muly;\n    y_index = get_group_id(1) * FUT_BLOCK_DIM * muly + get_local_id(1) +\n        get_local_id(0) % muly * FUT_BLOCK_DIM;\n    \n    uint index_in = y_index * width + x_index;\n    \n    if ((x_index < width && y_index < height) && index_in < input_size)\n        block[get_local_id(1) * (FUT_BLOCK_DIM + 1) + get_local_id(0)] =\n            idata[index_in];\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // Scatter the transposed matrix tile to global memory.\n    x_index = get_group_id(1) * FUT_BLOCK_DIM * muly + get_local_id(0) +\n        get_local_id(1) % muly * FUT_BLOCK_DIM;\n    y_index = get_group_id(0) * FUT_BLOCK_DIM + get_local_id(1) / muly;\n    \n    uint index_out = y_index * height + x_index;\n    \n    if ((x_index < height && y_index < width) && index_out < output_size)\n        odata[index_out] = block[get_local_id(0) * (FUT_BLOCK_DIM + 1) +\n                                 get_local_id(1)];\n}\n__kernel void kernel_replicate_12564(int32_t res_8691,\n                                     int32_t partition_sizze_8940, __global\n                                     unsigned char *mem_11896)\n{\n    const uint rep",
                   "licate_gtid_12564 = get_global_id(0);\n    \n    if (replicate_gtid_12564 >= partition_sizze_8940)\n        return;\n    *(__global int32_t *) &mem_11896[replicate_gtid_12564 * 4] = res_8691;\n}\n__kernel void kernel_replicate_12569(int32_t prime_8379,\n                                     int32_t partition_sizze_8940, __global\n                                     unsigned char *mem_11899)\n{\n    const uint replicate_gtid_12569 = get_global_id(0);\n    \n    if (replicate_gtid_12569 >= partition_sizze_8940)\n        return;\n    *(__global int32_t *) &mem_11899[replicate_gtid_12569 * 4] = prime_8379;\n}\n__kernel void map_kernel_10129(int32_t padding_8240, int32_t y_9924, __global\n                               unsigned char *mem_11642, __global\n                               unsigned char *mem_11645, __global\n                               unsigned char *mem_11672, __global\n                               unsigned char *mem_11675, __global\n                               unsigned char *mem_11678, __global\n                               unsigned char *mem_11681)\n{\n    int32_t wave_sizze_12253;\n    int32_t group_sizze_12254;\n    char thread_active_12255;\n    int32_t j_10112;\n    int32_t global_tid_10129;\n    int32_t local_tid_10130;\n    int32_t group_id_10131;\n    \n    global_tid_10129 = get_global_id(0);\n    local_tid_10130 = get_local_id(0);\n    group_sizze_12254 = get_local_size(0);\n    wave_sizze_12253 = LOCKSTEP_WIDTH;\n    group_id_10131 = get_group_id(0);\n    j_10112 = global_tid_10129;\n    thread_active_12255 = slt32(j_10112, padding_8240);\n    \n    int32_t binop_param_y_10108;\n    int32_t y_10109;\n    int32_t group_id_10117;\n    char cond_10118;\n    int32_t final_result_10121;\n    int32_t final_result_10122;\n    \n    if (thread_active_12255) {\n        binop_param_y_10108 = *(__global int32_t *) &mem_11642[j_10112 * 4];\n        y_10109 = *(__global int32_t *) &mem_11645[j_10112 * 4];\n        group_id_10117 = squot32(j_10112, y_9924);\n        cond_10118 = 0 == group_id_10117;\n",
                   "        if (cond_10118) {\n            final_result_10121 = binop_param_y_10108;\n            final_result_10122 = y_10109;\n        } else {\n            int32_t carry_in_index_10119 = group_id_10117 - 1;\n            int32_t binop_param_x_10106 = *(__global\n                                            int32_t *) &mem_11672[carry_in_index_10119 *\n                                                                  4];\n            int32_t x_10107 = *(__global\n                                int32_t *) &mem_11675[carry_in_index_10119 * 4];\n            int32_t res_10110 = binop_param_x_10106 + binop_param_y_10108;\n            int32_t zz_10111 = x_10107 + y_10109;\n            \n            final_result_10121 = res_10110;\n            final_result_10122 = zz_10111;\n        }\n    }\n    if (thread_active_12255) {\n        *(__global int32_t *) &mem_11678[j_10112 * 4] = final_result_10121;\n    }\n    if (thread_active_12255) {\n        *(__global int32_t *) &mem_11681[j_10112 * 4] = final_result_10122;\n    }\n}\n__kernel void map_kernel_10220(int32_t padding_8240, int32_t y_9924, __global\n                               unsigned char *mem_11686, __global\n                               unsigned char *mem_11698, __global\n                               unsigned char *mem_11701)\n{\n    int32_t wave_sizze_12281;\n    int32_t group_sizze_12282;\n    char thread_active_12283;\n    int32_t j_10205;\n    int32_t global_tid_10220;\n    int32_t local_tid_10221;\n    int32_t group_id_10222;\n    \n    global_tid_10220 = get_global_id(0);\n    local_tid_10221 = get_local_id(0);\n    group_sizze_12282 = get_local_size(0);\n    wave_sizze_12281 = LOCKSTEP_WIDTH;\n    group_id_10222 = get_group_id(0);\n    j_10205 = global_tid_10220;\n    thread_active_12283 = slt32(j_10205, padding_8240);\n    \n    int32_t binop_param_y_10203;\n    int32_t group_id_10210;\n    char cond_10211;\n    int32_t final_result_10213;\n    \n    if (thread_active_12283) {\n        binop_param_y_10203 = *(__global int32_t *) &mem_11686[j_10205 * 4];\n  ",
                   "      group_id_10210 = squot32(j_10205, y_9924);\n        cond_10211 = 0 == group_id_10210;\n        if (cond_10211) {\n            final_result_10213 = binop_param_y_10203;\n        } else {\n            int32_t carry_in_index_10212 = group_id_10210 - 1;\n            int32_t binop_param_x_10202 = *(__global\n                                            int32_t *) &mem_11698[carry_in_index_10212 *\n                                                                  4];\n            int32_t res_10204 = binop_param_x_10202 + binop_param_y_10203;\n            \n            final_result_10213 = res_10204;\n        }\n    }\n    if (thread_active_12283) {\n        *(__global int32_t *) &mem_11701[j_10205 * 4] = final_result_10213;\n    }\n}\n__kernel void map_kernel_10310(int32_t padding_8240, int32_t y_9924, __global\n                               unsigned char *mem_11704, __global\n                               unsigned char *mem_11716, __global\n                               unsigned char *mem_11719)\n{\n    int32_t wave_sizze_12306;\n    int32_t group_sizze_12307;\n    char thread_active_12308;\n    int32_t j_10295;\n    int32_t global_tid_10310;\n    int32_t local_tid_10311;\n    int32_t group_id_10312;\n    \n    global_tid_10310 = get_global_id(0);\n    local_tid_10311 = get_local_id(0);\n    group_sizze_12307 = get_local_size(0);\n    wave_sizze_12306 = LOCKSTEP_WIDTH;\n    group_id_10312 = get_group_id(0);\n    j_10295 = global_tid_10310;\n    thread_active_12308 = slt32(j_10295, padding_8240);\n    \n    int32_t y_10293;\n    int32_t group_id_10300;\n    char cond_10301;\n    int32_t final_result_10303;\n    \n    if (thread_active_12308) {\n        y_10293 = *(__global int32_t *) &mem_11704[j_10295 * 4];\n        group_id_10300 = squot32(j_10295, y_9924);\n        cond_10301 = 0 == group_id_10300;\n        if (cond_10301) {\n            final_result_10303 = y_10293;\n        } else {\n            int32_t carry_in_index_10302 = group_id_10300 - 1;\n            int32_t x_10292 = *(__global\n                       ",
                   "         int32_t *) &mem_11716[carry_in_index_10302 * 4];\n            int32_t zz_10294 = x_10292 + y_10293;\n            \n            final_result_10303 = zz_10294;\n        }\n    }\n    if (thread_active_12308) {\n        *(__global int32_t *) &mem_11719[j_10295 * 4] = final_result_10303;\n    }\n}\n__kernel void map_kernel_10380(int32_t prime_8239, int32_t padding_8240,\n                               int32_t res_8361, int32_t res_8366, __global\n                               unsigned char *mem_11651, __global\n                               unsigned char *b_mem_11683, __global\n                               unsigned char *mem_11734)\n{\n    int32_t wave_sizze_12317;\n    int32_t group_sizze_12318;\n    char thread_active_12319;\n    int32_t gtid_10373;\n    int32_t global_tid_10380;\n    int32_t local_tid_10381;\n    int32_t group_id_10382;\n    \n    global_tid_10380 = get_global_id(0);\n    local_tid_10381 = get_local_id(0);\n    group_sizze_12318 = get_local_size(0);\n    wave_sizze_12317 = LOCKSTEP_WIDTH;\n    group_id_10382 = get_group_id(0);\n    gtid_10373 = global_tid_10380;\n    thread_active_12319 = slt32(gtid_10373, padding_8240);\n    \n    int32_t i_p_o_10384;\n    int32_t rot_i_10385;\n    int32_t x_10386;\n    int32_t res_10387;\n    int32_t x_10388;\n    int32_t x_10389;\n    int32_t res_10390;\n    \n    if (thread_active_12319) {\n        i_p_o_10384 = gtid_10373 + res_8361;\n        rot_i_10385 = smod32(i_p_o_10384, padding_8240);\n        x_10386 = *(__global int32_t *) &mem_11651[rot_i_10385 * 4];\n        res_10387 = x_10386 * res_8366;\n        x_10388 = *(__global int32_t *) &b_mem_11683[gtid_10373 * 4];\n        x_10389 = x_10388 - res_10387;\n        res_10390 = smod32(x_10389, prime_8239);\n    }\n    if (thread_active_12319) {\n        *(__global int32_t *) &mem_11734[gtid_10373 * 4] = res_10390;\n    }\n}\n__kernel void map_kernel_10484(int32_t prime_8379, int32_t y_10431, __global\n                               unsigned char *mem_11614, __global\n                               unsi",
                   "gned char *mem_11629, __global\n                               unsigned char *mem_11632)\n{\n    int32_t wave_sizze_12347;\n    int32_t group_sizze_12348;\n    char thread_active_12349;\n    int32_t j_10469;\n    int32_t global_tid_10484;\n    int32_t local_tid_10485;\n    int32_t group_id_10486;\n    \n    global_tid_10484 = get_global_id(0);\n    local_tid_10485 = get_local_id(0);\n    group_sizze_12348 = get_local_size(0);\n    wave_sizze_12347 = LOCKSTEP_WIDTH;\n    group_id_10486 = get_group_id(0);\n    j_10469 = global_tid_10484;\n    thread_active_12349 = slt32(j_10469, prime_8379);\n    \n    int32_t y_10467;\n    int32_t group_id_10474;\n    char cond_10475;\n    int32_t final_result_10477;\n    \n    if (thread_active_12349) {\n        y_10467 = *(__global int32_t *) &mem_11614[j_10469 * 4];\n        group_id_10474 = squot32(j_10469, y_10431);\n        cond_10475 = 0 == group_id_10474;\n        if (cond_10475) {\n            final_result_10477 = y_10467;\n        } else {\n            int32_t carry_in_index_10476 = group_id_10474 - 1;\n            int32_t x_10466 = *(__global\n                                int32_t *) &mem_11629[carry_in_index_10476 * 4];\n            int32_t zz_10468 = x_10466 + y_10467;\n            \n            final_result_10477 = zz_10468;\n        }\n    }\n    if (thread_active_12349) {\n        *(__global int32_t *) &mem_11632[j_10469 * 4] = final_result_10477;\n    }\n}\n__kernel void map_kernel_10494(int32_t prime_8379, int32_t partition_sizze_8400,\n                               __global unsigned char *mem_11617, __global\n                               unsigned char *mem_11632, __global\n                               unsigned char *mem_11635, __global\n                               unsigned char *mem_11638, __global\n                               unsigned char *mem_11641)\n{\n    int32_t wave_sizze_12350;\n    int32_t group_sizze_12351;\n    char thread_active_12352;\n    int32_t write_i_10487;\n    int32_t global_tid_10494;\n    int32_t local_tid_10495;\n    int32_t group_id_",
                   "10496;\n    \n    global_tid_10494 = get_global_id(0);\n    local_tid_10495 = get_local_id(0);\n    group_sizze_12351 = get_local_size(0);\n    wave_sizze_12350 = LOCKSTEP_WIDTH;\n    group_id_10496 = get_group_id(0);\n    write_i_10487 = global_tid_10494;\n    thread_active_12352 = slt32(write_i_10487, prime_8379);\n    \n    int32_t c_8408;\n    int32_t offset_8409;\n    char is_this_one_8411;\n    int32_t this_offset_8412;\n    int32_t total_res_8413;\n    \n    if (thread_active_12352) {\n        c_8408 = *(__global int32_t *) &mem_11617[write_i_10487 * 4];\n        offset_8409 = *(__global int32_t *) &mem_11632[write_i_10487 * 4];\n        is_this_one_8411 = c_8408 == 0;\n        this_offset_8412 = offset_8409 + -1;\n        if (is_this_one_8411) {\n            total_res_8413 = this_offset_8412;\n        } else {\n            total_res_8413 = -1;\n        }\n    }\n    if (thread_active_12352 && (sle32(0, total_res_8413) &&\n                                slt32(total_res_8413, partition_sizze_8400))) {\n        *(__global int32_t *) &mem_11635[total_res_8413 * 4] = 0;\n    }\n    if (thread_active_12352 && (sle32(0, total_res_8413) &&\n                                slt32(total_res_8413, partition_sizze_8400))) {\n        *(__global int32_t *) &mem_11638[total_res_8413 * 4] = prime_8379;\n    }\n    if (thread_active_12352 && (sle32(0, total_res_8413) &&\n                                slt32(total_res_8413, partition_sizze_8400))) {\n        *(__global int32_t *) &mem_11641[total_res_8413 * 4] = write_i_10487;\n    }\n}\n__kernel void map_kernel_10590(int32_t arg_8414, int32_t y_10537, __global\n                               unsigned char *mem_11644, __global\n                               unsigned char *mem_11659, __global\n                               unsigned char *mem_11662)\n{\n    int32_t wave_sizze_12376;\n    int32_t group_sizze_12377;\n    char thread_active_12378;\n    int32_t j_10575;\n    int32_t global_tid_10590;\n    int32_t local_tid_10591;\n    int32_t group_id_10592;\n    \n    global_tid_",
                   "10590 = get_global_id(0);\n    local_tid_10591 = get_local_id(0);\n    group_sizze_12377 = get_local_size(0);\n    wave_sizze_12376 = LOCKSTEP_WIDTH;\n    group_id_10592 = get_group_id(0);\n    j_10575 = global_tid_10590;\n    thread_active_12378 = slt32(j_10575, arg_8414);\n    \n    int32_t y_10573;\n    int32_t group_id_10580;\n    char cond_10581;\n    int32_t final_result_10583;\n    \n    if (thread_active_12378) {\n        y_10573 = *(__global int32_t *) &mem_11644[j_10575 * 4];\n        group_id_10580 = squot32(j_10575, y_10537);\n        cond_10581 = 0 == group_id_10580;\n        if (cond_10581) {\n            final_result_10583 = y_10573;\n        } else {\n            int32_t carry_in_index_10582 = group_id_10580 - 1;\n            int32_t x_10572 = *(__global\n                                int32_t *) &mem_11659[carry_in_index_10582 * 4];\n            int32_t zz_10574 = x_10572 + y_10573;\n            \n            final_result_10583 = zz_10574;\n        }\n    }\n    if (thread_active_12378) {\n        *(__global int32_t *) &mem_11662[j_10575 * 4] = final_result_10583;\n    }\n}\n__kernel void map_kernel_10600(int32_t prime_8379, int32_t arg_8414,\n                               int32_t partition_sizze_8429, __global\n                               unsigned char *mem_11647, __global\n                               unsigned char *mem_11662, __global\n                               unsigned char *mem_11665, __global\n                               unsigned char *mem_11668, __global\n                               unsigned char *mem_11671)\n{\n    int32_t wave_sizze_12379;\n    int32_t group_sizze_12380;\n    char thread_active_12381;\n    int32_t write_i_10593;\n    int32_t global_tid_10600;\n    int32_t local_tid_10601;\n    int32_t group_id_10602;\n    \n    global_tid_10600 = get_global_id(0);\n    local_tid_10601 = get_local_id(0);\n    group_sizze_12380 = get_local_size(0);\n    wave_sizze_12379 = LOCKSTEP_WIDTH;\n    group_id_10602 = get_group_id(0);\n    write_i_10593 = global_tid_10600;\n    thread_a",
                   "ctive_12381 = slt32(write_i_10593, arg_8414);\n    \n    int32_t c_8437;\n    int32_t offset_8438;\n    char is_this_one_8440;\n    int32_t this_offset_8441;\n    int32_t total_res_8442;\n    \n    if (thread_active_12381) {\n        c_8437 = *(__global int32_t *) &mem_11647[write_i_10593 * 4];\n        offset_8438 = *(__global int32_t *) &mem_11662[write_i_10593 * 4];\n        is_this_one_8440 = c_8437 == 0;\n        this_offset_8441 = offset_8438 + -1;\n        if (is_this_one_8440) {\n            total_res_8442 = this_offset_8441;\n        } else {\n            total_res_8442 = -1;\n        }\n    }\n    if (thread_active_12381 && (sle32(0, total_res_8442) &&\n                                slt32(total_res_8442, partition_sizze_8429))) {\n        *(__global int32_t *) &mem_11665[total_res_8442 * 4] = 1;\n    }\n    if (thread_active_12381 && (sle32(0, total_res_8442) &&\n                                slt32(total_res_8442, partition_sizze_8429))) {\n        *(__global int32_t *) &mem_11668[total_res_8442 * 4] = prime_8379;\n    }\n    if (thread_active_12381 && (sle32(0, total_res_8442) &&\n                                slt32(total_res_8442, partition_sizze_8429))) {\n        *(__global int32_t *) &mem_11671[total_res_8442 * 4] = write_i_10593;\n    }\n}\n__kernel void map_kernel_10696(int32_t arg_8443, int32_t y_10643, __global\n                               unsigned char *mem_11674, __global\n                               unsigned char *mem_11689, __global\n                               unsigned char *mem_11692)\n{\n    int32_t wave_sizze_12405;\n    int32_t group_sizze_12406;\n    char thread_active_12407;\n    int32_t j_10681;\n    int32_t global_tid_10696;\n    int32_t local_tid_10697;\n    int32_t group_id_10698;\n    \n    global_tid_10696 = get_global_id(0);\n    local_tid_10697 = get_local_id(0);\n    group_sizze_12406 = get_local_size(0);\n    wave_sizze_12405 = LOCKSTEP_WIDTH;\n    group_id_10698 = get_group_id(0);\n    j_10681 = global_tid_10696;\n    thread_active_12407 = slt32(j_10681, arg_84",
                   "43);\n    \n    int32_t y_10679;\n    int32_t group_id_10686;\n    char cond_10687;\n    int32_t final_result_10689;\n    \n    if (thread_active_12407) {\n        y_10679 = *(__global int32_t *) &mem_11674[j_10681 * 4];\n        group_id_10686 = squot32(j_10681, y_10643);\n        cond_10687 = 0 == group_id_10686;\n        if (cond_10687) {\n            final_result_10689 = y_10679;\n        } else {\n            int32_t carry_in_index_10688 = group_id_10686 - 1;\n            int32_t x_10678 = *(__global\n                                int32_t *) &mem_11689[carry_in_index_10688 * 4];\n            int32_t zz_10680 = x_10678 + y_10679;\n            \n            final_result_10689 = zz_10680;\n        }\n    }\n    if (thread_active_12407) {\n        *(__global int32_t *) &mem_11692[j_10681 * 4] = final_result_10689;\n    }\n}\n__kernel void map_kernel_10706(int32_t prime_8379, int32_t arg_8443,\n                               int32_t partition_sizze_8458, __global\n                               unsigned char *mem_11677, __global\n                               unsigned char *mem_11692, __global\n                               unsigned char *mem_11695, __global\n                               unsigned char *mem_11698, __global\n                               unsigned char *mem_11701)\n{\n    int32_t wave_sizze_12408;\n    int32_t group_sizze_12409;\n    char thread_active_12410;\n    int32_t write_i_10699;\n    int32_t global_tid_10706;\n    int32_t local_tid_10707;\n    int32_t group_id_10708;\n    \n    global_tid_10706 = get_global_id(0);\n    local_tid_10707 = get_local_id(0);\n    group_sizze_12409 = get_local_size(0);\n    wave_sizze_12408 = LOCKSTEP_WIDTH;\n    group_id_10708 = get_group_id(0);\n    write_i_10699 = global_tid_10706;\n    thread_active_12410 = slt32(write_i_10699, arg_8443);\n    \n    int32_t c_8466;\n    int32_t offset_8467;\n    char is_this_one_8469;\n    int32_t this_offset_8470;\n    int32_t total_res_8471;\n    \n    if (thread_active_12410) {\n        c_8466 = *(__global int32_t *) &mem_116",
                   "77[write_i_10699 * 4];\n        offset_8467 = *(__global int32_t *) &mem_11692[write_i_10699 * 4];\n        is_this_one_8469 = c_8466 == 0;\n        this_offset_8470 = offset_8467 + -1;\n        if (is_this_one_8469) {\n            total_res_8471 = this_offset_8470;\n        } else {\n            total_res_8471 = -1;\n        }\n    }\n    if (thread_active_12410 && (sle32(0, total_res_8471) &&\n                                slt32(total_res_8471, partition_sizze_8458))) {\n        *(__global int32_t *) &mem_11695[total_res_8471 * 4] = 2;\n    }\n    if (thread_active_12410 && (sle32(0, total_res_8471) &&\n                                slt32(total_res_8471, partition_sizze_8458))) {\n        *(__global int32_t *) &mem_11698[total_res_8471 * 4] = prime_8379;\n    }\n    if (thread_active_12410 && (sle32(0, total_res_8471) &&\n                                slt32(total_res_8471, partition_sizze_8458))) {\n        *(__global int32_t *) &mem_11701[total_res_8471 * 4] = write_i_10699;\n    }\n}\n__kernel void map_kernel_10802(int32_t arg_8472, int32_t y_10749, __global\n                               unsigned char *mem_11704, __global\n                               unsigned char *mem_11719, __global\n                               unsigned char *mem_11722)\n{\n    int32_t wave_sizze_12434;\n    int32_t group_sizze_12435;\n    char thread_active_12436;\n    int32_t j_10787;\n    int32_t global_tid_10802;\n    int32_t local_tid_10803;\n    int32_t group_id_10804;\n    \n    global_tid_10802 = get_global_id(0);\n    local_tid_10803 = get_local_id(0);\n    group_sizze_12435 = get_local_size(0);\n    wave_sizze_12434 = LOCKSTEP_WIDTH;\n    group_id_10804 = get_group_id(0);\n    j_10787 = global_tid_10802;\n    thread_active_12436 = slt32(j_10787, arg_8472);\n    \n    int32_t y_10785;\n    int32_t group_id_10792;\n    char cond_10793;\n    int32_t final_result_10795;\n    \n    if (thread_active_12436) {\n        y_10785 = *(__global int32_t *) &mem_11704[j_10787 * 4];\n        group_id_10792 = squot32(j_10787, y_10749);",
                   "\n        cond_10793 = 0 == group_id_10792;\n        if (cond_10793) {\n            final_result_10795 = y_10785;\n        } else {\n            int32_t carry_in_index_10794 = group_id_10792 - 1;\n            int32_t x_10784 = *(__global\n                                int32_t *) &mem_11719[carry_in_index_10794 * 4];\n            int32_t zz_10786 = x_10784 + y_10785;\n            \n            final_result_10795 = zz_10786;\n        }\n    }\n    if (thread_active_12436) {\n        *(__global int32_t *) &mem_11722[j_10787 * 4] = final_result_10795;\n    }\n}\n__kernel void map_kernel_10812(int32_t prime_8379, int32_t arg_8472,\n                               int32_t partition_sizze_8487, __global\n                               unsigned char *mem_11707, __global\n                               unsigned char *mem_11722, __global\n                               unsigned char *mem_11725, __global\n                               unsigned char *mem_11728, __global\n                               unsigned char *mem_11731)\n{\n    int32_t wave_sizze_12437;\n    int32_t group_sizze_12438;\n    char thread_active_12439;\n    int32_t write_i_10805;\n    int32_t global_tid_10812;\n    int32_t local_tid_10813;\n    int32_t group_id_10814;\n    \n    global_tid_10812 = get_global_id(0);\n    local_tid_10813 = get_local_id(0);\n    group_sizze_12438 = get_local_size(0);\n    wave_sizze_12437 = LOCKSTEP_WIDTH;\n    group_id_10814 = get_group_id(0);\n    write_i_10805 = global_tid_10812;\n    thread_active_12439 = slt32(write_i_10805, arg_8472);\n    \n    int32_t c_8495;\n    int32_t offset_8496;\n    char is_this_one_8498;\n    int32_t this_offset_8499;\n    int32_t total_res_8500;\n    \n    if (thread_active_12439) {\n        c_8495 = *(__global int32_t *) &mem_11707[write_i_10805 * 4];\n        offset_8496 = *(__global int32_t *) &mem_11722[write_i_10805 * 4];\n        is_this_one_8498 = c_8495 == 0;\n        this_offset_8499 = offset_8496 + -1;\n        if (is_this_one_8498) {\n            total_res_8500 = this_offset_8499;\n  ",
                   "      } else {\n            total_res_8500 = -1;\n        }\n    }\n    if (thread_active_12439 && (sle32(0, total_res_8500) &&\n                                slt32(total_res_8500, partition_sizze_8487))) {\n        *(__global int32_t *) &mem_11725[total_res_8500 * 4] = 3;\n    }\n    if (thread_active_12439 && (sle32(0, total_res_8500) &&\n                                slt32(total_res_8500, partition_sizze_8487))) {\n        *(__global int32_t *) &mem_11728[total_res_8500 * 4] = prime_8379;\n    }\n    if (thread_active_12439 && (sle32(0, total_res_8500) &&\n                                slt32(total_res_8500, partition_sizze_8487))) {\n        *(__global int32_t *) &mem_11731[total_res_8500 * 4] = write_i_10805;\n    }\n}\n__kernel void map_kernel_10985(int32_t partition_sizze_8458, int32_t y_10930,\n                               __global unsigned char *mem_11734, __global\n                               unsigned char *mem_11752, __global\n                               unsigned char *mem_11755)\n{\n    int32_t wave_sizze_12465;\n    int32_t group_sizze_12466;\n    char thread_active_12467;\n    int32_t j_10970;\n    int32_t global_tid_10985;\n    int32_t local_tid_10986;\n    int32_t group_id_10987;\n    \n    global_tid_10985 = get_global_id(0);\n    local_tid_10986 = get_local_id(0);\n    group_sizze_12466 = get_local_size(0);\n    wave_sizze_12465 = LOCKSTEP_WIDTH;\n    group_id_10987 = get_group_id(0);\n    j_10970 = global_tid_10985;\n    thread_active_12467 = slt32(j_10970, partition_sizze_8458);\n    \n    int32_t y_10968;\n    int32_t group_id_10975;\n    char cond_10976;\n    int32_t final_result_10978;\n    \n    if (thread_active_12467) {\n        y_10968 = *(__global int32_t *) &mem_11734[j_10970 * 4];\n        group_id_10975 = squot32(j_10970, y_10930);\n        cond_10976 = 0 == group_id_10975;\n        if (cond_10976) {\n            final_result_10978 = y_10968;\n        } else {\n            int32_t carry_in_index_10977 = group_id_10975 - 1;\n            int32_t x_10967 = *(__global\n        ",
                   "                        int32_t *) &mem_11752[carry_in_index_10977 * 4];\n            int32_t zz_10969 = x_10967 + y_10968;\n            \n            final_result_10978 = zz_10969;\n        }\n    }\n    if (thread_active_12467) {\n        *(__global int32_t *) &mem_11755[j_10970 * 4] = final_result_10978;\n    }\n}\n__kernel void map_kernel_10995(int32_t partition_sizze_8458,\n                               int32_t partition_sizze_8555, __global\n                               unsigned char *mem_11695, __global\n                               unsigned char *mem_11698, __global\n                               unsigned char *mem_11701, __global\n                               unsigned char *mem_11737, __global\n                               unsigned char *mem_11755, __global\n                               unsigned char *mem_11758, __global\n                               unsigned char *mem_11761, __global\n                               unsigned char *mem_11764)\n{\n    int32_t wave_sizze_12468;\n    int32_t group_sizze_12469;\n    char thread_active_12470;\n    int32_t write_i_10988;\n    int32_t global_tid_10995;\n    int32_t local_tid_10996;\n    int32_t group_id_10997;\n    \n    global_tid_10995 = get_global_id(0);\n    local_tid_10996 = get_local_id(0);\n    group_sizze_12469 = get_local_size(0);\n    wave_sizze_12468 = LOCKSTEP_WIDTH;\n    group_id_10997 = get_group_id(0);\n    write_i_10988 = global_tid_10995;\n    thread_active_12470 = slt32(write_i_10988, partition_sizze_8458);\n    \n    int32_t c_8563;\n    int32_t offset_8564;\n    int32_t v_8565;\n    int32_t v_8566;\n    int32_t v_8567;\n    char is_this_one_8568;\n    int32_t this_offset_8569;\n    int32_t total_res_8570;\n    \n    if (thread_active_12470) {\n        c_8563 = *(__global int32_t *) &mem_11737[write_i_10988 * 4];\n        offset_8564 = *(__global int32_t *) &mem_11755[write_i_10988 * 4];\n        v_8565 = *(__global int32_t *) &mem_11695[write_i_10988 * 4];\n        v_8566 = *(__global int32_t *) &mem_11698[write_i_10988 * 4];\n    ",
                   "    v_8567 = *(__global int32_t *) &mem_11701[write_i_10988 * 4];\n        is_this_one_8568 = c_8563 == 0;\n        this_offset_8569 = offset_8564 + -1;\n        if (is_this_one_8568) {\n            total_res_8570 = this_offset_8569;\n        } else {\n            total_res_8570 = -1;\n        }\n    }\n    if (thread_active_12470 && (sle32(0, total_res_8570) &&\n                                slt32(total_res_8570, partition_sizze_8555))) {\n        *(__global int32_t *) &mem_11758[total_res_8570 * 4] = v_8565;\n    }\n    if (thread_active_12470 && (sle32(0, total_res_8570) &&\n                                slt32(total_res_8570, partition_sizze_8555))) {\n        *(__global int32_t *) &mem_11761[total_res_8570 * 4] = v_8566;\n    }\n    if (thread_active_12470 && (sle32(0, total_res_8570) &&\n                                slt32(total_res_8570, partition_sizze_8555))) {\n        *(__global int32_t *) &mem_11764[total_res_8570 * 4] = v_8567;\n    }\n}\n__kernel void map_kernel_11168(int32_t partition_sizze_8487, int32_t y_11113,\n                               __global unsigned char *mem_11767, __global\n                               unsigned char *mem_11785, __global\n                               unsigned char *mem_11788)\n{\n    int32_t wave_sizze_12496;\n    int32_t group_sizze_12497;\n    char thread_active_12498;\n    int32_t j_11153;\n    int32_t global_tid_11168;\n    int32_t local_tid_11169;\n    int32_t group_id_11170;\n    \n    global_tid_11168 = get_global_id(0);\n    local_tid_11169 = get_local_id(0);\n    group_sizze_12497 = get_local_size(0);\n    wave_sizze_12496 = LOCKSTEP_WIDTH;\n    group_id_11170 = get_group_id(0);\n    j_11153 = global_tid_11168;\n    thread_active_12498 = slt32(j_11153, partition_sizze_8487);\n    \n    int32_t y_11151;\n    int32_t group_id_11158;\n    char cond_11159;\n    int32_t final_result_11161;\n    \n    if (thread_active_12498) {\n        y_11151 = *(__global int32_t *) &mem_11767[j_11153 * 4];\n        group_id_11158 = squot32(j_11153, y_11113);\n        cond_",
                   "11159 = 0 == group_id_11158;\n        if (cond_11159) {\n            final_result_11161 = y_11151;\n        } else {\n            int32_t carry_in_index_11160 = group_id_11158 - 1;\n            int32_t x_11150 = *(__global\n                                int32_t *) &mem_11785[carry_in_index_11160 * 4];\n            int32_t zz_11152 = x_11150 + y_11151;\n            \n            final_result_11161 = zz_11152;\n        }\n    }\n    if (thread_active_12498) {\n        *(__global int32_t *) &mem_11788[j_11153 * 4] = final_result_11161;\n    }\n}\n__kernel void map_kernel_11178(int32_t partition_sizze_8487,\n                               int32_t partition_sizze_8624, __global\n                               unsigned char *mem_11725, __global\n                               unsigned char *mem_11728, __global\n                               unsigned char *mem_11731, __global\n                               unsigned char *mem_11770, __global\n                               unsigned char *mem_11788, __global\n                               unsigned char *mem_11791, __global\n                               unsigned char *mem_11794, __global\n                               unsigned char *mem_11797)\n{\n    int32_t wave_sizze_12499;\n    int32_t group_sizze_12500;\n    char thread_active_12501;\n    int32_t write_i_11171;\n    int32_t global_tid_11178;\n    int32_t local_tid_11179;\n    int32_t group_id_11180;\n    \n    global_tid_11178 = get_global_id(0);\n    local_tid_11179 = get_local_id(0);\n    group_sizze_12500 = get_local_size(0);\n    wave_sizze_12499 = LOCKSTEP_WIDTH;\n    group_id_11180 = get_group_id(0);\n    write_i_11171 = global_tid_11178;\n    thread_active_12501 = slt32(write_i_11171, partition_sizze_8487);\n    \n    int32_t c_8632;\n    int32_t offset_8633;\n    int32_t v_8634;\n    int32_t v_8635;\n    int32_t v_8636;\n    char is_this_one_8637;\n    int32_t this_offset_8638;\n    int32_t total_res_8639;\n    \n    if (thread_active_12501) {\n        c_8632 = *(__global int32_t *) &mem_11770[write_i_11171",
                   " * 4];\n        offset_8633 = *(__global int32_t *) &mem_11788[write_i_11171 * 4];\n        v_8634 = *(__global int32_t *) &mem_11725[write_i_11171 * 4];\n        v_8635 = *(__global int32_t *) &mem_11728[write_i_11171 * 4];\n        v_8636 = *(__global int32_t *) &mem_11731[write_i_11171 * 4];\n        is_this_one_8637 = c_8632 == 0;\n        this_offset_8638 = offset_8633 + -1;\n        if (is_this_one_8637) {\n            total_res_8639 = this_offset_8638;\n        } else {\n            total_res_8639 = -1;\n        }\n    }\n    if (thread_active_12501 && (sle32(0, total_res_8639) &&\n                                slt32(total_res_8639, partition_sizze_8624))) {\n        *(__global int32_t *) &mem_11791[total_res_8639 * 4] = v_8634;\n    }\n    if (thread_active_12501 && (sle32(0, total_res_8639) &&\n                                slt32(total_res_8639, partition_sizze_8624))) {\n        *(__global int32_t *) &mem_11794[total_res_8639 * 4] = v_8635;\n    }\n    if (thread_active_12501 && (sle32(0, total_res_8639) &&\n                                slt32(total_res_8639, partition_sizze_8624))) {\n        *(__global int32_t *) &mem_11797[total_res_8639 * 4] = v_8636;\n    }\n}\n__kernel void map_kernel_11512(int32_t arg_8695, int32_t y_11453, __global\n                               unsigned char *mem_11842, __global\n                               unsigned char *mem_11887, __global\n                               unsigned char *mem_11890)\n{\n    int32_t wave_sizze_12558;\n    int32_t group_sizze_12559;\n    char thread_active_12560;\n    int32_t j_11497;\n    int32_t global_tid_11512;\n    int32_t local_tid_11513;\n    int32_t group_id_11514;\n    \n    global_tid_11512 = get_global_id(0);\n    local_tid_11513 = get_local_id(0);\n    group_sizze_12559 = get_local_size(0);\n    wave_sizze_12558 = LOCKSTEP_WIDTH;\n    group_id_11514 = get_group_id(0);\n    j_11497 = global_tid_11512;\n    thread_active_12560 = slt32(j_11497, arg_8695);\n    \n    int32_t y_11495;\n    int32_t group_id_11502;\n    char cond_115",
                   "03;\n    int32_t final_result_11505;\n    \n    if (thread_active_12560) {\n        y_11495 = *(__global int32_t *) &mem_11842[j_11497 * 4];\n        group_id_11502 = squot32(j_11497, y_11453);\n        cond_11503 = 0 == group_id_11502;\n        if (cond_11503) {\n            final_result_11505 = y_11495;\n        } else {\n            int32_t carry_in_index_11504 = group_id_11502 - 1;\n            int32_t x_11494 = *(__global\n                                int32_t *) &mem_11887[carry_in_index_11504 * 4];\n            int32_t zz_11496 = x_11494 + y_11495;\n            \n            final_result_11505 = zz_11496;\n        }\n    }\n    if (thread_active_12560) {\n        *(__global int32_t *) &mem_11890[j_11497 * 4] = final_result_11505;\n    }\n}\n__kernel void map_kernel_11522(int32_t arg_8695, int32_t partition_sizze_8940,\n                               __global unsigned char *mem_11845, __global\n                               unsigned char *mem_11850, __global\n                               unsigned char *mem_11890, __global\n                               unsigned char *mem_11893)\n{\n    int32_t wave_sizze_12561;\n    int32_t group_sizze_12562;\n    char thread_active_12563;\n    int32_t write_i_11515;\n    int32_t global_tid_11522;\n    int32_t local_tid_11523;\n    int32_t group_id_11524;\n    \n    global_tid_11522 = get_global_id(0);\n    local_tid_11523 = get_local_id(0);\n    group_sizze_12562 = get_local_size(0);\n    wave_sizze_12561 = LOCKSTEP_WIDTH;\n    group_id_11524 = get_group_id(0);\n    write_i_11515 = global_tid_11522;\n    thread_active_12563 = slt32(write_i_11515, arg_8695);\n    \n    int32_t c_8944;\n    int32_t offset_8945;\n    int32_t v_8946;\n    char is_this_one_8948;\n    int32_t this_offset_8949;\n    int32_t total_res_8950;\n    \n    if (thread_active_12563) {\n        c_8944 = *(__global int32_t *) &mem_11845[write_i_11515 * 4];\n        offset_8945 = *(__global int32_t *) &mem_11890[write_i_11515 * 4];\n        v_8946 = *(__global int32_t *) &mem_11850[write_i_11515 * 4];\n     ",
                   "   is_this_one_8948 = c_8944 == 0;\n        this_offset_8949 = offset_8945 + -1;\n        if (is_this_one_8948) {\n            total_res_8950 = this_offset_8949;\n        } else {\n            total_res_8950 = -1;\n        }\n    }\n    if (thread_active_12563 && (sle32(0, total_res_8950) &&\n                                slt32(total_res_8950, partition_sizze_8940))) {\n        *(__global int32_t *) &mem_11893[total_res_8950 * 4] = v_8946;\n    }\n}\n__kernel void map_kernel_11535(int32_t sizze_8682, int32_t arg_8957, __global\n                               unsigned char *res_mem_11912, __global\n                               unsigned char *res_mem_11914, __global\n                               unsigned char *mem_11917, __global\n                               unsigned char *mem_11921)\n{\n    int32_t wave_sizze_12580;\n    int32_t group_sizze_12581;\n    char thread_active_12582;\n    int32_t gtid_11526;\n    int32_t gtid_11527;\n    int32_t global_tid_11535;\n    int32_t local_tid_11536;\n    int32_t group_id_11537;\n    \n    global_tid_11535 = get_global_id(0);\n    local_tid_11536 = get_local_id(0);\n    group_sizze_12581 = get_local_size(0);\n    wave_sizze_12580 = LOCKSTEP_WIDTH;\n    group_id_11537 = get_group_id(0);\n    gtid_11526 = squot32(global_tid_11535, arg_8957);\n    gtid_11527 = global_tid_11535 - squot32(global_tid_11535, arg_8957) *\n        arg_8957;\n    thread_active_12582 = slt32(gtid_11526, sizze_8682) && slt32(gtid_11527,\n                                                                 arg_8957);\n    \n    int32_t p_11538;\n    int32_t p_11539;\n    int32_t y_11540;\n    char cond_11542;\n    int32_t res_11543;\n    \n    if (thread_active_12582) {\n        p_11538 = *(__global int32_t *) &res_mem_11912[gtid_11526 * 4];\n        p_11539 = *(__global int32_t *) &res_mem_11914[gtid_11526 * 4];\n        y_11540 = *(__global int32_t *) &mem_11917[gtid_11526 * 4];\n        cond_11542 = slt32(gtid_11527, y_11540);\n        if (cond_11542) {\n            res_11543 = 0;\n        } else {\n    ",
                   "        int32_t x_11544 = arg_8957 - gtid_11527;\n            int32_t y_11545 = x_11544 - 1;\n            int32_t y_11546 = pow32(p_11538, y_11545);\n            int32_t x_11547 = sdiv32(p_11539, y_11546);\n            int32_t res_11548 = smod32(x_11547, p_11538);\n            \n            res_11543 = res_11548;\n        }\n    }\n    if (thread_active_12582) {\n        *(__global int32_t *) &mem_11921[(gtid_11526 * arg_8957 + gtid_11527) *\n                                         4] = res_11543;\n    }\n}\n__kernel void map_kernel_11556(int32_t sizze_8682, int32_t arg_8957, __global\n                               unsigned char *res_mem_11910, __global\n                               unsigned char *mem_11917)\n{\n    int32_t wave_sizze_12577;\n    int32_t group_sizze_12578;\n    char thread_active_12579;\n    int32_t gtid_11549;\n    int32_t global_tid_11556;\n    int32_t local_tid_11557;\n    int32_t group_id_11558;\n    \n    global_tid_11556 = get_global_id(0);\n    local_tid_11557 = get_local_id(0);\n    group_sizze_12578 = get_local_size(0);\n    wave_sizze_12577 = LOCKSTEP_WIDTH;\n    group_id_11558 = get_group_id(0);\n    gtid_11549 = global_tid_11556;\n    thread_active_12579 = slt32(gtid_11549, sizze_8682);\n    \n    int32_t p_11559;\n    int32_t x_11560;\n    int32_t y_11561;\n    \n    if (thread_active_12579) {\n        p_11559 = *(__global int32_t *) &res_mem_11910[gtid_11549 * 4];\n        x_11560 = arg_8957 - p_11559;\n        y_11561 = x_11560 - 1;\n    }\n    if (thread_active_12579) {\n        *(__global int32_t *) &mem_11917[gtid_11549 * 4] = y_11561;\n    }\n}\n__kernel void map_kernel_9072(int32_t arg_7931, int32_t y_9019, __global\n                              unsigned char *mem_11614, __global\n                              unsigned char *mem_11629, __global\n                              unsigned char *mem_11632)\n{\n    int32_t wave_sizze_11994;\n    int32_t group_sizze_11995;\n    char thread_active_11996;\n    int32_t j_9057;\n    int32_t global_tid_9072;\n    int32_t local_tid_9073;\n    in",
                   "t32_t group_id_9074;\n    \n    global_tid_9072 = get_global_id(0);\n    local_tid_9073 = get_local_id(0);\n    group_sizze_11995 = get_local_size(0);\n    wave_sizze_11994 = LOCKSTEP_WIDTH;\n    group_id_9074 = get_group_id(0);\n    j_9057 = global_tid_9072;\n    thread_active_11996 = slt32(j_9057, arg_7931);\n    \n    int32_t y_9055;\n    int32_t group_id_9062;\n    char cond_9063;\n    int32_t final_result_9065;\n    \n    if (thread_active_11996) {\n        y_9055 = *(__global int32_t *) &mem_11614[j_9057 * 4];\n        group_id_9062 = squot32(j_9057, y_9019);\n        cond_9063 = 0 == group_id_9062;\n        if (cond_9063) {\n            final_result_9065 = y_9055;\n        } else {\n            int32_t carry_in_index_9064 = group_id_9062 - 1;\n            int32_t x_9054 = *(__global\n                               int32_t *) &mem_11629[carry_in_index_9064 * 4];\n            int32_t zz_9056 = x_9054 + y_9055;\n            \n            final_result_9065 = zz_9056;\n        }\n    }\n    if (thread_active_11996) {\n        *(__global int32_t *) &mem_11632[j_9057 * 4] = final_result_9065;\n    }\n}\n__kernel void map_kernel_9082(int32_t degree_7928, int32_t prime_7929,\n                              int32_t arg_7931, int32_t partition_sizze_7952,\n                              __global unsigned char *mem_11617, __global\n                              unsigned char *mem_11632, __global\n                              unsigned char *mem_11635, __global\n                              unsigned char *mem_11638, __global\n                              unsigned char *mem_11641)\n{\n    int32_t wave_sizze_11997;\n    int32_t group_sizze_11998;\n    char thread_active_11999;\n    int32_t write_i_9075;\n    int32_t global_tid_9082;\n    int32_t local_tid_9083;\n    int32_t group_id_9084;\n    \n    global_tid_9082 = get_global_id(0);\n    local_tid_9083 = get_local_id(0);\n    group_sizze_11998 = get_local_size(0);\n    wave_sizze_11997 = LOCKSTEP_WIDTH;\n    group_id_9084 = get_group_id(0);\n    write_i_9075 = global_tid_9082",
                   ";\n    thread_active_11999 = slt32(write_i_9075, arg_7931);\n    \n    int32_t c_7960;\n    int32_t offset_7961;\n    char is_this_one_7963;\n    int32_t this_offset_7964;\n    int32_t total_res_7965;\n    \n    if (thread_active_11999) {\n        c_7960 = *(__global int32_t *) &mem_11617[write_i_9075 * 4];\n        offset_7961 = *(__global int32_t *) &mem_11632[write_i_9075 * 4];\n        is_this_one_7963 = c_7960 == 0;\n        this_offset_7964 = offset_7961 + -1;\n        if (is_this_one_7963) {\n            total_res_7965 = this_offset_7964;\n        } else {\n            total_res_7965 = -1;\n        }\n    }\n    if (thread_active_11999 && (sle32(0, total_res_7965) &&\n                                slt32(total_res_7965, partition_sizze_7952))) {\n        *(__global int32_t *) &mem_11635[total_res_7965 * 4] = degree_7928;\n    }\n    if (thread_active_11999 && (sle32(0, total_res_7965) &&\n                                slt32(total_res_7965, partition_sizze_7952))) {\n        *(__global int32_t *) &mem_11638[total_res_7965 * 4] = prime_7929;\n    }\n    if (thread_active_11999 && (sle32(0, total_res_7965) &&\n                                slt32(total_res_7965, partition_sizze_7952))) {\n        *(__global int32_t *) &mem_11641[total_res_7965 * 4] = write_i_9075;\n    }\n}\n__kernel void map_kernel_9184(int32_t padding_8004, int32_t p_8006,\n                              int32_t x_8007, int32_t y_8015, __global\n                              unsigned char *mem_11614)\n{\n    int32_t wave_sizze_12014;\n    int32_t group_sizze_12015;\n    char thread_active_12016;\n    int32_t gtid_9177;\n    int32_t global_tid_9184;\n    int32_t local_tid_9185;\n    int32_t group_id_9186;\n    \n    global_tid_9184 = get_global_id(0);\n    local_tid_9185 = get_local_id(0);\n    group_sizze_12015 = get_local_size(0);\n    wave_sizze_12014 = LOCKSTEP_WIDTH;\n    group_id_9186 = get_group_id(0);\n    gtid_9177 = global_tid_9184;\n    thread_active_12016 = slt32(gtid_9177, padding_8004);\n    \n    char cond_9188;\n    int32_t res_",
                   "9189;\n    \n    if (thread_active_12016) {\n        cond_9188 = slt32(gtid_9177, y_8015);\n        if (cond_9188) {\n            res_9189 = 0;\n        } else {\n            int32_t x_9190 = padding_8004 - gtid_9177;\n            int32_t y_9191 = x_9190 - 1;\n            int32_t y_9192 = pow32(p_8006, y_9191);\n            int32_t x_9193 = sdiv32(x_8007, y_9192);\n            int32_t res_9194 = smod32(x_9193, p_8006);\n            \n            res_9189 = res_9194;\n        }\n    }\n    if (thread_active_12016) {\n        *(__global int32_t *) &mem_11614[gtid_9177 * 4] = res_9189;\n    }\n}\n__kernel void map_kernel_9288(int32_t arg_8028, int32_t y_9235, __global\n                              unsigned char *mem_11614, __global\n                              unsigned char *mem_11629, __global\n                              unsigned char *mem_11632)\n{\n    int32_t wave_sizze_12044;\n    int32_t group_sizze_12045;\n    char thread_active_12046;\n    int32_t j_9273;\n    int32_t global_tid_9288;\n    int32_t local_tid_9289;\n    int32_t group_id_9290;\n    \n    global_tid_9288 = get_global_id(0);\n    local_tid_9289 = get_local_id(0);\n    group_sizze_12045 = get_local_size(0);\n    wave_sizze_12044 = LOCKSTEP_WIDTH;\n    group_id_9290 = get_group_id(0);\n    j_9273 = global_tid_9288;\n    thread_active_12046 = slt32(j_9273, arg_8028);\n    \n    int32_t y_9271;\n    int32_t group_id_9278;\n    char cond_9279;\n    int32_t final_result_9281;\n    \n    if (thread_active_12046) {\n        y_9271 = *(__global int32_t *) &mem_11614[j_9273 * 4];\n        group_id_9278 = squot32(j_9273, y_9235);\n        cond_9279 = 0 == group_id_9278;\n        if (cond_9279) {\n            final_result_9281 = y_9271;\n        } else {\n            int32_t carry_in_index_9280 = group_id_9278 - 1;\n            int32_t x_9270 = *(__global\n                               int32_t *) &mem_11629[carry_in_index_9280 * 4];\n            int32_t zz_9272 = x_9270 + y_9271;\n            \n            final_result_9281 = zz_9272;\n        }\n    }\n    if (th",
                   "read_active_12046) {\n        *(__global int32_t *) &mem_11632[j_9273 * 4] = final_result_9281;\n    }\n}\n__kernel void map_kernel_9298(int32_t degree_8025, int32_t prime_8026,\n                              int32_t arg_8028, int32_t partition_sizze_8049,\n                              __global unsigned char *mem_11617, __global\n                              unsigned char *mem_11632, __global\n                              unsigned char *mem_11635, __global\n                              unsigned char *mem_11638, __global\n                              unsigned char *mem_11641)\n{\n    int32_t wave_sizze_12047;\n    int32_t group_sizze_12048;\n    char thread_active_12049;\n    int32_t write_i_9291;\n    int32_t global_tid_9298;\n    int32_t local_tid_9299;\n    int32_t group_id_9300;\n    \n    global_tid_9298 = get_global_id(0);\n    local_tid_9299 = get_local_id(0);\n    group_sizze_12048 = get_local_size(0);\n    wave_sizze_12047 = LOCKSTEP_WIDTH;\n    group_id_9300 = get_group_id(0);\n    write_i_9291 = global_tid_9298;\n    thread_active_12049 = slt32(write_i_9291, arg_8028);\n    \n    int32_t c_8057;\n    int32_t offset_8058;\n    char is_this_one_8060;\n    int32_t this_offset_8061;\n    int32_t total_res_8062;\n    \n    if (thread_active_12049) {\n        c_8057 = *(__global int32_t *) &mem_11617[write_i_9291 * 4];\n        offset_8058 = *(__global int32_t *) &mem_11632[write_i_9291 * 4];\n        is_this_one_8060 = c_8057 == 0;\n        this_offset_8061 = offset_8058 + -1;\n        if (is_this_one_8060) {\n            total_res_8062 = this_offset_8061;\n        } else {\n            total_res_8062 = -1;\n        }\n    }\n    if (thread_active_12049 && (sle32(0, total_res_8062) &&\n                                slt32(total_res_8062, partition_sizze_8049))) {\n        *(__global int32_t *) &mem_11635[total_res_8062 * 4] = degree_8025;\n    }\n    if (thread_active_12049 && (sle32(0, total_res_8062) &&\n                                slt32(total_res_8062, partition_sizze_8049))) {\n        *(__global ",
                   "int32_t *) &mem_11638[total_res_8062 * 4] = prime_8026;\n    }\n    if (thread_active_12049 && (sle32(0, total_res_8062) &&\n                                slt32(total_res_8062, partition_sizze_8049))) {\n        *(__global int32_t *) &mem_11641[total_res_8062 * 4] = write_i_9291;\n    }\n}\n__kernel void map_kernel_9308(int32_t partition_sizze_8049, __global\n                              unsigned char *mem_11635, __global\n                              unsigned char *mem_11638, __global\n                              unsigned char *mem_11641, __global\n                              unsigned char *mem_11644, __global\n                              unsigned char *mem_11649)\n{\n    int32_t wave_sizze_12050;\n    int32_t group_sizze_12051;\n    char thread_active_12052;\n    int32_t gtid_9301;\n    int32_t global_tid_9308;\n    int32_t local_tid_9309;\n    int32_t group_id_9310;\n    \n    global_tid_9308 = get_global_id(0);\n    local_tid_9309 = get_local_id(0);\n    group_sizze_12051 = get_local_size(0);\n    wave_sizze_12050 = LOCKSTEP_WIDTH;\n    group_id_9310 = get_group_id(0);\n    gtid_9301 = global_tid_9308;\n    thread_active_12052 = slt32(gtid_9301, partition_sizze_8049);\n    \n    int32_t d_9311;\n    int32_t p_9312;\n    int32_t i_9313;\n    \n    if (thread_active_12052) {\n        d_9311 = *(__global int32_t *) &mem_11635[gtid_9301 * 4];\n        p_9312 = *(__global int32_t *) &mem_11638[gtid_9301 * 4];\n        i_9313 = *(__global int32_t *) &mem_11641[gtid_9301 * 4];\n        *(__global int32_t *) &mem_11644[(group_id_9310 * (3 *\n                                                           group_sizze_9195) +\n                                          local_tid_9309) * 4] = d_9311;\n        *(__global int32_t *) &mem_11644[(group_id_9310 * (3 *\n                                                           group_sizze_9195) +\n                                          group_sizze_9195 + local_tid_9309) *\n                                         4] = p_9312;\n        *(__global int32_t *) &mem_116",
                   "44[(group_id_9310 * (3 *\n                                                           group_sizze_9195) +\n                                          2 * group_sizze_9195 +\n                                          local_tid_9309) * 4] = i_9313;\n    }\n    if (thread_active_12052) {\n        for (int32_t i_12053 = 0; i_12053 < 3; i_12053++) {\n            *(__global int32_t *) &mem_11649[(i_12053 * partition_sizze_8049 +\n                                              gtid_9301) * 4] = *(__global\n                                                                  int32_t *) &mem_11644[(group_id_9310 *\n                                                                                         (3 *\n                                                                                          group_sizze_9195) +\n                                                                                         i_12053 *\n                                                                                         group_sizze_9195 +\n                                                                                         local_tid_9309) *\n                                                                                        4];\n        }\n    }\n}\n__kernel void map_kernel_9322(int32_t p_8069, int32_t x_8070, int32_t arg_8071,\n                              __global unsigned char *mem_11614)\n{\n    int32_t wave_sizze_12057;\n    int32_t group_sizze_12058;\n    char thread_active_12059;\n    int32_t gtid_9315;\n    int32_t global_tid_9322;\n    int32_t local_tid_9323;\n    int32_t group_id_9324;\n    \n    global_tid_9322 = get_global_id(0);\n    local_tid_9323 = get_local_id(0);\n    group_sizze_12058 = get_local_size(0);\n    wave_sizze_12057 = LOCKSTEP_WIDTH;\n    group_id_9324 = get_group_id(0);\n    gtid_9315 = global_tid_9322;\n    thread_active_12059 = slt32(gtid_9315, arg_8071);\n    \n    char cond_9326;\n    int32_t res_9327;\n    \n    if (thread_active_12059) {\n        cond_9326 = slt32(gtid_9315, 0);\n        if (cond",
                   "_9326) {\n            res_9327 = 0;\n        } else {\n            int32_t x_9328 = arg_8071 - gtid_9315;\n            int32_t y_9329 = x_9328 - 1;\n            int32_t y_9330 = pow32(p_8069, y_9329);\n            int32_t x_9331 = sdiv32(x_8070, y_9330);\n            int32_t res_9332 = smod32(x_9331, p_8069);\n            \n            res_9327 = res_9332;\n        }\n    }\n    if (thread_active_12059) {\n        *(__global int32_t *) &mem_11614[gtid_9315 * 4] = res_9327;\n    }\n}\n__kernel void map_kernel_9340(int32_t p_8088, int32_t x_8089, int32_t arg_8090,\n                              int32_t y_8097, __global unsigned char *mem_11614)\n{\n    int32_t wave_sizze_12063;\n    int32_t group_sizze_12064;\n    char thread_active_12065;\n    int32_t gtid_9333;\n    int32_t global_tid_9340;\n    int32_t local_tid_9341;\n    int32_t group_id_9342;\n    \n    global_tid_9340 = get_global_id(0);\n    local_tid_9341 = get_local_id(0);\n    group_sizze_12064 = get_local_size(0);\n    wave_sizze_12063 = LOCKSTEP_WIDTH;\n    group_id_9342 = get_group_id(0);\n    gtid_9333 = global_tid_9340;\n    thread_active_12065 = slt32(gtid_9333, arg_8090);\n    \n    char cond_9344;\n    int32_t res_9345;\n    \n    if (thread_active_12065) {\n        cond_9344 = slt32(gtid_9333, y_8097);\n        if (cond_9344) {\n            res_9345 = 0;\n        } else {\n            int32_t x_9346 = arg_8090 - gtid_9333;\n            int32_t y_9347 = x_9346 - 1;\n            int32_t y_9348 = pow32(p_8088, y_9347);\n            int32_t x_9349 = sdiv32(x_8089, y_9348);\n            int32_t res_9350 = smod32(x_9349, p_8088);\n            \n            res_9345 = res_9350;\n        }\n    }\n    if (thread_active_12065) {\n        *(__global int32_t *) &mem_11614[gtid_9333 * 4] = res_9345;\n    }\n}\n__kernel void map_kernel_9460(int32_t padding_8114, int32_t y_9404, __global\n                              unsigned char *mem_11614, __global\n                              unsigned char *mem_11632, __global\n                              unsigned char *mem_116",
                   "35)\n{\n    int32_t wave_sizze_12091;\n    int32_t group_sizze_12092;\n    char thread_active_12093;\n    int32_t j_9445;\n    int32_t global_tid_9460;\n    int32_t local_tid_9461;\n    int32_t group_id_9462;\n    \n    global_tid_9460 = get_global_id(0);\n    local_tid_9461 = get_local_id(0);\n    group_sizze_12092 = get_local_size(0);\n    wave_sizze_12091 = LOCKSTEP_WIDTH;\n    group_id_9462 = get_group_id(0);\n    j_9445 = global_tid_9460;\n    thread_active_12093 = slt32(j_9445, padding_8114);\n    \n    int32_t binop_param_y_9443;\n    int32_t group_id_9450;\n    char cond_9451;\n    int32_t final_result_9453;\n    \n    if (thread_active_12093) {\n        binop_param_y_9443 = *(__global int32_t *) &mem_11614[j_9445 * 4];\n        group_id_9450 = squot32(j_9445, y_9404);\n        cond_9451 = 0 == group_id_9450;\n        if (cond_9451) {\n            final_result_9453 = binop_param_y_9443;\n        } else {\n            int32_t carry_in_index_9452 = group_id_9450 - 1;\n            int32_t binop_param_x_9442 = *(__global\n                                           int32_t *) &mem_11632[carry_in_index_9452 *\n                                                                 4];\n            int32_t res_9444 = binop_param_x_9442 + binop_param_y_9443;\n            \n            final_result_9453 = res_9444;\n        }\n    }\n    if (thread_active_12093) {\n        *(__global int32_t *) &mem_11635[j_9445 * 4] = final_result_9453;\n    }\n}\n__kernel void map_kernel_9609(int32_t padding_8114, int32_t y_9404, __global\n                              unsigned char *mem_11638, __global\n                              unsigned char *mem_11641, __global\n                              unsigned char *mem_11668, __global\n                              unsigned char *mem_11671, __global\n                              unsigned char *mem_11674, __global\n                              unsigned char *mem_11677)\n{\n    int32_t wave_sizze_12126;\n    int32_t group_sizze_12127;\n    char thread_active_12128;\n    int32_t j_9592;\n    int",
                   "32_t global_tid_9609;\n    int32_t local_tid_9610;\n    int32_t group_id_9611;\n    \n    global_tid_9609 = get_global_id(0);\n    local_tid_9610 = get_local_id(0);\n    group_sizze_12127 = get_local_size(0);\n    wave_sizze_12126 = LOCKSTEP_WIDTH;\n    group_id_9611 = get_group_id(0);\n    j_9592 = global_tid_9609;\n    thread_active_12128 = slt32(j_9592, padding_8114);\n    \n    int32_t binop_param_y_9588;\n    int32_t y_9589;\n    int32_t group_id_9597;\n    char cond_9598;\n    int32_t final_result_9601;\n    int32_t final_result_9602;\n    \n    if (thread_active_12128) {\n        binop_param_y_9588 = *(__global int32_t *) &mem_11638[j_9592 * 4];\n        y_9589 = *(__global int32_t *) &mem_11641[j_9592 * 4];\n        group_id_9597 = squot32(j_9592, y_9404);\n        cond_9598 = 0 == group_id_9597;\n        if (cond_9598) {\n            final_result_9601 = binop_param_y_9588;\n            final_result_9602 = y_9589;\n        } else {\n            int32_t carry_in_index_9599 = group_id_9597 - 1;\n            int32_t binop_param_x_9586 = *(__global\n                                           int32_t *) &mem_11668[carry_in_index_9599 *\n                                                                 4];\n            int32_t x_9587 = *(__global\n                               int32_t *) &mem_11671[carry_in_index_9599 * 4];\n            int32_t res_9590 = binop_param_x_9586 + binop_param_y_9588;\n            int32_t zz_9591 = x_9587 + y_9589;\n            \n            final_result_9601 = res_9590;\n            final_result_9602 = zz_9591;\n        }\n    }\n    if (thread_active_12128) {\n        *(__global int32_t *) &mem_11674[j_9592 * 4] = final_result_9601;\n    }\n    if (thread_active_12128) {\n        *(__global int32_t *) &mem_11677[j_9592 * 4] = final_result_9602;\n    }\n}\n__kernel void map_kernel_9700(int32_t padding_8114, int32_t y_9404, __global\n                              unsigned char *mem_11682, __global\n                              unsigned char *mem_11694, __global\n                       ",
                   "       unsigned char *mem_11697)\n{\n    int32_t wave_sizze_12154;\n    int32_t group_sizze_12155;\n    char thread_active_12156;\n    int32_t j_9685;\n    int32_t global_tid_9700;\n    int32_t local_tid_9701;\n    int32_t group_id_9702;\n    \n    global_tid_9700 = get_global_id(0);\n    local_tid_9701 = get_local_id(0);\n    group_sizze_12155 = get_local_size(0);\n    wave_sizze_12154 = LOCKSTEP_WIDTH;\n    group_id_9702 = get_group_id(0);\n    j_9685 = global_tid_9700;\n    thread_active_12156 = slt32(j_9685, padding_8114);\n    \n    int32_t binop_param_y_9683;\n    int32_t group_id_9690;\n    char cond_9691;\n    int32_t final_result_9693;\n    \n    if (thread_active_12156) {\n        binop_param_y_9683 = *(__global int32_t *) &mem_11682[j_9685 * 4];\n        group_id_9690 = squot32(j_9685, y_9404);\n        cond_9691 = 0 == group_id_9690;\n        if (cond_9691) {\n            final_result_9693 = binop_param_y_9683;\n        } else {\n            int32_t carry_in_index_9692 = group_id_9690 - 1;\n            int32_t binop_param_x_9682 = *(__global\n                                           int32_t *) &mem_11694[carry_in_index_9692 *\n                                                                 4];\n            int32_t res_9684 = binop_param_x_9682 + binop_param_y_9683;\n            \n            final_result_9693 = res_9684;\n        }\n    }\n    if (thread_active_12156) {\n        *(__global int32_t *) &mem_11697[j_9685 * 4] = final_result_9693;\n    }\n}\n__kernel void map_kernel_9790(int32_t padding_8114, int32_t y_9404, __global\n                              unsigned char *mem_11700, __global\n                              unsigned char *mem_11712, __global\n                              unsigned char *mem_11715)\n{\n    int32_t wave_sizze_12179;\n    int32_t group_sizze_12180;\n    char thread_active_12181;\n    int32_t j_9775;\n    int32_t global_tid_9790;\n    int32_t local_tid_9791;\n    int32_t group_id_9792;\n    \n    global_tid_9790 = get_global_id(0);\n    local_tid_9791 = get_local_id(0);\n    gr",
                   "oup_sizze_12180 = get_local_size(0);\n    wave_sizze_12179 = LOCKSTEP_WIDTH;\n    group_id_9792 = get_group_id(0);\n    j_9775 = global_tid_9790;\n    thread_active_12181 = slt32(j_9775, padding_8114);\n    \n    int32_t y_9773;\n    int32_t group_id_9780;\n    char cond_9781;\n    int32_t final_result_9783;\n    \n    if (thread_active_12181) {\n        y_9773 = *(__global int32_t *) &mem_11700[j_9775 * 4];\n        group_id_9780 = squot32(j_9775, y_9404);\n        cond_9781 = 0 == group_id_9780;\n        if (cond_9781) {\n            final_result_9783 = y_9773;\n        } else {\n            int32_t carry_in_index_9782 = group_id_9780 - 1;\n            int32_t x_9772 = *(__global\n                               int32_t *) &mem_11712[carry_in_index_9782 * 4];\n            int32_t zz_9774 = x_9772 + y_9773;\n            \n            final_result_9783 = zz_9774;\n        }\n    }\n    if (thread_active_12181) {\n        *(__global int32_t *) &mem_11715[j_9775 * 4] = final_result_9783;\n    }\n}\n__kernel void map_kernel_9860(int32_t prime_8113, int32_t padding_8114,\n                              int32_t res_8217, int32_t res_8222, __global\n                              unsigned char *mem_11647, __global\n                              unsigned char *b_mem_11679, __global\n                              unsigned char *mem_11730)\n{\n    int32_t wave_sizze_12190;\n    int32_t group_sizze_12191;\n    char thread_active_12192;\n    int32_t gtid_9853;\n    int32_t global_tid_9860;\n    int32_t local_tid_9861;\n    int32_t group_id_9862;\n    \n    global_tid_9860 = get_global_id(0);\n    local_tid_9861 = get_local_id(0);\n    group_sizze_12191 = get_local_size(0);\n    wave_sizze_12190 = LOCKSTEP_WIDTH;\n    group_id_9862 = get_group_id(0);\n    gtid_9853 = global_tid_9860;\n    thread_active_12192 = slt32(gtid_9853, padding_8114);\n    \n    int32_t i_p_o_9864;\n    int32_t rot_i_9865;\n    int32_t x_9866;\n    int32_t res_9867;\n    int32_t x_9868;\n    int32_t x_9869;\n    int32_t res_9870;\n    \n    if (thread_active_12192) ",
                   "{\n        i_p_o_9864 = gtid_9853 + res_8217;\n        rot_i_9865 = smod32(i_p_o_9864, padding_8114);\n        x_9866 = *(__global int32_t *) &mem_11647[rot_i_9865 * 4];\n        res_9867 = x_9866 * res_8222;\n        x_9868 = *(__global int32_t *) &b_mem_11679[gtid_9853 * 4];\n        x_9869 = x_9868 - res_9867;\n        res_9870 = smod32(x_9869, prime_8113);\n    }\n    if (thread_active_12192) {\n        *(__global int32_t *) &mem_11730[gtid_9853 * 4] = res_9870;\n    }\n}\n__kernel void map_kernel_9980(int32_t padding_8240, int32_t y_9924, __global\n                              unsigned char *mem_11618, __global\n                              unsigned char *mem_11636, __global\n                              unsigned char *mem_11639)\n{\n    int32_t wave_sizze_12218;\n    int32_t group_sizze_12219;\n    char thread_active_12220;\n    int32_t j_9965;\n    int32_t global_tid_9980;\n    int32_t local_tid_9981;\n    int32_t group_id_9982;\n    \n    global_tid_9980 = get_global_id(0);\n    local_tid_9981 = get_local_id(0);\n    group_sizze_12219 = get_local_size(0);\n    wave_sizze_12218 = LOCKSTEP_WIDTH;\n    group_id_9982 = get_group_id(0);\n    j_9965 = global_tid_9980;\n    thread_active_12220 = slt32(j_9965, padding_8240);\n    \n    int32_t binop_param_y_9963;\n    int32_t group_id_9970;\n    char cond_9971;\n    int32_t final_result_9973;\n    \n    if (thread_active_12220) {\n        binop_param_y_9963 = *(__global int32_t *) &mem_11618[j_9965 * 4];\n        group_id_9970 = squot32(j_9965, y_9924);\n        cond_9971 = 0 == group_id_9970;\n        if (cond_9971) {\n            final_result_9973 = binop_param_y_9963;\n        } else {\n            int32_t carry_in_index_9972 = group_id_9970 - 1;\n            int32_t binop_param_x_9962 = *(__global\n                                           int32_t *) &mem_11636[carry_in_index_9972 *\n                                                                 4];\n            int32_t res_9964 = binop_param_x_9962 + binop_param_y_9963;\n            \n            final_res",
                   "ult_9973 = res_9964;\n        }\n    }\n    if (thread_active_12220) {\n        *(__global int32_t *) &mem_11639[j_9965 * 4] = final_result_9973;\n    }\n}\n__kernel void reduce_kernel_10365(__local volatile int64_t *mem_aligned_0,\n                                  int32_t num_groups_9877, __global\n                                  unsigned char *mem_11725, __global\n                                  unsigned char *mem_11731)\n{\n    __local volatile char *restrict mem_11728 = mem_aligned_0;\n    int32_t wave_sizze_12313;\n    int32_t group_sizze_12314;\n    char thread_active_12315;\n    int32_t global_tid_10365;\n    int32_t local_tid_10366;\n    int32_t group_id_10367;\n    \n    global_tid_10365 = get_global_id(0);\n    local_tid_10366 = get_local_id(0);\n    group_sizze_12314 = get_local_size(0);\n    wave_sizze_12313 = LOCKSTEP_WIDTH;\n    group_id_10367 = get_group_id(0);\n    thread_active_12315 = 1;\n    \n    char in_bounds_10368;\n    \n    if (thread_active_12315) {\n        in_bounds_10368 = slt32(local_tid_10366, num_groups_9877);\n    }\n    \n    int32_t final_result_10372;\n    \n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10366, group_sizze_9871) && 1) {\n        int32_t elem_10370;\n        \n        if (in_bounds_10368) {\n            int32_t x_10369 = *(__global\n                                int32_t *) &mem_11725[global_tid_10365 * 4];\n            \n            elem_10370 = x_10369;\n        } else {\n            elem_10370 = 0;\n        }\n        *(__local int32_t *) &mem_11728[local_tid_10366 * 4] = elem_10370;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t skip_waves_12316;\n    int32_t binop_param_x_10313;\n    int32_t binop_param_y_10314;\n    int32_t my_index_10336;\n    int32_t other_offset_10337;\n    \n    my_index_10336 = local_tid_10366;\n    other_offset_10337 = 0;\n    binop_param_x_10313 = *(__local int32_t *) &mem_11728[(local_tid_10366 +\n                                                           other_offset_10337) *\n                                   ",
                   "                       4];\n    other_offset_10337 = 1;\n    while (slt32(other_offset_10337, wave_sizze_12313)) {\n        if (slt32(local_tid_10366 + other_offset_10337, group_sizze_9871) &&\n            ((local_tid_10366 - squot32(local_tid_10366, wave_sizze_12313) *\n              wave_sizze_12313) & (2 * other_offset_10337 - 1)) == 0) {\n            // read array element\n            {\n                binop_param_y_10314 = *(volatile __local\n                                        int32_t *) &mem_11728[(local_tid_10366 +\n                                                               other_offset_10337) *\n                                                              4];\n            }\n            \n            int32_t res_10315;\n            \n            if (thread_active_12315) {\n                res_10315 = binop_param_x_10313 + binop_param_y_10314;\n            }\n            binop_param_x_10313 = res_10315;\n            *(volatile __local int32_t *) &mem_11728[local_tid_10366 * 4] =\n                binop_param_x_10313;\n        }\n        other_offset_10337 *= 2;\n    }\n    skip_waves_12316 = 1;\n    while (slt32(skip_waves_12316, squot32(group_sizze_12314 +\n                                           wave_sizze_12313 - 1,\n                                           wave_sizze_12313))) {\n        barrier(CLK_LOCAL_MEM_FENCE);\n        other_offset_10337 = skip_waves_12316 * wave_sizze_12313;\n        if ((local_tid_10366 - squot32(local_tid_10366, wave_sizze_12313) *\n             wave_sizze_12313) == 0 && (squot32(local_tid_10366,\n                                                wave_sizze_12313) & (2 *\n                                                                     skip_waves_12316 -\n                                                                     1)) == 0) {\n            // read array element\n            {\n                binop_param_y_10314 = *(__local\n                                        int32_t *) &mem_11728[(local_tid_10366 +\n                                       ",
                   "                        other_offset_10337) *\n                                                              4];\n            }\n            \n            int32_t res_10315;\n            \n            if (thread_active_12315) {\n                res_10315 = binop_param_x_10313 + binop_param_y_10314;\n            }\n            binop_param_x_10313 = res_10315;\n            *(__local int32_t *) &mem_11728[local_tid_10366 * 4] =\n                binop_param_x_10313;\n        }\n        skip_waves_12316 *= 2;\n    }\n    final_result_10372 = binop_param_x_10313;\n    if (local_tid_10366 == 0) {\n        *(__global int32_t *) &mem_11731[group_id_10367 * 4] =\n            final_result_10372;\n    }\n}\n__kernel void reduce_kernel_9169(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_9107, __global\n                                 unsigned char *mem_11619, __global\n                                 unsigned char *mem_11625)\n{\n    __local volatile char *restrict mem_11622 = mem_aligned_0;\n    int32_t wave_sizze_12007;\n    int32_t group_sizze_12008;\n    char thread_active_12009;\n    int32_t global_tid_9169;\n    int32_t local_tid_9170;\n    int32_t group_id_9171;\n    \n    global_tid_9169 = get_global_id(0);\n    local_tid_9170 = get_local_id(0);\n    group_sizze_12008 = get_local_size(0);\n    wave_sizze_12007 = LOCKSTEP_WIDTH;\n    group_id_9171 = get_group_id(0);\n    thread_active_12009 = 1;\n    \n    char in_bounds_9172;\n    \n    if (thread_active_12009) {\n        in_bounds_9172 = slt32(local_tid_9170, num_groups_9107);\n    }\n    \n    int32_t final_result_9176;\n    \n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9170, group_sizze_9101) && 1) {\n        int32_t elem_9174;\n        \n        if (in_bounds_9172) {\n            int32_t x_9173 = *(__global int32_t *) &mem_11619[global_tid_9169 *\n                                                              4];\n            \n            elem_9174 = x_9173;\n        } else {\n            elem_9174 = 1;\n        ",
                   "}\n        *(__local int32_t *) &mem_11622[local_tid_9170 * 4] = elem_9174;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t skip_waves_12010;\n    int32_t x_7978;\n    int32_t y_7979;\n    int32_t my_index_9114;\n    int32_t other_offset_9115;\n    \n    my_index_9114 = local_tid_9170;\n    other_offset_9115 = 0;\n    x_7978 = *(__local int32_t *) &mem_11622[(local_tid_9170 +\n                                              other_offset_9115) * 4];\n    other_offset_9115 = 1;\n    while (slt32(other_offset_9115, wave_sizze_12007)) {\n        if (slt32(local_tid_9170 + other_offset_9115, group_sizze_9101) &&\n            ((local_tid_9170 - squot32(local_tid_9170, wave_sizze_12007) *\n              wave_sizze_12007) & (2 * other_offset_9115 - 1)) == 0) {\n            // read array element\n            {\n                y_7979 = *(volatile __local\n                           int32_t *) &mem_11622[(local_tid_9170 +\n                                                  other_offset_9115) * 4];\n            }\n            \n            char cond_7980;\n            int32_t res_7981;\n            \n            if (thread_active_12009) {\n                cond_7980 = y_7979 == 0;\n                if (cond_7980) {\n                    res_7981 = 0;\n                } else {\n                    res_7981 = x_7978;\n                }\n            }\n            x_7978 = res_7981;\n            *(volatile __local int32_t *) &mem_11622[local_tid_9170 * 4] =\n                x_7978;\n        }\n        other_offset_9115 *= 2;\n    }\n    skip_waves_12010 = 1;\n    while (slt32(skip_waves_12010, squot32(group_sizze_12008 +\n                                           wave_sizze_12007 - 1,\n                                           wave_sizze_12007))) {\n        barrier(CLK_LOCAL_MEM_FENCE);\n        other_offset_9115 = skip_waves_12010 * wave_sizze_12007;\n        if ((local_tid_9170 - squot32(local_tid_9170, wave_sizze_12007) *\n             wave_sizze_12007) == 0 && (squot32(local_tid_9170,\n                                ",
                   "                wave_sizze_12007) & (2 *\n                                                                     skip_waves_12010 -\n                                                                     1)) == 0) {\n            // read array element\n            {\n                y_7979 = *(__local int32_t *) &mem_11622[(local_tid_9170 +\n                                                          other_offset_9115) *\n                                                         4];\n            }\n            \n            char cond_7980;\n            int32_t res_7981;\n            \n            if (thread_active_12009) {\n                cond_7980 = y_7979 == 0;\n                if (cond_7980) {\n                    res_7981 = 0;\n                } else {\n                    res_7981 = x_7978;\n                }\n            }\n            x_7978 = res_7981;\n            *(__local int32_t *) &mem_11622[local_tid_9170 * 4] = x_7978;\n        }\n        skip_waves_12010 *= 2;\n    }\n    final_result_9176 = x_7978;\n    if (local_tid_9170 == 0) {\n        *(__global int32_t *) &mem_11625[group_id_9171 * 4] = final_result_9176;\n    }\n}\n__kernel void reduce_kernel_9845(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_9357, __global\n                                 unsigned char *mem_11721, __global\n                                 unsigned char *mem_11727)\n{\n    __local volatile char *restrict mem_11724 = mem_aligned_0;\n    int32_t wave_sizze_12186;\n    int32_t group_sizze_12187;\n    char thread_active_12188;\n    int32_t global_tid_9845;\n    int32_t local_tid_9846;\n    int32_t group_id_9847;\n    \n    global_tid_9845 = get_global_id(0);\n    local_tid_9846 = get_local_id(0);\n    group_sizze_12187 = get_local_size(0);\n    wave_sizze_12186 = LOCKSTEP_WIDTH;\n    group_id_9847 = get_group_id(0);\n    thread_active_12188 = 1;\n    \n    char in_bounds_9848;\n    \n    if (thread_active_12188) {\n        in_bounds_9848 = slt32(local_tid_9846, num_groups_9357);\n    }\n    \n ",
                   "   int32_t final_result_9852;\n    \n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9846, group_sizze_9351) && 1) {\n        int32_t elem_9850;\n        \n        if (in_bounds_9848) {\n            int32_t x_9849 = *(__global int32_t *) &mem_11721[global_tid_9845 *\n                                                              4];\n            \n            elem_9850 = x_9849;\n        } else {\n            elem_9850 = 0;\n        }\n        *(__local int32_t *) &mem_11724[local_tid_9846 * 4] = elem_9850;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t skip_waves_12189;\n    int32_t binop_param_x_9793;\n    int32_t binop_param_y_9794;\n    int32_t my_index_9816;\n    int32_t other_offset_9817;\n    \n    my_index_9816 = local_tid_9846;\n    other_offset_9817 = 0;\n    binop_param_x_9793 = *(__local int32_t *) &mem_11724[(local_tid_9846 +\n                                                          other_offset_9817) *\n                                                         4];\n    other_offset_9817 = 1;\n    while (slt32(other_offset_9817, wave_sizze_12186)) {\n        if (slt32(local_tid_9846 + other_offset_9817, group_sizze_9351) &&\n            ((local_tid_9846 - squot32(local_tid_9846, wave_sizze_12186) *\n              wave_sizze_12186) & (2 * other_offset_9817 - 1)) == 0) {\n            // read array element\n            {\n                binop_param_y_9794 = *(volatile __local\n                                       int32_t *) &mem_11724[(local_tid_9846 +\n                                                              other_offset_9817) *\n                                                             4];\n            }\n            \n            int32_t res_9795;\n            \n            if (thread_active_12188) {\n                res_9795 = binop_param_x_9793 + binop_param_y_9794;\n            }\n            binop_param_x_9793 = res_9795;\n            *(volatile __local int32_t *) &mem_11724[local_tid_9846 * 4] =\n                binop_param_x_9793;\n        }\n        other_offset",
                   "_9817 *= 2;\n    }\n    skip_waves_12189 = 1;\n    while (slt32(skip_waves_12189, squot32(group_sizze_12187 +\n                                           wave_sizze_12186 - 1,\n                                           wave_sizze_12186))) {\n        barrier(CLK_LOCAL_MEM_FENCE);\n        other_offset_9817 = skip_waves_12189 * wave_sizze_12186;\n        if ((local_tid_9846 - squot32(local_tid_9846, wave_sizze_12186) *\n             wave_sizze_12186) == 0 && (squot32(local_tid_9846,\n                                                wave_sizze_12186) & (2 *\n                                                                     skip_waves_12189 -\n                                                                     1)) == 0) {\n            // read array element\n            {\n                binop_param_y_9794 = *(__local\n                                       int32_t *) &mem_11724[(local_tid_9846 +\n                                                              other_offset_9817) *\n                                                             4];\n            }\n            \n            int32_t res_9795;\n            \n            if (thread_active_12188) {\n                res_9795 = binop_param_x_9793 + binop_param_y_9794;\n            }\n            binop_param_x_9793 = res_9795;\n            *(__local int32_t *) &mem_11724[local_tid_9846 * 4] =\n                binop_param_x_9793;\n        }\n        skip_waves_12189 *= 2;\n    }\n    final_result_9852 = binop_param_x_9793;\n    if (local_tid_9846 == 0) {\n        *(__global int32_t *) &mem_11727[group_id_9847 * 4] = final_result_9852;\n    }\n}\n__kernel void scan1_kernel_10040(__local volatile int64_t *mem_aligned_0,\n                                 __local volatile int64_t *mem_aligned_1,\n                                 int32_t padding_8240, int32_t arg_8246,\n                                 int32_t arg_8249, int32_t arg_8255,\n                                 int32_t arg_8258, int32_t y_8265,\n                                 int32_t y_8267, int32",
                   "_t num_iterations_9921,\n                                 int32_t y_9924, __global\n                                 unsigned char *mem_11639, __global\n                                 unsigned char *mem_11642, __global\n                                 unsigned char *mem_11645, __global\n                                 unsigned char *mem_11648, __global\n                                 unsigned char *mem_11651, __global\n                                 unsigned char *mem_11660, __global\n                                 unsigned char *mem_11663)\n{\n    __local volatile char *restrict mem_11654 = mem_aligned_0;\n    __local volatile char *restrict mem_11657 = mem_aligned_1;\n    int32_t wave_sizze_12221;\n    int32_t group_sizze_12222;\n    char thread_active_12223;\n    int32_t global_tid_10040;\n    int32_t local_tid_10041;\n    int32_t group_id_10042;\n    \n    global_tid_10040 = get_global_id(0);\n    local_tid_10041 = get_local_id(0);\n    group_sizze_12222 = get_local_size(0);\n    wave_sizze_12221 = LOCKSTEP_WIDTH;\n    group_id_10042 = get_group_id(0);\n    thread_active_12223 = 1;\n    \n    int32_t x_10050;\n    char is_first_thread_10074;\n    int32_t result_10083;\n    int32_t result_10084;\n    \n    if (thread_active_12223) {\n        x_10050 = group_id_10042 * y_9924;\n        is_first_thread_10074 = local_tid_10041 == 0;\n        \n        int32_t binop_param_x_merge_10046;\n        int32_t x_merge_10047;\n        \n        binop_param_x_merge_10046 = 0;\n        x_merge_10047 = 0;\n        for (int32_t i_10048 = 0; i_10048 < num_iterations_9921; i_10048++) {\n            int32_t y_10051 = i_10048 * group_sizze_9871;\n            int32_t offset_10052 = x_10050 + y_10051;\n            int32_t j_10053 = offset_10052 + local_tid_10041;\n            char cond_10054 = slt32(j_10053, padding_8240);\n            int32_t foldres_10059;\n            int32_t foldres_10060;\n            \n            if (cond_10054) {\n                int32_t res_elem_10056 = *(__global\n                                 ",
                   "          int32_t *) &mem_11639[j_10053 * 4];\n                char cond_10001 = slt32(j_10053, y_8265);\n                int32_t res_10002;\n                \n                if (cond_10001) {\n                    res_10002 = 0;\n                } else {\n                    int32_t x_10003 = padding_8240 - j_10053;\n                    int32_t y_10004 = x_10003 - 1;\n                    int32_t y_10005 = pow32(arg_8255, y_10004);\n                    int32_t x_10006 = sdiv32(arg_8258, y_10005);\n                    int32_t res_10007 = smod32(x_10006, arg_8255);\n                    \n                    res_10002 = res_10007;\n                }\n                \n                char cond_10008 = slt32(j_10053, y_8267);\n                int32_t res_10009;\n                \n                if (cond_10008) {\n                    res_10009 = 0;\n                } else {\n                    int32_t x_10010 = padding_8240 - j_10053;\n                    int32_t y_10011 = x_10010 - 1;\n                    int32_t y_10012 = pow32(arg_8246, y_10011);\n                    int32_t x_10013 = sdiv32(arg_8249, y_10012);\n                    int32_t res_10014 = smod32(x_10013, arg_8246);\n                    \n                    res_10009 = res_10014;\n                }\n                \n                int32_t res_10015 = binop_param_x_merge_10046 + res_10002;\n                char res_10016 = res_elem_10056 == 0;\n                int32_t partition_incr_10017;\n                \n                if (res_10016) {\n                    partition_incr_10017 = 1;\n                } else {\n                    partition_incr_10017 = 0;\n                }\n                \n                int32_t zz_10018 = x_merge_10047 + partition_incr_10017;\n                \n                *(__global int32_t *) &mem_11648[j_10053 * 4] = res_10009;\n                *(__global int32_t *) &mem_11651[j_10053 * 4] = res_10002;\n                foldres_10059 = res_10015;\n                foldres_10060 = zz_10018;\n            } else {\n       ",
                   "         foldres_10059 = binop_param_x_merge_10046;\n                foldres_10060 = x_merge_10047;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10041, group_sizze_9871) && 1) {\n                *(__local int32_t *) &mem_11654[local_tid_10041 * 4] =\n                    foldres_10059;\n                *(__local int32_t *) &mem_11657[local_tid_10041 * 4] =\n                    foldres_10060;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10019;\n            int32_t other_index_10020;\n            int32_t binop_param_x_10021;\n            int32_t x_10022;\n            int32_t binop_param_y_10023;\n            int32_t y_10024;\n            int32_t my_index_12230;\n            int32_t other_index_12231;\n            int32_t binop_param_x_12232;\n            int32_t x_12233;\n            int32_t binop_param_y_12234;\n            int32_t y_12235;\n            \n            my_index_10019 = local_tid_10041;\n            if (slt32(local_tid_10041, group_sizze_9871)) {\n                binop_param_y_10023 = *(volatile __local\n                                        int32_t *) &mem_11654[local_tid_10041 *\n                                                              sizeof(int32_t)];\n                y_10024 = *(volatile __local\n                            int32_t *) &mem_11657[local_tid_10041 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12238 = 1;\n                \n                while (slt32(skip_threads_12238, 32)) {\n                    if (slt32(local_tid_10041, group_sizze_9871) &&\n                        sle32(skip_threads_12238, local_tid_10041 -\n                              squot32(local_tid_10041, 32) * 32)) {\n                        // read operands\n                        {\n                            binop_param_x_10021 = *(volatile __local\n   ",
                   "                                                 int32_t *) &mem_11654[(local_tid_10041 -\n                                                                           skip_threads_12238) *\n                                                                          sizeof(int32_t)];\n                            x_10022 = *(volatile __local\n                                        int32_t *) &mem_11657[(local_tid_10041 -\n                                                               skip_threads_12238) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t res_10025 = binop_param_x_10021 +\n                                    binop_param_y_10023;\n                            int32_t zz_10026 = x_10022 + y_10024;\n                            \n                            binop_param_y_10023 = res_10025;\n                            y_10024 = zz_10026;\n                        }\n                    }\n                    if (sle32(wave_sizze_12221, skip_threads_12238)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10041, group_sizze_9871) &&\n                        sle32(skip_threads_12238, local_tid_10041 -\n                              squot32(local_tid_10041, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11654[local_tid_10041 *\n                                                    sizeof(int32_t)] =\n                                binop_param_y_10023;\n                            *(volatile __local\n                              int32_t *) &mem_11657[local_tid_10041 *\n                                                    sizeof(int32_t)] = y_10024;\n                        }\n                    }\n                    if (sle32(wave_sizz",
                   "e_12221, skip_threads_12238)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12238 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10041 - squot32(local_tid_10041, 32) * 32) ==\n                    31 && slt32(local_tid_10041, group_sizze_9871)) {\n                    *(volatile __local\n                      int32_t *) &mem_11654[squot32(local_tid_10041, 32) *\n                                            sizeof(int32_t)] =\n                        binop_param_y_10023;\n                    *(volatile __local\n                      int32_t *) &mem_11657[squot32(local_tid_10041, 32) *\n                                            sizeof(int32_t)] = y_10024;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10041, 32) == 0 && slt32(local_tid_10041,\n                                                               group_sizze_9871)) {\n                    binop_param_y_12234 = *(volatile __local\n                                            int32_t *) &mem_11654[local_tid_10041 *\n                                                                  sizeof(int32_t)];\n                    y_12235 = *(volatile __local\n                                int32_t *) &mem_11657[local_tid_10041 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12239 = 1;\n                    \n                    while (slt32(skip_threads_12239, 32)) {\n                        if ((squot32(local_tid_10041, 32) == 0 &&\n                             slt32(local_tid_10041, group_sizze_98",
                   "71)) &&\n                            sle32(skip_threads_12239, local_tid_10041 -\n                                  squot32(local_tid_10041, 32) * 32)) {\n                            // read operands\n                            {\n                                binop_param_x_12232 = *(volatile __local\n                                                        int32_t *) &mem_11654[(local_tid_10041 -\n                                                                               skip_threads_12239) *\n                                                                              sizeof(int32_t)];\n                                x_12233 = *(volatile __local\n                                            int32_t *) &mem_11657[(local_tid_10041 -\n                                                                   skip_threads_12239) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t res_12236 = binop_param_x_12232 +\n                                        binop_param_y_12234;\n                                int32_t zz_12237 = x_12233 + y_12235;\n                                \n                                binop_param_y_12234 = res_12236;\n                                y_12235 = zz_12237;\n                            }\n                        }\n                        if (sle32(wave_sizze_12221, skip_threads_12239)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10041, 32) == 0 &&\n                             slt32(local_tid_10041, group_sizze_9871)) &&\n                            sle32(skip_threads_12239, local_tid_10041 -\n                                  squot32(local_tid_10041, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local",
                   "\n                                  int32_t *) &mem_11654[local_tid_10041 *\n                                                        sizeof(int32_t)] =\n                                    binop_param_y_12234;\n                                *(volatile __local\n                                  int32_t *) &mem_11657[local_tid_10041 *\n                                                        sizeof(int32_t)] =\n                                    y_12235;\n                            }\n                        }\n                        if (sle32(wave_sizze_12221, skip_threads_12239)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12239 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10041, 32) == 0 ||\n                      !slt32(local_tid_10041, group_sizze_9871))) {\n                    // read operands\n                    {\n                        binop_param_x_10021 = *(volatile __local\n                                                int32_t *) &mem_11654[(squot32(local_tid_10041,\n                                                                               32) -\n                                                                       1) *\n                                                                      sizeof(int32_t)];\n                        x_10022 = *(volatile __local\n                                    int32_t *) &mem_11657[(squot32(local_tid_10041,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_10025 = binop_param_x_10021 +\n                                binop_param_y_10023;\n                        in",
                   "t32_t zz_10026 = x_10022 + y_10024;\n                        \n                        binop_param_y_10023 = res_10025;\n                        y_10024 = zz_10026;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11654[local_tid_10041 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_10023;\n                        *(volatile __local\n                          int32_t *) &mem_11657[local_tid_10041 *\n                                                sizeof(int32_t)] = y_10024;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10041, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11654[local_tid_10041 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_10023;\n                    *(volatile __local int32_t *) &mem_11657[local_tid_10041 *\n                                                             sizeof(int32_t)] =\n                        y_10024;\n                }\n            }\n            if (cond_10054) {\n                int32_t scanned_elem_10068 = *(__local\n                                               int32_t *) &mem_11654[local_tid_10041 *\n                                                                     4];\n                int32_t scanned_elem_10069 = *(__local\n                                               int32_t *) &mem_11657[local_tid_10041 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11642[j_10053 * 4] =\n                    scanned_elem_10068;\n                *(__global int32_t *) &mem_11645[j_10053 * 4] =\n                    scanned_elem_10069;\n        ",
                   "    }\n            \n            int32_t new_carry_10077;\n            int32_t new_carry_10078;\n            \n            if (is_first_thread_10074) {\n                int32_t carry_10075 = *(__local int32_t *) &mem_11654[y_9873 *\n                                                                      4];\n                int32_t carry_10076 = *(__local int32_t *) &mem_11657[y_9873 *\n                                                                      4];\n                \n                new_carry_10077 = carry_10075;\n                new_carry_10078 = carry_10076;\n            } else {\n                new_carry_10077 = 0;\n                new_carry_10078 = 0;\n            }\n            \n            int32_t binop_param_x_merge_tmp_12228 = new_carry_10077;\n            int32_t x_merge_tmp_12229;\n            \n            x_merge_tmp_12229 = new_carry_10078;\n            binop_param_x_merge_10046 = binop_param_x_merge_tmp_12228;\n            x_merge_10047 = x_merge_tmp_12229;\n        }\n        result_10083 = binop_param_x_merge_10046;\n        result_10084 = x_merge_10047;\n    }\n    if (local_tid_10041 == 0) {\n        *(__global int32_t *) &mem_11660[group_id_10042 * 4] = result_10083;\n    }\n    if (local_tid_10041 == 0) {\n        *(__global int32_t *) &mem_11663[group_id_10042 * 4] = result_10084;\n    }\n}\n__kernel void scan1_kernel_10162(__local volatile int64_t *mem_aligned_0,\n                                 int32_t padding_8240,\n                                 int32_t num_iterations_9921, int32_t y_9924,\n                                 __global unsigned char *b_mem_11683, __global\n                                 unsigned char *mem_11686, __global\n                                 unsigned char *mem_11692)\n{\n    __local volatile char *restrict mem_11689 = mem_aligned_0;\n    int32_t wave_sizze_12259;\n    int32_t group_sizze_12260;\n    char thread_active_12261;\n    int32_t global_tid_10162;\n    int32_t local_tid_10163;\n    int32_t group_id_10164;\n    \n    global_tid_10162 = get_gl",
                   "obal_id(0);\n    local_tid_10163 = get_local_id(0);\n    group_sizze_12260 = get_local_size(0);\n    wave_sizze_12259 = LOCKSTEP_WIDTH;\n    group_id_10164 = get_group_id(0);\n    thread_active_12261 = 1;\n    \n    int32_t x_10171;\n    char is_first_thread_10184;\n    int32_t result_10188;\n    \n    if (thread_active_12261) {\n        x_10171 = group_id_10164 * y_9924;\n        is_first_thread_10184 = local_tid_10163 == 0;\n        \n        int32_t binop_param_x_merge_10168 = 0;\n        \n        for (int32_t i_10169 = 0; i_10169 < num_iterations_9921; i_10169++) {\n            int32_t y_10172 = i_10169 * group_sizze_9871;\n            int32_t offset_10173 = x_10171 + y_10172;\n            int32_t j_10174 = offset_10173 + local_tid_10163;\n            char cond_10175 = slt32(j_10174, padding_8240);\n            int32_t foldres_10177;\n            \n            if (cond_10175) {\n                int32_t b_elem_10176 = *(__global\n                                         int32_t *) &b_mem_11683[j_10174 * 4];\n                int32_t res_10151 = binop_param_x_merge_10168 + b_elem_10176;\n                \n                foldres_10177 = res_10151;\n            } else {\n                foldres_10177 = binop_param_x_merge_10168;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10163, group_sizze_9871) && 1) {\n                *(__local int32_t *) &mem_11689[local_tid_10163 * 4] =\n                    foldres_10177;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10152;\n            int32_t other_index_10153;\n            int32_t binop_param_x_10154;\n            int32_t binop_param_y_10155;\n            int32_t my_index_12264;\n            int32_t other_index_12265;\n            int32_t binop_param_x_12266;\n            int32_t binop_param_y_12267;\n            \n            my_index_10152 = local_tid_10163;\n            if (slt32(local_tid_10163, group_sizze_9871)) {\n                binop_param_y_10155 = *(volatile __l",
                   "ocal\n                                        int32_t *) &mem_11689[local_tid_10163 *\n                                                              sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12269 = 1;\n                \n                while (slt32(skip_threads_12269, 32)) {\n                    if (slt32(local_tid_10163, group_sizze_9871) &&\n                        sle32(skip_threads_12269, local_tid_10163 -\n                              squot32(local_tid_10163, 32) * 32)) {\n                        // read operands\n                        {\n                            binop_param_x_10154 = *(volatile __local\n                                                    int32_t *) &mem_11689[(local_tid_10163 -\n                                                                           skip_threads_12269) *\n                                                                          sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t res_10156 = binop_param_x_10154 +\n                                    binop_param_y_10155;\n                            \n                            binop_param_y_10155 = res_10156;\n                        }\n                    }\n                    if (sle32(wave_sizze_12259, skip_threads_12269)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10163, group_sizze_9871) &&\n                        sle32(skip_threads_12269, local_tid_10163 -\n                              squot32(local_tid_10163, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11689[local_tid_10163 *\n                                                    sizeof(int32_t)] =\n                                binop",
                   "_param_y_10155;\n                        }\n                    }\n                    if (sle32(wave_sizze_12259, skip_threads_12269)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12269 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10163 - squot32(local_tid_10163, 32) * 32) ==\n                    31 && slt32(local_tid_10163, group_sizze_9871)) {\n                    *(volatile __local\n                      int32_t *) &mem_11689[squot32(local_tid_10163, 32) *\n                                            sizeof(int32_t)] =\n                        binop_param_y_10155;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10163, 32) == 0 && slt32(local_tid_10163,\n                                                               group_sizze_9871)) {\n                    binop_param_y_12267 = *(volatile __local\n                                            int32_t *) &mem_11689[local_tid_10163 *\n                                                                  sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12270 = 1;\n                    \n                    while (slt32(skip_threads_12270, 32)) {\n                        if ((squot32(local_tid_10163, 32) == 0 &&\n                             slt32(local_tid_10163, group_sizze_9871)) &&\n                            sle32(skip_threads_12270, local_tid_10163 -\n                                  squot32(local_tid_10163, 32) * 32)) {\n                            // read operands\n                            {\n                                binop_param_x_122",
                   "66 = *(volatile __local\n                                                        int32_t *) &mem_11689[(local_tid_10163 -\n                                                                               skip_threads_12270) *\n                                                                              sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t res_12268 = binop_param_x_12266 +\n                                        binop_param_y_12267;\n                                \n                                binop_param_y_12267 = res_12268;\n                            }\n                        }\n                        if (sle32(wave_sizze_12259, skip_threads_12270)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10163, 32) == 0 &&\n                             slt32(local_tid_10163, group_sizze_9871)) &&\n                            sle32(skip_threads_12270, local_tid_10163 -\n                                  squot32(local_tid_10163, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11689[local_tid_10163 *\n                                                        sizeof(int32_t)] =\n                                    binop_param_y_12267;\n                            }\n                        }\n                        if (sle32(wave_sizze_12259, skip_threads_12270)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12270 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10163, 32) == 0 ||\n                    ",
                   "  !slt32(local_tid_10163, group_sizze_9871))) {\n                    // read operands\n                    {\n                        binop_param_x_10154 = *(volatile __local\n                                                int32_t *) &mem_11689[(squot32(local_tid_10163,\n                                                                               32) -\n                                                                       1) *\n                                                                      sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_10156 = binop_param_x_10154 +\n                                binop_param_y_10155;\n                        \n                        binop_param_y_10155 = res_10156;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11689[local_tid_10163 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_10155;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10163, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11689[local_tid_10163 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_10155;\n                }\n            }\n            if (cond_10175) {\n                int32_t scanned_elem_10181 = *(__local\n                                               int32_t *) &mem_11689[local_tid_10163 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11686[j_10174 * 4] =\n                    scanned_elem_10181;\n            }\n            \n            int32_t new_carry_10186;",
                   "\n            \n            if (is_first_thread_10184) {\n                int32_t carry_10185 = *(__local int32_t *) &mem_11689[y_9873 *\n                                                                      4];\n                \n                new_carry_10186 = carry_10185;\n            } else {\n                new_carry_10186 = 0;\n            }\n            \n            int32_t binop_param_x_merge_tmp_12263 = new_carry_10186;\n            \n            binop_param_x_merge_10168 = binop_param_x_merge_tmp_12263;\n        }\n        result_10188 = binop_param_x_merge_10168;\n    }\n    if (local_tid_10163 == 0) {\n        *(__global int32_t *) &mem_11692[group_id_10164 * 4] = result_10188;\n    }\n}\n__kernel void scan1_kernel_10252(__local volatile int64_t *mem_aligned_0,\n                                 int32_t padding_8240,\n                                 int32_t num_iterations_9921, int32_t y_9924,\n                                 __global unsigned char *mem_11701, __global\n                                 unsigned char *mem_11704, __global\n                                 unsigned char *mem_11710)\n{\n    __local volatile char *restrict mem_11707 = mem_aligned_0;\n    int32_t wave_sizze_12284;\n    int32_t group_sizze_12285;\n    char thread_active_12286;\n    int32_t global_tid_10252;\n    int32_t local_tid_10253;\n    int32_t group_id_10254;\n    \n    global_tid_10252 = get_global_id(0);\n    local_tid_10253 = get_local_id(0);\n    group_sizze_12285 = get_local_size(0);\n    wave_sizze_12284 = LOCKSTEP_WIDTH;\n    group_id_10254 = get_group_id(0);\n    thread_active_12286 = 1;\n    \n    int32_t x_10261;\n    char is_first_thread_10274;\n    int32_t result_10278;\n    \n    if (thread_active_12286) {\n        x_10261 = group_id_10254 * y_9924;\n        is_first_thread_10274 = local_tid_10253 == 0;\n        \n        int32_t x_merge_10258 = 0;\n        \n        for (int32_t i_10259 = 0; i_10259 < num_iterations_9921; i_10259++) {\n            int32_t y_10262 = i_10259 * group_sizze_9871;\n            i",
                   "nt32_t offset_10263 = x_10261 + y_10262;\n            int32_t j_10264 = offset_10263 + local_tid_10253;\n            char cond_10265 = slt32(j_10264, padding_8240);\n            int32_t foldres_10267;\n            \n            if (cond_10265) {\n                int32_t res_elem_10266 = *(__global\n                                           int32_t *) &mem_11701[j_10264 * 4];\n                char res_10239 = res_elem_10266 == 0;\n                int32_t partition_incr_10240;\n                \n                if (res_10239) {\n                    partition_incr_10240 = 1;\n                } else {\n                    partition_incr_10240 = 0;\n                }\n                \n                int32_t zz_10241 = x_merge_10258 + partition_incr_10240;\n                \n                foldres_10267 = zz_10241;\n            } else {\n                foldres_10267 = x_merge_10258;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10253, group_sizze_9871) && 1) {\n                *(__local int32_t *) &mem_11707[local_tid_10253 * 4] =\n                    foldres_10267;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10242;\n            int32_t other_index_10243;\n            int32_t x_10244;\n            int32_t y_10245;\n            int32_t my_index_12289;\n            int32_t other_index_12290;\n            int32_t x_12291;\n            int32_t y_12292;\n            \n            my_index_10242 = local_tid_10253;\n            if (slt32(local_tid_10253, group_sizze_9871)) {\n                y_10245 = *(volatile __local\n                            int32_t *) &mem_11707[local_tid_10253 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12294 = 1;\n                \n                while (slt32(skip_threads_12294, 32)) {\n                    if (slt32(local_tid_10253, group_si",
                   "zze_9871) &&\n                        sle32(skip_threads_12294, local_tid_10253 -\n                              squot32(local_tid_10253, 32) * 32)) {\n                        // read operands\n                        {\n                            x_10244 = *(volatile __local\n                                        int32_t *) &mem_11707[(local_tid_10253 -\n                                                               skip_threads_12294) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_10246 = x_10244 + y_10245;\n                            \n                            y_10245 = zz_10246;\n                        }\n                    }\n                    if (sle32(wave_sizze_12284, skip_threads_12294)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10253, group_sizze_9871) &&\n                        sle32(skip_threads_12294, local_tid_10253 -\n                              squot32(local_tid_10253, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11707[local_tid_10253 *\n                                                    sizeof(int32_t)] = y_10245;\n                        }\n                    }\n                    if (sle32(wave_sizze_12284, skip_threads_12294)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12294 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10253 - squot32(local_tid_10253, 32) * 32) ==\n                    31 && slt32(local_tid_10253, group_sizze_9871)) {\n                    *(volatile _",
                   "_local\n                      int32_t *) &mem_11707[squot32(local_tid_10253, 32) *\n                                            sizeof(int32_t)] = y_10245;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10253, 32) == 0 && slt32(local_tid_10253,\n                                                               group_sizze_9871)) {\n                    y_12292 = *(volatile __local\n                                int32_t *) &mem_11707[local_tid_10253 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12295 = 1;\n                    \n                    while (slt32(skip_threads_12295, 32)) {\n                        if ((squot32(local_tid_10253, 32) == 0 &&\n                             slt32(local_tid_10253, group_sizze_9871)) &&\n                            sle32(skip_threads_12295, local_tid_10253 -\n                                  squot32(local_tid_10253, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12291 = *(volatile __local\n                                            int32_t *) &mem_11707[(local_tid_10253 -\n                                                                   skip_threads_12295) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12293 = x_12291 + y_12292;\n                                \n                                y_12292 = zz_12293;\n                            }\n                        }\n                        if (sle32(wave_sizze_12284, skip_threads_12295)) {\n  ",
                   "                          barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10253, 32) == 0 &&\n                             slt32(local_tid_10253, group_sizze_9871)) &&\n                            sle32(skip_threads_12295, local_tid_10253 -\n                                  squot32(local_tid_10253, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11707[local_tid_10253 *\n                                                        sizeof(int32_t)] =\n                                    y_12292;\n                            }\n                        }\n                        if (sle32(wave_sizze_12284, skip_threads_12295)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12295 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10253, 32) == 0 ||\n                      !slt32(local_tid_10253, group_sizze_9871))) {\n                    // read operands\n                    {\n                        x_10244 = *(volatile __local\n                                    int32_t *) &mem_11707[(squot32(local_tid_10253,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_10246 = x_10244 + y_10245;\n                        \n                        y_10245 = zz_10246;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11707[local_tid_1025",
                   "3 *\n                                                sizeof(int32_t)] = y_10245;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10253, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11707[local_tid_10253 *\n                                                             sizeof(int32_t)] =\n                        y_10245;\n                }\n            }\n            if (cond_10265) {\n                int32_t scanned_elem_10271 = *(__local\n                                               int32_t *) &mem_11707[local_tid_10253 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11704[j_10264 * 4] =\n                    scanned_elem_10271;\n            }\n            \n            int32_t new_carry_10276;\n            \n            if (is_first_thread_10274) {\n                int32_t carry_10275 = *(__local int32_t *) &mem_11707[y_9873 *\n                                                                      4];\n                \n                new_carry_10276 = carry_10275;\n            } else {\n                new_carry_10276 = 0;\n            }\n            \n            int32_t x_merge_tmp_12288 = new_carry_10276;\n            \n            x_merge_10258 = x_merge_tmp_12288;\n        }\n        result_10278 = x_merge_10258;\n    }\n    if (local_tid_10253 == 0) {\n        *(__global int32_t *) &mem_11710[group_id_10254 * 4] = result_10278;\n    }\n}\n__kernel void scan1_kernel_10423(__local volatile int64_t *mem_aligned_0,\n                                 int32_t prime_8379,\n                                 int32_t num_iterations_10428, int32_t y_10431,\n                                 __global unsigned char *mem_11614, __global\n                                 unsigned char *mem_11617, __global\n                                 unsigned char *m",
                   "em_11623)\n{\n    __local volatile char *restrict mem_11620 = mem_aligned_0;\n    int32_t wave_sizze_12324;\n    int32_t group_sizze_12325;\n    char thread_active_12326;\n    int32_t global_tid_10423;\n    int32_t local_tid_10424;\n    int32_t group_id_10425;\n    \n    global_tid_10423 = get_global_id(0);\n    local_tid_10424 = get_local_id(0);\n    group_sizze_12325 = get_local_size(0);\n    wave_sizze_12324 = LOCKSTEP_WIDTH;\n    group_id_10425 = get_group_id(0);\n    thread_active_12326 = 1;\n    \n    int32_t x_10432;\n    char is_first_thread_10447;\n    int32_t result_10452;\n    \n    if (thread_active_12326) {\n        x_10432 = group_id_10425 * y_10431;\n        is_first_thread_10447 = local_tid_10424 == 0;\n        \n        int32_t x_merge_10429 = 0;\n        \n        for (int32_t i_10430 = 0; i_10430 < num_iterations_10428; i_10430++) {\n            int32_t y_10433 = i_10430 * group_sizze_10391;\n            int32_t offset_10434 = x_10432 + y_10433;\n            int32_t j_10435 = offset_10434 + local_tid_10424;\n            char cond_10436 = slt32(j_10435, prime_8379);\n            int32_t foldres_10439;\n            \n            if (cond_10436) {\n                char res_10407 = sle32(1, j_10435);\n                int32_t eq_class_10408;\n                int32_t partition_incr_10409;\n                \n                if (res_10407) {\n                    eq_class_10408 = 0;\n                    partition_incr_10409 = 1;\n                } else {\n                    eq_class_10408 = 1;\n                    partition_incr_10409 = 0;\n                }\n                \n                int32_t zz_10410 = x_merge_10429 + partition_incr_10409;\n                \n                *(__global int32_t *) &mem_11617[j_10435 * 4] = eq_class_10408;\n                foldres_10439 = zz_10410;\n            } else {\n                foldres_10439 = x_merge_10429;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10424, group_sizze_10391) && 1) {\n                *(__local int3",
                   "2_t *) &mem_11620[local_tid_10424 * 4] =\n                    foldres_10439;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10411;\n            int32_t other_index_10412;\n            int32_t x_10413;\n            int32_t y_10414;\n            int32_t my_index_12330;\n            int32_t other_index_12331;\n            int32_t x_12332;\n            int32_t y_12333;\n            \n            my_index_10411 = local_tid_10424;\n            if (slt32(local_tid_10424, group_sizze_10391)) {\n                y_10414 = *(volatile __local\n                            int32_t *) &mem_11620[local_tid_10424 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12335 = 1;\n                \n                while (slt32(skip_threads_12335, 32)) {\n                    if (slt32(local_tid_10424, group_sizze_10391) &&\n                        sle32(skip_threads_12335, local_tid_10424 -\n                              squot32(local_tid_10424, 32) * 32)) {\n                        // read operands\n                        {\n                            x_10413 = *(volatile __local\n                                        int32_t *) &mem_11620[(local_tid_10424 -\n                                                               skip_threads_12335) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_10415 = x_10413 + y_10414;\n                            \n                            y_10414 = zz_10415;\n                        }\n                    }\n                    if (sle32(wave_sizze_12324, skip_threads_12335)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10424, group_sizze_10391) &&\n",
                   "                        sle32(skip_threads_12335, local_tid_10424 -\n                              squot32(local_tid_10424, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11620[local_tid_10424 *\n                                                    sizeof(int32_t)] = y_10414;\n                        }\n                    }\n                    if (sle32(wave_sizze_12324, skip_threads_12335)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12335 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10424 - squot32(local_tid_10424, 32) * 32) ==\n                    31 && slt32(local_tid_10424, group_sizze_10391)) {\n                    *(volatile __local\n                      int32_t *) &mem_11620[squot32(local_tid_10424, 32) *\n                                            sizeof(int32_t)] = y_10414;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10424, 32) == 0 && slt32(local_tid_10424,\n                                                               group_sizze_10391)) {\n                    y_12333 = *(volatile __local\n                                int32_t *) &mem_11620[local_tid_10424 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12336 = 1;\n                    \n                    while (slt32(skip_threads_12336, 32)) {\n                        if ((squot32(local_tid_10424, 32) == 0 &&\n                             ",
                   "slt32(local_tid_10424, group_sizze_10391)) &&\n                            sle32(skip_threads_12336, local_tid_10424 -\n                                  squot32(local_tid_10424, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12332 = *(volatile __local\n                                            int32_t *) &mem_11620[(local_tid_10424 -\n                                                                   skip_threads_12336) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12334 = x_12332 + y_12333;\n                                \n                                y_12333 = zz_12334;\n                            }\n                        }\n                        if (sle32(wave_sizze_12324, skip_threads_12336)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10424, 32) == 0 &&\n                             slt32(local_tid_10424, group_sizze_10391)) &&\n                            sle32(skip_threads_12336, local_tid_10424 -\n                                  squot32(local_tid_10424, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11620[local_tid_10424 *\n                                                        sizeof(int32_t)] =\n                                    y_12333;\n                            }\n                        }\n                        if (sle32(wave_sizze_12324, skip_threads_12336)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12336 *= 2;\n                    }\n                }\n            }\n            bar",
                   "rier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10424, 32) == 0 ||\n                      !slt32(local_tid_10424, group_sizze_10391))) {\n                    // read operands\n                    {\n                        x_10413 = *(volatile __local\n                                    int32_t *) &mem_11620[(squot32(local_tid_10424,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_10415 = x_10413 + y_10414;\n                        \n                        y_10414 = zz_10415;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11620[local_tid_10424 *\n                                                sizeof(int32_t)] = y_10414;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10424, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11620[local_tid_10424 *\n                                                             sizeof(int32_t)] =\n                        y_10414;\n                }\n            }\n            if (cond_10436) {\n                int32_t scanned_elem_10444 = *(__local\n                                               int32_t *) &mem_11620[local_tid_10424 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11614[j_10435 * 4] =\n                    scanned_elem_10444;\n            }\n            \n            int32_t new_carry_10449;\n            \n            if (is_first_thread_10447) {\n        ",
                   "        int32_t carry_10448 = *(__local int32_t *) &mem_11620[y_10393 *\n                                                                      4];\n                \n                new_carry_10449 = carry_10448;\n            } else {\n                new_carry_10449 = 0;\n            }\n            \n            int32_t x_merge_tmp_12329 = new_carry_10449;\n            \n            x_merge_10429 = x_merge_tmp_12329;\n        }\n        result_10452 = x_merge_10429;\n    }\n    if (local_tid_10424 == 0) {\n        *(__global int32_t *) &mem_11623[group_id_10425 * 4] = result_10452;\n    }\n}\n__kernel void scan1_kernel_10529(__local volatile int64_t *mem_aligned_0,\n                                 int32_t prime_8379, int32_t arg_8414,\n                                 int32_t num_iterations_10534, int32_t y_10537,\n                                 __global unsigned char *mem_11644, __global\n                                 unsigned char *mem_11647, __global\n                                 unsigned char *mem_11653)\n{\n    __local volatile char *restrict mem_11650 = mem_aligned_0;\n    int32_t wave_sizze_12353;\n    int32_t group_sizze_12354;\n    char thread_active_12355;\n    int32_t global_tid_10529;\n    int32_t local_tid_10530;\n    int32_t group_id_10531;\n    \n    global_tid_10529 = get_global_id(0);\n    local_tid_10530 = get_local_id(0);\n    group_sizze_12354 = get_local_size(0);\n    wave_sizze_12353 = LOCKSTEP_WIDTH;\n    group_id_10531 = get_group_id(0);\n    thread_active_12355 = 1;\n    \n    int32_t x_10538;\n    char is_first_thread_10553;\n    int32_t result_10558;\n    \n    if (thread_active_12355) {\n        x_10538 = group_id_10531 * y_10537;\n        is_first_thread_10553 = local_tid_10530 == 0;\n        \n        int32_t x_merge_10535 = 0;\n        \n        for (int32_t i_10536 = 0; i_10536 < num_iterations_10534; i_10536++) {\n            int32_t y_10539 = i_10536 * group_sizze_10391;\n            int32_t offset_10540 = x_10538 + y_10539;\n            int32_t j_10541 = offset_10540 + loc",
                   "al_tid_10530;\n            char cond_10542 = slt32(j_10541, arg_8414);\n            int32_t foldres_10545;\n            \n            if (cond_10542) {\n                char res_10513 = sle32(prime_8379, j_10541);\n                int32_t eq_class_10514;\n                int32_t partition_incr_10515;\n                \n                if (res_10513) {\n                    eq_class_10514 = 0;\n                    partition_incr_10515 = 1;\n                } else {\n                    eq_class_10514 = 1;\n                    partition_incr_10515 = 0;\n                }\n                \n                int32_t zz_10516 = x_merge_10535 + partition_incr_10515;\n                \n                *(__global int32_t *) &mem_11647[j_10541 * 4] = eq_class_10514;\n                foldres_10545 = zz_10516;\n            } else {\n                foldres_10545 = x_merge_10535;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10530, group_sizze_10391) && 1) {\n                *(__local int32_t *) &mem_11650[local_tid_10530 * 4] =\n                    foldres_10545;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10517;\n            int32_t other_index_10518;\n            int32_t x_10519;\n            int32_t y_10520;\n            int32_t my_index_12359;\n            int32_t other_index_12360;\n            int32_t x_12361;\n            int32_t y_12362;\n            \n            my_index_10517 = local_tid_10530;\n            if (slt32(local_tid_10530, group_sizze_10391)) {\n                y_10520 = *(volatile __local\n                            int32_t *) &mem_11650[local_tid_10530 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12364 = 1;\n                \n                while (slt32(skip_threads_12364, 32)) {\n                    if (slt32(local_tid_10530, group_sizze_10391) &&\n ",
                   "                       sle32(skip_threads_12364, local_tid_10530 -\n                              squot32(local_tid_10530, 32) * 32)) {\n                        // read operands\n                        {\n                            x_10519 = *(volatile __local\n                                        int32_t *) &mem_11650[(local_tid_10530 -\n                                                               skip_threads_12364) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_10521 = x_10519 + y_10520;\n                            \n                            y_10520 = zz_10521;\n                        }\n                    }\n                    if (sle32(wave_sizze_12353, skip_threads_12364)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10530, group_sizze_10391) &&\n                        sle32(skip_threads_12364, local_tid_10530 -\n                              squot32(local_tid_10530, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11650[local_tid_10530 *\n                                                    sizeof(int32_t)] = y_10520;\n                        }\n                    }\n                    if (sle32(wave_sizze_12353, skip_threads_12364)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12364 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10530 - squot32(local_tid_10530, 32) * 32) ==\n                    31 && slt32(local_tid_10530, group_sizze_10391)) {\n                    *(volatile __local\n     ",
                   "                 int32_t *) &mem_11650[squot32(local_tid_10530, 32) *\n                                            sizeof(int32_t)] = y_10520;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10530, 32) == 0 && slt32(local_tid_10530,\n                                                               group_sizze_10391)) {\n                    y_12362 = *(volatile __local\n                                int32_t *) &mem_11650[local_tid_10530 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12365 = 1;\n                    \n                    while (slt32(skip_threads_12365, 32)) {\n                        if ((squot32(local_tid_10530, 32) == 0 &&\n                             slt32(local_tid_10530, group_sizze_10391)) &&\n                            sle32(skip_threads_12365, local_tid_10530 -\n                                  squot32(local_tid_10530, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12361 = *(volatile __local\n                                            int32_t *) &mem_11650[(local_tid_10530 -\n                                                                   skip_threads_12365) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12363 = x_12361 + y_12362;\n                                \n                                y_12362 = zz_12363;\n                            }\n                        }\n                        if (sle32(wave_sizze_12353, skip_threads_12365)) {\n            ",
                   "                barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10530, 32) == 0 &&\n                             slt32(local_tid_10530, group_sizze_10391)) &&\n                            sle32(skip_threads_12365, local_tid_10530 -\n                                  squot32(local_tid_10530, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11650[local_tid_10530 *\n                                                        sizeof(int32_t)] =\n                                    y_12362;\n                            }\n                        }\n                        if (sle32(wave_sizze_12353, skip_threads_12365)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12365 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10530, 32) == 0 ||\n                      !slt32(local_tid_10530, group_sizze_10391))) {\n                    // read operands\n                    {\n                        x_10519 = *(volatile __local\n                                    int32_t *) &mem_11650[(squot32(local_tid_10530,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_10521 = x_10519 + y_10520;\n                        \n                        y_10520 = zz_10521;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11650[local_tid_10530 *\n    ",
                   "                                            sizeof(int32_t)] = y_10520;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10530, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11650[local_tid_10530 *\n                                                             sizeof(int32_t)] =\n                        y_10520;\n                }\n            }\n            if (cond_10542) {\n                int32_t scanned_elem_10550 = *(__local\n                                               int32_t *) &mem_11650[local_tid_10530 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11644[j_10541 * 4] =\n                    scanned_elem_10550;\n            }\n            \n            int32_t new_carry_10555;\n            \n            if (is_first_thread_10553) {\n                int32_t carry_10554 = *(__local int32_t *) &mem_11650[y_10393 *\n                                                                      4];\n                \n                new_carry_10555 = carry_10554;\n            } else {\n                new_carry_10555 = 0;\n            }\n            \n            int32_t x_merge_tmp_12358 = new_carry_10555;\n            \n            x_merge_10535 = x_merge_tmp_12358;\n        }\n        result_10558 = x_merge_10535;\n    }\n    if (local_tid_10530 == 0) {\n        *(__global int32_t *) &mem_11653[group_id_10531 * 4] = result_10558;\n    }\n}\n__kernel void scan1_kernel_10635(__local volatile int64_t *mem_aligned_0,\n                                 int32_t arg_8414, int32_t arg_8443,\n                                 int32_t num_iterations_10640, int32_t y_10643,\n                                 __global unsigned char *mem_11674, __global\n                                 unsigned char *mem_11677, __global\n                                 unsigne",
                   "d char *mem_11683)\n{\n    __local volatile char *restrict mem_11680 = mem_aligned_0;\n    int32_t wave_sizze_12382;\n    int32_t group_sizze_12383;\n    char thread_active_12384;\n    int32_t global_tid_10635;\n    int32_t local_tid_10636;\n    int32_t group_id_10637;\n    \n    global_tid_10635 = get_global_id(0);\n    local_tid_10636 = get_local_id(0);\n    group_sizze_12383 = get_local_size(0);\n    wave_sizze_12382 = LOCKSTEP_WIDTH;\n    group_id_10637 = get_group_id(0);\n    thread_active_12384 = 1;\n    \n    int32_t x_10644;\n    char is_first_thread_10659;\n    int32_t result_10664;\n    \n    if (thread_active_12384) {\n        x_10644 = group_id_10637 * y_10643;\n        is_first_thread_10659 = local_tid_10636 == 0;\n        \n        int32_t x_merge_10641 = 0;\n        \n        for (int32_t i_10642 = 0; i_10642 < num_iterations_10640; i_10642++) {\n            int32_t y_10645 = i_10642 * group_sizze_10391;\n            int32_t offset_10646 = x_10644 + y_10645;\n            int32_t j_10647 = offset_10646 + local_tid_10636;\n            char cond_10648 = slt32(j_10647, arg_8443);\n            int32_t foldres_10651;\n            \n            if (cond_10648) {\n                char res_10619 = sle32(arg_8414, j_10647);\n                int32_t eq_class_10620;\n                int32_t partition_incr_10621;\n                \n                if (res_10619) {\n                    eq_class_10620 = 0;\n                    partition_incr_10621 = 1;\n                } else {\n                    eq_class_10620 = 1;\n                    partition_incr_10621 = 0;\n                }\n                \n                int32_t zz_10622 = x_merge_10641 + partition_incr_10621;\n                \n                *(__global int32_t *) &mem_11677[j_10647 * 4] = eq_class_10620;\n                foldres_10651 = zz_10622;\n            } else {\n                foldres_10651 = x_merge_10641;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10636, group_sizze_10391) && 1) {\n                ",
                   "*(__local int32_t *) &mem_11680[local_tid_10636 * 4] =\n                    foldres_10651;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10623;\n            int32_t other_index_10624;\n            int32_t x_10625;\n            int32_t y_10626;\n            int32_t my_index_12388;\n            int32_t other_index_12389;\n            int32_t x_12390;\n            int32_t y_12391;\n            \n            my_index_10623 = local_tid_10636;\n            if (slt32(local_tid_10636, group_sizze_10391)) {\n                y_10626 = *(volatile __local\n                            int32_t *) &mem_11680[local_tid_10636 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12393 = 1;\n                \n                while (slt32(skip_threads_12393, 32)) {\n                    if (slt32(local_tid_10636, group_sizze_10391) &&\n                        sle32(skip_threads_12393, local_tid_10636 -\n                              squot32(local_tid_10636, 32) * 32)) {\n                        // read operands\n                        {\n                            x_10625 = *(volatile __local\n                                        int32_t *) &mem_11680[(local_tid_10636 -\n                                                               skip_threads_12393) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_10627 = x_10625 + y_10626;\n                            \n                            y_10626 = zz_10627;\n                        }\n                    }\n                    if (sle32(wave_sizze_12382, skip_threads_12393)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10636, group_si",
                   "zze_10391) &&\n                        sle32(skip_threads_12393, local_tid_10636 -\n                              squot32(local_tid_10636, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11680[local_tid_10636 *\n                                                    sizeof(int32_t)] = y_10626;\n                        }\n                    }\n                    if (sle32(wave_sizze_12382, skip_threads_12393)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12393 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10636 - squot32(local_tid_10636, 32) * 32) ==\n                    31 && slt32(local_tid_10636, group_sizze_10391)) {\n                    *(volatile __local\n                      int32_t *) &mem_11680[squot32(local_tid_10636, 32) *\n                                            sizeof(int32_t)] = y_10626;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10636, 32) == 0 && slt32(local_tid_10636,\n                                                               group_sizze_10391)) {\n                    y_12391 = *(volatile __local\n                                int32_t *) &mem_11680[local_tid_10636 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12394 = 1;\n                    \n                    while (slt32(skip_threads_12394, 32)) {\n                        if ((squot32(local_tid_10636, 32) == 0 &&\n               ",
                   "              slt32(local_tid_10636, group_sizze_10391)) &&\n                            sle32(skip_threads_12394, local_tid_10636 -\n                                  squot32(local_tid_10636, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12390 = *(volatile __local\n                                            int32_t *) &mem_11680[(local_tid_10636 -\n                                                                   skip_threads_12394) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12392 = x_12390 + y_12391;\n                                \n                                y_12391 = zz_12392;\n                            }\n                        }\n                        if (sle32(wave_sizze_12382, skip_threads_12394)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10636, 32) == 0 &&\n                             slt32(local_tid_10636, group_sizze_10391)) &&\n                            sle32(skip_threads_12394, local_tid_10636 -\n                                  squot32(local_tid_10636, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11680[local_tid_10636 *\n                                                        sizeof(int32_t)] =\n                                    y_12391;\n                            }\n                        }\n                        if (sle32(wave_sizze_12382, skip_threads_12394)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12394 *= 2;\n                    }\n                }\n            }\n ",
                   "           barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10636, 32) == 0 ||\n                      !slt32(local_tid_10636, group_sizze_10391))) {\n                    // read operands\n                    {\n                        x_10625 = *(volatile __local\n                                    int32_t *) &mem_11680[(squot32(local_tid_10636,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_10627 = x_10625 + y_10626;\n                        \n                        y_10626 = zz_10627;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11680[local_tid_10636 *\n                                                sizeof(int32_t)] = y_10626;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10636, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11680[local_tid_10636 *\n                                                             sizeof(int32_t)] =\n                        y_10626;\n                }\n            }\n            if (cond_10648) {\n                int32_t scanned_elem_10656 = *(__local\n                                               int32_t *) &mem_11680[local_tid_10636 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11674[j_10647 * 4] =\n                    scanned_elem_10656;\n            }\n            \n            int32_t new_carry_10661;\n            \n            if (is_first_thread_106",
                   "59) {\n                int32_t carry_10660 = *(__local int32_t *) &mem_11680[y_10393 *\n                                                                      4];\n                \n                new_carry_10661 = carry_10660;\n            } else {\n                new_carry_10661 = 0;\n            }\n            \n            int32_t x_merge_tmp_12387 = new_carry_10661;\n            \n            x_merge_10641 = x_merge_tmp_12387;\n        }\n        result_10664 = x_merge_10641;\n    }\n    if (local_tid_10636 == 0) {\n        *(__global int32_t *) &mem_11683[group_id_10637 * 4] = result_10664;\n    }\n}\n__kernel void scan1_kernel_10741(__local volatile int64_t *mem_aligned_0,\n                                 int32_t arg_8443, int32_t arg_8472,\n                                 int32_t num_iterations_10746, int32_t y_10749,\n                                 __global unsigned char *mem_11704, __global\n                                 unsigned char *mem_11707, __global\n                                 unsigned char *mem_11713)\n{\n    __local volatile char *restrict mem_11710 = mem_aligned_0;\n    int32_t wave_sizze_12411;\n    int32_t group_sizze_12412;\n    char thread_active_12413;\n    int32_t global_tid_10741;\n    int32_t local_tid_10742;\n    int32_t group_id_10743;\n    \n    global_tid_10741 = get_global_id(0);\n    local_tid_10742 = get_local_id(0);\n    group_sizze_12412 = get_local_size(0);\n    wave_sizze_12411 = LOCKSTEP_WIDTH;\n    group_id_10743 = get_group_id(0);\n    thread_active_12413 = 1;\n    \n    int32_t x_10750;\n    char is_first_thread_10765;\n    int32_t result_10770;\n    \n    if (thread_active_12413) {\n        x_10750 = group_id_10743 * y_10749;\n        is_first_thread_10765 = local_tid_10742 == 0;\n        \n        int32_t x_merge_10747 = 0;\n        \n        for (int32_t i_10748 = 0; i_10748 < num_iterations_10746; i_10748++) {\n            int32_t y_10751 = i_10748 * group_sizze_10391;\n            int32_t offset_10752 = x_10750 + y_10751;\n            int32_t j_10753 = offset",
                   "_10752 + local_tid_10742;\n            char cond_10754 = slt32(j_10753, arg_8472);\n            int32_t foldres_10757;\n            \n            if (cond_10754) {\n                char res_10725 = sle32(arg_8443, j_10753);\n                int32_t eq_class_10726;\n                int32_t partition_incr_10727;\n                \n                if (res_10725) {\n                    eq_class_10726 = 0;\n                    partition_incr_10727 = 1;\n                } else {\n                    eq_class_10726 = 1;\n                    partition_incr_10727 = 0;\n                }\n                \n                int32_t zz_10728 = x_merge_10747 + partition_incr_10727;\n                \n                *(__global int32_t *) &mem_11707[j_10753 * 4] = eq_class_10726;\n                foldres_10757 = zz_10728;\n            } else {\n                foldres_10757 = x_merge_10747;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10742, group_sizze_10391) && 1) {\n                *(__local int32_t *) &mem_11710[local_tid_10742 * 4] =\n                    foldres_10757;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10729;\n            int32_t other_index_10730;\n            int32_t x_10731;\n            int32_t y_10732;\n            int32_t my_index_12417;\n            int32_t other_index_12418;\n            int32_t x_12419;\n            int32_t y_12420;\n            \n            my_index_10729 = local_tid_10742;\n            if (slt32(local_tid_10742, group_sizze_10391)) {\n                y_10732 = *(volatile __local\n                            int32_t *) &mem_11710[local_tid_10742 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12422 = 1;\n                \n                while (slt32(skip_threads_12422, 32)) {\n                    if (slt32(local_tid_10742, group_sizze_1",
                   "0391) &&\n                        sle32(skip_threads_12422, local_tid_10742 -\n                              squot32(local_tid_10742, 32) * 32)) {\n                        // read operands\n                        {\n                            x_10731 = *(volatile __local\n                                        int32_t *) &mem_11710[(local_tid_10742 -\n                                                               skip_threads_12422) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_10733 = x_10731 + y_10732;\n                            \n                            y_10732 = zz_10733;\n                        }\n                    }\n                    if (sle32(wave_sizze_12411, skip_threads_12422)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10742, group_sizze_10391) &&\n                        sle32(skip_threads_12422, local_tid_10742 -\n                              squot32(local_tid_10742, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11710[local_tid_10742 *\n                                                    sizeof(int32_t)] = y_10732;\n                        }\n                    }\n                    if (sle32(wave_sizze_12411, skip_threads_12422)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12422 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10742 - squot32(local_tid_10742, 32) * 32) ==\n                    31 && slt32(local_tid_10742, group_sizze_10391)) {\n                    *(volatile __l",
                   "ocal\n                      int32_t *) &mem_11710[squot32(local_tid_10742, 32) *\n                                            sizeof(int32_t)] = y_10732;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10742, 32) == 0 && slt32(local_tid_10742,\n                                                               group_sizze_10391)) {\n                    y_12420 = *(volatile __local\n                                int32_t *) &mem_11710[local_tid_10742 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12423 = 1;\n                    \n                    while (slt32(skip_threads_12423, 32)) {\n                        if ((squot32(local_tid_10742, 32) == 0 &&\n                             slt32(local_tid_10742, group_sizze_10391)) &&\n                            sle32(skip_threads_12423, local_tid_10742 -\n                                  squot32(local_tid_10742, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12419 = *(volatile __local\n                                            int32_t *) &mem_11710[(local_tid_10742 -\n                                                                   skip_threads_12423) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12421 = x_12419 + y_12420;\n                                \n                                y_12420 = zz_12421;\n                            }\n                        }\n                        if (sle32(wave_sizze_12411, skip_threads_12423)) {\n  ",
                   "                          barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10742, 32) == 0 &&\n                             slt32(local_tid_10742, group_sizze_10391)) &&\n                            sle32(skip_threads_12423, local_tid_10742 -\n                                  squot32(local_tid_10742, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11710[local_tid_10742 *\n                                                        sizeof(int32_t)] =\n                                    y_12420;\n                            }\n                        }\n                        if (sle32(wave_sizze_12411, skip_threads_12423)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12423 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10742, 32) == 0 ||\n                      !slt32(local_tid_10742, group_sizze_10391))) {\n                    // read operands\n                    {\n                        x_10731 = *(volatile __local\n                                    int32_t *) &mem_11710[(squot32(local_tid_10742,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_10733 = x_10731 + y_10732;\n                        \n                        y_10732 = zz_10733;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11710[local_tid_10",
                   "742 *\n                                                sizeof(int32_t)] = y_10732;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10742, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11710[local_tid_10742 *\n                                                             sizeof(int32_t)] =\n                        y_10732;\n                }\n            }\n            if (cond_10754) {\n                int32_t scanned_elem_10762 = *(__local\n                                               int32_t *) &mem_11710[local_tid_10742 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11704[j_10753 * 4] =\n                    scanned_elem_10762;\n            }\n            \n            int32_t new_carry_10767;\n            \n            if (is_first_thread_10765) {\n                int32_t carry_10766 = *(__local int32_t *) &mem_11710[y_10393 *\n                                                                      4];\n                \n                new_carry_10767 = carry_10766;\n            } else {\n                new_carry_10767 = 0;\n            }\n            \n            int32_t x_merge_tmp_12416 = new_carry_10767;\n            \n            x_merge_10747 = x_merge_tmp_12416;\n        }\n        result_10770 = x_merge_10747;\n    }\n    if (local_tid_10742 == 0) {\n        *(__global int32_t *) &mem_11713[group_id_10743 * 4] = result_10770;\n    }\n}\n__kernel void scan1_kernel_10922(__local volatile int64_t *mem_aligned_0,\n                                 int32_t prime_8379,\n                                 int32_t partition_sizze_8458,\n                                 int32_t num_iterations_10927, int32_t y_10930,\n                                 __global unsigned char *mem_11695, __global\n                                 unsigned char *mem",
                   "_11698, __global\n                                 unsigned char *mem_11701, __global\n                                 unsigned char *mem_11734, __global\n                                 unsigned char *mem_11737, __global\n                                 unsigned char *mem_11740, __global\n                                 unsigned char *mem_11746)\n{\n    __local volatile char *restrict mem_11743 = mem_aligned_0;\n    int32_t wave_sizze_12440;\n    int32_t group_sizze_12441;\n    char thread_active_12442;\n    int32_t global_tid_10922;\n    int32_t local_tid_10923;\n    int32_t group_id_10924;\n    \n    global_tid_10922 = get_global_id(0);\n    local_tid_10923 = get_local_id(0);\n    group_sizze_12441 = get_local_size(0);\n    wave_sizze_12440 = LOCKSTEP_WIDTH;\n    group_id_10924 = get_group_id(0);\n    thread_active_12442 = 1;\n    \n    int32_t x_10931;\n    char is_first_thread_10948;\n    int32_t result_10953;\n    \n    if (thread_active_12442) {\n        x_10931 = group_id_10924 * y_10930;\n        is_first_thread_10948 = local_tid_10923 == 0;\n        \n        int32_t x_merge_10928 = 0;\n        \n        for (int32_t i_10929 = 0; i_10929 < num_iterations_10927; i_10929++) {\n            int32_t y_10932 = i_10929 * group_sizze_10391;\n            int32_t offset_10933 = x_10931 + y_10932;\n            int32_t j_10934 = offset_10933 + local_tid_10923;\n            char cond_10935 = slt32(j_10934, partition_sizze_8458);\n            int32_t foldres_10940;\n            \n            if (cond_10935) {\n                int32_t partition_res_elem_10936 = *(__global\n                                                     int32_t *) &mem_11695[j_10934 *\n                                                                           4];\n                int32_t partition_res_elem_10937 = *(__global\n                                                     int32_t *) &mem_11698[j_10934 *\n                                                                           4];\n                int32_t partition_res_elem_10938 = *",
                   "(__global\n                                                     int32_t *) &mem_11701[j_10934 *\n                                                                           4];\n                int32_t x_10855 = 3 - partition_res_elem_10936;\n                int32_t y_10856 = x_10855 - 1;\n                \n                for (int32_t i_10860 = 0; i_10860 < 3; i_10860++) {\n                    char cond_10862 = slt32(i_10860, y_10856);\n                    int32_t res_10863;\n                    \n                    if (cond_10862) {\n                        res_10863 = 0;\n                    } else {\n                        int32_t x_10864 = 3 - i_10860;\n                        int32_t y_10865 = x_10864 - 1;\n                        int32_t y_10866 = pow32(partition_res_elem_10937,\n                                                y_10865);\n                        int32_t x_10867 = sdiv32(partition_res_elem_10938,\n                                                 y_10866);\n                        int32_t res_10868 = smod32(x_10867,\n                                                   partition_res_elem_10937);\n                        \n                        res_10863 = res_10868;\n                    }\n                    *(__global int32_t *) &mem_11740[(group_id_10924 * (3 *\n                                                                        group_sizze_10391) +\n                                                      i_10860 *\n                                                      group_sizze_10391 +\n                                                      local_tid_10923) * 4] =\n                        res_10863;\n                }\n                \n                int32_t x_10870;\n                int32_t x_10873 = 1;\n                \n                for (int32_t chunk_offset_10872 = 0; chunk_offset_10872 <\n                     prime_8379; chunk_offset_10872++) {\n                    int32_t res_10882;\n                    int32_t x_10885 = 0;\n                    int32_t chunk_sizze_",
                   "10883;\n                    int32_t chunk_offset_10884 = 0;\n                    \n                    chunk_sizze_10883 = 3;\n                    \n                    int32_t res_10887;\n                    int32_t acc_10890 = x_10885;\n                    int32_t groupstream_mapaccum_dummy_chunk_sizze_10888 = 1;\n                    \n                    if (1) {\n                        if (chunk_sizze_10883 == 3) {\n                            for (int32_t i_10889 = 0; i_10889 < 3; i_10889++) {\n                                int32_t convop_x_11571 = chunk_offset_10884 +\n                                        i_10889;\n                                char cond_10894 = convop_x_11571 == 2;\n                                int32_t res_10895;\n                                \n                                if (cond_10894) {\n                                    int32_t res_10896 = *(__global\n                                                          int32_t *) &mem_11740[(group_id_10924 *\n                                                                                 (3 *\n                                                                                  group_sizze_10391) +\n                                                                                 convop_x_11571 *\n                                                                                 group_sizze_10391 +\n                                                                                 local_tid_10923) *\n                                                                                4];\n                                    \n                                    res_10895 = res_10896;\n                                } else {\n                                    int32_t x_10897 = *(__global\n                                                        int32_t *) &mem_11740[(group_id_10924 *\n                                                                               (3 *\n                                                       ",
                   "                         group_sizze_10391) +\n                                                                               convop_x_11571 *\n                                                                               group_sizze_10391 +\n                                                                               local_tid_10923) *\n                                                                              4];\n                                    int32_t y_10898 = 2 - convop_x_11571;\n                                    int32_t y_10899 = pow32(chunk_offset_10872,\n                                                            y_10898);\n                                    int32_t res_10900 = x_10897 * y_10899;\n                                    \n                                    res_10895 = res_10900;\n                                }\n                                \n                                int32_t x_10901 = acc_10890 + res_10895;\n                                int32_t res_10902 = smod32(x_10901, prime_8379);\n                                \n                                acc_10890 = res_10902;\n                            }\n                        } else {\n                            for (int32_t i_10889 = 0; i_10889 <\n                                 chunk_sizze_10883; i_10889++) {\n                                int32_t convop_x_11571 = chunk_offset_10884 +\n                                        i_10889;\n                                char cond_10894 = convop_x_11571 == 2;\n                                int32_t res_10895;\n                                \n                                if (cond_10894) {\n                                    int32_t res_10896 = *(__global\n                                                          int32_t *) &mem_11740[(group_id_10924 *\n                                                                                 (3 *\n                                                                                  group_sizze_10391)",
                   " +\n                                                                                 convop_x_11571 *\n                                                                                 group_sizze_10391 +\n                                                                                 local_tid_10923) *\n                                                                                4];\n                                    \n                                    res_10895 = res_10896;\n                                } else {\n                                    int32_t x_10897 = *(__global\n                                                        int32_t *) &mem_11740[(group_id_10924 *\n                                                                               (3 *\n                                                                                group_sizze_10391) +\n                                                                               convop_x_11571 *\n                                                                               group_sizze_10391 +\n                                                                               local_tid_10923) *\n                                                                              4];\n                                    int32_t y_10898 = 2 - convop_x_11571;\n                                    int32_t y_10899 = pow32(chunk_offset_10872,\n                                                            y_10898);\n                                    int32_t res_10900 = x_10897 * y_10899;\n                                    \n                                    res_10895 = res_10900;\n                                }\n                                \n                                int32_t x_10901 = acc_10890 + res_10895;\n                                int32_t res_10902 = smod32(x_10901, prime_8379);\n                                \n                                acc_10890 = res_10902;\n                            }\n                      ",
                   "  }\n                    }\n                    res_10887 = acc_10890;\n                    x_10885 = res_10887;\n                    res_10882 = x_10885;\n                    \n                    char cond_10903 = res_10882 == 0;\n                    int32_t res_10904;\n                    \n                    if (cond_10903) {\n                        res_10904 = 0;\n                    } else {\n                        res_10904 = x_10873;\n                    }\n                    \n                    int32_t x_tmp_12447 = res_10904;\n                    \n                    x_10873 = x_tmp_12447;\n                }\n                x_10870 = x_10873;\n                \n                char res_10905 = x_10870 == 0;\n                char res_10906 = !res_10905;\n                int32_t eq_class_10907;\n                int32_t partition_incr_10908;\n                \n                if (res_10906) {\n                    eq_class_10907 = 0;\n                    partition_incr_10908 = 1;\n                } else {\n                    eq_class_10907 = 1;\n                    partition_incr_10908 = 0;\n                }\n                \n                int32_t zz_10909 = x_merge_10928 + partition_incr_10908;\n                \n                *(__global int32_t *) &mem_11737[j_10934 * 4] = eq_class_10907;\n                foldres_10940 = zz_10909;\n            } else {\n                foldres_10940 = x_merge_10928;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_10923, group_sizze_10391) && 1) {\n                *(__local int32_t *) &mem_11743[local_tid_10923 * 4] =\n                    foldres_10940;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_10910;\n            int32_t other_index_10911;\n            int32_t x_10912;\n            int32_t y_10913;\n            int32_t my_index_12448;\n            int32_t other_index_12449;\n            int32_t x_12450;\n            int32_t y_12451;\n            \n            my_in",
                   "dex_10910 = local_tid_10923;\n            if (slt32(local_tid_10923, group_sizze_10391)) {\n                y_10913 = *(volatile __local\n                            int32_t *) &mem_11743[local_tid_10923 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12453 = 1;\n                \n                while (slt32(skip_threads_12453, 32)) {\n                    if (slt32(local_tid_10923, group_sizze_10391) &&\n                        sle32(skip_threads_12453, local_tid_10923 -\n                              squot32(local_tid_10923, 32) * 32)) {\n                        // read operands\n                        {\n                            x_10912 = *(volatile __local\n                                        int32_t *) &mem_11743[(local_tid_10923 -\n                                                               skip_threads_12453) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_10914 = x_10912 + y_10913;\n                            \n                            y_10913 = zz_10914;\n                        }\n                    }\n                    if (sle32(wave_sizze_12440, skip_threads_12453)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_10923, group_sizze_10391) &&\n                        sle32(skip_threads_12453, local_tid_10923 -\n                              squot32(local_tid_10923, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11743[local_tid_10923 *\n                                                    sizeof(int32_t)] = y_10913;\n                        }\n                ",
                   "    }\n                    if (sle32(wave_sizze_12440, skip_threads_12453)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12453 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_10923 - squot32(local_tid_10923, 32) * 32) ==\n                    31 && slt32(local_tid_10923, group_sizze_10391)) {\n                    *(volatile __local\n                      int32_t *) &mem_11743[squot32(local_tid_10923, 32) *\n                                            sizeof(int32_t)] = y_10913;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_10923, 32) == 0 && slt32(local_tid_10923,\n                                                               group_sizze_10391)) {\n                    y_12451 = *(volatile __local\n                                int32_t *) &mem_11743[local_tid_10923 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12454 = 1;\n                    \n                    while (slt32(skip_threads_12454, 32)) {\n                        if ((squot32(local_tid_10923, 32) == 0 &&\n                             slt32(local_tid_10923, group_sizze_10391)) &&\n                            sle32(skip_threads_12454, local_tid_10923 -\n                                  squot32(local_tid_10923, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12450 = *(volatile __local\n                                            int32_t *) &mem_11743[(local_tid_10923 -\n                              ",
                   "                                     skip_threads_12454) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12452 = x_12450 + y_12451;\n                                \n                                y_12451 = zz_12452;\n                            }\n                        }\n                        if (sle32(wave_sizze_12440, skip_threads_12454)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_10923, 32) == 0 &&\n                             slt32(local_tid_10923, group_sizze_10391)) &&\n                            sle32(skip_threads_12454, local_tid_10923 -\n                                  squot32(local_tid_10923, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11743[local_tid_10923 *\n                                                        sizeof(int32_t)] =\n                                    y_12451;\n                            }\n                        }\n                        if (sle32(wave_sizze_12440, skip_threads_12454)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12454 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_10923, 32) == 0 ||\n                      !slt32(local_tid_10923, group_sizze_10391))) {\n                    // read operands\n                    {\n                        x_10912 = *(volatile __local\n                                    int32_t *) &mem_11743[(squot32(local_tid_10923,\n                   ",
                   "                                                32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_10914 = x_10912 + y_10913;\n                        \n                        y_10913 = zz_10914;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11743[local_tid_10923 *\n                                                sizeof(int32_t)] = y_10913;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_10923, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11743[local_tid_10923 *\n                                                             sizeof(int32_t)] =\n                        y_10913;\n                }\n            }\n            if (cond_10935) {\n                int32_t scanned_elem_10945 = *(__local\n                                               int32_t *) &mem_11743[local_tid_10923 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11734[j_10934 * 4] =\n                    scanned_elem_10945;\n            }\n            \n            int32_t new_carry_10950;\n            \n            if (is_first_thread_10948) {\n                int32_t carry_10949 = *(__local int32_t *) &mem_11743[y_10393 *\n                                                                      4];\n                \n                new_carry_10950 = carry_10949;\n            } else {\n                new_carry_10950 = 0;\n            }\n            \n            int32_t x_merge_tmp_12445 = new_carry_10950;\n            \n            x_merge_10928 = x_merge_tmp_12445;\n        }\n        result_1095",
                   "3 = x_merge_10928;\n    }\n    if (local_tid_10923 == 0) {\n        *(__global int32_t *) &mem_11746[group_id_10924 * 4] = result_10953;\n    }\n}\n__kernel void scan1_kernel_11105(__local volatile int64_t *mem_aligned_0,\n                                 int32_t prime_8379,\n                                 int32_t partition_sizze_8487,\n                                 int32_t num_iterations_11110, int32_t y_11113,\n                                 __global unsigned char *mem_11725, __global\n                                 unsigned char *mem_11728, __global\n                                 unsigned char *mem_11731, __global\n                                 unsigned char *mem_11767, __global\n                                 unsigned char *mem_11770, __global\n                                 unsigned char *mem_11773, __global\n                                 unsigned char *mem_11779)\n{\n    __local volatile char *restrict mem_11776 = mem_aligned_0;\n    int32_t wave_sizze_12471;\n    int32_t group_sizze_12472;\n    char thread_active_12473;\n    int32_t global_tid_11105;\n    int32_t local_tid_11106;\n    int32_t group_id_11107;\n    \n    global_tid_11105 = get_global_id(0);\n    local_tid_11106 = get_local_id(0);\n    group_sizze_12472 = get_local_size(0);\n    wave_sizze_12471 = LOCKSTEP_WIDTH;\n    group_id_11107 = get_group_id(0);\n    thread_active_12473 = 1;\n    \n    int32_t x_11114;\n    char is_first_thread_11131;\n    int32_t result_11136;\n    \n    if (thread_active_12473) {\n        x_11114 = group_id_11107 * y_11113;\n        is_first_thread_11131 = local_tid_11106 == 0;\n        \n        int32_t x_merge_11111 = 0;\n        \n        for (int32_t i_11112 = 0; i_11112 < num_iterations_11110; i_11112++) {\n            int32_t y_11115 = i_11112 * group_sizze_10391;\n            int32_t offset_11116 = x_11114 + y_11115;\n            int32_t j_11117 = offset_11116 + local_tid_11106;\n            char cond_11118 = slt32(j_11117, partition_sizze_8487);\n            int32_t foldres_11123;\n       ",
                   "     \n            if (cond_11118) {\n                int32_t partition_res_elem_11119 = *(__global\n                                                     int32_t *) &mem_11725[j_11117 *\n                                                                           4];\n                int32_t partition_res_elem_11120 = *(__global\n                                                     int32_t *) &mem_11728[j_11117 *\n                                                                           4];\n                int32_t partition_res_elem_11121 = *(__global\n                                                     int32_t *) &mem_11731[j_11117 *\n                                                                           4];\n                int32_t x_11038 = 4 - partition_res_elem_11119;\n                int32_t y_11039 = x_11038 - 1;\n                \n                for (int32_t i_11043 = 0; i_11043 < 4; i_11043++) {\n                    char cond_11045 = slt32(i_11043, y_11039);\n                    int32_t res_11046;\n                    \n                    if (cond_11045) {\n                        res_11046 = 0;\n                    } else {\n                        int32_t x_11047 = 4 - i_11043;\n                        int32_t y_11048 = x_11047 - 1;\n                        int32_t y_11049 = pow32(partition_res_elem_11120,\n                                                y_11048);\n                        int32_t x_11050 = sdiv32(partition_res_elem_11121,\n                                                 y_11049);\n                        int32_t res_11051 = smod32(x_11050,\n                                                   partition_res_elem_11120);\n                        \n                        res_11046 = res_11051;\n                    }\n                    *(__global int32_t *) &mem_11773[(group_id_11107 * (4 *\n                                                                        group_sizze_10391) +\n                                                      i_11043 *\n                    ",
                   "                                  group_sizze_10391 +\n                                                      local_tid_11106) * 4] =\n                        res_11046;\n                }\n                \n                int32_t x_11053;\n                int32_t x_11056 = 1;\n                \n                for (int32_t chunk_offset_11055 = 0; chunk_offset_11055 <\n                     prime_8379; chunk_offset_11055++) {\n                    int32_t res_11065;\n                    int32_t x_11068 = 0;\n                    int32_t chunk_sizze_11066;\n                    int32_t chunk_offset_11067 = 0;\n                    \n                    chunk_sizze_11066 = 4;\n                    \n                    int32_t res_11070;\n                    int32_t acc_11073 = x_11068;\n                    int32_t groupstream_mapaccum_dummy_chunk_sizze_11071 = 1;\n                    \n                    if (1) {\n                        if (chunk_sizze_11066 == 4) {\n                            for (int32_t i_11072 = 0; i_11072 < 4; i_11072++) {\n                                int32_t convop_x_11580 = chunk_offset_11067 +\n                                        i_11072;\n                                char cond_11077 = convop_x_11580 == 3;\n                                int32_t res_11078;\n                                \n                                if (cond_11077) {\n                                    int32_t res_11079 = *(__global\n                                                          int32_t *) &mem_11773[(group_id_11107 *\n                                                                                 (4 *\n                                                                                  group_sizze_10391) +\n                                                                                 convop_x_11580 *\n                                                                                 group_sizze_10391 +\n                                                                                 ",
                   "local_tid_11106) *\n                                                                                4];\n                                    \n                                    res_11078 = res_11079;\n                                } else {\n                                    int32_t x_11080 = *(__global\n                                                        int32_t *) &mem_11773[(group_id_11107 *\n                                                                               (4 *\n                                                                                group_sizze_10391) +\n                                                                               convop_x_11580 *\n                                                                               group_sizze_10391 +\n                                                                               local_tid_11106) *\n                                                                              4];\n                                    int32_t y_11081 = 3 - convop_x_11580;\n                                    int32_t y_11082 = pow32(chunk_offset_11055,\n                                                            y_11081);\n                                    int32_t res_11083 = x_11080 * y_11082;\n                                    \n                                    res_11078 = res_11083;\n                                }\n                                \n                                int32_t x_11084 = acc_11073 + res_11078;\n                                int32_t res_11085 = smod32(x_11084, prime_8379);\n                                \n                                acc_11073 = res_11085;\n                            }\n                        } else {\n                            for (int32_t i_11072 = 0; i_11072 <\n                                 chunk_sizze_11066; i_11072++) {\n                                int32_t convop_x_11580 = chunk_offset_11067 +\n                                        i_11072;\n                ",
                   "                char cond_11077 = convop_x_11580 == 3;\n                                int32_t res_11078;\n                                \n                                if (cond_11077) {\n                                    int32_t res_11079 = *(__global\n                                                          int32_t *) &mem_11773[(group_id_11107 *\n                                                                                 (4 *\n                                                                                  group_sizze_10391) +\n                                                                                 convop_x_11580 *\n                                                                                 group_sizze_10391 +\n                                                                                 local_tid_11106) *\n                                                                                4];\n                                    \n                                    res_11078 = res_11079;\n                                } else {\n                                    int32_t x_11080 = *(__global\n                                                        int32_t *) &mem_11773[(group_id_11107 *\n                                                                               (4 *\n                                                                                group_sizze_10391) +\n                                                                               convop_x_11580 *\n                                                                               group_sizze_10391 +\n                                                                               local_tid_11106) *\n                                                                              4];\n                                    int32_t y_11081 = 3 - convop_x_11580;\n                                    int32_t y_11082 = pow32(chunk_offset_11055,\n                                                            y_",
                   "11081);\n                                    int32_t res_11083 = x_11080 * y_11082;\n                                    \n                                    res_11078 = res_11083;\n                                }\n                                \n                                int32_t x_11084 = acc_11073 + res_11078;\n                                int32_t res_11085 = smod32(x_11084, prime_8379);\n                                \n                                acc_11073 = res_11085;\n                            }\n                        }\n                    }\n                    res_11070 = acc_11073;\n                    x_11068 = res_11070;\n                    res_11065 = x_11068;\n                    \n                    char cond_11086 = res_11065 == 0;\n                    int32_t res_11087;\n                    \n                    if (cond_11086) {\n                        res_11087 = 0;\n                    } else {\n                        res_11087 = x_11056;\n                    }\n                    \n                    int32_t x_tmp_12478 = res_11087;\n                    \n                    x_11056 = x_tmp_12478;\n                }\n                x_11053 = x_11056;\n                \n                char res_11088 = x_11053 == 0;\n                char res_11089 = !res_11088;\n                int32_t eq_class_11090;\n                int32_t partition_incr_11091;\n                \n                if (res_11089) {\n                    eq_class_11090 = 0;\n                    partition_incr_11091 = 1;\n                } else {\n                    eq_class_11090 = 1;\n                    partition_incr_11091 = 0;\n                }\n                \n                int32_t zz_11092 = x_merge_11111 + partition_incr_11091;\n                \n                *(__global int32_t *) &mem_11770[j_11117 * 4] = eq_class_11090;\n                foldres_11123 = zz_11092;\n            } else {\n                foldres_11123 = x_merge_11111;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE",
                   ");\n            if (slt32(local_tid_11106, group_sizze_10391) && 1) {\n                *(__local int32_t *) &mem_11776[local_tid_11106 * 4] =\n                    foldres_11123;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_11093;\n            int32_t other_index_11094;\n            int32_t x_11095;\n            int32_t y_11096;\n            int32_t my_index_12479;\n            int32_t other_index_12480;\n            int32_t x_12481;\n            int32_t y_12482;\n            \n            my_index_11093 = local_tid_11106;\n            if (slt32(local_tid_11106, group_sizze_10391)) {\n                y_11096 = *(volatile __local\n                            int32_t *) &mem_11776[local_tid_11106 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12484 = 1;\n                \n                while (slt32(skip_threads_12484, 32)) {\n                    if (slt32(local_tid_11106, group_sizze_10391) &&\n                        sle32(skip_threads_12484, local_tid_11106 -\n                              squot32(local_tid_11106, 32) * 32)) {\n                        // read operands\n                        {\n                            x_11095 = *(volatile __local\n                                        int32_t *) &mem_11776[(local_tid_11106 -\n                                                               skip_threads_12484) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_11097 = x_11095 + y_11096;\n                            \n                            y_11096 = zz_11097;\n                        }\n                    }\n                    if (sle32(wave_sizze_12471, skip_threads_12484)) {\n                        barrier(CLK_LOCAL_MEM_",
                   "FENCE);\n                    }\n                    if (slt32(local_tid_11106, group_sizze_10391) &&\n                        sle32(skip_threads_12484, local_tid_11106 -\n                              squot32(local_tid_11106, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11776[local_tid_11106 *\n                                                    sizeof(int32_t)] = y_11096;\n                        }\n                    }\n                    if (sle32(wave_sizze_12471, skip_threads_12484)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12484 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_11106 - squot32(local_tid_11106, 32) * 32) ==\n                    31 && slt32(local_tid_11106, group_sizze_10391)) {\n                    *(volatile __local\n                      int32_t *) &mem_11776[squot32(local_tid_11106, 32) *\n                                            sizeof(int32_t)] = y_11096;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_11106, 32) == 0 && slt32(local_tid_11106,\n                                                               group_sizze_10391)) {\n                    y_12482 = *(volatile __local\n                                int32_t *) &mem_11776[local_tid_11106 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12485 = 1;\n                    \n                    while (slt32(skip_threads_12485, 32)",
                   ") {\n                        if ((squot32(local_tid_11106, 32) == 0 &&\n                             slt32(local_tid_11106, group_sizze_10391)) &&\n                            sle32(skip_threads_12485, local_tid_11106 -\n                                  squot32(local_tid_11106, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12481 = *(volatile __local\n                                            int32_t *) &mem_11776[(local_tid_11106 -\n                                                                   skip_threads_12485) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12483 = x_12481 + y_12482;\n                                \n                                y_12482 = zz_12483;\n                            }\n                        }\n                        if (sle32(wave_sizze_12471, skip_threads_12485)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_11106, 32) == 0 &&\n                             slt32(local_tid_11106, group_sizze_10391)) &&\n                            sle32(skip_threads_12485, local_tid_11106 -\n                                  squot32(local_tid_11106, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11776[local_tid_11106 *\n                                                        sizeof(int32_t)] =\n                                    y_12482;\n                            }\n                        }\n                        if (sle32(wave_sizze_12471, skip_threads_12485)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                   ",
                   "     skip_threads_12485 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_11106, 32) == 0 ||\n                      !slt32(local_tid_11106, group_sizze_10391))) {\n                    // read operands\n                    {\n                        x_11095 = *(volatile __local\n                                    int32_t *) &mem_11776[(squot32(local_tid_11106,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_11097 = x_11095 + y_11096;\n                        \n                        y_11096 = zz_11097;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11776[local_tid_11106 *\n                                                sizeof(int32_t)] = y_11096;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_11106, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11776[local_tid_11106 *\n                                                             sizeof(int32_t)] =\n                        y_11096;\n                }\n            }\n            if (cond_11118) {\n                int32_t scanned_elem_11128 = *(__local\n                                               int32_t *) &mem_11776[local_tid_11106 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11767[j_11117 * 4] =\n                    scanned_elem_11128;\n            }\n            \n",
                   "            int32_t new_carry_11133;\n            \n            if (is_first_thread_11131) {\n                int32_t carry_11132 = *(__local int32_t *) &mem_11776[y_10393 *\n                                                                      4];\n                \n                new_carry_11133 = carry_11132;\n            } else {\n                new_carry_11133 = 0;\n            }\n            \n            int32_t x_merge_tmp_12476 = new_carry_11133;\n            \n            x_merge_11111 = x_merge_tmp_12476;\n        }\n        result_11136 = x_merge_11111;\n    }\n    if (local_tid_11106 == 0) {\n        *(__global int32_t *) &mem_11779[group_id_11107 * 4] = result_11136;\n    }\n}\n__kernel void scan1_kernel_11445(__local volatile int64_t *mem_aligned_0,\n                                 int32_t prime_8379, int32_t sizze_8686,\n                                 int32_t y_8692, int32_t y_8694,\n                                 int32_t arg_8695, int32_t last_index_8702,\n                                 int32_t num_iterations_11450, int32_t y_11453,\n                                 __global unsigned char *polys_mem_11835,\n                                 __global unsigned char *polys_mem_11837,\n                                 __global unsigned char *polys_mem_11839,\n                                 __global unsigned char *mem_11842, __global\n                                 unsigned char *mem_11845, __global\n                                 unsigned char *mem_11847, __global\n                                 unsigned char *mem_11850, __global\n                                 unsigned char *mem_11853, __global\n                                 unsigned char *mem_11856, __global\n                                 unsigned char *mem_11859, __global\n                                 unsigned char *mem_11862, __global\n                                 unsigned char *mem_11865, __global\n                                 unsigned char *mem_11868, __global\n                                 unsign",
                   "ed char *mem_11871, __global\n                                 unsigned char *mem_11874, __global\n                                 unsigned char *mem_11881)\n{\n    __local volatile char *restrict mem_11878 = mem_aligned_0;\n    int32_t wave_sizze_12518;\n    int32_t group_sizze_12519;\n    char thread_active_12520;\n    int32_t global_tid_11445;\n    int32_t local_tid_11446;\n    int32_t group_id_11447;\n    \n    global_tid_11445 = get_global_id(0);\n    local_tid_11446 = get_local_id(0);\n    group_sizze_12519 = get_local_size(0);\n    wave_sizze_12518 = LOCKSTEP_WIDTH;\n    group_id_11447 = get_group_id(0);\n    thread_active_12520 = 1;\n    \n    int32_t x_11454;\n    char is_first_thread_11473;\n    int32_t result_11480;\n    \n    if (thread_active_12520) {\n        x_11454 = group_id_11447 * y_11453;\n        is_first_thread_11473 = local_tid_11446 == 0;\n        \n        int32_t x_merge_11451 = 0;\n        \n        for (int32_t i_11452 = 0; i_11452 < num_iterations_11450; i_11452++) {\n            int32_t y_11455 = i_11452 * group_sizze_10391;\n            int32_t offset_11456 = x_11454 + y_11455;\n            int32_t j_11457 = offset_11456 + local_tid_11446;\n            char cond_11458 = slt32(j_11457, arg_8695);\n            int32_t foldres_11463;\n            \n            if (cond_11458) {\n                int32_t res_11269 = j_11457 + y_8694;\n                \n                for (int32_t i_11273 = 0; i_11273 < y_8692; i_11273++) {\n                    int32_t x_11275 = y_8692 - i_11273;\n                    int32_t y_11276 = x_11275 - 1;\n                    int32_t y_11277 = pow32(prime_8379, y_11276);\n                    int32_t x_11278 = sdiv32(res_11269, y_11277);\n                    int32_t res_11279 = smod32(x_11278, prime_8379);\n                    \n                    *(__global int32_t *) &mem_11853[(group_id_11447 * (y_8692 *\n                                                                        group_sizze_10391) +\n                                                      i_11273",
                   " *\n                                                      group_sizze_10391 +\n                                                      local_tid_11446) * 4] =\n                        res_11279;\n                }\n                \n                char res_11281;\n                char binop_param_x_11284 = 1;\n                \n                for (int32_t chunk_offset_11283 = 0; chunk_offset_11283 <\n                     sizze_8686; chunk_offset_11283++) {\n                    int32_t x_11295 = *(__global\n                                        int32_t *) &polys_mem_11835[chunk_offset_11283 *\n                                                                    4];\n                    int32_t x_11296 = *(__global\n                                        int32_t *) &polys_mem_11837[chunk_offset_11283 *\n                                                                    4];\n                    int32_t x_11297 = *(__global\n                                        int32_t *) &polys_mem_11839[chunk_offset_11283 *\n                                                                    4];\n                    int32_t x_11299 = y_8692 - x_11295;\n                    int32_t y_11300 = x_11299 - 1;\n                    int32_t discard_11303;\n                    int32_t acc_11306 = 0;\n                    \n                    for (int32_t i_11309 = 0; i_11309 < y_8692; i_11309++) {\n                        char cond_11312 = slt32(i_11309, y_11300);\n                        int32_t res_11313;\n                        \n                        if (cond_11312) {\n                            res_11313 = 0;\n                        } else {\n                            int32_t x_11314 = y_8692 - i_11309;\n                            int32_t y_11315 = x_11314 - 1;\n                            int32_t y_11316 = pow32(x_11296, y_11315);\n                            int32_t x_11317 = sdiv32(x_11297, y_11316);\n                            int32_t res_11318 = smod32(x_11317, x_11296);\n                            \n      ",
                   "                      res_11313 = res_11318;\n                        }\n                        \n                        int32_t res_11319 = acc_11306 + res_11313;\n                        \n                        *(__global int32_t *) &mem_11856[(group_id_11447 *\n                                                          (y_8692 *\n                                                           group_sizze_10391) +\n                                                          i_11309 *\n                                                          group_sizze_10391 +\n                                                          local_tid_11446) *\n                                                         4] = res_11319;\n                        \n                        int32_t row_11322 = *(__global\n                                              int32_t *) &mem_11856[(group_id_11447 *\n                                                                     (y_8692 *\n                                                                      group_sizze_10391) +\n                                                                     i_11309 *\n                                                                     group_sizze_10391 +\n                                                                     local_tid_11446) *\n                                                                    4];\n                        int32_t acc_tmp_12528 = row_11322;\n                        \n                        acc_11306 = acc_tmp_12528;\n                    }\n                    discard_11303 = acc_11306;\n                    \n                    int32_t discard_11327;\n                    int32_t acc_11332 = 0;\n                    \n                    for (int32_t i_11336 = 0; i_11336 < y_8692; i_11336++) {\n                        int32_t partition_param_11340 = *(__global\n                                                          int32_t *) &mem_11856[(group_id_11447 *\n                                                       ",
                   "                          (y_8692 *\n                                                                                  group_sizze_10391) +\n                                                                                 i_11336 *\n                                                                                 group_sizze_10391 +\n                                                                                 local_tid_11446) *\n                                                                                4];\n                        char cond_11341 = slt32(i_11336, y_11300);\n                        int32_t res_11342;\n                        \n                        if (cond_11341) {\n                            res_11342 = 0;\n                        } else {\n                            int32_t x_11343 = y_8692 - i_11336;\n                            int32_t y_11344 = x_11343 - 1;\n                            int32_t y_11345 = pow32(x_11296, y_11344);\n                            int32_t x_11346 = sdiv32(x_11297, y_11345);\n                            int32_t res_11347 = smod32(x_11346, x_11296);\n                            \n                            res_11342 = res_11347;\n                        }\n                        \n                        char res_11349 = partition_param_11340 == 0;\n                        int32_t partition_incr_11350;\n                        \n                        if (res_11349) {\n                            partition_incr_11350 = 1;\n                        } else {\n                            partition_incr_11350 = 0;\n                        }\n                        \n                        int32_t zz_11351 = acc_11332 + partition_incr_11350;\n                        \n                        *(__global int32_t *) &mem_11859[(group_id_11447 *\n                                                          (y_8692 *\n                                                           group_sizze_10391) +\n                                                       ",
                   "   i_11336 *\n                                                          group_sizze_10391 +\n                                                          local_tid_11446) *\n                                                         4] = zz_11351;\n                        *(__global int32_t *) &mem_11862[(group_id_11447 *\n                                                          (y_8692 *\n                                                           group_sizze_10391) +\n                                                          i_11336 *\n                                                          group_sizze_10391 +\n                                                          local_tid_11446) *\n                                                         4] = res_11342;\n                        \n                        int32_t row_11356 = *(__global\n                                              int32_t *) &mem_11859[(group_id_11447 *\n                                                                     (y_8692 *\n                                                                      group_sizze_10391) +\n                                                                     i_11336 *\n                                                                     group_sizze_10391 +\n                                                                     local_tid_11446) *\n                                                                    4];\n                        int32_t acc_tmp_12530 = row_11356;\n                        \n                        acc_11332 = acc_tmp_12530;\n                    }\n                    discard_11327 = acc_11332;\n                    \n                    int32_t last_offset_11357 = *(__global\n                                                  int32_t *) &mem_11859[(group_id_11447 *\n                                                                         (y_8692 *\n                                                                          group_sizze_10391) +\n                         ",
                   "                                                last_index_8702 *\n                                                                         group_sizze_10391 +\n                                                                         local_tid_11446) *\n                                                                        4];\n                    char cond_11358 = prime_8379 == x_11296;\n                    int32_t res_11359;\n                    \n                    if (cond_11358) {\n                        res_11359 = y_8692;\n                    } else {\n                        res_11359 = 0;\n                    }\n                    for (int32_t i_12533 = 0; i_12533 < y_8692; i_12533++) {\n                        *(__global int32_t *) &mem_11865[(group_id_11447 *\n                                                          (y_8692 *\n                                                           group_sizze_10391) +\n                                                          i_12533 *\n                                                          group_sizze_10391 +\n                                                          local_tid_11446) *\n                                                         4] = *(__global\n                                                                int32_t *) &mem_11853[(group_id_11447 *\n                                                                                       (y_8692 *\n                                                                                        group_sizze_10391) +\n                                                                                       i_12533 *\n                                                                                       group_sizze_10391 +\n                                                                                       local_tid_11446) *\n                                                                                      4];\n                    }\n                    \n                    int32_t y_1",
                   "1406 = *(__global\n                                        int32_t *) &mem_11862[(group_id_11447 *\n                                                               (y_8692 *\n                                                                group_sizze_10391) +\n                                                               last_offset_11357 *\n                                                               group_sizze_10391 +\n                                                               local_tid_11446) *\n                                                              4];\n                    char res_11362;\n                    char nameless_11366 = 1;\n                    int32_t dummy_chunk_sizze_11363 = 1;\n                    \n                    if (1) {\n                        for (int32_t i_11364 = 0; i_11364 < res_11359;\n                             i_11364++) {\n                            int32_t discard_11368;\n                            int32_t acc_11370 = 0;\n                            \n                            for (int32_t i_11372 = 0; i_11372 < y_8692;\n                                 i_11372++) {\n                                int32_t binop_param_y_11374 = *(__global\n                                                                int32_t *) &mem_11865[(group_id_11447 *\n                                                                                       (y_8692 *\n                                                                                        group_sizze_10391) +\n                                                                                       i_11372 *\n                                                                                       group_sizze_10391 +\n                                                                                       local_tid_11446) *\n                                                                                      4];\n                                int32_t res_11375 = acc_11370 +\n                                 ",
                   "       binop_param_y_11374;\n                                \n                                *(__global\n                                  int32_t *) &mem_11868[(group_id_11447 *\n                                                         (y_8692 *\n                                                          group_sizze_10391) +\n                                                         i_11372 *\n                                                         group_sizze_10391 +\n                                                         local_tid_11446) * 4] =\n                                    res_11375;\n                                \n                                int32_t row_11377 = *(__global\n                                                      int32_t *) &mem_11868[(group_id_11447 *\n                                                                             (y_8692 *\n                                                                              group_sizze_10391) +\n                                                                             i_11372 *\n                                                                             group_sizze_10391 +\n                                                                             local_tid_11446) *\n                                                                            4];\n                                int32_t acc_tmp_12534 = row_11377;\n                                \n                                acc_11370 = acc_tmp_12534;\n                            }\n                            discard_11368 = acc_11370;\n                            \n                            int32_t discard_11379;\n                            int32_t acc_11381 = 0;\n                            \n                            for (int32_t i_11383 = 0; i_11383 < y_8692;\n                                 i_11383++) {\n                                int32_t partition_param_11385 = *(__global\n                                                                  int32",
                   "_t *) &mem_11868[(group_id_11447 *\n                                                                                         (y_8692 *\n                                                                                          group_sizze_10391) +\n                                                                                         i_11383 *\n                                                                                         group_sizze_10391 +\n                                                                                         local_tid_11446) *\n                                                                                        4];\n                                char res_11386 = partition_param_11385 == 0;\n                                int32_t partition_incr_11387;\n                                \n                                if (res_11386) {\n                                    partition_incr_11387 = 1;\n                                } else {\n                                    partition_incr_11387 = 0;\n                                }\n                                \n                                int32_t zz_11388 = acc_11381 +\n                                        partition_incr_11387;\n                                \n                                *(__global\n                                  int32_t *) &mem_11871[(group_id_11447 *\n                                                         (y_8692 *\n                                                          group_sizze_10391) +\n                                                         i_11383 *\n                                                         group_sizze_10391 +\n                                                         local_tid_11446) * 4] =\n                                    zz_11388;\n                                \n                                int32_t row_11390 = *(__global\n                                                      int32_t *) &mem_11871[(group_id_11447 *\n            ",
                   "                                                                 (y_8692 *\n                                                                              group_sizze_10391) +\n                                                                             i_11383 *\n                                                                             group_sizze_10391 +\n                                                                             local_tid_11446) *\n                                                                            4];\n                                int32_t acc_tmp_12536 = row_11390;\n                                \n                                acc_11381 = acc_tmp_12536;\n                            }\n                            discard_11379 = acc_11381;\n                            \n                            int32_t last_offset_11391 = *(__global\n                                                          int32_t *) &mem_11871[(group_id_11447 *\n                                                                                 (y_8692 *\n                                                                                  group_sizze_10391) +\n                                                                                 last_index_8702 *\n                                                                                 group_sizze_10391 +\n                                                                                 local_tid_11446) *\n                                                                                4];\n                            char res_11392 = slt32(last_offset_11357,\n                                                   last_offset_11391);\n                            int32_t x_11395;\n                            int32_t acc_11397 = 0;\n                            \n                            for (int32_t i_11398 = 0; i_11398 < y_8692;\n                                 i_11398++) {\n                                int32_t binop_param_y_11400 = *(__",
                   "global\n                                                                int32_t *) &mem_11865[(group_id_11447 *\n                                                                                       (y_8692 *\n                                                                                        group_sizze_10391) +\n                                                                                       i_11398 *\n                                                                                       group_sizze_10391 +\n                                                                                       local_tid_11446) *\n                                                                                      4];\n                                int32_t res_11401 = acc_11397 +\n                                        binop_param_y_11400;\n                                int32_t acc_tmp_12538 = res_11401;\n                                \n                                acc_11397 = acc_tmp_12538;\n                            }\n                            x_11395 = acc_11397;\n                            \n                            char res_11402 = x_11395 == 0;\n                            int32_t res_11403 = last_offset_11357 -\n                                    last_offset_11391;\n                            int32_t res_11404;\n                            \n                            if (res_11402) {\n                                res_11404 = 0;\n                            } else {\n                                int32_t res_11405 = *(__global\n                                                      int32_t *) &mem_11865[(group_id_11447 *\n                                                                             (y_8692 *\n                                                                              group_sizze_10391) +\n                                                                             last_offset_11391 *\n                                                                ",
                   "             group_sizze_10391 +\n                                                                             local_tid_11446) *\n                                                                            4];\n                                \n                                res_11404 = res_11405;\n                            }\n                            \n                            int32_t res_11407 = sdiv32(res_11404, y_11406);\n                            int32_t res_11408;\n                            \n                            if (res_11392) {\n                                res_11408 = 0;\n                            } else {\n                                res_11408 = res_11407;\n                            }\n                            for (int32_t i_11413 = 0; i_11413 < y_8692;\n                                 i_11413++) {\n                                int32_t i_p_o_11415 = i_11413 + res_11403;\n                                int32_t rot_i_11416 = smod32(i_p_o_11415,\n                                                             y_8692);\n                                int32_t x_11417 = *(__global\n                                                    int32_t *) &mem_11862[(group_id_11447 *\n                                                                           (y_8692 *\n                                                                            group_sizze_10391) +\n                                                                           rot_i_11416 *\n                                                                           group_sizze_10391 +\n                                                                           local_tid_11446) *\n                                                                          4];\n                                int32_t res_11418 = x_11417 * res_11408;\n                                int32_t x_11419 = *(__global\n                                                    int32_t *) &mem_11865[(group_id_11447 *\n                        ",
                   "                                                   (y_8692 *\n                                                                            group_sizze_10391) +\n                                                                           i_11413 *\n                                                                           group_sizze_10391 +\n                                                                           local_tid_11446) *\n                                                                          4];\n                                int32_t x_11420 = x_11419 - res_11418;\n                                int32_t res_11421 = smod32(x_11420, prime_8379);\n                                \n                                *(__global\n                                  int32_t *) &mem_11874[(group_id_11447 *\n                                                         (y_8692 *\n                                                          group_sizze_10391) +\n                                                         i_11413 *\n                                                         group_sizze_10391 +\n                                                         local_tid_11446) * 4] =\n                                    res_11421;\n                            }\n                            \n                            char res_11423 = !res_11402;\n                            char x_11424 = res_11392 && res_11423;\n                            \n                            for (int32_t i_12540 = 0; i_12540 < y_8692;\n                                 i_12540++) {\n                                *(__global\n                                  int32_t *) &mem_11865[(group_id_11447 *\n                                                         (y_8692 *\n                                                          group_sizze_10391) +\n                                                         i_12540 *\n                                                         group_sizze_10391 +\n                                 ",
                   "                        local_tid_11446) * 4] =\n                                    *(__global\n                                      int32_t *) &mem_11874[(group_id_11447 *\n                                                             (y_8692 *\n                                                              group_sizze_10391) +\n                                                             i_12540 *\n                                                             group_sizze_10391 +\n                                                             local_tid_11446) *\n                                                            4];\n                            }\n                            nameless_11366 = x_11424;\n                        }\n                    }\n                    res_11362 = nameless_11366;\n                    \n                    char x_11425 = binop_param_x_11284 && res_11362;\n                    char binop_param_x_tmp_12527 = x_11425;\n                    \n                    binop_param_x_11284 = binop_param_x_tmp_12527;\n                }\n                res_11281 = binop_param_x_11284;\n                \n                int32_t eq_class_11426;\n                int32_t partition_incr_11427;\n                \n                if (res_11281) {\n                    eq_class_11426 = 0;\n                    partition_incr_11427 = 1;\n                } else {\n                    eq_class_11426 = 1;\n                    partition_incr_11427 = 0;\n                }\n                \n                int32_t zz_11428 = x_merge_11451 + partition_incr_11427;\n                \n                *(__global int32_t *) &mem_11845[j_11457 * 4] = eq_class_11426;\n                *(__global char *) &mem_11847[j_11457] = res_11281;\n                *(__global int32_t *) &mem_11850[j_11457 * 4] = res_11269;\n                foldres_11463 = zz_11428;\n            } else {\n                foldres_11463 = x_merge_11451;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_",
                   "tid_11446, group_sizze_10391) && 1) {\n                *(__local int32_t *) &mem_11878[local_tid_11446 * 4] =\n                    foldres_11463;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_11429;\n            int32_t other_index_11430;\n            int32_t x_11431;\n            int32_t y_11432;\n            int32_t my_index_12541;\n            int32_t other_index_12542;\n            int32_t x_12543;\n            int32_t y_12544;\n            \n            my_index_11429 = local_tid_11446;\n            if (slt32(local_tid_11446, group_sizze_10391)) {\n                y_11432 = *(volatile __local\n                            int32_t *) &mem_11878[local_tid_11446 *\n                                                  sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12546 = 1;\n                \n                while (slt32(skip_threads_12546, 32)) {\n                    if (slt32(local_tid_11446, group_sizze_10391) &&\n                        sle32(skip_threads_12546, local_tid_11446 -\n                              squot32(local_tid_11446, 32) * 32)) {\n                        // read operands\n                        {\n                            x_11431 = *(volatile __local\n                                        int32_t *) &mem_11878[(local_tid_11446 -\n                                                               skip_threads_12546) *\n                                                              sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_11433 = x_11431 + y_11432;\n                            \n                            y_11432 = zz_11433;\n                        }\n                    }\n                    if (sle32(wave_sizze_12518, skip_threads_12546)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n ",
                   "                   if (slt32(local_tid_11446, group_sizze_10391) &&\n                        sle32(skip_threads_12546, local_tid_11446 -\n                              squot32(local_tid_11446, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11878[local_tid_11446 *\n                                                    sizeof(int32_t)] = y_11432;\n                        }\n                    }\n                    if (sle32(wave_sizze_12518, skip_threads_12546)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12546 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_11446 - squot32(local_tid_11446, 32) * 32) ==\n                    31 && slt32(local_tid_11446, group_sizze_10391)) {\n                    *(volatile __local\n                      int32_t *) &mem_11878[squot32(local_tid_11446, 32) *\n                                            sizeof(int32_t)] = y_11432;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_11446, 32) == 0 && slt32(local_tid_11446,\n                                                               group_sizze_10391)) {\n                    y_12544 = *(volatile __local\n                                int32_t *) &mem_11878[local_tid_11446 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12547 = 1;\n                    \n                    while (slt32(skip_threads_12547, 32)) {\n                        if ",
                   "((squot32(local_tid_11446, 32) == 0 &&\n                             slt32(local_tid_11446, group_sizze_10391)) &&\n                            sle32(skip_threads_12547, local_tid_11446 -\n                                  squot32(local_tid_11446, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12543 = *(volatile __local\n                                            int32_t *) &mem_11878[(local_tid_11446 -\n                                                                   skip_threads_12547) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12545 = x_12543 + y_12544;\n                                \n                                y_12544 = zz_12545;\n                            }\n                        }\n                        if (sle32(wave_sizze_12518, skip_threads_12547)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_11446, 32) == 0 &&\n                             slt32(local_tid_11446, group_sizze_10391)) &&\n                            sle32(skip_threads_12547, local_tid_11446 -\n                                  squot32(local_tid_11446, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11878[local_tid_11446 *\n                                                        sizeof(int32_t)] =\n                                    y_12544;\n                            }\n                        }\n                        if (sle32(wave_sizze_12518, skip_threads_12547)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12547 *= 2;\n ",
                   "                   }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_11446, 32) == 0 ||\n                      !slt32(local_tid_11446, group_sizze_10391))) {\n                    // read operands\n                    {\n                        x_11431 = *(volatile __local\n                                    int32_t *) &mem_11878[(squot32(local_tid_11446,\n                                                                   32) - 1) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_11433 = x_11431 + y_11432;\n                        \n                        y_11432 = zz_11433;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11878[local_tid_11446 *\n                                                sizeof(int32_t)] = y_11432;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_11446, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11878[local_tid_11446 *\n                                                             sizeof(int32_t)] =\n                        y_11432;\n                }\n            }\n            if (cond_11458) {\n                int32_t scanned_elem_11470 = *(__local\n                                               int32_t *) &mem_11878[local_tid_11446 *\n                                                                     4];\n                \n                *(__global int32_t *) &mem_11842[j_11457 * 4] =\n                    scanned_elem_11470;\n            }\n            \n            int32_t new_carry_1",
                   "1475;\n            \n            if (is_first_thread_11473) {\n                int32_t carry_11474 = *(__local int32_t *) &mem_11878[y_10393 *\n                                                                      4];\n                \n                new_carry_11475 = carry_11474;\n            } else {\n                new_carry_11475 = 0;\n            }\n            \n            int32_t x_merge_tmp_12525 = new_carry_11475;\n            \n            x_merge_11451 = x_merge_tmp_12525;\n        }\n        result_11480 = x_merge_11451;\n    }\n    if (local_tid_11446 == 0) {\n        *(__global int32_t *) &mem_11881[group_id_11447 * 4] = result_11480;\n    }\n}\n__kernel void scan1_kernel_9011(__local volatile int64_t *mem_aligned_0,\n                                int32_t arg_7931, int32_t y_7938,\n                                int32_t num_iterations_9016, int32_t y_9019,\n                                __global unsigned char *mem_11614, __global\n                                unsigned char *mem_11617, __global\n                                unsigned char *mem_11623)\n{\n    __local volatile char *restrict mem_11620 = mem_aligned_0;\n    int32_t wave_sizze_11971;\n    int32_t group_sizze_11972;\n    char thread_active_11973;\n    int32_t global_tid_9011;\n    int32_t local_tid_9012;\n    int32_t group_id_9013;\n    \n    global_tid_9011 = get_global_id(0);\n    local_tid_9012 = get_local_id(0);\n    group_sizze_11972 = get_local_size(0);\n    wave_sizze_11971 = LOCKSTEP_WIDTH;\n    group_id_9013 = get_group_id(0);\n    thread_active_11973 = 1;\n    \n    int32_t x_9020;\n    char is_first_thread_9035;\n    int32_t result_9040;\n    \n    if (thread_active_11973) {\n        x_9020 = group_id_9013 * y_9019;\n        is_first_thread_9035 = local_tid_9012 == 0;\n        \n        int32_t x_merge_9017 = 0;\n        \n        for (int32_t i_9018 = 0; i_9018 < num_iterations_9016; i_9018++) {\n            int32_t y_9021 = i_9018 * group_sizze_8979;\n            int32_t offset_9022 = x_9020 + y_9021;\n            int32",
                   "_t j_9023 = offset_9022 + local_tid_9012;\n            char cond_9024 = slt32(j_9023, arg_7931);\n            int32_t foldres_9027;\n            \n            if (cond_9024) {\n                char res_8995 = sle32(y_7938, j_9023);\n                int32_t eq_class_8996;\n                int32_t partition_incr_8997;\n                \n                if (res_8995) {\n                    eq_class_8996 = 0;\n                    partition_incr_8997 = 1;\n                } else {\n                    eq_class_8996 = 1;\n                    partition_incr_8997 = 0;\n                }\n                \n                int32_t zz_8998 = x_merge_9017 + partition_incr_8997;\n                \n                *(__global int32_t *) &mem_11617[j_9023 * 4] = eq_class_8996;\n                foldres_9027 = zz_8998;\n            } else {\n                foldres_9027 = x_merge_9017;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_9012, group_sizze_8979) && 1) {\n                *(__local int32_t *) &mem_11620[local_tid_9012 * 4] =\n                    foldres_9027;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_8999;\n            int32_t other_index_9000;\n            int32_t x_9001;\n            int32_t y_9002;\n            int32_t my_index_11977;\n            int32_t other_index_11978;\n            int32_t x_11979;\n            int32_t y_11980;\n            \n            my_index_8999 = local_tid_9012;\n            if (slt32(local_tid_9012, group_sizze_8979)) {\n                y_9002 = *(volatile __local\n                           int32_t *) &mem_11620[local_tid_9012 *\n                                                 sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_11982 = 1;\n                \n                while (slt32(skip_threads_11982, 32)) {\n                    if (slt32(local_tid_9012, group_sizze_8979) &&\n                 ",
                   "       sle32(skip_threads_11982, local_tid_9012 -\n                              squot32(local_tid_9012, 32) * 32)) {\n                        // read operands\n                        {\n                            x_9001 = *(volatile __local\n                                       int32_t *) &mem_11620[(local_tid_9012 -\n                                                              skip_threads_11982) *\n                                                             sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_9003 = x_9001 + y_9002;\n                            \n                            y_9002 = zz_9003;\n                        }\n                    }\n                    if (sle32(wave_sizze_11971, skip_threads_11982)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_9012, group_sizze_8979) &&\n                        sle32(skip_threads_11982, local_tid_9012 -\n                              squot32(local_tid_9012, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11620[local_tid_9012 *\n                                                    sizeof(int32_t)] = y_9002;\n                        }\n                    }\n                    if (sle32(wave_sizze_11971, skip_threads_11982)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_11982 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_9012 - squot32(local_tid_9012, 32) * 32) == 31 &&\n                    slt32(local_tid_9012, group_sizze_8979)) {\n                    *(volatile __local\n                      int32_t *) &mem_11620",
                   "[squot32(local_tid_9012, 32) *\n                                            sizeof(int32_t)] = y_9002;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_9012, 32) == 0 && slt32(local_tid_9012,\n                                                              group_sizze_8979)) {\n                    y_11980 = *(volatile __local\n                                int32_t *) &mem_11620[local_tid_9012 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_11983 = 1;\n                    \n                    while (slt32(skip_threads_11983, 32)) {\n                        if ((squot32(local_tid_9012, 32) == 0 &&\n                             slt32(local_tid_9012, group_sizze_8979)) &&\n                            sle32(skip_threads_11983, local_tid_9012 -\n                                  squot32(local_tid_9012, 32) * 32)) {\n                            // read operands\n                            {\n                                x_11979 = *(volatile __local\n                                            int32_t *) &mem_11620[(local_tid_9012 -\n                                                                   skip_threads_11983) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_11981 = x_11979 + y_11980;\n                                \n                                y_11980 = zz_11981;\n                            }\n                        }\n                        if (sle32(wave_sizze_11971, skip_threads_11983)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n     ",
                   "                   }\n                        if ((squot32(local_tid_9012, 32) == 0 &&\n                             slt32(local_tid_9012, group_sizze_8979)) &&\n                            sle32(skip_threads_11983, local_tid_9012 -\n                                  squot32(local_tid_9012, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11620[local_tid_9012 *\n                                                        sizeof(int32_t)] =\n                                    y_11980;\n                            }\n                        }\n                        if (sle32(wave_sizze_11971, skip_threads_11983)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_11983 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_9012, 32) == 0 || !slt32(local_tid_9012,\n                                                                 group_sizze_8979))) {\n                    // read operands\n                    {\n                        x_9001 = *(volatile __local\n                                   int32_t *) &mem_11620[(squot32(local_tid_9012,\n                                                                  32) - 1) *\n                                                         sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_9003 = x_9001 + y_9002;\n                        \n                        y_9002 = zz_9003;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11620[local_tid_9012 *\n                                ",
                   "                sizeof(int32_t)] = y_9002;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_9012, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11620[local_tid_9012 *\n                                                             sizeof(int32_t)] =\n                        y_9002;\n                }\n            }\n            if (cond_9024) {\n                int32_t scanned_elem_9032 = *(__local\n                                              int32_t *) &mem_11620[local_tid_9012 *\n                                                                    4];\n                \n                *(__global int32_t *) &mem_11614[j_9023 * 4] =\n                    scanned_elem_9032;\n            }\n            \n            int32_t new_carry_9037;\n            \n            if (is_first_thread_9035) {\n                int32_t carry_9036 = *(__local int32_t *) &mem_11620[y_8981 *\n                                                                     4];\n                \n                new_carry_9037 = carry_9036;\n            } else {\n                new_carry_9037 = 0;\n            }\n            \n            int32_t x_merge_tmp_11976 = new_carry_9037;\n            \n            x_merge_9017 = x_merge_tmp_11976;\n        }\n        result_9040 = x_merge_9017;\n    }\n    if (local_tid_9012 == 0) {\n        *(__global int32_t *) &mem_11623[group_id_9013 * 4] = result_9040;\n    }\n}\n__kernel void scan1_kernel_9227(__local volatile int64_t *mem_aligned_0,\n                                int32_t arg_8028, int32_t y_8035,\n                                int32_t num_iterations_9232, int32_t y_9235,\n                                __global unsigned char *mem_11614, __global\n                                unsigned char *mem_11617, __global\n                                unsigned char *mem_11623)\n{\n    __local volatile char *restrict mem_116",
                   "20 = mem_aligned_0;\n    int32_t wave_sizze_12021;\n    int32_t group_sizze_12022;\n    char thread_active_12023;\n    int32_t global_tid_9227;\n    int32_t local_tid_9228;\n    int32_t group_id_9229;\n    \n    global_tid_9227 = get_global_id(0);\n    local_tid_9228 = get_local_id(0);\n    group_sizze_12022 = get_local_size(0);\n    wave_sizze_12021 = LOCKSTEP_WIDTH;\n    group_id_9229 = get_group_id(0);\n    thread_active_12023 = 1;\n    \n    int32_t x_9236;\n    char is_first_thread_9251;\n    int32_t result_9256;\n    \n    if (thread_active_12023) {\n        x_9236 = group_id_9229 * y_9235;\n        is_first_thread_9251 = local_tid_9228 == 0;\n        \n        int32_t x_merge_9233 = 0;\n        \n        for (int32_t i_9234 = 0; i_9234 < num_iterations_9232; i_9234++) {\n            int32_t y_9237 = i_9234 * group_sizze_9195;\n            int32_t offset_9238 = x_9236 + y_9237;\n            int32_t j_9239 = offset_9238 + local_tid_9228;\n            char cond_9240 = slt32(j_9239, arg_8028);\n            int32_t foldres_9243;\n            \n            if (cond_9240) {\n                char res_9211 = sle32(y_8035, j_9239);\n                int32_t eq_class_9212;\n                int32_t partition_incr_9213;\n                \n                if (res_9211) {\n                    eq_class_9212 = 0;\n                    partition_incr_9213 = 1;\n                } else {\n                    eq_class_9212 = 1;\n                    partition_incr_9213 = 0;\n                }\n                \n                int32_t zz_9214 = x_merge_9233 + partition_incr_9213;\n                \n                *(__global int32_t *) &mem_11617[j_9239 * 4] = eq_class_9212;\n                foldres_9243 = zz_9214;\n            } else {\n                foldres_9243 = x_merge_9233;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_9228, group_sizze_9195) && 1) {\n                *(__local int32_t *) &mem_11620[local_tid_9228 * 4] =\n                    foldres_9243;\n            }\n            barr",
                   "ier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_9215;\n            int32_t other_index_9216;\n            int32_t x_9217;\n            int32_t y_9218;\n            int32_t my_index_12027;\n            int32_t other_index_12028;\n            int32_t x_12029;\n            int32_t y_12030;\n            \n            my_index_9215 = local_tid_9228;\n            if (slt32(local_tid_9228, group_sizze_9195)) {\n                y_9218 = *(volatile __local\n                           int32_t *) &mem_11620[local_tid_9228 *\n                                                 sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12032 = 1;\n                \n                while (slt32(skip_threads_12032, 32)) {\n                    if (slt32(local_tid_9228, group_sizze_9195) &&\n                        sle32(skip_threads_12032, local_tid_9228 -\n                              squot32(local_tid_9228, 32) * 32)) {\n                        // read operands\n                        {\n                            x_9217 = *(volatile __local\n                                       int32_t *) &mem_11620[(local_tid_9228 -\n                                                              skip_threads_12032) *\n                                                             sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_9219 = x_9217 + y_9218;\n                            \n                            y_9218 = zz_9219;\n                        }\n                    }\n                    if (sle32(wave_sizze_12021, skip_threads_12032)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_9228, group_sizze_9195) &&\n                        sle32(skip_threads_12032, local_tid_9228 -\n                              squot32(local_tid_9228, 32) * 32)) {\n",
                   "                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11620[local_tid_9228 *\n                                                    sizeof(int32_t)] = y_9218;\n                        }\n                    }\n                    if (sle32(wave_sizze_12021, skip_threads_12032)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12032 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_9228 - squot32(local_tid_9228, 32) * 32) == 31 &&\n                    slt32(local_tid_9228, group_sizze_9195)) {\n                    *(volatile __local\n                      int32_t *) &mem_11620[squot32(local_tid_9228, 32) *\n                                            sizeof(int32_t)] = y_9218;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_9228, 32) == 0 && slt32(local_tid_9228,\n                                                              group_sizze_9195)) {\n                    y_12030 = *(volatile __local\n                                int32_t *) &mem_11620[local_tid_9228 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12033 = 1;\n                    \n                    while (slt32(skip_threads_12033, 32)) {\n                        if ((squot32(local_tid_9228, 32) == 0 &&\n                             slt32(local_tid_9228, group_sizze_9195)) &&\n                            sle32(skip_threads_12033, local_tid_9228 -\n                                  s",
                   "quot32(local_tid_9228, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12029 = *(volatile __local\n                                            int32_t *) &mem_11620[(local_tid_9228 -\n                                                                   skip_threads_12033) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12031 = x_12029 + y_12030;\n                                \n                                y_12030 = zz_12031;\n                            }\n                        }\n                        if (sle32(wave_sizze_12021, skip_threads_12033)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_9228, 32) == 0 &&\n                             slt32(local_tid_9228, group_sizze_9195)) &&\n                            sle32(skip_threads_12033, local_tid_9228 -\n                                  squot32(local_tid_9228, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11620[local_tid_9228 *\n                                                        sizeof(int32_t)] =\n                                    y_12030;\n                            }\n                        }\n                        if (sle32(wave_sizze_12021, skip_threads_12033)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12033 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_9228, 32) == 0 || !slt3",
                   "2(local_tid_9228,\n                                                                 group_sizze_9195))) {\n                    // read operands\n                    {\n                        x_9217 = *(volatile __local\n                                   int32_t *) &mem_11620[(squot32(local_tid_9228,\n                                                                  32) - 1) *\n                                                         sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_9219 = x_9217 + y_9218;\n                        \n                        y_9218 = zz_9219;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11620[local_tid_9228 *\n                                                sizeof(int32_t)] = y_9218;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_9228, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11620[local_tid_9228 *\n                                                             sizeof(int32_t)] =\n                        y_9218;\n                }\n            }\n            if (cond_9240) {\n                int32_t scanned_elem_9248 = *(__local\n                                              int32_t *) &mem_11620[local_tid_9228 *\n                                                                    4];\n                \n                *(__global int32_t *) &mem_11614[j_9239 * 4] =\n                    scanned_elem_9248;\n            }\n            \n            int32_t new_carry_9253;\n            \n            if (is_first_thread_9251) {\n                int32_t carry_9252 = *(__local int32_t *) &mem_11620[y_9197 *\n                                                                     4];\n  ",
                   "              \n                new_carry_9253 = carry_9252;\n            } else {\n                new_carry_9253 = 0;\n            }\n            \n            int32_t x_merge_tmp_12026 = new_carry_9253;\n            \n            x_merge_9233 = x_merge_tmp_12026;\n        }\n        result_9256 = x_merge_9233;\n    }\n    if (local_tid_9228 == 0) {\n        *(__global int32_t *) &mem_11623[group_id_9229 * 4] = result_9256;\n    }\n}\n__kernel void scan1_kernel_9396(__local volatile int64_t *mem_aligned_0,\n                                int32_t bb_8108, int32_t bb_8109,\n                                int32_t aa_8111, int32_t aa_8112,\n                                int32_t padding_8114, int32_t y_8121,\n                                int32_t y_8123, int32_t num_iterations_9401,\n                                int32_t y_9404, __global\n                                unsigned char *mem_11614, __global\n                                unsigned char *mem_11617, __global\n                                unsigned char *mem_11620, __global\n                                unsigned char *mem_11626)\n{\n    __local volatile char *restrict mem_11623 = mem_aligned_0;\n    int32_t wave_sizze_12067;\n    int32_t group_sizze_12068;\n    char thread_active_12069;\n    int32_t global_tid_9396;\n    int32_t local_tid_9397;\n    int32_t group_id_9398;\n    \n    global_tid_9396 = get_global_id(0);\n    local_tid_9397 = get_local_id(0);\n    group_sizze_12068 = get_local_size(0);\n    wave_sizze_12067 = LOCKSTEP_WIDTH;\n    group_id_9398 = get_group_id(0);\n    thread_active_12069 = 1;\n    \n    int32_t x_9405;\n    char is_first_thread_9422;\n    int32_t result_9428;\n    \n    if (thread_active_12069) {\n        x_9405 = group_id_9398 * y_9404;\n        is_first_thread_9422 = local_tid_9397 == 0;\n        \n        int32_t binop_param_x_merge_9402 = 0;\n        \n        for (int32_t i_9403 = 0; i_9403 < num_iterations_9401; i_9403++) {\n            int32_t y_9406 = i_9403 * group_sizze_9351;\n            int32_t offset_9407",
                   " = x_9405 + y_9406;\n            int32_t j_9408 = offset_9407 + local_tid_9397;\n            char cond_9409 = slt32(j_9408, padding_8114);\n            int32_t foldres_9413;\n            \n            if (cond_9409) {\n                char cond_9367 = slt32(j_9408, y_8121);\n                int32_t res_9368;\n                \n                if (cond_9367) {\n                    res_9368 = 0;\n                } else {\n                    int32_t x_9369 = padding_8114 - j_9408;\n                    int32_t y_9370 = x_9369 - 1;\n                    int32_t y_9371 = pow32(aa_8111, y_9370);\n                    int32_t x_9372 = sdiv32(aa_8112, y_9371);\n                    int32_t res_9373 = smod32(x_9372, aa_8111);\n                    \n                    res_9368 = res_9373;\n                }\n                \n                char cond_9374 = slt32(j_9408, y_8123);\n                int32_t res_9375;\n                \n                if (cond_9374) {\n                    res_9375 = 0;\n                } else {\n                    int32_t x_9376 = padding_8114 - j_9408;\n                    int32_t y_9377 = x_9376 - 1;\n                    int32_t y_9378 = pow32(bb_8108, y_9377);\n                    int32_t x_9379 = sdiv32(bb_8109, y_9378);\n                    int32_t res_9380 = smod32(x_9379, bb_8108);\n                    \n                    res_9375 = res_9380;\n                }\n                \n                int32_t res_9381 = binop_param_x_merge_9402 + res_9368;\n                \n                *(__global int32_t *) &mem_11617[j_9408 * 4] = res_9375;\n                *(__global int32_t *) &mem_11620[j_9408 * 4] = res_9368;\n                foldres_9413 = res_9381;\n            } else {\n                foldres_9413 = binop_param_x_merge_9402;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_9397, group_sizze_9351) && 1) {\n                *(__local int32_t *) &mem_11623[local_tid_9397 * 4] =\n                    foldres_9413;\n            }\n           ",
                   " barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_9382;\n            int32_t other_index_9383;\n            int32_t binop_param_x_9384;\n            int32_t binop_param_y_9385;\n            int32_t my_index_12074;\n            int32_t other_index_12075;\n            int32_t binop_param_x_12076;\n            int32_t binop_param_y_12077;\n            \n            my_index_9382 = local_tid_9397;\n            if (slt32(local_tid_9397, group_sizze_9351)) {\n                binop_param_y_9385 = *(volatile __local\n                                       int32_t *) &mem_11623[local_tid_9397 *\n                                                             sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12079 = 1;\n                \n                while (slt32(skip_threads_12079, 32)) {\n                    if (slt32(local_tid_9397, group_sizze_9351) &&\n                        sle32(skip_threads_12079, local_tid_9397 -\n                              squot32(local_tid_9397, 32) * 32)) {\n                        // read operands\n                        {\n                            binop_param_x_9384 = *(volatile __local\n                                                   int32_t *) &mem_11623[(local_tid_9397 -\n                                                                          skip_threads_12079) *\n                                                                         sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t res_9386 = binop_param_x_9384 +\n                                    binop_param_y_9385;\n                            \n                            binop_param_y_9385 = res_9386;\n                        }\n                    }\n                    if (sle32(wave_sizze_12067, skip_threads_12079)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n            ",
                   "        }\n                    if (slt32(local_tid_9397, group_sizze_9351) &&\n                        sle32(skip_threads_12079, local_tid_9397 -\n                              squot32(local_tid_9397, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11623[local_tid_9397 *\n                                                    sizeof(int32_t)] =\n                                binop_param_y_9385;\n                        }\n                    }\n                    if (sle32(wave_sizze_12067, skip_threads_12079)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12079 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_9397 - squot32(local_tid_9397, 32) * 32) == 31 &&\n                    slt32(local_tid_9397, group_sizze_9351)) {\n                    *(volatile __local\n                      int32_t *) &mem_11623[squot32(local_tid_9397, 32) *\n                                            sizeof(int32_t)] =\n                        binop_param_y_9385;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_9397, 32) == 0 && slt32(local_tid_9397,\n                                                              group_sizze_9351)) {\n                    binop_param_y_12077 = *(volatile __local\n                                            int32_t *) &mem_11623[local_tid_9397 *\n                                                                  sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12080 = 1",
                   ";\n                    \n                    while (slt32(skip_threads_12080, 32)) {\n                        if ((squot32(local_tid_9397, 32) == 0 &&\n                             slt32(local_tid_9397, group_sizze_9351)) &&\n                            sle32(skip_threads_12080, local_tid_9397 -\n                                  squot32(local_tid_9397, 32) * 32)) {\n                            // read operands\n                            {\n                                binop_param_x_12076 = *(volatile __local\n                                                        int32_t *) &mem_11623[(local_tid_9397 -\n                                                                               skip_threads_12080) *\n                                                                              sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t res_12078 = binop_param_x_12076 +\n                                        binop_param_y_12077;\n                                \n                                binop_param_y_12077 = res_12078;\n                            }\n                        }\n                        if (sle32(wave_sizze_12067, skip_threads_12080)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_9397, 32) == 0 &&\n                             slt32(local_tid_9397, group_sizze_9351)) &&\n                            sle32(skip_threads_12080, local_tid_9397 -\n                                  squot32(local_tid_9397, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11623[local_tid_9397 *\n                                                        sizeof(int32_t)] =\n                                    binop_param_y_12077;\n                            }",
                   "\n                        }\n                        if (sle32(wave_sizze_12067, skip_threads_12080)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12080 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_9397, 32) == 0 || !slt32(local_tid_9397,\n                                                                 group_sizze_9351))) {\n                    // read operands\n                    {\n                        binop_param_x_9384 = *(volatile __local\n                                               int32_t *) &mem_11623[(squot32(local_tid_9397,\n                                                                              32) -\n                                                                      1) *\n                                                                     sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_9386 = binop_param_x_9384 +\n                                binop_param_y_9385;\n                        \n                        binop_param_y_9385 = res_9386;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11623[local_tid_9397 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_9385;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_9397, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11623[local_tid_9397 *\n                                                             sizeof(int32_t)] =",
                   "\n                        binop_param_y_9385;\n                }\n            }\n            if (cond_9409) {\n                int32_t scanned_elem_9419 = *(__local\n                                              int32_t *) &mem_11623[local_tid_9397 *\n                                                                    4];\n                \n                *(__global int32_t *) &mem_11614[j_9408 * 4] =\n                    scanned_elem_9419;\n            }\n            \n            int32_t new_carry_9424;\n            \n            if (is_first_thread_9422) {\n                int32_t carry_9423 = *(__local int32_t *) &mem_11623[y_9353 *\n                                                                     4];\n                \n                new_carry_9424 = carry_9423;\n            } else {\n                new_carry_9424 = 0;\n            }\n            \n            int32_t binop_param_x_merge_tmp_12073 = new_carry_9424;\n            \n            binop_param_x_merge_9402 = binop_param_x_merge_tmp_12073;\n        }\n        result_9428 = binop_param_x_merge_9402;\n    }\n    if (local_tid_9397 == 0) {\n        *(__global int32_t *) &mem_11626[group_id_9398 * 4] = result_9428;\n    }\n}\n__kernel void scan1_kernel_9520(__local volatile int64_t *mem_aligned_0,\n                                __local volatile int64_t *mem_aligned_1,\n                                int32_t bb_8108, int32_t bb_8109,\n                                int32_t aa_8111, int32_t aa_8112,\n                                int32_t padding_8114, int32_t y_8121,\n                                int32_t y_8123, int32_t num_iterations_9401,\n                                int32_t y_9404, __global\n                                unsigned char *mem_11635, __global\n                                unsigned char *mem_11638, __global\n                                unsigned char *mem_11641, __global\n                                unsigned char *mem_11644, __global\n                                unsigned char *mem_11647, __global\n     ",
                   "                           unsigned char *mem_11656, __global\n                                unsigned char *mem_11659)\n{\n    __local volatile char *restrict mem_11650 = mem_aligned_0;\n    __local volatile char *restrict mem_11653 = mem_aligned_1;\n    int32_t wave_sizze_12094;\n    int32_t group_sizze_12095;\n    char thread_active_12096;\n    int32_t global_tid_9520;\n    int32_t local_tid_9521;\n    int32_t group_id_9522;\n    \n    global_tid_9520 = get_global_id(0);\n    local_tid_9521 = get_local_id(0);\n    group_sizze_12095 = get_local_size(0);\n    wave_sizze_12094 = LOCKSTEP_WIDTH;\n    group_id_9522 = get_group_id(0);\n    thread_active_12096 = 1;\n    \n    int32_t x_9530;\n    char is_first_thread_9554;\n    int32_t result_9563;\n    int32_t result_9564;\n    \n    if (thread_active_12096) {\n        x_9530 = group_id_9522 * y_9404;\n        is_first_thread_9554 = local_tid_9521 == 0;\n        \n        int32_t binop_param_x_merge_9526;\n        int32_t x_merge_9527;\n        \n        binop_param_x_merge_9526 = 0;\n        x_merge_9527 = 0;\n        for (int32_t i_9528 = 0; i_9528 < num_iterations_9401; i_9528++) {\n            int32_t y_9531 = i_9528 * group_sizze_9351;\n            int32_t offset_9532 = x_9530 + y_9531;\n            int32_t j_9533 = offset_9532 + local_tid_9521;\n            char cond_9534 = slt32(j_9533, padding_8114);\n            int32_t foldres_9539;\n            int32_t foldres_9540;\n            \n            if (cond_9534) {\n                int32_t res_elem_9536 = *(__global\n                                          int32_t *) &mem_11635[j_9533 * 4];\n                char cond_9481 = slt32(j_9533, y_8121);\n                int32_t res_9482;\n                \n                if (cond_9481) {\n                    res_9482 = 0;\n                } else {\n                    int32_t x_9483 = padding_8114 - j_9533;\n                    int32_t y_9484 = x_9483 - 1;\n                    int32_t y_9485 = pow32(aa_8111, y_9484);\n                    int32_t x_9486 = sdiv32(aa_8112",
                   ", y_9485);\n                    int32_t res_9487 = smod32(x_9486, aa_8111);\n                    \n                    res_9482 = res_9487;\n                }\n                \n                char cond_9488 = slt32(j_9533, y_8123);\n                int32_t res_9489;\n                \n                if (cond_9488) {\n                    res_9489 = 0;\n                } else {\n                    int32_t x_9490 = padding_8114 - j_9533;\n                    int32_t y_9491 = x_9490 - 1;\n                    int32_t y_9492 = pow32(bb_8108, y_9491);\n                    int32_t x_9493 = sdiv32(bb_8109, y_9492);\n                    int32_t res_9494 = smod32(x_9493, bb_8108);\n                    \n                    res_9489 = res_9494;\n                }\n                \n                int32_t res_9495 = binop_param_x_merge_9526 + res_9482;\n                char res_9496 = res_elem_9536 == 0;\n                int32_t partition_incr_9497;\n                \n                if (res_9496) {\n                    partition_incr_9497 = 1;\n                } else {\n                    partition_incr_9497 = 0;\n                }\n                \n                int32_t zz_9498 = x_merge_9527 + partition_incr_9497;\n                \n                *(__global int32_t *) &mem_11644[j_9533 * 4] = res_9489;\n                *(__global int32_t *) &mem_11647[j_9533 * 4] = res_9482;\n                foldres_9539 = res_9495;\n                foldres_9540 = zz_9498;\n            } else {\n                foldres_9539 = binop_param_x_merge_9526;\n                foldres_9540 = x_merge_9527;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_9521, group_sizze_9351) && 1) {\n                *(__local int32_t *) &mem_11650[local_tid_9521 * 4] =\n                    foldres_9539;\n                *(__local int32_t *) &mem_11653[local_tid_9521 * 4] =\n                    foldres_9540;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_9499;",
                   "\n            int32_t other_index_9500;\n            int32_t binop_param_x_9501;\n            int32_t x_9502;\n            int32_t binop_param_y_9503;\n            int32_t y_9504;\n            int32_t my_index_12103;\n            int32_t other_index_12104;\n            int32_t binop_param_x_12105;\n            int32_t x_12106;\n            int32_t binop_param_y_12107;\n            int32_t y_12108;\n            \n            my_index_9499 = local_tid_9521;\n            if (slt32(local_tid_9521, group_sizze_9351)) {\n                binop_param_y_9503 = *(volatile __local\n                                       int32_t *) &mem_11650[local_tid_9521 *\n                                                             sizeof(int32_t)];\n                y_9504 = *(volatile __local\n                           int32_t *) &mem_11653[local_tid_9521 *\n                                                 sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12111 = 1;\n                \n                while (slt32(skip_threads_12111, 32)) {\n                    if (slt32(local_tid_9521, group_sizze_9351) &&\n                        sle32(skip_threads_12111, local_tid_9521 -\n                              squot32(local_tid_9521, 32) * 32)) {\n                        // read operands\n                        {\n                            binop_param_x_9501 = *(volatile __local\n                                                   int32_t *) &mem_11650[(local_tid_9521 -\n                                                                          skip_threads_12111) *\n                                                                         sizeof(int32_t)];\n                            x_9502 = *(volatile __local\n                                       int32_t *) &mem_11653[(local_tid_9521 -\n                                                              skip_threads_12111) *\n                                                             s",
                   "izeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t res_9505 = binop_param_x_9501 +\n                                    binop_param_y_9503;\n                            int32_t zz_9506 = x_9502 + y_9504;\n                            \n                            binop_param_y_9503 = res_9505;\n                            y_9504 = zz_9506;\n                        }\n                    }\n                    if (sle32(wave_sizze_12094, skip_threads_12111)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_9521, group_sizze_9351) &&\n                        sle32(skip_threads_12111, local_tid_9521 -\n                              squot32(local_tid_9521, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11650[local_tid_9521 *\n                                                    sizeof(int32_t)] =\n                                binop_param_y_9503;\n                            *(volatile __local\n                              int32_t *) &mem_11653[local_tid_9521 *\n                                                    sizeof(int32_t)] = y_9504;\n                        }\n                    }\n                    if (sle32(wave_sizze_12094, skip_threads_12111)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12111 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_9521 - squot32(local_tid_9521, 32) * 32) == 31 &&\n                    slt32(local_tid_9521, group_sizze_9351)) {\n                    *(volatile __local\n                      int32_t *) &mem_11650[squot32(local_tid_9521, 32) *\n            ",
                   "                                sizeof(int32_t)] =\n                        binop_param_y_9503;\n                    *(volatile __local\n                      int32_t *) &mem_11653[squot32(local_tid_9521, 32) *\n                                            sizeof(int32_t)] = y_9504;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_9521, 32) == 0 && slt32(local_tid_9521,\n                                                              group_sizze_9351)) {\n                    binop_param_y_12107 = *(volatile __local\n                                            int32_t *) &mem_11650[local_tid_9521 *\n                                                                  sizeof(int32_t)];\n                    y_12108 = *(volatile __local\n                                int32_t *) &mem_11653[local_tid_9521 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12112 = 1;\n                    \n                    while (slt32(skip_threads_12112, 32)) {\n                        if ((squot32(local_tid_9521, 32) == 0 &&\n                             slt32(local_tid_9521, group_sizze_9351)) &&\n                            sle32(skip_threads_12112, local_tid_9521 -\n                                  squot32(local_tid_9521, 32) * 32)) {\n                            // read operands\n                            {\n                                binop_param_x_12105 = *(volatile __local\n                                                        int32_t *) &mem_11650[(local_tid_9521 -\n                                                                               skip_threads_12112) *\n                                                                              sizeof(int32_t)];\n     ",
                   "                           x_12106 = *(volatile __local\n                                            int32_t *) &mem_11653[(local_tid_9521 -\n                                                                   skip_threads_12112) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t res_12109 = binop_param_x_12105 +\n                                        binop_param_y_12107;\n                                int32_t zz_12110 = x_12106 + y_12108;\n                                \n                                binop_param_y_12107 = res_12109;\n                                y_12108 = zz_12110;\n                            }\n                        }\n                        if (sle32(wave_sizze_12094, skip_threads_12112)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_9521, 32) == 0 &&\n                             slt32(local_tid_9521, group_sizze_9351)) &&\n                            sle32(skip_threads_12112, local_tid_9521 -\n                                  squot32(local_tid_9521, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11650[local_tid_9521 *\n                                                        sizeof(int32_t)] =\n                                    binop_param_y_12107;\n                                *(volatile __local\n                                  int32_t *) &mem_11653[local_tid_9521 *\n                                                        sizeof(int32_t)] =\n                                    y_12108;\n                            }\n                        }\n                        if (sle32(wave_sizze_12094, skip_threads_12112)) {\n                       ",
                   "     barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12112 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_9521, 32) == 0 || !slt32(local_tid_9521,\n                                                                 group_sizze_9351))) {\n                    // read operands\n                    {\n                        binop_param_x_9501 = *(volatile __local\n                                               int32_t *) &mem_11650[(squot32(local_tid_9521,\n                                                                              32) -\n                                                                      1) *\n                                                                     sizeof(int32_t)];\n                        x_9502 = *(volatile __local\n                                   int32_t *) &mem_11653[(squot32(local_tid_9521,\n                                                                  32) - 1) *\n                                                         sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_9505 = binop_param_x_9501 +\n                                binop_param_y_9503;\n                        int32_t zz_9506 = x_9502 + y_9504;\n                        \n                        binop_param_y_9503 = res_9505;\n                        y_9504 = zz_9506;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11650[local_tid_9521 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_9503;\n                        *(volatile __local\n                          int32_t *) &mem_11653[local_tid_9521 *\n       ",
                   "                                         sizeof(int32_t)] = y_9504;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_9521, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11650[local_tid_9521 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_9503;\n                    *(volatile __local int32_t *) &mem_11653[local_tid_9521 *\n                                                             sizeof(int32_t)] =\n                        y_9504;\n                }\n            }\n            if (cond_9534) {\n                int32_t scanned_elem_9548 = *(__local\n                                              int32_t *) &mem_11650[local_tid_9521 *\n                                                                    4];\n                int32_t scanned_elem_9549 = *(__local\n                                              int32_t *) &mem_11653[local_tid_9521 *\n                                                                    4];\n                \n                *(__global int32_t *) &mem_11638[j_9533 * 4] =\n                    scanned_elem_9548;\n                *(__global int32_t *) &mem_11641[j_9533 * 4] =\n                    scanned_elem_9549;\n            }\n            \n            int32_t new_carry_9557;\n            int32_t new_carry_9558;\n            \n            if (is_first_thread_9554) {\n                int32_t carry_9555 = *(__local int32_t *) &mem_11650[y_9353 *\n                                                                     4];\n                int32_t carry_9556 = *(__local int32_t *) &mem_11653[y_9353 *\n                                                                     4];\n                \n                new_carry_9557 = carry_9555;\n                new_carry_9558 = carry_9556;\n            } else {\n                new_carry_9557 = 0",
                   ";\n                new_carry_9558 = 0;\n            }\n            \n            int32_t binop_param_x_merge_tmp_12101 = new_carry_9557;\n            int32_t x_merge_tmp_12102;\n            \n            x_merge_tmp_12102 = new_carry_9558;\n            binop_param_x_merge_9526 = binop_param_x_merge_tmp_12101;\n            x_merge_9527 = x_merge_tmp_12102;\n        }\n        result_9563 = binop_param_x_merge_9526;\n        result_9564 = x_merge_9527;\n    }\n    if (local_tid_9521 == 0) {\n        *(__global int32_t *) &mem_11656[group_id_9522 * 4] = result_9563;\n    }\n    if (local_tid_9521 == 0) {\n        *(__global int32_t *) &mem_11659[group_id_9522 * 4] = result_9564;\n    }\n}\n__kernel void scan1_kernel_9642(__local volatile int64_t *mem_aligned_0,\n                                int32_t padding_8114,\n                                int32_t num_iterations_9401, int32_t y_9404,\n                                __global unsigned char *b_mem_11679, __global\n                                unsigned char *mem_11682, __global\n                                unsigned char *mem_11688)\n{\n    __local volatile char *restrict mem_11685 = mem_aligned_0;\n    int32_t wave_sizze_12132;\n    int32_t group_sizze_12133;\n    char thread_active_12134;\n    int32_t global_tid_9642;\n    int32_t local_tid_9643;\n    int32_t group_id_9644;\n    \n    global_tid_9642 = get_global_id(0);\n    local_tid_9643 = get_local_id(0);\n    group_sizze_12133 = get_local_size(0);\n    wave_sizze_12132 = LOCKSTEP_WIDTH;\n    group_id_9644 = get_group_id(0);\n    thread_active_12134 = 1;\n    \n    int32_t x_9651;\n    char is_first_thread_9664;\n    int32_t result_9668;\n    \n    if (thread_active_12134) {\n        x_9651 = group_id_9644 * y_9404;\n        is_first_thread_9664 = local_tid_9643 == 0;\n        \n        int32_t binop_param_x_merge_9648 = 0;\n        \n        for (int32_t i_9649 = 0; i_9649 < num_iterations_9401; i_9649++) {\n            int32_t y_9652 = i_9649 * group_sizze_9351;\n            int32_t offset_9653 = x_9651 +",
                   " y_9652;\n            int32_t j_9654 = offset_9653 + local_tid_9643;\n            char cond_9655 = slt32(j_9654, padding_8114);\n            int32_t foldres_9657;\n            \n            if (cond_9655) {\n                int32_t b_elem_9656 = *(__global\n                                        int32_t *) &b_mem_11679[j_9654 * 4];\n                int32_t res_9631 = binop_param_x_merge_9648 + b_elem_9656;\n                \n                foldres_9657 = res_9631;\n            } else {\n                foldres_9657 = binop_param_x_merge_9648;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_9643, group_sizze_9351) && 1) {\n                *(__local int32_t *) &mem_11685[local_tid_9643 * 4] =\n                    foldres_9657;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_9632;\n            int32_t other_index_9633;\n            int32_t binop_param_x_9634;\n            int32_t binop_param_y_9635;\n            int32_t my_index_12137;\n            int32_t other_index_12138;\n            int32_t binop_param_x_12139;\n            int32_t binop_param_y_12140;\n            \n            my_index_9632 = local_tid_9643;\n            if (slt32(local_tid_9643, group_sizze_9351)) {\n                binop_param_y_9635 = *(volatile __local\n                                       int32_t *) &mem_11685[local_tid_9643 *\n                                                             sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12142 = 1;\n                \n                while (slt32(skip_threads_12142, 32)) {\n                    if (slt32(local_tid_9643, group_sizze_9351) &&\n                        sle32(skip_threads_12142, local_tid_9643 -\n                              squot32(local_tid_9643, 32) * 32)) {\n                        // read operands\n                        {\n                            binop_param_x_9634 = *(v",
                   "olatile __local\n                                                   int32_t *) &mem_11685[(local_tid_9643 -\n                                                                          skip_threads_12142) *\n                                                                         sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t res_9636 = binop_param_x_9634 +\n                                    binop_param_y_9635;\n                            \n                            binop_param_y_9635 = res_9636;\n                        }\n                    }\n                    if (sle32(wave_sizze_12132, skip_threads_12142)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_9643, group_sizze_9351) &&\n                        sle32(skip_threads_12142, local_tid_9643 -\n                              squot32(local_tid_9643, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11685[local_tid_9643 *\n                                                    sizeof(int32_t)] =\n                                binop_param_y_9635;\n                        }\n                    }\n                    if (sle32(wave_sizze_12132, skip_threads_12142)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12142 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_9643 - squot32(local_tid_9643, 32) * 32) == 31 &&\n                    slt32(local_tid_9643, group_sizze_9351)) {\n                    *(volatile __local\n                      int32_t *) &mem_11685[squot32(local_tid_9643, 32) *\n                                       ",
                   "     sizeof(int32_t)] =\n                        binop_param_y_9635;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_9643, 32) == 0 && slt32(local_tid_9643,\n                                                              group_sizze_9351)) {\n                    binop_param_y_12140 = *(volatile __local\n                                            int32_t *) &mem_11685[local_tid_9643 *\n                                                                  sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12143 = 1;\n                    \n                    while (slt32(skip_threads_12143, 32)) {\n                        if ((squot32(local_tid_9643, 32) == 0 &&\n                             slt32(local_tid_9643, group_sizze_9351)) &&\n                            sle32(skip_threads_12143, local_tid_9643 -\n                                  squot32(local_tid_9643, 32) * 32)) {\n                            // read operands\n                            {\n                                binop_param_x_12139 = *(volatile __local\n                                                        int32_t *) &mem_11685[(local_tid_9643 -\n                                                                               skip_threads_12143) *\n                                                                              sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t res_12141 = binop_param_x_12139 +\n                                        binop_param_y_12140;\n                                \n                                binop_param_y_12140 = res_12141;\n                            }\n                        }\n          ",
                   "              if (sle32(wave_sizze_12132, skip_threads_12143)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_9643, 32) == 0 &&\n                             slt32(local_tid_9643, group_sizze_9351)) &&\n                            sle32(skip_threads_12143, local_tid_9643 -\n                                  squot32(local_tid_9643, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11685[local_tid_9643 *\n                                                        sizeof(int32_t)] =\n                                    binop_param_y_12140;\n                            }\n                        }\n                        if (sle32(wave_sizze_12132, skip_threads_12143)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12143 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_9643, 32) == 0 || !slt32(local_tid_9643,\n                                                                 group_sizze_9351))) {\n                    // read operands\n                    {\n                        binop_param_x_9634 = *(volatile __local\n                                               int32_t *) &mem_11685[(squot32(local_tid_9643,\n                                                                              32) -\n                                                                      1) *\n                                                                     sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_9636 = binop_param_x_9634 +\n                              ",
                   "  binop_param_y_9635;\n                        \n                        binop_param_y_9635 = res_9636;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11685[local_tid_9643 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_9635;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_9643, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11685[local_tid_9643 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_9635;\n                }\n            }\n            if (cond_9655) {\n                int32_t scanned_elem_9661 = *(__local\n                                              int32_t *) &mem_11685[local_tid_9643 *\n                                                                    4];\n                \n                *(__global int32_t *) &mem_11682[j_9654 * 4] =\n                    scanned_elem_9661;\n            }\n            \n            int32_t new_carry_9666;\n            \n            if (is_first_thread_9664) {\n                int32_t carry_9665 = *(__local int32_t *) &mem_11685[y_9353 *\n                                                                     4];\n                \n                new_carry_9666 = carry_9665;\n            } else {\n                new_carry_9666 = 0;\n            }\n            \n            int32_t binop_param_x_merge_tmp_12136 = new_carry_9666;\n            \n            binop_param_x_merge_9648 = binop_param_x_merge_tmp_12136;\n        }\n        result_9668 = binop_param_x_merge_9648;\n    }\n    if (local_tid_9643 == 0) {\n        *(__global int32_t *) &mem_11688[group_id_9644 * 4] = result_9668;\n    }\n}\n__kernel void scan1_kernel_9732(__",
                   "local volatile int64_t *mem_aligned_0,\n                                int32_t padding_8114,\n                                int32_t num_iterations_9401, int32_t y_9404,\n                                __global unsigned char *mem_11697, __global\n                                unsigned char *mem_11700, __global\n                                unsigned char *mem_11706)\n{\n    __local volatile char *restrict mem_11703 = mem_aligned_0;\n    int32_t wave_sizze_12157;\n    int32_t group_sizze_12158;\n    char thread_active_12159;\n    int32_t global_tid_9732;\n    int32_t local_tid_9733;\n    int32_t group_id_9734;\n    \n    global_tid_9732 = get_global_id(0);\n    local_tid_9733 = get_local_id(0);\n    group_sizze_12158 = get_local_size(0);\n    wave_sizze_12157 = LOCKSTEP_WIDTH;\n    group_id_9734 = get_group_id(0);\n    thread_active_12159 = 1;\n    \n    int32_t x_9741;\n    char is_first_thread_9754;\n    int32_t result_9758;\n    \n    if (thread_active_12159) {\n        x_9741 = group_id_9734 * y_9404;\n        is_first_thread_9754 = local_tid_9733 == 0;\n        \n        int32_t x_merge_9738 = 0;\n        \n        for (int32_t i_9739 = 0; i_9739 < num_iterations_9401; i_9739++) {\n            int32_t y_9742 = i_9739 * group_sizze_9351;\n            int32_t offset_9743 = x_9741 + y_9742;\n            int32_t j_9744 = offset_9743 + local_tid_9733;\n            char cond_9745 = slt32(j_9744, padding_8114);\n            int32_t foldres_9747;\n            \n            if (cond_9745) {\n                int32_t res_elem_9746 = *(__global\n                                          int32_t *) &mem_11697[j_9744 * 4];\n                char res_9719 = res_elem_9746 == 0;\n                int32_t partition_incr_9720;\n                \n                if (res_9719) {\n                    partition_incr_9720 = 1;\n                } else {\n                    partition_incr_9720 = 0;\n                }\n                \n                int32_t zz_9721 = x_merge_9738 + partition_incr_9720;\n                \n          ",
                   "      foldres_9747 = zz_9721;\n            } else {\n                foldres_9747 = x_merge_9738;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_9733, group_sizze_9351) && 1) {\n                *(__local int32_t *) &mem_11703[local_tid_9733 * 4] =\n                    foldres_9747;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_9722;\n            int32_t other_index_9723;\n            int32_t x_9724;\n            int32_t y_9725;\n            int32_t my_index_12162;\n            int32_t other_index_12163;\n            int32_t x_12164;\n            int32_t y_12165;\n            \n            my_index_9722 = local_tid_9733;\n            if (slt32(local_tid_9733, group_sizze_9351)) {\n                y_9725 = *(volatile __local\n                           int32_t *) &mem_11703[local_tid_9733 *\n                                                 sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12167 = 1;\n                \n                while (slt32(skip_threads_12167, 32)) {\n                    if (slt32(local_tid_9733, group_sizze_9351) &&\n                        sle32(skip_threads_12167, local_tid_9733 -\n                              squot32(local_tid_9733, 32) * 32)) {\n                        // read operands\n                        {\n                            x_9724 = *(volatile __local\n                                       int32_t *) &mem_11703[(local_tid_9733 -\n                                                              skip_threads_12167) *\n                                                             sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t zz_9726 = x_9724 + y_9725;\n                            \n                            y_9725 = zz_9726;\n                        }\n                    ",
                   "}\n                    if (sle32(wave_sizze_12157, skip_threads_12167)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_9733, group_sizze_9351) &&\n                        sle32(skip_threads_12167, local_tid_9733 -\n                              squot32(local_tid_9733, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11703[local_tid_9733 *\n                                                    sizeof(int32_t)] = y_9725;\n                        }\n                    }\n                    if (sle32(wave_sizze_12157, skip_threads_12167)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12167 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_9733 - squot32(local_tid_9733, 32) * 32) == 31 &&\n                    slt32(local_tid_9733, group_sizze_9351)) {\n                    *(volatile __local\n                      int32_t *) &mem_11703[squot32(local_tid_9733, 32) *\n                                            sizeof(int32_t)] = y_9725;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_9733, 32) == 0 && slt32(local_tid_9733,\n                                                              group_sizze_9351)) {\n                    y_12165 = *(volatile __local\n                                int32_t *) &mem_11703[local_tid_9733 *\n                                                      sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t",
                   " skip_threads_12168 = 1;\n                    \n                    while (slt32(skip_threads_12168, 32)) {\n                        if ((squot32(local_tid_9733, 32) == 0 &&\n                             slt32(local_tid_9733, group_sizze_9351)) &&\n                            sle32(skip_threads_12168, local_tid_9733 -\n                                  squot32(local_tid_9733, 32) * 32)) {\n                            // read operands\n                            {\n                                x_12164 = *(volatile __local\n                                            int32_t *) &mem_11703[(local_tid_9733 -\n                                                                   skip_threads_12168) *\n                                                                  sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t zz_12166 = x_12164 + y_12165;\n                                \n                                y_12165 = zz_12166;\n                            }\n                        }\n                        if (sle32(wave_sizze_12157, skip_threads_12168)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_9733, 32) == 0 &&\n                             slt32(local_tid_9733, group_sizze_9351)) &&\n                            sle32(skip_threads_12168, local_tid_9733 -\n                                  squot32(local_tid_9733, 32) * 32)) {\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11703[local_tid_9733 *\n                                                        sizeof(int32_t)] =\n                                    y_12165;\n                            }\n                        }\n                        if (sle32(wave_sizze_12157, skip_threads_12168)) {\n             ",
                   "               barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12168 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_9733, 32) == 0 || !slt32(local_tid_9733,\n                                                                 group_sizze_9351))) {\n                    // read operands\n                    {\n                        x_9724 = *(volatile __local\n                                   int32_t *) &mem_11703[(squot32(local_tid_9733,\n                                                                  32) - 1) *\n                                                         sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_9726 = x_9724 + y_9725;\n                        \n                        y_9725 = zz_9726;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11703[local_tid_9733 *\n                                                sizeof(int32_t)] = y_9725;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_9733, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11703[local_tid_9733 *\n                                                             sizeof(int32_t)] =\n                        y_9725;\n                }\n            }\n            if (cond_9745) {\n                int32_t scanned_elem_9751 = *(__local\n                                              int32_t *) &mem_11703[local_tid_9733 *\n                                                                    4];\n                \n                *(__g",
                   "lobal int32_t *) &mem_11700[j_9744 * 4] =\n                    scanned_elem_9751;\n            }\n            \n            int32_t new_carry_9756;\n            \n            if (is_first_thread_9754) {\n                int32_t carry_9755 = *(__local int32_t *) &mem_11703[y_9353 *\n                                                                     4];\n                \n                new_carry_9756 = carry_9755;\n            } else {\n                new_carry_9756 = 0;\n            }\n            \n            int32_t x_merge_tmp_12161 = new_carry_9756;\n            \n            x_merge_9738 = x_merge_tmp_12161;\n        }\n        result_9758 = x_merge_9738;\n    }\n    if (local_tid_9733 == 0) {\n        *(__global int32_t *) &mem_11706[group_id_9734 * 4] = result_9758;\n    }\n}\n__kernel void scan1_kernel_9916(__local volatile int64_t *mem_aligned_0,\n                                int32_t padding_8240, int32_t arg_8246,\n                                int32_t arg_8249, int32_t arg_8255,\n                                int32_t arg_8258, int32_t y_8265,\n                                int32_t y_8267, int32_t num_iterations_9921,\n                                int32_t y_9924, __global\n                                unsigned char *mem_11618, __global\n                                unsigned char *mem_11621, __global\n                                unsigned char *mem_11624, __global\n                                unsigned char *mem_11630)\n{\n    __local volatile char *restrict mem_11627 = mem_aligned_0;\n    int32_t wave_sizze_12194;\n    int32_t group_sizze_12195;\n    char thread_active_12196;\n    int32_t global_tid_9916;\n    int32_t local_tid_9917;\n    int32_t group_id_9918;\n    \n    global_tid_9916 = get_global_id(0);\n    local_tid_9917 = get_local_id(0);\n    group_sizze_12195 = get_local_size(0);\n    wave_sizze_12194 = LOCKSTEP_WIDTH;\n    group_id_9918 = get_group_id(0);\n    thread_active_12196 = 1;\n    \n    int32_t x_9925;\n    char is_first_thread_9942;\n    int32_t result_9948;\n ",
                   "   \n    if (thread_active_12196) {\n        x_9925 = group_id_9918 * y_9924;\n        is_first_thread_9942 = local_tid_9917 == 0;\n        \n        int32_t binop_param_x_merge_9922 = 0;\n        \n        for (int32_t i_9923 = 0; i_9923 < num_iterations_9921; i_9923++) {\n            int32_t y_9926 = i_9923 * group_sizze_9871;\n            int32_t offset_9927 = x_9925 + y_9926;\n            int32_t j_9928 = offset_9927 + local_tid_9917;\n            char cond_9929 = slt32(j_9928, padding_8240);\n            int32_t foldres_9933;\n            \n            if (cond_9929) {\n                char cond_9887 = slt32(j_9928, y_8265);\n                int32_t res_9888;\n                \n                if (cond_9887) {\n                    res_9888 = 0;\n                } else {\n                    int32_t x_9889 = padding_8240 - j_9928;\n                    int32_t y_9890 = x_9889 - 1;\n                    int32_t y_9891 = pow32(arg_8255, y_9890);\n                    int32_t x_9892 = sdiv32(arg_8258, y_9891);\n                    int32_t res_9893 = smod32(x_9892, arg_8255);\n                    \n                    res_9888 = res_9893;\n                }\n                \n                char cond_9894 = slt32(j_9928, y_8267);\n                int32_t res_9895;\n                \n                if (cond_9894) {\n                    res_9895 = 0;\n                } else {\n                    int32_t x_9896 = padding_8240 - j_9928;\n                    int32_t y_9897 = x_9896 - 1;\n                    int32_t y_9898 = pow32(arg_8246, y_9897);\n                    int32_t x_9899 = sdiv32(arg_8249, y_9898);\n                    int32_t res_9900 = smod32(x_9899, arg_8246);\n                    \n                    res_9895 = res_9900;\n                }\n                \n                int32_t res_9901 = binop_param_x_merge_9922 + res_9888;\n                \n                *(__global int32_t *) &mem_11621[j_9928 * 4] = res_9895;\n                *(__global int32_t *) &mem_11624[j_9928 * 4] = res_9888;\n        ",
                   "        foldres_9933 = res_9901;\n            } else {\n                foldres_9933 = binop_param_x_merge_9922;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            if (slt32(local_tid_9917, group_sizze_9871) && 1) {\n                *(__local int32_t *) &mem_11627[local_tid_9917 * 4] =\n                    foldres_9933;\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            \n            int32_t my_index_9902;\n            int32_t other_index_9903;\n            int32_t binop_param_x_9904;\n            int32_t binop_param_y_9905;\n            int32_t my_index_12201;\n            int32_t other_index_12202;\n            int32_t binop_param_x_12203;\n            int32_t binop_param_y_12204;\n            \n            my_index_9902 = local_tid_9917;\n            if (slt32(local_tid_9917, group_sizze_9871)) {\n                binop_param_y_9905 = *(volatile __local\n                                       int32_t *) &mem_11627[local_tid_9917 *\n                                                             sizeof(int32_t)];\n            }\n            // in-block scan (hopefully no barriers needed)\n            {\n                int32_t skip_threads_12206 = 1;\n                \n                while (slt32(skip_threads_12206, 32)) {\n                    if (slt32(local_tid_9917, group_sizze_9871) &&\n                        sle32(skip_threads_12206, local_tid_9917 -\n                              squot32(local_tid_9917, 32) * 32)) {\n                        // read operands\n                        {\n                            binop_param_x_9904 = *(volatile __local\n                                                   int32_t *) &mem_11627[(local_tid_9917 -\n                                                                          skip_threads_12206) *\n                                                                         sizeof(int32_t)];\n                        }\n                        // perform operation\n                        {\n                            int32_t r",
                   "es_9906 = binop_param_x_9904 +\n                                    binop_param_y_9905;\n                            \n                            binop_param_y_9905 = res_9906;\n                        }\n                    }\n                    if (sle32(wave_sizze_12194, skip_threads_12206)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    if (slt32(local_tid_9917, group_sizze_9871) &&\n                        sle32(skip_threads_12206, local_tid_9917 -\n                              squot32(local_tid_9917, 32) * 32)) {\n                        // write result\n                        {\n                            *(volatile __local\n                              int32_t *) &mem_11627[local_tid_9917 *\n                                                    sizeof(int32_t)] =\n                                binop_param_y_9905;\n                        }\n                    }\n                    if (sle32(wave_sizze_12194, skip_threads_12206)) {\n                        barrier(CLK_LOCAL_MEM_FENCE);\n                    }\n                    skip_threads_12206 *= 2;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // last thread of block 'i' writes its result to offset 'i'\n            {\n                if ((local_tid_9917 - squot32(local_tid_9917, 32) * 32) == 31 &&\n                    slt32(local_tid_9917, group_sizze_9871)) {\n                    *(volatile __local\n                      int32_t *) &mem_11627[squot32(local_tid_9917, 32) *\n                                            sizeof(int32_t)] =\n                        binop_param_y_9905;\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n            {\n                if (squot32(local_tid_9917, 32) == 0 && slt32(local_tid_9917,\n                                                              group_sizze_9871)) {\n                  ",
                   "  binop_param_y_12204 = *(volatile __local\n                                            int32_t *) &mem_11627[local_tid_9917 *\n                                                                  sizeof(int32_t)];\n                }\n                // in-block scan (hopefully no barriers needed)\n                {\n                    int32_t skip_threads_12207 = 1;\n                    \n                    while (slt32(skip_threads_12207, 32)) {\n                        if ((squot32(local_tid_9917, 32) == 0 &&\n                             slt32(local_tid_9917, group_sizze_9871)) &&\n                            sle32(skip_threads_12207, local_tid_9917 -\n                                  squot32(local_tid_9917, 32) * 32)) {\n                            // read operands\n                            {\n                                binop_param_x_12203 = *(volatile __local\n                                                        int32_t *) &mem_11627[(local_tid_9917 -\n                                                                               skip_threads_12207) *\n                                                                              sizeof(int32_t)];\n                            }\n                            // perform operation\n                            {\n                                int32_t res_12205 = binop_param_x_12203 +\n                                        binop_param_y_12204;\n                                \n                                binop_param_y_12204 = res_12205;\n                            }\n                        }\n                        if (sle32(wave_sizze_12194, skip_threads_12207)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        if ((squot32(local_tid_9917, 32) == 0 &&\n                             slt32(local_tid_9917, group_sizze_9871)) &&\n                            sle32(skip_threads_12207, local_tid_9917 -\n                                  squot32(local_tid_9917, 32) * 32)) {",
                   "\n                            // write result\n                            {\n                                *(volatile __local\n                                  int32_t *) &mem_11627[local_tid_9917 *\n                                                        sizeof(int32_t)] =\n                                    binop_param_y_12204;\n                            }\n                        }\n                        if (sle32(wave_sizze_12194, skip_threads_12207)) {\n                            barrier(CLK_LOCAL_MEM_FENCE);\n                        }\n                        skip_threads_12207 *= 2;\n                    }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // carry-in for every block except the first\n            {\n                if (!(squot32(local_tid_9917, 32) == 0 || !slt32(local_tid_9917,\n                                                                 group_sizze_9871))) {\n                    // read operands\n                    {\n                        binop_param_x_9904 = *(volatile __local\n                                               int32_t *) &mem_11627[(squot32(local_tid_9917,\n                                                                              32) -\n                                                                      1) *\n                                                                     sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_9906 = binop_param_x_9904 +\n                                binop_param_y_9905;\n                        \n                        binop_param_y_9905 = res_9906;\n                    }\n                    // write final result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11627[local_tid_9917 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_9905;\n                 ",
                   "   }\n                }\n            }\n            barrier(CLK_LOCAL_MEM_FENCE);\n            // restore correct values for first block\n            {\n                if (squot32(local_tid_9917, 32) == 0) {\n                    *(volatile __local int32_t *) &mem_11627[local_tid_9917 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_9905;\n                }\n            }\n            if (cond_9929) {\n                int32_t scanned_elem_9939 = *(__local\n                                              int32_t *) &mem_11627[local_tid_9917 *\n                                                                    4];\n                \n                *(__global int32_t *) &mem_11618[j_9928 * 4] =\n                    scanned_elem_9939;\n            }\n            \n            int32_t new_carry_9944;\n            \n            if (is_first_thread_9942) {\n                int32_t carry_9943 = *(__local int32_t *) &mem_11627[y_9873 *\n                                                                     4];\n                \n                new_carry_9944 = carry_9943;\n            } else {\n                new_carry_9944 = 0;\n            }\n            \n            int32_t binop_param_x_merge_tmp_12200 = new_carry_9944;\n            \n            binop_param_x_merge_9922 = binop_param_x_merge_tmp_12200;\n        }\n        result_9948 = binop_param_x_merge_9922;\n    }\n    if (local_tid_9917 == 0) {\n        *(__global int32_t *) &mem_11630[group_id_9918 * 4] = result_9948;\n    }\n}\n__kernel void scan2_kernel_10093(__local volatile int64_t *mem_aligned_0,\n                                 __local volatile int64_t *mem_aligned_1,\n                                 int32_t num_groups_9877, __global\n                                 unsigned char *mem_11660, __global\n                                 unsigned char *mem_11663, __global\n                                 unsigned char *mem_11672, __global\n                                 unsigned c",
                   "har *mem_11675)\n{\n    __local volatile char *restrict mem_11666 = mem_aligned_0;\n    __local volatile char *restrict mem_11669 = mem_aligned_1;\n    int32_t wave_sizze_12240;\n    int32_t group_sizze_12241;\n    char thread_active_12242;\n    int32_t global_tid_10093;\n    int32_t local_tid_10094;\n    int32_t group_id_10095;\n    \n    global_tid_10093 = get_global_id(0);\n    local_tid_10094 = get_local_id(0);\n    group_sizze_12241 = get_local_size(0);\n    wave_sizze_12240 = LOCKSTEP_WIDTH;\n    group_id_10095 = get_group_id(0);\n    thread_active_12242 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10094, num_groups_9877) && 1) {\n        int32_t res_group_sums_elem_10096 = *(__global\n                                              int32_t *) &mem_11660[local_tid_10094 *\n                                                                    4];\n        int32_t offsets_group_sums_elem_10097 = *(__global\n                                                  int32_t *) &mem_11663[local_tid_10094 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11666[local_tid_10094 * 4] =\n            res_group_sums_elem_10096;\n        *(__local int32_t *) &mem_11669[local_tid_10094 * 4] =\n            offsets_group_sums_elem_10097;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10085;\n    int32_t other_index_10086;\n    int32_t binop_param_x_10087;\n    int32_t x_10088;\n    int32_t binop_param_y_10089;\n    int32_t y_10090;\n    int32_t my_index_12243;\n    int32_t other_index_12244;\n    int32_t binop_param_x_12245;\n    int32_t x_12246;\n    int32_t binop_param_y_12247;\n    int32_t y_12248;\n    \n    my_index_10085 = local_tid_10094;\n    if (slt32(local_tid_10094, num_groups_9877)) {\n        binop_param_y_10089 = *(volatile __local\n                                int32_t *) &mem_11666[local_tid_10094 *\n                                                      sizeof(int32_t)];\n        y_10090 = *(volatile __lo",
                   "cal int32_t *) &mem_11669[local_tid_10094 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12251 = 1;\n        \n        while (slt32(skip_threads_12251, 32)) {\n            if (slt32(local_tid_10094, num_groups_9877) &&\n                sle32(skip_threads_12251, local_tid_10094 -\n                      squot32(local_tid_10094, 32) * 32)) {\n                // read operands\n                {\n                    binop_param_x_10087 = *(volatile __local\n                                            int32_t *) &mem_11666[(local_tid_10094 -\n                                                                   skip_threads_12251) *\n                                                                  sizeof(int32_t)];\n                    x_10088 = *(volatile __local\n                                int32_t *) &mem_11669[(local_tid_10094 -\n                                                       skip_threads_12251) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t res_10091;\n                    int32_t zz_10092;\n                    \n                    if (thread_active_12242) {\n                        res_10091 = binop_param_x_10087 + binop_param_y_10089;\n                        zz_10092 = x_10088 + y_10090;\n                    }\n                    binop_param_y_10089 = res_10091;\n                    y_10090 = zz_10092;\n                }\n            }\n            if (sle32(wave_sizze_12240, skip_threads_12251)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10094, num_groups_9877) &&\n                sle32(skip_threads_12251, local_tid_10094 -\n                      squot32(local_tid_10094, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *",
                   ") &mem_11666[local_tid_10094 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_10089;\n                    *(volatile __local int32_t *) &mem_11669[local_tid_10094 *\n                                                             sizeof(int32_t)] =\n                        y_10090;\n                }\n            }\n            if (sle32(wave_sizze_12240, skip_threads_12251)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12251 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_10094 - squot32(local_tid_10094, 32) * 32) == 31 &&\n            slt32(local_tid_10094, num_groups_9877)) {\n            *(volatile __local int32_t *) &mem_11666[squot32(local_tid_10094,\n                                                             32) *\n                                                     sizeof(int32_t)] =\n                binop_param_y_10089;\n            *(volatile __local int32_t *) &mem_11669[squot32(local_tid_10094,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_10090;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10094, 32) == 0 && slt32(local_tid_10094,\n                                                       num_groups_9877)) {\n            binop_param_y_12247 = *(volatile __local\n                                    int32_t *) &mem_11666[local_tid_10094 *\n                                                          sizeof(int32_t)];\n            y_12248 = *(volatile __local int32_t *) &mem_11669[local_tid_10094 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {",
                   "\n            int32_t skip_threads_12252 = 1;\n            \n            while (slt32(skip_threads_12252, 32)) {\n                if ((squot32(local_tid_10094, 32) == 0 && slt32(local_tid_10094,\n                                                                num_groups_9877)) &&\n                    sle32(skip_threads_12252, local_tid_10094 -\n                          squot32(local_tid_10094, 32) * 32)) {\n                    // read operands\n                    {\n                        binop_param_x_12245 = *(volatile __local\n                                                int32_t *) &mem_11666[(local_tid_10094 -\n                                                                       skip_threads_12252) *\n                                                                      sizeof(int32_t)];\n                        x_12246 = *(volatile __local\n                                    int32_t *) &mem_11669[(local_tid_10094 -\n                                                           skip_threads_12252) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_12249;\n                        int32_t zz_12250;\n                        \n                        if (thread_active_12242) {\n                            res_12249 = binop_param_x_12245 +\n                                binop_param_y_12247;\n                            zz_12250 = x_12246 + y_12248;\n                        }\n                        binop_param_y_12247 = res_12249;\n                        y_12248 = zz_12250;\n                    }\n                }\n                if (sle32(wave_sizze_12240, skip_threads_12252)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_10094, 32) == 0 && slt32(local_tid_10094,\n                                                                num_groups_9877)) &&\n                    sle32(sk",
                   "ip_threads_12252, local_tid_10094 -\n                          squot32(local_tid_10094, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11666[local_tid_10094 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_12247;\n                        *(volatile __local\n                          int32_t *) &mem_11669[local_tid_10094 *\n                                                sizeof(int32_t)] = y_12248;\n                    }\n                }\n                if (sle32(wave_sizze_12240, skip_threads_12252)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12252 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10094, 32) == 0 || !slt32(local_tid_10094,\n                                                          num_groups_9877))) {\n            // read operands\n            {\n                binop_param_x_10087 = *(volatile __local\n                                        int32_t *) &mem_11666[(squot32(local_tid_10094,\n                                                                       32) -\n                                                               1) *\n                                                              sizeof(int32_t)];\n                x_10088 = *(volatile __local\n                            int32_t *) &mem_11669[(squot32(local_tid_10094,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t res_10091;\n                int32_t zz_10092;\n                \n                if (thread_active_12242) {\n                    res_10091 = binop_param_x_10087 + binop_param_y_10089;\n      ",
                   "              zz_10092 = x_10088 + y_10090;\n                }\n                binop_param_y_10089 = res_10091;\n                y_10090 = zz_10092;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11666[local_tid_10094 *\n                                                         sizeof(int32_t)] =\n                    binop_param_y_10089;\n                *(volatile __local int32_t *) &mem_11669[local_tid_10094 *\n                                                         sizeof(int32_t)] =\n                    y_10090;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10094, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11666[local_tid_10094 *\n                                                     sizeof(int32_t)] =\n                binop_param_y_10089;\n            *(volatile __local int32_t *) &mem_11669[local_tid_10094 *\n                                                     sizeof(int32_t)] = y_10090;\n        }\n    }\n    \n    int32_t scanned_elem_10102;\n    int32_t scanned_elem_10103;\n    \n    if (thread_active_12242) {\n        scanned_elem_10102 = *(__local int32_t *) &mem_11666[local_tid_10094 *\n                                                             4];\n        scanned_elem_10103 = *(__local int32_t *) &mem_11669[local_tid_10094 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11672[global_tid_10093 * 4] = scanned_elem_10102;\n    *(__global int32_t *) &mem_11675[global_tid_10093 * 4] = scanned_elem_10103;\n}\n__kernel void scan2_kernel_10194(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_9877, __global\n                                 unsigned char *mem_11692, __global\n                                 unsigned char *mem_11698)\n{\n    __local volatile char *restrict mem_11695 = mem_aligned_0;\n    int32_",
                   "t wave_sizze_12271;\n    int32_t group_sizze_12272;\n    char thread_active_12273;\n    int32_t global_tid_10194;\n    int32_t local_tid_10195;\n    int32_t group_id_10196;\n    \n    global_tid_10194 = get_global_id(0);\n    local_tid_10195 = get_local_id(0);\n    group_sizze_12272 = get_local_size(0);\n    wave_sizze_12271 = LOCKSTEP_WIDTH;\n    group_id_10196 = get_group_id(0);\n    thread_active_12273 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10195, num_groups_9877) && 1) {\n        int32_t res_group_sums_elem_10197 = *(__global\n                                              int32_t *) &mem_11692[local_tid_10195 *\n                                                                    4];\n        \n        *(__local int32_t *) &mem_11695[local_tid_10195 * 4] =\n            res_group_sums_elem_10197;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10189;\n    int32_t other_index_10190;\n    int32_t binop_param_x_10191;\n    int32_t binop_param_y_10192;\n    int32_t my_index_12274;\n    int32_t other_index_12275;\n    int32_t binop_param_x_12276;\n    int32_t binop_param_y_12277;\n    \n    my_index_10189 = local_tid_10195;\n    if (slt32(local_tid_10195, num_groups_9877)) {\n        binop_param_y_10192 = *(volatile __local\n                                int32_t *) &mem_11695[local_tid_10195 *\n                                                      sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12279 = 1;\n        \n        while (slt32(skip_threads_12279, 32)) {\n            if (slt32(local_tid_10195, num_groups_9877) &&\n                sle32(skip_threads_12279, local_tid_10195 -\n                      squot32(local_tid_10195, 32) * 32)) {\n                // read operands\n                {\n                    binop_param_x_10191 = *(volatile __local\n                                            int32_t *) &mem_11695[(local_tid_10195 -\n                                                                 ",
                   "  skip_threads_12279) *\n                                                                  sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t res_10193;\n                    \n                    if (thread_active_12273) {\n                        res_10193 = binop_param_x_10191 + binop_param_y_10192;\n                    }\n                    binop_param_y_10192 = res_10193;\n                }\n            }\n            if (sle32(wave_sizze_12271, skip_threads_12279)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10195, num_groups_9877) &&\n                sle32(skip_threads_12279, local_tid_10195 -\n                      squot32(local_tid_10195, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11695[local_tid_10195 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_10192;\n                }\n            }\n            if (sle32(wave_sizze_12271, skip_threads_12279)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12279 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_10195 - squot32(local_tid_10195, 32) * 32) == 31 &&\n            slt32(local_tid_10195, num_groups_9877)) {\n            *(volatile __local int32_t *) &mem_11695[squot32(local_tid_10195,\n                                                             32) *\n                                                     sizeof(int32_t)] =\n                binop_param_y_10192;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10195, 32) == 0 && slt32(local_tid_10195,\n                                                       num_gr",
                   "oups_9877)) {\n            binop_param_y_12277 = *(volatile __local\n                                    int32_t *) &mem_11695[local_tid_10195 *\n                                                          sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12280 = 1;\n            \n            while (slt32(skip_threads_12280, 32)) {\n                if ((squot32(local_tid_10195, 32) == 0 && slt32(local_tid_10195,\n                                                                num_groups_9877)) &&\n                    sle32(skip_threads_12280, local_tid_10195 -\n                          squot32(local_tid_10195, 32) * 32)) {\n                    // read operands\n                    {\n                        binop_param_x_12276 = *(volatile __local\n                                                int32_t *) &mem_11695[(local_tid_10195 -\n                                                                       skip_threads_12280) *\n                                                                      sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_12278;\n                        \n                        if (thread_active_12273) {\n                            res_12278 = binop_param_x_12276 +\n                                binop_param_y_12277;\n                        }\n                        binop_param_y_12277 = res_12278;\n                    }\n                }\n                if (sle32(wave_sizze_12271, skip_threads_12280)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_10195, 32) == 0 && slt32(local_tid_10195,\n                                                                num_groups_9877)) &&\n                    sle32(skip_threads_12280, local_tid_10195 -\n                          squot32(local_tid_10195, 32) * 32)) {\n                    // write result\n ",
                   "                   {\n                        *(volatile __local\n                          int32_t *) &mem_11695[local_tid_10195 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_12277;\n                    }\n                }\n                if (sle32(wave_sizze_12271, skip_threads_12280)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12280 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10195, 32) == 0 || !slt32(local_tid_10195,\n                                                          num_groups_9877))) {\n            // read operands\n            {\n                binop_param_x_10191 = *(volatile __local\n                                        int32_t *) &mem_11695[(squot32(local_tid_10195,\n                                                                       32) -\n                                                               1) *\n                                                              sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t res_10193;\n                \n                if (thread_active_12273) {\n                    res_10193 = binop_param_x_10191 + binop_param_y_10192;\n                }\n                binop_param_y_10192 = res_10193;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11695[local_tid_10195 *\n                                                         sizeof(int32_t)] =\n                    binop_param_y_10192;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10195, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11695[local_tid_10195 *\n                                                     sizeof(int3",
                   "2_t)] =\n                binop_param_y_10192;\n        }\n    }\n    \n    int32_t scanned_elem_10200;\n    \n    if (thread_active_12273) {\n        scanned_elem_10200 = *(__local int32_t *) &mem_11695[local_tid_10195 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11698[global_tid_10194 * 4] = scanned_elem_10200;\n}\n__kernel void scan2_kernel_10284(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_9877, __global\n                                 unsigned char *mem_11710, __global\n                                 unsigned char *mem_11716)\n{\n    __local volatile char *restrict mem_11713 = mem_aligned_0;\n    int32_t wave_sizze_12296;\n    int32_t group_sizze_12297;\n    char thread_active_12298;\n    int32_t global_tid_10284;\n    int32_t local_tid_10285;\n    int32_t group_id_10286;\n    \n    global_tid_10284 = get_global_id(0);\n    local_tid_10285 = get_local_id(0);\n    group_sizze_12297 = get_local_size(0);\n    wave_sizze_12296 = LOCKSTEP_WIDTH;\n    group_id_10286 = get_group_id(0);\n    thread_active_12298 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10285, num_groups_9877) && 1) {\n        int32_t offsets_group_sums_elem_10287 = *(__global\n                                                  int32_t *) &mem_11710[local_tid_10285 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11713[local_tid_10285 * 4] =\n            offsets_group_sums_elem_10287;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10279;\n    int32_t other_index_10280;\n    int32_t x_10281;\n    int32_t y_10282;\n    int32_t my_index_12299;\n    int32_t other_index_12300;\n    int32_t x_12301;\n    int32_t y_12302;\n    \n    my_index_10279 = local_tid_10285;\n    if (slt32(local_tid_10285, num_groups_9877)) {\n        y_10282 = *(volatile __local int32_t *) &mem_11713[local_tid_10285 *\n                                        ",
                   "                   sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12304 = 1;\n        \n        while (slt32(skip_threads_12304, 32)) {\n            if (slt32(local_tid_10285, num_groups_9877) &&\n                sle32(skip_threads_12304, local_tid_10285 -\n                      squot32(local_tid_10285, 32) * 32)) {\n                // read operands\n                {\n                    x_10281 = *(volatile __local\n                                int32_t *) &mem_11713[(local_tid_10285 -\n                                                       skip_threads_12304) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_10283;\n                    \n                    if (thread_active_12298) {\n                        zz_10283 = x_10281 + y_10282;\n                    }\n                    y_10282 = zz_10283;\n                }\n            }\n            if (sle32(wave_sizze_12296, skip_threads_12304)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10285, num_groups_9877) &&\n                sle32(skip_threads_12304, local_tid_10285 -\n                      squot32(local_tid_10285, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11713[local_tid_10285 *\n                                                             sizeof(int32_t)] =\n                        y_10282;\n                }\n            }\n            if (sle32(wave_sizze_12296, skip_threads_12304)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12304 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_10285 - squot32(local_tid_10285, 32) * 32) == 31 &&\n            slt32(local_tid_10285, num_grou",
                   "ps_9877)) {\n            *(volatile __local int32_t *) &mem_11713[squot32(local_tid_10285,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_10282;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10285, 32) == 0 && slt32(local_tid_10285,\n                                                       num_groups_9877)) {\n            y_12302 = *(volatile __local int32_t *) &mem_11713[local_tid_10285 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12305 = 1;\n            \n            while (slt32(skip_threads_12305, 32)) {\n                if ((squot32(local_tid_10285, 32) == 0 && slt32(local_tid_10285,\n                                                                num_groups_9877)) &&\n                    sle32(skip_threads_12305, local_tid_10285 -\n                          squot32(local_tid_10285, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12301 = *(volatile __local\n                                    int32_t *) &mem_11713[(local_tid_10285 -\n                                                           skip_threads_12305) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12303;\n                        \n                        if (thread_active_12298) {\n                            zz_12303 = x_12301 + y_12302;\n                        }\n                        y_12302 = zz_12303;\n                    }\n                }\n                if (sle32(wave_sizze_12296, skip_threads_12305)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);",
                   "\n                }\n                if ((squot32(local_tid_10285, 32) == 0 && slt32(local_tid_10285,\n                                                                num_groups_9877)) &&\n                    sle32(skip_threads_12305, local_tid_10285 -\n                          squot32(local_tid_10285, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11713[local_tid_10285 *\n                                                sizeof(int32_t)] = y_12302;\n                    }\n                }\n                if (sle32(wave_sizze_12296, skip_threads_12305)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12305 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10285, 32) == 0 || !slt32(local_tid_10285,\n                                                          num_groups_9877))) {\n            // read operands\n            {\n                x_10281 = *(volatile __local\n                            int32_t *) &mem_11713[(squot32(local_tid_10285,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_10283;\n                \n                if (thread_active_12298) {\n                    zz_10283 = x_10281 + y_10282;\n                }\n                y_10282 = zz_10283;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11713[local_tid_10285 *\n                                                         sizeof(int32_t)] =\n                    y_10282;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10285,",
                   " 32) == 0) {\n            *(volatile __local int32_t *) &mem_11713[local_tid_10285 *\n                                                     sizeof(int32_t)] = y_10282;\n        }\n    }\n    \n    int32_t scanned_elem_10290;\n    \n    if (thread_active_12298) {\n        scanned_elem_10290 = *(__local int32_t *) &mem_11713[local_tid_10285 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11716[global_tid_10284 * 4] = scanned_elem_10290;\n}\n__kernel void scan2_kernel_10458(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_10397, __global\n                                 unsigned char *mem_11623, __global\n                                 unsigned char *mem_11629)\n{\n    __local volatile char *restrict mem_11626 = mem_aligned_0;\n    int32_t wave_sizze_12337;\n    int32_t group_sizze_12338;\n    char thread_active_12339;\n    int32_t global_tid_10458;\n    int32_t local_tid_10459;\n    int32_t group_id_10460;\n    \n    global_tid_10458 = get_global_id(0);\n    local_tid_10459 = get_local_id(0);\n    group_sizze_12338 = get_local_size(0);\n    wave_sizze_12337 = LOCKSTEP_WIDTH;\n    group_id_10460 = get_group_id(0);\n    thread_active_12339 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10459, num_groups_10397) && 1) {\n        int32_t offsets_group_sums_elem_10461 = *(__global\n                                                  int32_t *) &mem_11623[local_tid_10459 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11626[local_tid_10459 * 4] =\n            offsets_group_sums_elem_10461;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10453;\n    int32_t other_index_10454;\n    int32_t x_10455;\n    int32_t y_10456;\n    int32_t my_index_12340;\n    int32_t other_index_12341;\n    int32_t x_12342;\n    int32_t y_12343;\n    \n    my_index_10453 = local_tid_10459;\n    if (slt32(local_tid_10459, num_groups_1039",
                   "7)) {\n        y_10456 = *(volatile __local int32_t *) &mem_11626[local_tid_10459 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12345 = 1;\n        \n        while (slt32(skip_threads_12345, 32)) {\n            if (slt32(local_tid_10459, num_groups_10397) &&\n                sle32(skip_threads_12345, local_tid_10459 -\n                      squot32(local_tid_10459, 32) * 32)) {\n                // read operands\n                {\n                    x_10455 = *(volatile __local\n                                int32_t *) &mem_11626[(local_tid_10459 -\n                                                       skip_threads_12345) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_10457;\n                    \n                    if (thread_active_12339) {\n                        zz_10457 = x_10455 + y_10456;\n                    }\n                    y_10456 = zz_10457;\n                }\n            }\n            if (sle32(wave_sizze_12337, skip_threads_12345)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10459, num_groups_10397) &&\n                sle32(skip_threads_12345, local_tid_10459 -\n                      squot32(local_tid_10459, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11626[local_tid_10459 *\n                                                             sizeof(int32_t)] =\n                        y_10456;\n                }\n            }\n            if (sle32(wave_sizze_12337, skip_threads_12345)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12345 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'",
                   "\n    {\n        if ((local_tid_10459 - squot32(local_tid_10459, 32) * 32) == 31 &&\n            slt32(local_tid_10459, num_groups_10397)) {\n            *(volatile __local int32_t *) &mem_11626[squot32(local_tid_10459,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_10456;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10459, 32) == 0 && slt32(local_tid_10459,\n                                                       num_groups_10397)) {\n            y_12343 = *(volatile __local int32_t *) &mem_11626[local_tid_10459 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12346 = 1;\n            \n            while (slt32(skip_threads_12346, 32)) {\n                if ((squot32(local_tid_10459, 32) == 0 && slt32(local_tid_10459,\n                                                                num_groups_10397)) &&\n                    sle32(skip_threads_12346, local_tid_10459 -\n                          squot32(local_tid_10459, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12342 = *(volatile __local\n                                    int32_t *) &mem_11626[(local_tid_10459 -\n                                                           skip_threads_12346) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12344;\n                        \n                        if (thread_active_12339) {\n                            zz_12344 = x_12342 + y_12343;\n                        }\n                        y_12343 = zz_12344;\n                    }\n      ",
                   "          }\n                if (sle32(wave_sizze_12337, skip_threads_12346)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_10459, 32) == 0 && slt32(local_tid_10459,\n                                                                num_groups_10397)) &&\n                    sle32(skip_threads_12346, local_tid_10459 -\n                          squot32(local_tid_10459, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11626[local_tid_10459 *\n                                                sizeof(int32_t)] = y_12343;\n                    }\n                }\n                if (sle32(wave_sizze_12337, skip_threads_12346)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12346 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10459, 32) == 0 || !slt32(local_tid_10459,\n                                                          num_groups_10397))) {\n            // read operands\n            {\n                x_10455 = *(volatile __local\n                            int32_t *) &mem_11626[(squot32(local_tid_10459,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_10457;\n                \n                if (thread_active_12339) {\n                    zz_10457 = x_10455 + y_10456;\n                }\n                y_10456 = zz_10457;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11626[local_tid_10459 *\n                                                         sizeof(int32_t)] =\n                    y_10456;\n            }\n        ",
                   "}\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10459, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11626[local_tid_10459 *\n                                                     sizeof(int32_t)] = y_10456;\n        }\n    }\n    \n    int32_t scanned_elem_10464;\n    \n    if (thread_active_12339) {\n        scanned_elem_10464 = *(__local int32_t *) &mem_11626[local_tid_10459 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11629[global_tid_10458 * 4] = scanned_elem_10464;\n}\n__kernel void scan2_kernel_10564(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_10503, __global\n                                 unsigned char *mem_11653, __global\n                                 unsigned char *mem_11659)\n{\n    __local volatile char *restrict mem_11656 = mem_aligned_0;\n    int32_t wave_sizze_12366;\n    int32_t group_sizze_12367;\n    char thread_active_12368;\n    int32_t global_tid_10564;\n    int32_t local_tid_10565;\n    int32_t group_id_10566;\n    \n    global_tid_10564 = get_global_id(0);\n    local_tid_10565 = get_local_id(0);\n    group_sizze_12367 = get_local_size(0);\n    wave_sizze_12366 = LOCKSTEP_WIDTH;\n    group_id_10566 = get_group_id(0);\n    thread_active_12368 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10565, num_groups_10503) && 1) {\n        int32_t offsets_group_sums_elem_10567 = *(__global\n                                                  int32_t *) &mem_11653[local_tid_10565 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11656[local_tid_10565 * 4] =\n            offsets_group_sums_elem_10567;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10559;\n    int32_t other_index_10560;\n    int32_t x_10561;\n    int32_t y_10562;\n    int32_t my_index_12369;\n    int32_t other_index_12370;\n ",
                   "   int32_t x_12371;\n    int32_t y_12372;\n    \n    my_index_10559 = local_tid_10565;\n    if (slt32(local_tid_10565, num_groups_10503)) {\n        y_10562 = *(volatile __local int32_t *) &mem_11656[local_tid_10565 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12374 = 1;\n        \n        while (slt32(skip_threads_12374, 32)) {\n            if (slt32(local_tid_10565, num_groups_10503) &&\n                sle32(skip_threads_12374, local_tid_10565 -\n                      squot32(local_tid_10565, 32) * 32)) {\n                // read operands\n                {\n                    x_10561 = *(volatile __local\n                                int32_t *) &mem_11656[(local_tid_10565 -\n                                                       skip_threads_12374) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_10563;\n                    \n                    if (thread_active_12368) {\n                        zz_10563 = x_10561 + y_10562;\n                    }\n                    y_10562 = zz_10563;\n                }\n            }\n            if (sle32(wave_sizze_12366, skip_threads_12374)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10565, num_groups_10503) &&\n                sle32(skip_threads_12374, local_tid_10565 -\n                      squot32(local_tid_10565, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11656[local_tid_10565 *\n                                                             sizeof(int32_t)] =\n                        y_10562;\n                }\n            }\n            if (sle32(wave_sizze_12366, skip_threads_12374)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_thr",
                   "eads_12374 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_10565 - squot32(local_tid_10565, 32) * 32) == 31 &&\n            slt32(local_tid_10565, num_groups_10503)) {\n            *(volatile __local int32_t *) &mem_11656[squot32(local_tid_10565,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_10562;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10565, 32) == 0 && slt32(local_tid_10565,\n                                                       num_groups_10503)) {\n            y_12372 = *(volatile __local int32_t *) &mem_11656[local_tid_10565 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12375 = 1;\n            \n            while (slt32(skip_threads_12375, 32)) {\n                if ((squot32(local_tid_10565, 32) == 0 && slt32(local_tid_10565,\n                                                                num_groups_10503)) &&\n                    sle32(skip_threads_12375, local_tid_10565 -\n                          squot32(local_tid_10565, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12371 = *(volatile __local\n                                    int32_t *) &mem_11656[(local_tid_10565 -\n                                                           skip_threads_12375) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12373;\n                        \n                        if (thread_active_12368) {\n                          ",
                   "  zz_12373 = x_12371 + y_12372;\n                        }\n                        y_12372 = zz_12373;\n                    }\n                }\n                if (sle32(wave_sizze_12366, skip_threads_12375)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_10565, 32) == 0 && slt32(local_tid_10565,\n                                                                num_groups_10503)) &&\n                    sle32(skip_threads_12375, local_tid_10565 -\n                          squot32(local_tid_10565, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11656[local_tid_10565 *\n                                                sizeof(int32_t)] = y_12372;\n                    }\n                }\n                if (sle32(wave_sizze_12366, skip_threads_12375)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12375 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10565, 32) == 0 || !slt32(local_tid_10565,\n                                                          num_groups_10503))) {\n            // read operands\n            {\n                x_10561 = *(volatile __local\n                            int32_t *) &mem_11656[(squot32(local_tid_10565,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_10563;\n                \n                if (thread_active_12368) {\n                    zz_10563 = x_10561 + y_10562;\n                }\n                y_10562 = zz_10563;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11656[local_tid_10565",
                   " *\n                                                         sizeof(int32_t)] =\n                    y_10562;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10565, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11656[local_tid_10565 *\n                                                     sizeof(int32_t)] = y_10562;\n        }\n    }\n    \n    int32_t scanned_elem_10570;\n    \n    if (thread_active_12368) {\n        scanned_elem_10570 = *(__local int32_t *) &mem_11656[local_tid_10565 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11659[global_tid_10564 * 4] = scanned_elem_10570;\n}\n__kernel void scan2_kernel_10670(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_10609, __global\n                                 unsigned char *mem_11683, __global\n                                 unsigned char *mem_11689)\n{\n    __local volatile char *restrict mem_11686 = mem_aligned_0;\n    int32_t wave_sizze_12395;\n    int32_t group_sizze_12396;\n    char thread_active_12397;\n    int32_t global_tid_10670;\n    int32_t local_tid_10671;\n    int32_t group_id_10672;\n    \n    global_tid_10670 = get_global_id(0);\n    local_tid_10671 = get_local_id(0);\n    group_sizze_12396 = get_local_size(0);\n    wave_sizze_12395 = LOCKSTEP_WIDTH;\n    group_id_10672 = get_group_id(0);\n    thread_active_12397 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10671, num_groups_10609) && 1) {\n        int32_t offsets_group_sums_elem_10673 = *(__global\n                                                  int32_t *) &mem_11683[local_tid_10671 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11686[local_tid_10671 * 4] =\n            offsets_group_sums_elem_10673;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10665;\n   ",
                   " int32_t other_index_10666;\n    int32_t x_10667;\n    int32_t y_10668;\n    int32_t my_index_12398;\n    int32_t other_index_12399;\n    int32_t x_12400;\n    int32_t y_12401;\n    \n    my_index_10665 = local_tid_10671;\n    if (slt32(local_tid_10671, num_groups_10609)) {\n        y_10668 = *(volatile __local int32_t *) &mem_11686[local_tid_10671 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12403 = 1;\n        \n        while (slt32(skip_threads_12403, 32)) {\n            if (slt32(local_tid_10671, num_groups_10609) &&\n                sle32(skip_threads_12403, local_tid_10671 -\n                      squot32(local_tid_10671, 32) * 32)) {\n                // read operands\n                {\n                    x_10667 = *(volatile __local\n                                int32_t *) &mem_11686[(local_tid_10671 -\n                                                       skip_threads_12403) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_10669;\n                    \n                    if (thread_active_12397) {\n                        zz_10669 = x_10667 + y_10668;\n                    }\n                    y_10668 = zz_10669;\n                }\n            }\n            if (sle32(wave_sizze_12395, skip_threads_12403)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10671, num_groups_10609) &&\n                sle32(skip_threads_12403, local_tid_10671 -\n                      squot32(local_tid_10671, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11686[local_tid_10671 *\n                                                             sizeof(int32_t)] =\n                        y_10668;\n                }\n            }\n            i",
                   "f (sle32(wave_sizze_12395, skip_threads_12403)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12403 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_10671 - squot32(local_tid_10671, 32) * 32) == 31 &&\n            slt32(local_tid_10671, num_groups_10609)) {\n            *(volatile __local int32_t *) &mem_11686[squot32(local_tid_10671,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_10668;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10671, 32) == 0 && slt32(local_tid_10671,\n                                                       num_groups_10609)) {\n            y_12401 = *(volatile __local int32_t *) &mem_11686[local_tid_10671 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12404 = 1;\n            \n            while (slt32(skip_threads_12404, 32)) {\n                if ((squot32(local_tid_10671, 32) == 0 && slt32(local_tid_10671,\n                                                                num_groups_10609)) &&\n                    sle32(skip_threads_12404, local_tid_10671 -\n                          squot32(local_tid_10671, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12400 = *(volatile __local\n                                    int32_t *) &mem_11686[(local_tid_10671 -\n                                                           skip_threads_12404) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n              ",
                   "          int32_t zz_12402;\n                        \n                        if (thread_active_12397) {\n                            zz_12402 = x_12400 + y_12401;\n                        }\n                        y_12401 = zz_12402;\n                    }\n                }\n                if (sle32(wave_sizze_12395, skip_threads_12404)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_10671, 32) == 0 && slt32(local_tid_10671,\n                                                                num_groups_10609)) &&\n                    sle32(skip_threads_12404, local_tid_10671 -\n                          squot32(local_tid_10671, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11686[local_tid_10671 *\n                                                sizeof(int32_t)] = y_12401;\n                    }\n                }\n                if (sle32(wave_sizze_12395, skip_threads_12404)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12404 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10671, 32) == 0 || !slt32(local_tid_10671,\n                                                          num_groups_10609))) {\n            // read operands\n            {\n                x_10667 = *(volatile __local\n                            int32_t *) &mem_11686[(squot32(local_tid_10671,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_10669;\n                \n                if (thread_active_12397) {\n                    zz_10669 = x_10667 + y_10668;\n                }\n                y_10668 = zz_10669;\n    ",
                   "        }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11686[local_tid_10671 *\n                                                         sizeof(int32_t)] =\n                    y_10668;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10671, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11686[local_tid_10671 *\n                                                     sizeof(int32_t)] = y_10668;\n        }\n    }\n    \n    int32_t scanned_elem_10676;\n    \n    if (thread_active_12397) {\n        scanned_elem_10676 = *(__local int32_t *) &mem_11686[local_tid_10671 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11689[global_tid_10670 * 4] = scanned_elem_10676;\n}\n__kernel void scan2_kernel_10776(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_10715, __global\n                                 unsigned char *mem_11713, __global\n                                 unsigned char *mem_11719)\n{\n    __local volatile char *restrict mem_11716 = mem_aligned_0;\n    int32_t wave_sizze_12424;\n    int32_t group_sizze_12425;\n    char thread_active_12426;\n    int32_t global_tid_10776;\n    int32_t local_tid_10777;\n    int32_t group_id_10778;\n    \n    global_tid_10776 = get_global_id(0);\n    local_tid_10777 = get_local_id(0);\n    group_sizze_12425 = get_local_size(0);\n    wave_sizze_12424 = LOCKSTEP_WIDTH;\n    group_id_10778 = get_group_id(0);\n    thread_active_12426 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10777, num_groups_10715) && 1) {\n        int32_t offsets_group_sums_elem_10779 = *(__global\n                                                  int32_t *) &mem_11713[local_tid_10777 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11716[local_tid_10",
                   "777 * 4] =\n            offsets_group_sums_elem_10779;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10771;\n    int32_t other_index_10772;\n    int32_t x_10773;\n    int32_t y_10774;\n    int32_t my_index_12427;\n    int32_t other_index_12428;\n    int32_t x_12429;\n    int32_t y_12430;\n    \n    my_index_10771 = local_tid_10777;\n    if (slt32(local_tid_10777, num_groups_10715)) {\n        y_10774 = *(volatile __local int32_t *) &mem_11716[local_tid_10777 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12432 = 1;\n        \n        while (slt32(skip_threads_12432, 32)) {\n            if (slt32(local_tid_10777, num_groups_10715) &&\n                sle32(skip_threads_12432, local_tid_10777 -\n                      squot32(local_tid_10777, 32) * 32)) {\n                // read operands\n                {\n                    x_10773 = *(volatile __local\n                                int32_t *) &mem_11716[(local_tid_10777 -\n                                                       skip_threads_12432) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_10775;\n                    \n                    if (thread_active_12426) {\n                        zz_10775 = x_10773 + y_10774;\n                    }\n                    y_10774 = zz_10775;\n                }\n            }\n            if (sle32(wave_sizze_12424, skip_threads_12432)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10777, num_groups_10715) &&\n                sle32(skip_threads_12432, local_tid_10777 -\n                      squot32(local_tid_10777, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11716[local_tid_10777 *\n                            ",
                   "                                 sizeof(int32_t)] =\n                        y_10774;\n                }\n            }\n            if (sle32(wave_sizze_12424, skip_threads_12432)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12432 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_10777 - squot32(local_tid_10777, 32) * 32) == 31 &&\n            slt32(local_tid_10777, num_groups_10715)) {\n            *(volatile __local int32_t *) &mem_11716[squot32(local_tid_10777,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_10774;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10777, 32) == 0 && slt32(local_tid_10777,\n                                                       num_groups_10715)) {\n            y_12430 = *(volatile __local int32_t *) &mem_11716[local_tid_10777 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12433 = 1;\n            \n            while (slt32(skip_threads_12433, 32)) {\n                if ((squot32(local_tid_10777, 32) == 0 && slt32(local_tid_10777,\n                                                                num_groups_10715)) &&\n                    sle32(skip_threads_12433, local_tid_10777 -\n                          squot32(local_tid_10777, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12429 = *(volatile __local\n                                    int32_t *) &mem_11716[(local_tid_10777 -\n                                                           skip_threads_12433) *\n                                             ",
                   "             sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12431;\n                        \n                        if (thread_active_12426) {\n                            zz_12431 = x_12429 + y_12430;\n                        }\n                        y_12430 = zz_12431;\n                    }\n                }\n                if (sle32(wave_sizze_12424, skip_threads_12433)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_10777, 32) == 0 && slt32(local_tid_10777,\n                                                                num_groups_10715)) &&\n                    sle32(skip_threads_12433, local_tid_10777 -\n                          squot32(local_tid_10777, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11716[local_tid_10777 *\n                                                sizeof(int32_t)] = y_12430;\n                    }\n                }\n                if (sle32(wave_sizze_12424, skip_threads_12433)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12433 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10777, 32) == 0 || !slt32(local_tid_10777,\n                                                          num_groups_10715))) {\n            // read operands\n            {\n                x_10773 = *(volatile __local\n                            int32_t *) &mem_11716[(squot32(local_tid_10777,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_10775;\n                \n                if (t",
                   "hread_active_12426) {\n                    zz_10775 = x_10773 + y_10774;\n                }\n                y_10774 = zz_10775;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11716[local_tid_10777 *\n                                                         sizeof(int32_t)] =\n                    y_10774;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10777, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11716[local_tid_10777 *\n                                                     sizeof(int32_t)] = y_10774;\n        }\n    }\n    \n    int32_t scanned_elem_10782;\n    \n    if (thread_active_12426) {\n        scanned_elem_10782 = *(__local int32_t *) &mem_11716[local_tid_10777 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11719[global_tid_10776 * 4] = scanned_elem_10782;\n}\n__kernel void scan2_kernel_10959(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_10843, __global\n                                 unsigned char *mem_11746, __global\n                                 unsigned char *mem_11752)\n{\n    __local volatile char *restrict mem_11749 = mem_aligned_0;\n    int32_t wave_sizze_12455;\n    int32_t group_sizze_12456;\n    char thread_active_12457;\n    int32_t global_tid_10959;\n    int32_t local_tid_10960;\n    int32_t group_id_10961;\n    \n    global_tid_10959 = get_global_id(0);\n    local_tid_10960 = get_local_id(0);\n    group_sizze_12456 = get_local_size(0);\n    wave_sizze_12455 = LOCKSTEP_WIDTH;\n    group_id_10961 = get_group_id(0);\n    thread_active_12457 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_10960, num_groups_10843) && 1) {\n        int32_t offsets_group_sums_elem_10962 = *(__global\n                                                  int32_t *) &mem_11746[local_tid_10960 *\n       ",
                   "                                                                 4];\n        \n        *(__local int32_t *) &mem_11749[local_tid_10960 * 4] =\n            offsets_group_sums_elem_10962;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_10954;\n    int32_t other_index_10955;\n    int32_t x_10956;\n    int32_t y_10957;\n    int32_t my_index_12458;\n    int32_t other_index_12459;\n    int32_t x_12460;\n    int32_t y_12461;\n    \n    my_index_10954 = local_tid_10960;\n    if (slt32(local_tid_10960, num_groups_10843)) {\n        y_10957 = *(volatile __local int32_t *) &mem_11749[local_tid_10960 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12463 = 1;\n        \n        while (slt32(skip_threads_12463, 32)) {\n            if (slt32(local_tid_10960, num_groups_10843) &&\n                sle32(skip_threads_12463, local_tid_10960 -\n                      squot32(local_tid_10960, 32) * 32)) {\n                // read operands\n                {\n                    x_10956 = *(volatile __local\n                                int32_t *) &mem_11749[(local_tid_10960 -\n                                                       skip_threads_12463) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_10958;\n                    \n                    if (thread_active_12457) {\n                        zz_10958 = x_10956 + y_10957;\n                    }\n                    y_10957 = zz_10958;\n                }\n            }\n            if (sle32(wave_sizze_12455, skip_threads_12463)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_10960, num_groups_10843) &&\n                sle32(skip_threads_12463, local_tid_10960 -\n                      squot32(local_tid_10960, 32) * 32)) {\n                // write re",
                   "sult\n                {\n                    *(volatile __local int32_t *) &mem_11749[local_tid_10960 *\n                                                             sizeof(int32_t)] =\n                        y_10957;\n                }\n            }\n            if (sle32(wave_sizze_12455, skip_threads_12463)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12463 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_10960 - squot32(local_tid_10960, 32) * 32) == 31 &&\n            slt32(local_tid_10960, num_groups_10843)) {\n            *(volatile __local int32_t *) &mem_11749[squot32(local_tid_10960,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_10957;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_10960, 32) == 0 && slt32(local_tid_10960,\n                                                       num_groups_10843)) {\n            y_12461 = *(volatile __local int32_t *) &mem_11749[local_tid_10960 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12464 = 1;\n            \n            while (slt32(skip_threads_12464, 32)) {\n                if ((squot32(local_tid_10960, 32) == 0 && slt32(local_tid_10960,\n                                                                num_groups_10843)) &&\n                    sle32(skip_threads_12464, local_tid_10960 -\n                          squot32(local_tid_10960, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12460 = *(volatile __local\n                                    int32_t *) &mem_11749[(local_tid_1096",
                   "0 -\n                                                           skip_threads_12464) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12462;\n                        \n                        if (thread_active_12457) {\n                            zz_12462 = x_12460 + y_12461;\n                        }\n                        y_12461 = zz_12462;\n                    }\n                }\n                if (sle32(wave_sizze_12455, skip_threads_12464)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_10960, 32) == 0 && slt32(local_tid_10960,\n                                                                num_groups_10843)) &&\n                    sle32(skip_threads_12464, local_tid_10960 -\n                          squot32(local_tid_10960, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11749[local_tid_10960 *\n                                                sizeof(int32_t)] = y_12461;\n                    }\n                }\n                if (sle32(wave_sizze_12455, skip_threads_12464)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12464 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_10960, 32) == 0 || !slt32(local_tid_10960,\n                                                          num_groups_10843))) {\n            // read operands\n            {\n                x_10956 = *(volatile __local\n                            int32_t *) &mem_11749[(squot32(local_tid_10960,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n   ",
                   "         }\n            // perform operation\n            {\n                int32_t zz_10958;\n                \n                if (thread_active_12457) {\n                    zz_10958 = x_10956 + y_10957;\n                }\n                y_10957 = zz_10958;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11749[local_tid_10960 *\n                                                         sizeof(int32_t)] =\n                    y_10957;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_10960, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11749[local_tid_10960 *\n                                                     sizeof(int32_t)] = y_10957;\n        }\n    }\n    \n    int32_t scanned_elem_10965;\n    \n    if (thread_active_12457) {\n        scanned_elem_10965 = *(__local int32_t *) &mem_11749[local_tid_10960 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11752[global_tid_10959 * 4] = scanned_elem_10965;\n}\n__kernel void scan2_kernel_11142(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_11026, __global\n                                 unsigned char *mem_11779, __global\n                                 unsigned char *mem_11785)\n{\n    __local volatile char *restrict mem_11782 = mem_aligned_0;\n    int32_t wave_sizze_12486;\n    int32_t group_sizze_12487;\n    char thread_active_12488;\n    int32_t global_tid_11142;\n    int32_t local_tid_11143;\n    int32_t group_id_11144;\n    \n    global_tid_11142 = get_global_id(0);\n    local_tid_11143 = get_local_id(0);\n    group_sizze_12487 = get_local_size(0);\n    wave_sizze_12486 = LOCKSTEP_WIDTH;\n    group_id_11144 = get_group_id(0);\n    thread_active_12488 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_11143, num_groups_11026) && 1) {\n        int32_t offsets_gr",
                   "oup_sums_elem_11145 = *(__global\n                                                  int32_t *) &mem_11779[local_tid_11143 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11782[local_tid_11143 * 4] =\n            offsets_group_sums_elem_11145;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_11137;\n    int32_t other_index_11138;\n    int32_t x_11139;\n    int32_t y_11140;\n    int32_t my_index_12489;\n    int32_t other_index_12490;\n    int32_t x_12491;\n    int32_t y_12492;\n    \n    my_index_11137 = local_tid_11143;\n    if (slt32(local_tid_11143, num_groups_11026)) {\n        y_11140 = *(volatile __local int32_t *) &mem_11782[local_tid_11143 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12494 = 1;\n        \n        while (slt32(skip_threads_12494, 32)) {\n            if (slt32(local_tid_11143, num_groups_11026) &&\n                sle32(skip_threads_12494, local_tid_11143 -\n                      squot32(local_tid_11143, 32) * 32)) {\n                // read operands\n                {\n                    x_11139 = *(volatile __local\n                                int32_t *) &mem_11782[(local_tid_11143 -\n                                                       skip_threads_12494) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_11141;\n                    \n                    if (thread_active_12488) {\n                        zz_11141 = x_11139 + y_11140;\n                    }\n                    y_11140 = zz_11141;\n                }\n            }\n            if (sle32(wave_sizze_12486, skip_threads_12494)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_11143, num_groups_11026) &&\n                s",
                   "le32(skip_threads_12494, local_tid_11143 -\n                      squot32(local_tid_11143, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11782[local_tid_11143 *\n                                                             sizeof(int32_t)] =\n                        y_11140;\n                }\n            }\n            if (sle32(wave_sizze_12486, skip_threads_12494)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12494 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_11143 - squot32(local_tid_11143, 32) * 32) == 31 &&\n            slt32(local_tid_11143, num_groups_11026)) {\n            *(volatile __local int32_t *) &mem_11782[squot32(local_tid_11143,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_11140;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_11143, 32) == 0 && slt32(local_tid_11143,\n                                                       num_groups_11026)) {\n            y_12492 = *(volatile __local int32_t *) &mem_11782[local_tid_11143 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12495 = 1;\n            \n            while (slt32(skip_threads_12495, 32)) {\n                if ((squot32(local_tid_11143, 32) == 0 && slt32(local_tid_11143,\n                                                                num_groups_11026)) &&\n                    sle32(skip_threads_12495, local_tid_11143 -\n                          squot32(local_tid_11143, 32) * 32)) {\n                    // read operands\n                  ",
                   "  {\n                        x_12491 = *(volatile __local\n                                    int32_t *) &mem_11782[(local_tid_11143 -\n                                                           skip_threads_12495) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12493;\n                        \n                        if (thread_active_12488) {\n                            zz_12493 = x_12491 + y_12492;\n                        }\n                        y_12492 = zz_12493;\n                    }\n                }\n                if (sle32(wave_sizze_12486, skip_threads_12495)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_11143, 32) == 0 && slt32(local_tid_11143,\n                                                                num_groups_11026)) &&\n                    sle32(skip_threads_12495, local_tid_11143 -\n                          squot32(local_tid_11143, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11782[local_tid_11143 *\n                                                sizeof(int32_t)] = y_12492;\n                    }\n                }\n                if (sle32(wave_sizze_12486, skip_threads_12495)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12495 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_11143, 32) == 0 || !slt32(local_tid_11143,\n                                                          num_groups_11026))) {\n            // read operands\n            {\n                x_11139 = *(volatile __local\n                            int32_t *) &mem_11782[(squot32(local_tid_11143,\n           ",
                   "                                                32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_11141;\n                \n                if (thread_active_12488) {\n                    zz_11141 = x_11139 + y_11140;\n                }\n                y_11140 = zz_11141;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11782[local_tid_11143 *\n                                                         sizeof(int32_t)] =\n                    y_11140;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_11143, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11782[local_tid_11143 *\n                                                     sizeof(int32_t)] = y_11140;\n        }\n    }\n    \n    int32_t scanned_elem_11148;\n    \n    if (thread_active_12488) {\n        scanned_elem_11148 = *(__local int32_t *) &mem_11782[local_tid_11143 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11785[global_tid_11142 * 4] = scanned_elem_11148;\n}\n__kernel void scan2_kernel_11486(__local volatile int64_t *mem_aligned_0,\n                                 int32_t num_groups_11259, __global\n                                 unsigned char *mem_11881, __global\n                                 unsigned char *mem_11887)\n{\n    __local volatile char *restrict mem_11884 = mem_aligned_0;\n    int32_t wave_sizze_12548;\n    int32_t group_sizze_12549;\n    char thread_active_12550;\n    int32_t global_tid_11486;\n    int32_t local_tid_11487;\n    int32_t group_id_11488;\n    \n    global_tid_11486 = get_global_id(0);\n    local_tid_11487 = get_local_id(0);\n    group_sizze_12549 = get_local_size(0);\n    wave_sizze_12548 = LOCKSTEP_WIDTH;\n    group_id_11488 = get_group_id(0);\n    thread_activ",
                   "e_12550 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_11487, num_groups_11259) && 1) {\n        int32_t offsets_group_sums_elem_11489 = *(__global\n                                                  int32_t *) &mem_11881[local_tid_11487 *\n                                                                        4];\n        \n        *(__local int32_t *) &mem_11884[local_tid_11487 * 4] =\n            offsets_group_sums_elem_11489;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_11481;\n    int32_t other_index_11482;\n    int32_t x_11483;\n    int32_t y_11484;\n    int32_t my_index_12551;\n    int32_t other_index_12552;\n    int32_t x_12553;\n    int32_t y_12554;\n    \n    my_index_11481 = local_tid_11487;\n    if (slt32(local_tid_11487, num_groups_11259)) {\n        y_11484 = *(volatile __local int32_t *) &mem_11884[local_tid_11487 *\n                                                           sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12556 = 1;\n        \n        while (slt32(skip_threads_12556, 32)) {\n            if (slt32(local_tid_11487, num_groups_11259) &&\n                sle32(skip_threads_12556, local_tid_11487 -\n                      squot32(local_tid_11487, 32) * 32)) {\n                // read operands\n                {\n                    x_11483 = *(volatile __local\n                                int32_t *) &mem_11884[(local_tid_11487 -\n                                                       skip_threads_12556) *\n                                                      sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_11485;\n                    \n                    if (thread_active_12550) {\n                        zz_11485 = x_11483 + y_11484;\n                    }\n                    y_11484 = zz_11485;\n                }\n            }\n            if (sle32(wave_sizze_12548, skip_threads_12556)) {\n       ",
                   "         barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_11487, num_groups_11259) &&\n                sle32(skip_threads_12556, local_tid_11487 -\n                      squot32(local_tid_11487, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11884[local_tid_11487 *\n                                                             sizeof(int32_t)] =\n                        y_11484;\n                }\n            }\n            if (sle32(wave_sizze_12548, skip_threads_12556)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12556 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_11487 - squot32(local_tid_11487, 32) * 32) == 31 &&\n            slt32(local_tid_11487, num_groups_11259)) {\n            *(volatile __local int32_t *) &mem_11884[squot32(local_tid_11487,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_11484;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_11487, 32) == 0 && slt32(local_tid_11487,\n                                                       num_groups_11259)) {\n            y_12554 = *(volatile __local int32_t *) &mem_11884[local_tid_11487 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12557 = 1;\n            \n            while (slt32(skip_threads_12557, 32)) {\n                if ((squot32(local_tid_11487, 32) == 0 && slt32(local_tid_11487,\n                                                                num_groups_11259)) &&\n                    sle32(skip_threads_12557, local_t",
                   "id_11487 -\n                          squot32(local_tid_11487, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12553 = *(volatile __local\n                                    int32_t *) &mem_11884[(local_tid_11487 -\n                                                           skip_threads_12557) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12555;\n                        \n                        if (thread_active_12550) {\n                            zz_12555 = x_12553 + y_12554;\n                        }\n                        y_12554 = zz_12555;\n                    }\n                }\n                if (sle32(wave_sizze_12548, skip_threads_12557)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_11487, 32) == 0 && slt32(local_tid_11487,\n                                                                num_groups_11259)) &&\n                    sle32(skip_threads_12557, local_tid_11487 -\n                          squot32(local_tid_11487, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11884[local_tid_11487 *\n                                                sizeof(int32_t)] = y_12554;\n                    }\n                }\n                if (sle32(wave_sizze_12548, skip_threads_12557)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12557 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_11487, 32) == 0 || !slt32(local_tid_11487,\n                                                          num_groups_11259))) {\n            // read operands\n            {\n  ",
                   "              x_11483 = *(volatile __local\n                            int32_t *) &mem_11884[(squot32(local_tid_11487,\n                                                           32) - 1) *\n                                                  sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_11485;\n                \n                if (thread_active_12550) {\n                    zz_11485 = x_11483 + y_11484;\n                }\n                y_11484 = zz_11485;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11884[local_tid_11487 *\n                                                         sizeof(int32_t)] =\n                    y_11484;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_11487, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11884[local_tid_11487 *\n                                                     sizeof(int32_t)] = y_11484;\n        }\n    }\n    \n    int32_t scanned_elem_11492;\n    \n    if (thread_active_12550) {\n        scanned_elem_11492 = *(__local int32_t *) &mem_11884[local_tid_11487 *\n                                                             4];\n    }\n    *(__global int32_t *) &mem_11887[global_tid_11486 * 4] = scanned_elem_11492;\n}\n__kernel void scan2_kernel_9046(__local volatile int64_t *mem_aligned_0,\n                                int32_t num_groups_8985, __global\n                                unsigned char *mem_11623, __global\n                                unsigned char *mem_11629)\n{\n    __local volatile char *restrict mem_11626 = mem_aligned_0;\n    int32_t wave_sizze_11984;\n    int32_t group_sizze_11985;\n    char thread_active_11986;\n    int32_t global_tid_9046;\n    int32_t local_tid_9047;\n    int32_t group_id_9048;\n    \n    global_tid_9046 = get_global_id(0);\n    local_tid_9047 = get_local_id(0);\n    group_sizze_",
                   "11985 = get_local_size(0);\n    wave_sizze_11984 = LOCKSTEP_WIDTH;\n    group_id_9048 = get_group_id(0);\n    thread_active_11986 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9047, num_groups_8985) && 1) {\n        int32_t offsets_group_sums_elem_9049 = *(__global\n                                                 int32_t *) &mem_11623[local_tid_9047 *\n                                                                       4];\n        \n        *(__local int32_t *) &mem_11626[local_tid_9047 * 4] =\n            offsets_group_sums_elem_9049;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_9041;\n    int32_t other_index_9042;\n    int32_t x_9043;\n    int32_t y_9044;\n    int32_t my_index_11987;\n    int32_t other_index_11988;\n    int32_t x_11989;\n    int32_t y_11990;\n    \n    my_index_9041 = local_tid_9047;\n    if (slt32(local_tid_9047, num_groups_8985)) {\n        y_9044 = *(volatile __local int32_t *) &mem_11626[local_tid_9047 *\n                                                          sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_11992 = 1;\n        \n        while (slt32(skip_threads_11992, 32)) {\n            if (slt32(local_tid_9047, num_groups_8985) &&\n                sle32(skip_threads_11992, local_tid_9047 -\n                      squot32(local_tid_9047, 32) * 32)) {\n                // read operands\n                {\n                    x_9043 = *(volatile __local\n                               int32_t *) &mem_11626[(local_tid_9047 -\n                                                      skip_threads_11992) *\n                                                     sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_9045;\n                    \n                    if (thread_active_11986) {\n                        zz_9045 = x_9043 + y_9044;\n                    }\n                    y_9044 = zz_9045;\n                }",
                   "\n            }\n            if (sle32(wave_sizze_11984, skip_threads_11992)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_9047, num_groups_8985) &&\n                sle32(skip_threads_11992, local_tid_9047 -\n                      squot32(local_tid_9047, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11626[local_tid_9047 *\n                                                             sizeof(int32_t)] =\n                        y_9044;\n                }\n            }\n            if (sle32(wave_sizze_11984, skip_threads_11992)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_11992 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_9047 - squot32(local_tid_9047, 32) * 32) == 31 &&\n            slt32(local_tid_9047, num_groups_8985)) {\n            *(volatile __local int32_t *) &mem_11626[squot32(local_tid_9047,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_9044;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_9047, 32) == 0 && slt32(local_tid_9047,\n                                                      num_groups_8985)) {\n            y_11990 = *(volatile __local int32_t *) &mem_11626[local_tid_9047 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_11993 = 1;\n            \n            while (slt32(skip_threads_11993, 32)) {\n                if ((squot32(local_tid_9047, 32) == 0 && slt32(local_tid_9047,\n                                                               num_groups",
                   "_8985)) &&\n                    sle32(skip_threads_11993, local_tid_9047 -\n                          squot32(local_tid_9047, 32) * 32)) {\n                    // read operands\n                    {\n                        x_11989 = *(volatile __local\n                                    int32_t *) &mem_11626[(local_tid_9047 -\n                                                           skip_threads_11993) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_11991;\n                        \n                        if (thread_active_11986) {\n                            zz_11991 = x_11989 + y_11990;\n                        }\n                        y_11990 = zz_11991;\n                    }\n                }\n                if (sle32(wave_sizze_11984, skip_threads_11993)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_9047, 32) == 0 && slt32(local_tid_9047,\n                                                               num_groups_8985)) &&\n                    sle32(skip_threads_11993, local_tid_9047 -\n                          squot32(local_tid_9047, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11626[local_tid_9047 *\n                                                sizeof(int32_t)] = y_11990;\n                    }\n                }\n                if (sle32(wave_sizze_11984, skip_threads_11993)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_11993 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_9047, 32) == 0 || !slt32(local_tid_9047,\n                                                         num_groups_8985)",
                   ")) {\n            // read operands\n            {\n                x_9043 = *(volatile __local\n                           int32_t *) &mem_11626[(squot32(local_tid_9047, 32) -\n                                                  1) * sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_9045;\n                \n                if (thread_active_11986) {\n                    zz_9045 = x_9043 + y_9044;\n                }\n                y_9044 = zz_9045;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11626[local_tid_9047 *\n                                                         sizeof(int32_t)] =\n                    y_9044;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_9047, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11626[local_tid_9047 *\n                                                     sizeof(int32_t)] = y_9044;\n        }\n    }\n    \n    int32_t scanned_elem_9052;\n    \n    if (thread_active_11986) {\n        scanned_elem_9052 = *(__local int32_t *) &mem_11626[local_tid_9047 * 4];\n    }\n    *(__global int32_t *) &mem_11629[global_tid_9046 * 4] = scanned_elem_9052;\n}\n__kernel void scan2_kernel_9262(__local volatile int64_t *mem_aligned_0,\n                                int32_t num_groups_9201, __global\n                                unsigned char *mem_11623, __global\n                                unsigned char *mem_11629)\n{\n    __local volatile char *restrict mem_11626 = mem_aligned_0;\n    int32_t wave_sizze_12034;\n    int32_t group_sizze_12035;\n    char thread_active_12036;\n    int32_t global_tid_9262;\n    int32_t local_tid_9263;\n    int32_t group_id_9264;\n    \n    global_tid_9262 = get_global_id(0);\n    local_tid_9263 = get_local_id(0);\n    group_sizze_12035 = get_local_size(0);\n    wave_sizze_12034 = LOCKSTEP_WIDTH;\n    group_id_9264 = get",
                   "_group_id(0);\n    thread_active_12036 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9263, num_groups_9201) && 1) {\n        int32_t offsets_group_sums_elem_9265 = *(__global\n                                                 int32_t *) &mem_11623[local_tid_9263 *\n                                                                       4];\n        \n        *(__local int32_t *) &mem_11626[local_tid_9263 * 4] =\n            offsets_group_sums_elem_9265;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_9257;\n    int32_t other_index_9258;\n    int32_t x_9259;\n    int32_t y_9260;\n    int32_t my_index_12037;\n    int32_t other_index_12038;\n    int32_t x_12039;\n    int32_t y_12040;\n    \n    my_index_9257 = local_tid_9263;\n    if (slt32(local_tid_9263, num_groups_9201)) {\n        y_9260 = *(volatile __local int32_t *) &mem_11626[local_tid_9263 *\n                                                          sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12042 = 1;\n        \n        while (slt32(skip_threads_12042, 32)) {\n            if (slt32(local_tid_9263, num_groups_9201) &&\n                sle32(skip_threads_12042, local_tid_9263 -\n                      squot32(local_tid_9263, 32) * 32)) {\n                // read operands\n                {\n                    x_9259 = *(volatile __local\n                               int32_t *) &mem_11626[(local_tid_9263 -\n                                                      skip_threads_12042) *\n                                                     sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_9261;\n                    \n                    if (thread_active_12036) {\n                        zz_9261 = x_9259 + y_9260;\n                    }\n                    y_9260 = zz_9261;\n                }\n            }\n            if (sle32(wave_sizze_12034, skip_threads_12042)) {\n           ",
                   "     barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_9263, num_groups_9201) &&\n                sle32(skip_threads_12042, local_tid_9263 -\n                      squot32(local_tid_9263, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11626[local_tid_9263 *\n                                                             sizeof(int32_t)] =\n                        y_9260;\n                }\n            }\n            if (sle32(wave_sizze_12034, skip_threads_12042)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12042 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_9263 - squot32(local_tid_9263, 32) * 32) == 31 &&\n            slt32(local_tid_9263, num_groups_9201)) {\n            *(volatile __local int32_t *) &mem_11626[squot32(local_tid_9263,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_9260;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_9263, 32) == 0 && slt32(local_tid_9263,\n                                                      num_groups_9201)) {\n            y_12040 = *(volatile __local int32_t *) &mem_11626[local_tid_9263 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12043 = 1;\n            \n            while (slt32(skip_threads_12043, 32)) {\n                if ((squot32(local_tid_9263, 32) == 0 && slt32(local_tid_9263,\n                                                               num_groups_9201)) &&\n                    sle32(skip_threads_12043, local_tid_9263 -\n               ",
                   "           squot32(local_tid_9263, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12039 = *(volatile __local\n                                    int32_t *) &mem_11626[(local_tid_9263 -\n                                                           skip_threads_12043) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12041;\n                        \n                        if (thread_active_12036) {\n                            zz_12041 = x_12039 + y_12040;\n                        }\n                        y_12040 = zz_12041;\n                    }\n                }\n                if (sle32(wave_sizze_12034, skip_threads_12043)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_9263, 32) == 0 && slt32(local_tid_9263,\n                                                               num_groups_9201)) &&\n                    sle32(skip_threads_12043, local_tid_9263 -\n                          squot32(local_tid_9263, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11626[local_tid_9263 *\n                                                sizeof(int32_t)] = y_12040;\n                    }\n                }\n                if (sle32(wave_sizze_12034, skip_threads_12043)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12043 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_9263, 32) == 0 || !slt32(local_tid_9263,\n                                                         num_groups_9201))) {\n            // read operands\n            {\n                x_9259 = *(volatile __loc",
                   "al\n                           int32_t *) &mem_11626[(squot32(local_tid_9263, 32) -\n                                                  1) * sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_9261;\n                \n                if (thread_active_12036) {\n                    zz_9261 = x_9259 + y_9260;\n                }\n                y_9260 = zz_9261;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11626[local_tid_9263 *\n                                                         sizeof(int32_t)] =\n                    y_9260;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_9263, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11626[local_tid_9263 *\n                                                     sizeof(int32_t)] = y_9260;\n        }\n    }\n    \n    int32_t scanned_elem_9268;\n    \n    if (thread_active_12036) {\n        scanned_elem_9268 = *(__local int32_t *) &mem_11626[local_tid_9263 * 4];\n    }\n    *(__global int32_t *) &mem_11629[global_tid_9262 * 4] = scanned_elem_9268;\n}\n__kernel void scan2_kernel_9434(__local volatile int64_t *mem_aligned_0,\n                                int32_t num_groups_9357, __global\n                                unsigned char *mem_11626, __global\n                                unsigned char *mem_11632)\n{\n    __local volatile char *restrict mem_11629 = mem_aligned_0;\n    int32_t wave_sizze_12081;\n    int32_t group_sizze_12082;\n    char thread_active_12083;\n    int32_t global_tid_9434;\n    int32_t local_tid_9435;\n    int32_t group_id_9436;\n    \n    global_tid_9434 = get_global_id(0);\n    local_tid_9435 = get_local_id(0);\n    group_sizze_12082 = get_local_size(0);\n    wave_sizze_12081 = LOCKSTEP_WIDTH;\n    group_id_9436 = get_group_id(0);\n    thread_active_12083 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt3",
                   "2(local_tid_9435, num_groups_9357) && 1) {\n        int32_t res_group_sums_elem_9437 = *(__global\n                                             int32_t *) &mem_11626[local_tid_9435 *\n                                                                   4];\n        \n        *(__local int32_t *) &mem_11629[local_tid_9435 * 4] =\n            res_group_sums_elem_9437;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_9429;\n    int32_t other_index_9430;\n    int32_t binop_param_x_9431;\n    int32_t binop_param_y_9432;\n    int32_t my_index_12084;\n    int32_t other_index_12085;\n    int32_t binop_param_x_12086;\n    int32_t binop_param_y_12087;\n    \n    my_index_9429 = local_tid_9435;\n    if (slt32(local_tid_9435, num_groups_9357)) {\n        binop_param_y_9432 = *(volatile __local\n                               int32_t *) &mem_11629[local_tid_9435 *\n                                                     sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12089 = 1;\n        \n        while (slt32(skip_threads_12089, 32)) {\n            if (slt32(local_tid_9435, num_groups_9357) &&\n                sle32(skip_threads_12089, local_tid_9435 -\n                      squot32(local_tid_9435, 32) * 32)) {\n                // read operands\n                {\n                    binop_param_x_9431 = *(volatile __local\n                                           int32_t *) &mem_11629[(local_tid_9435 -\n                                                                  skip_threads_12089) *\n                                                                 sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t res_9433;\n                    \n                    if (thread_active_12083) {\n                        res_9433 = binop_param_x_9431 + binop_param_y_9432;\n                    }\n                    binop_param_y_9432 = res_9433;\n                }\n            }\n      ",
                   "      if (sle32(wave_sizze_12081, skip_threads_12089)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_9435, num_groups_9357) &&\n                sle32(skip_threads_12089, local_tid_9435 -\n                      squot32(local_tid_9435, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11629[local_tid_9435 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_9432;\n                }\n            }\n            if (sle32(wave_sizze_12081, skip_threads_12089)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12089 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_9435 - squot32(local_tid_9435, 32) * 32) == 31 &&\n            slt32(local_tid_9435, num_groups_9357)) {\n            *(volatile __local int32_t *) &mem_11629[squot32(local_tid_9435,\n                                                             32) *\n                                                     sizeof(int32_t)] =\n                binop_param_y_9432;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_9435, 32) == 0 && slt32(local_tid_9435,\n                                                      num_groups_9357)) {\n            binop_param_y_12087 = *(volatile __local\n                                    int32_t *) &mem_11629[local_tid_9435 *\n                                                          sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12090 = 1;\n            \n            while (slt32(skip_threads_12090, 32)) {\n                if ((squot32(local_tid_9435, 32) == 0 && slt32(local_tid_9435,\n           ",
                   "                                                    num_groups_9357)) &&\n                    sle32(skip_threads_12090, local_tid_9435 -\n                          squot32(local_tid_9435, 32) * 32)) {\n                    // read operands\n                    {\n                        binop_param_x_12086 = *(volatile __local\n                                                int32_t *) &mem_11629[(local_tid_9435 -\n                                                                       skip_threads_12090) *\n                                                                      sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_12088;\n                        \n                        if (thread_active_12083) {\n                            res_12088 = binop_param_x_12086 +\n                                binop_param_y_12087;\n                        }\n                        binop_param_y_12087 = res_12088;\n                    }\n                }\n                if (sle32(wave_sizze_12081, skip_threads_12090)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_9435, 32) == 0 && slt32(local_tid_9435,\n                                                               num_groups_9357)) &&\n                    sle32(skip_threads_12090, local_tid_9435 -\n                          squot32(local_tid_9435, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11629[local_tid_9435 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_12087;\n                    }\n                }\n                if (sle32(wave_sizze_12081, skip_threads_12090)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12090 *= 2;\n            }\n        }\n    }\n    barrier(CL",
                   "K_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_9435, 32) == 0 || !slt32(local_tid_9435,\n                                                         num_groups_9357))) {\n            // read operands\n            {\n                binop_param_x_9431 = *(volatile __local\n                                       int32_t *) &mem_11629[(squot32(local_tid_9435,\n                                                                      32) - 1) *\n                                                             sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t res_9433;\n                \n                if (thread_active_12083) {\n                    res_9433 = binop_param_x_9431 + binop_param_y_9432;\n                }\n                binop_param_y_9432 = res_9433;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11629[local_tid_9435 *\n                                                         sizeof(int32_t)] =\n                    binop_param_y_9432;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_9435, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11629[local_tid_9435 *\n                                                     sizeof(int32_t)] =\n                binop_param_y_9432;\n        }\n    }\n    \n    int32_t scanned_elem_9440;\n    \n    if (thread_active_12083) {\n        scanned_elem_9440 = *(__local int32_t *) &mem_11629[local_tid_9435 * 4];\n    }\n    *(__global int32_t *) &mem_11632[global_tid_9434 * 4] = scanned_elem_9440;\n}\n__kernel void scan2_kernel_9573(__local volatile int64_t *mem_aligned_0,\n                                __local volatile int64_t *mem_aligned_1,\n                                int32_t num_groups_9357, __global\n                                unsigned char *mem_11656, __global\n        ",
                   "                        unsigned char *mem_11659, __global\n                                unsigned char *mem_11668, __global\n                                unsigned char *mem_11671)\n{\n    __local volatile char *restrict mem_11662 = mem_aligned_0;\n    __local volatile char *restrict mem_11665 = mem_aligned_1;\n    int32_t wave_sizze_12113;\n    int32_t group_sizze_12114;\n    char thread_active_12115;\n    int32_t global_tid_9573;\n    int32_t local_tid_9574;\n    int32_t group_id_9575;\n    \n    global_tid_9573 = get_global_id(0);\n    local_tid_9574 = get_local_id(0);\n    group_sizze_12114 = get_local_size(0);\n    wave_sizze_12113 = LOCKSTEP_WIDTH;\n    group_id_9575 = get_group_id(0);\n    thread_active_12115 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9574, num_groups_9357) && 1) {\n        int32_t res_group_sums_elem_9576 = *(__global\n                                             int32_t *) &mem_11656[local_tid_9574 *\n                                                                   4];\n        int32_t offsets_group_sums_elem_9577 = *(__global\n                                                 int32_t *) &mem_11659[local_tid_9574 *\n                                                                       4];\n        \n        *(__local int32_t *) &mem_11662[local_tid_9574 * 4] =\n            res_group_sums_elem_9576;\n        *(__local int32_t *) &mem_11665[local_tid_9574 * 4] =\n            offsets_group_sums_elem_9577;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_9565;\n    int32_t other_index_9566;\n    int32_t binop_param_x_9567;\n    int32_t x_9568;\n    int32_t binop_param_y_9569;\n    int32_t y_9570;\n    int32_t my_index_12116;\n    int32_t other_index_12117;\n    int32_t binop_param_x_12118;\n    int32_t x_12119;\n    int32_t binop_param_y_12120;\n    int32_t y_12121;\n    \n    my_index_9565 = local_tid_9574;\n    if (slt32(local_tid_9574, num_groups_9357)) {\n        binop_param_y_9569 = *(volatile __local\n                               int32_t",
                   " *) &mem_11662[local_tid_9574 *\n                                                     sizeof(int32_t)];\n        y_9570 = *(volatile __local int32_t *) &mem_11665[local_tid_9574 *\n                                                          sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12124 = 1;\n        \n        while (slt32(skip_threads_12124, 32)) {\n            if (slt32(local_tid_9574, num_groups_9357) &&\n                sle32(skip_threads_12124, local_tid_9574 -\n                      squot32(local_tid_9574, 32) * 32)) {\n                // read operands\n                {\n                    binop_param_x_9567 = *(volatile __local\n                                           int32_t *) &mem_11662[(local_tid_9574 -\n                                                                  skip_threads_12124) *\n                                                                 sizeof(int32_t)];\n                    x_9568 = *(volatile __local\n                               int32_t *) &mem_11665[(local_tid_9574 -\n                                                      skip_threads_12124) *\n                                                     sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t res_9571;\n                    int32_t zz_9572;\n                    \n                    if (thread_active_12115) {\n                        res_9571 = binop_param_x_9567 + binop_param_y_9569;\n                        zz_9572 = x_9568 + y_9570;\n                    }\n                    binop_param_y_9569 = res_9571;\n                    y_9570 = zz_9572;\n                }\n            }\n            if (sle32(wave_sizze_12113, skip_threads_12124)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_9574, num_groups_9357) &&\n                sle32(skip_threads_12124, local_tid_9574 -\n                      squot32(local_tid_9574, 32) * ",
                   "32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11662[local_tid_9574 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_9569;\n                    *(volatile __local int32_t *) &mem_11665[local_tid_9574 *\n                                                             sizeof(int32_t)] =\n                        y_9570;\n                }\n            }\n            if (sle32(wave_sizze_12113, skip_threads_12124)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12124 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_9574 - squot32(local_tid_9574, 32) * 32) == 31 &&\n            slt32(local_tid_9574, num_groups_9357)) {\n            *(volatile __local int32_t *) &mem_11662[squot32(local_tid_9574,\n                                                             32) *\n                                                     sizeof(int32_t)] =\n                binop_param_y_9569;\n            *(volatile __local int32_t *) &mem_11665[squot32(local_tid_9574,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_9570;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_9574, 32) == 0 && slt32(local_tid_9574,\n                                                      num_groups_9357)) {\n            binop_param_y_12120 = *(volatile __local\n                                    int32_t *) &mem_11662[local_tid_9574 *\n                                                          sizeof(int32_t)];\n            y_12121 = *(volatile __local int32_t *) &mem_11665[local_tid_9574 *\n                                                               size",
                   "of(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12125 = 1;\n            \n            while (slt32(skip_threads_12125, 32)) {\n                if ((squot32(local_tid_9574, 32) == 0 && slt32(local_tid_9574,\n                                                               num_groups_9357)) &&\n                    sle32(skip_threads_12125, local_tid_9574 -\n                          squot32(local_tid_9574, 32) * 32)) {\n                    // read operands\n                    {\n                        binop_param_x_12118 = *(volatile __local\n                                                int32_t *) &mem_11662[(local_tid_9574 -\n                                                                       skip_threads_12125) *\n                                                                      sizeof(int32_t)];\n                        x_12119 = *(volatile __local\n                                    int32_t *) &mem_11665[(local_tid_9574 -\n                                                           skip_threads_12125) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_12122;\n                        int32_t zz_12123;\n                        \n                        if (thread_active_12115) {\n                            res_12122 = binop_param_x_12118 +\n                                binop_param_y_12120;\n                            zz_12123 = x_12119 + y_12121;\n                        }\n                        binop_param_y_12120 = res_12122;\n                        y_12121 = zz_12123;\n                    }\n                }\n                if (sle32(wave_sizze_12113, skip_threads_12125)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_9574, 32) == 0 && slt32(local_tid_9574,\n                                 ",
                   "                              num_groups_9357)) &&\n                    sle32(skip_threads_12125, local_tid_9574 -\n                          squot32(local_tid_9574, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11662[local_tid_9574 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_12120;\n                        *(volatile __local\n                          int32_t *) &mem_11665[local_tid_9574 *\n                                                sizeof(int32_t)] = y_12121;\n                    }\n                }\n                if (sle32(wave_sizze_12113, skip_threads_12125)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12125 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_9574, 32) == 0 || !slt32(local_tid_9574,\n                                                         num_groups_9357))) {\n            // read operands\n            {\n                binop_param_x_9567 = *(volatile __local\n                                       int32_t *) &mem_11662[(squot32(local_tid_9574,\n                                                                      32) - 1) *\n                                                             sizeof(int32_t)];\n                x_9568 = *(volatile __local\n                           int32_t *) &mem_11665[(squot32(local_tid_9574, 32) -\n                                                  1) * sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t res_9571;\n                int32_t zz_9572;\n                \n                if (thread_active_12115) {\n                    res_9571 = binop_param_x_9567 + binop_param_y_9569;\n                    zz_9572 = x_9568 + y_9570;\n                }\n    ",
                   "            binop_param_y_9569 = res_9571;\n                y_9570 = zz_9572;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11662[local_tid_9574 *\n                                                         sizeof(int32_t)] =\n                    binop_param_y_9569;\n                *(volatile __local int32_t *) &mem_11665[local_tid_9574 *\n                                                         sizeof(int32_t)] =\n                    y_9570;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_9574, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11662[local_tid_9574 *\n                                                     sizeof(int32_t)] =\n                binop_param_y_9569;\n            *(volatile __local int32_t *) &mem_11665[local_tid_9574 *\n                                                     sizeof(int32_t)] = y_9570;\n        }\n    }\n    \n    int32_t scanned_elem_9582;\n    int32_t scanned_elem_9583;\n    \n    if (thread_active_12115) {\n        scanned_elem_9582 = *(__local int32_t *) &mem_11662[local_tid_9574 * 4];\n        scanned_elem_9583 = *(__local int32_t *) &mem_11665[local_tid_9574 * 4];\n    }\n    *(__global int32_t *) &mem_11668[global_tid_9573 * 4] = scanned_elem_9582;\n    *(__global int32_t *) &mem_11671[global_tid_9573 * 4] = scanned_elem_9583;\n}\n__kernel void scan2_kernel_9674(__local volatile int64_t *mem_aligned_0,\n                                int32_t num_groups_9357, __global\n                                unsigned char *mem_11688, __global\n                                unsigned char *mem_11694)\n{\n    __local volatile char *restrict mem_11691 = mem_aligned_0;\n    int32_t wave_sizze_12144;\n    int32_t group_sizze_12145;\n    char thread_active_12146;\n    int32_t global_tid_9674;\n    int32_t local_tid_9675;\n    int32_t group_id_9676;\n    \n    global_tid_9674 = get_global_id(0);\n    l",
                   "ocal_tid_9675 = get_local_id(0);\n    group_sizze_12145 = get_local_size(0);\n    wave_sizze_12144 = LOCKSTEP_WIDTH;\n    group_id_9676 = get_group_id(0);\n    thread_active_12146 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9675, num_groups_9357) && 1) {\n        int32_t res_group_sums_elem_9677 = *(__global\n                                             int32_t *) &mem_11688[local_tid_9675 *\n                                                                   4];\n        \n        *(__local int32_t *) &mem_11691[local_tid_9675 * 4] =\n            res_group_sums_elem_9677;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_9669;\n    int32_t other_index_9670;\n    int32_t binop_param_x_9671;\n    int32_t binop_param_y_9672;\n    int32_t my_index_12147;\n    int32_t other_index_12148;\n    int32_t binop_param_x_12149;\n    int32_t binop_param_y_12150;\n    \n    my_index_9669 = local_tid_9675;\n    if (slt32(local_tid_9675, num_groups_9357)) {\n        binop_param_y_9672 = *(volatile __local\n                               int32_t *) &mem_11691[local_tid_9675 *\n                                                     sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12152 = 1;\n        \n        while (slt32(skip_threads_12152, 32)) {\n            if (slt32(local_tid_9675, num_groups_9357) &&\n                sle32(skip_threads_12152, local_tid_9675 -\n                      squot32(local_tid_9675, 32) * 32)) {\n                // read operands\n                {\n                    binop_param_x_9671 = *(volatile __local\n                                           int32_t *) &mem_11691[(local_tid_9675 -\n                                                                  skip_threads_12152) *\n                                                                 sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t res_9673;\n                    \n       ",
                   "             if (thread_active_12146) {\n                        res_9673 = binop_param_x_9671 + binop_param_y_9672;\n                    }\n                    binop_param_y_9672 = res_9673;\n                }\n            }\n            if (sle32(wave_sizze_12144, skip_threads_12152)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_9675, num_groups_9357) &&\n                sle32(skip_threads_12152, local_tid_9675 -\n                      squot32(local_tid_9675, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11691[local_tid_9675 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_9672;\n                }\n            }\n            if (sle32(wave_sizze_12144, skip_threads_12152)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12152 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_9675 - squot32(local_tid_9675, 32) * 32) == 31 &&\n            slt32(local_tid_9675, num_groups_9357)) {\n            *(volatile __local int32_t *) &mem_11691[squot32(local_tid_9675,\n                                                             32) *\n                                                     sizeof(int32_t)] =\n                binop_param_y_9672;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_9675, 32) == 0 && slt32(local_tid_9675,\n                                                      num_groups_9357)) {\n            binop_param_y_12150 = *(volatile __local\n                                    int32_t *) &mem_11691[local_tid_9675 *\n                                                          sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no",
                   " barriers needed)\n        {\n            int32_t skip_threads_12153 = 1;\n            \n            while (slt32(skip_threads_12153, 32)) {\n                if ((squot32(local_tid_9675, 32) == 0 && slt32(local_tid_9675,\n                                                               num_groups_9357)) &&\n                    sle32(skip_threads_12153, local_tid_9675 -\n                          squot32(local_tid_9675, 32) * 32)) {\n                    // read operands\n                    {\n                        binop_param_x_12149 = *(volatile __local\n                                                int32_t *) &mem_11691[(local_tid_9675 -\n                                                                       skip_threads_12153) *\n                                                                      sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_12151;\n                        \n                        if (thread_active_12146) {\n                            res_12151 = binop_param_x_12149 +\n                                binop_param_y_12150;\n                        }\n                        binop_param_y_12150 = res_12151;\n                    }\n                }\n                if (sle32(wave_sizze_12144, skip_threads_12153)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_9675, 32) == 0 && slt32(local_tid_9675,\n                                                               num_groups_9357)) &&\n                    sle32(skip_threads_12153, local_tid_9675 -\n                          squot32(local_tid_9675, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11691[local_tid_9675 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_12150;\n                    }\n           ",
                   "     }\n                if (sle32(wave_sizze_12144, skip_threads_12153)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12153 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_9675, 32) == 0 || !slt32(local_tid_9675,\n                                                         num_groups_9357))) {\n            // read operands\n            {\n                binop_param_x_9671 = *(volatile __local\n                                       int32_t *) &mem_11691[(squot32(local_tid_9675,\n                                                                      32) - 1) *\n                                                             sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t res_9673;\n                \n                if (thread_active_12146) {\n                    res_9673 = binop_param_x_9671 + binop_param_y_9672;\n                }\n                binop_param_y_9672 = res_9673;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11691[local_tid_9675 *\n                                                         sizeof(int32_t)] =\n                    binop_param_y_9672;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_9675, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11691[local_tid_9675 *\n                                                     sizeof(int32_t)] =\n                binop_param_y_9672;\n        }\n    }\n    \n    int32_t scanned_elem_9680;\n    \n    if (thread_active_12146) {\n        scanned_elem_9680 = *(__local int32_t *) &mem_11691[local_tid_9675 * 4];\n    }\n    *(__global int32_t *) &mem_11694[global_tid_9674 * 4] = scanned_elem_9680;\n}\n__kernel void scan2_kernel_9764(__local volatile int64_t *me",
                   "m_aligned_0,\n                                int32_t num_groups_9357, __global\n                                unsigned char *mem_11706, __global\n                                unsigned char *mem_11712)\n{\n    __local volatile char *restrict mem_11709 = mem_aligned_0;\n    int32_t wave_sizze_12169;\n    int32_t group_sizze_12170;\n    char thread_active_12171;\n    int32_t global_tid_9764;\n    int32_t local_tid_9765;\n    int32_t group_id_9766;\n    \n    global_tid_9764 = get_global_id(0);\n    local_tid_9765 = get_local_id(0);\n    group_sizze_12170 = get_local_size(0);\n    wave_sizze_12169 = LOCKSTEP_WIDTH;\n    group_id_9766 = get_group_id(0);\n    thread_active_12171 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9765, num_groups_9357) && 1) {\n        int32_t offsets_group_sums_elem_9767 = *(__global\n                                                 int32_t *) &mem_11706[local_tid_9765 *\n                                                                       4];\n        \n        *(__local int32_t *) &mem_11709[local_tid_9765 * 4] =\n            offsets_group_sums_elem_9767;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_9759;\n    int32_t other_index_9760;\n    int32_t x_9761;\n    int32_t y_9762;\n    int32_t my_index_12172;\n    int32_t other_index_12173;\n    int32_t x_12174;\n    int32_t y_12175;\n    \n    my_index_9759 = local_tid_9765;\n    if (slt32(local_tid_9765, num_groups_9357)) {\n        y_9762 = *(volatile __local int32_t *) &mem_11709[local_tid_9765 *\n                                                          sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12177 = 1;\n        \n        while (slt32(skip_threads_12177, 32)) {\n            if (slt32(local_tid_9765, num_groups_9357) &&\n                sle32(skip_threads_12177, local_tid_9765 -\n                      squot32(local_tid_9765, 32) * 32)) {\n                // read operands\n                {\n                    x_9761 = ",
                   "*(volatile __local\n                               int32_t *) &mem_11709[(local_tid_9765 -\n                                                      skip_threads_12177) *\n                                                     sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t zz_9763;\n                    \n                    if (thread_active_12171) {\n                        zz_9763 = x_9761 + y_9762;\n                    }\n                    y_9762 = zz_9763;\n                }\n            }\n            if (sle32(wave_sizze_12169, skip_threads_12177)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_9765, num_groups_9357) &&\n                sle32(skip_threads_12177, local_tid_9765 -\n                      squot32(local_tid_9765, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11709[local_tid_9765 *\n                                                             sizeof(int32_t)] =\n                        y_9762;\n                }\n            }\n            if (sle32(wave_sizze_12169, skip_threads_12177)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12177 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_9765 - squot32(local_tid_9765, 32) * 32) == 31 &&\n            slt32(local_tid_9765, num_groups_9357)) {\n            *(volatile __local int32_t *) &mem_11709[squot32(local_tid_9765,\n                                                             32) *\n                                                     sizeof(int32_t)] = y_9762;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+1'\n    {\n        if (squot32(local_tid_9765, 32) == 0 && slt32(local_tid_9765,\n                             ",
                   "                         num_groups_9357)) {\n            y_12175 = *(volatile __local int32_t *) &mem_11709[local_tid_9765 *\n                                                               sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12178 = 1;\n            \n            while (slt32(skip_threads_12178, 32)) {\n                if ((squot32(local_tid_9765, 32) == 0 && slt32(local_tid_9765,\n                                                               num_groups_9357)) &&\n                    sle32(skip_threads_12178, local_tid_9765 -\n                          squot32(local_tid_9765, 32) * 32)) {\n                    // read operands\n                    {\n                        x_12174 = *(volatile __local\n                                    int32_t *) &mem_11709[(local_tid_9765 -\n                                                           skip_threads_12178) *\n                                                          sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t zz_12176;\n                        \n                        if (thread_active_12171) {\n                            zz_12176 = x_12174 + y_12175;\n                        }\n                        y_12175 = zz_12176;\n                    }\n                }\n                if (sle32(wave_sizze_12169, skip_threads_12178)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_9765, 32) == 0 && slt32(local_tid_9765,\n                                                               num_groups_9357)) &&\n                    sle32(skip_threads_12178, local_tid_9765 -\n                          squot32(local_tid_9765, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11709[local_tid_9765 *\n              ",
                   "                                  sizeof(int32_t)] = y_12175;\n                    }\n                }\n                if (sle32(wave_sizze_12169, skip_threads_12178)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12178 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_9765, 32) == 0 || !slt32(local_tid_9765,\n                                                         num_groups_9357))) {\n            // read operands\n            {\n                x_9761 = *(volatile __local\n                           int32_t *) &mem_11709[(squot32(local_tid_9765, 32) -\n                                                  1) * sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t zz_9763;\n                \n                if (thread_active_12171) {\n                    zz_9763 = x_9761 + y_9762;\n                }\n                y_9762 = zz_9763;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11709[local_tid_9765 *\n                                                         sizeof(int32_t)] =\n                    y_9762;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_9765, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11709[local_tid_9765 *\n                                                     sizeof(int32_t)] = y_9762;\n        }\n    }\n    \n    int32_t scanned_elem_9770;\n    \n    if (thread_active_12171) {\n        scanned_elem_9770 = *(__local int32_t *) &mem_11709[local_tid_9765 * 4];\n    }\n    *(__global int32_t *) &mem_11712[global_tid_9764 * 4] = scanned_elem_9770;\n}\n__kernel void scan2_kernel_9954(__local volatile int64_t *mem_aligned_0,\n                                int32_t num_groups_9877, __global\n          ",
                   "                      unsigned char *mem_11630, __global\n                                unsigned char *mem_11636)\n{\n    __local volatile char *restrict mem_11633 = mem_aligned_0;\n    int32_t wave_sizze_12208;\n    int32_t group_sizze_12209;\n    char thread_active_12210;\n    int32_t global_tid_9954;\n    int32_t local_tid_9955;\n    int32_t group_id_9956;\n    \n    global_tid_9954 = get_global_id(0);\n    local_tid_9955 = get_local_id(0);\n    group_sizze_12209 = get_local_size(0);\n    wave_sizze_12208 = LOCKSTEP_WIDTH;\n    group_id_9956 = get_group_id(0);\n    thread_active_12210 = 1;\n    barrier(CLK_LOCAL_MEM_FENCE);\n    if (slt32(local_tid_9955, num_groups_9877) && 1) {\n        int32_t res_group_sums_elem_9957 = *(__global\n                                             int32_t *) &mem_11630[local_tid_9955 *\n                                                                   4];\n        \n        *(__local int32_t *) &mem_11633[local_tid_9955 * 4] =\n            res_group_sums_elem_9957;\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    \n    int32_t my_index_9949;\n    int32_t other_index_9950;\n    int32_t binop_param_x_9951;\n    int32_t binop_param_y_9952;\n    int32_t my_index_12211;\n    int32_t other_index_12212;\n    int32_t binop_param_x_12213;\n    int32_t binop_param_y_12214;\n    \n    my_index_9949 = local_tid_9955;\n    if (slt32(local_tid_9955, num_groups_9877)) {\n        binop_param_y_9952 = *(volatile __local\n                               int32_t *) &mem_11633[local_tid_9955 *\n                                                     sizeof(int32_t)];\n    }\n    // in-block scan (hopefully no barriers needed)\n    {\n        int32_t skip_threads_12216 = 1;\n        \n        while (slt32(skip_threads_12216, 32)) {\n            if (slt32(local_tid_9955, num_groups_9877) &&\n                sle32(skip_threads_12216, local_tid_9955 -\n                      squot32(local_tid_9955, 32) * 32)) {\n                // read operands\n                {\n                    binop_param_x_9951 = *(volat",
                   "ile __local\n                                           int32_t *) &mem_11633[(local_tid_9955 -\n                                                                  skip_threads_12216) *\n                                                                 sizeof(int32_t)];\n                }\n                // perform operation\n                {\n                    int32_t res_9953;\n                    \n                    if (thread_active_12210) {\n                        res_9953 = binop_param_x_9951 + binop_param_y_9952;\n                    }\n                    binop_param_y_9952 = res_9953;\n                }\n            }\n            if (sle32(wave_sizze_12208, skip_threads_12216)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            if (slt32(local_tid_9955, num_groups_9877) &&\n                sle32(skip_threads_12216, local_tid_9955 -\n                      squot32(local_tid_9955, 32) * 32)) {\n                // write result\n                {\n                    *(volatile __local int32_t *) &mem_11633[local_tid_9955 *\n                                                             sizeof(int32_t)] =\n                        binop_param_y_9952;\n                }\n            }\n            if (sle32(wave_sizze_12208, skip_threads_12216)) {\n                barrier(CLK_LOCAL_MEM_FENCE);\n            }\n            skip_threads_12216 *= 2;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // last thread of block 'i' writes its result to offset 'i'\n    {\n        if ((local_tid_9955 - squot32(local_tid_9955, 32) * 32) == 31 &&\n            slt32(local_tid_9955, num_groups_9877)) {\n            *(volatile __local int32_t *) &mem_11633[squot32(local_tid_9955,\n                                                             32) *\n                                                     sizeof(int32_t)] =\n                binop_param_y_9952;\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // scan the first block, after which offset 'i' contains carry-in for warp 'i+",
                   "1'\n    {\n        if (squot32(local_tid_9955, 32) == 0 && slt32(local_tid_9955,\n                                                      num_groups_9877)) {\n            binop_param_y_12214 = *(volatile __local\n                                    int32_t *) &mem_11633[local_tid_9955 *\n                                                          sizeof(int32_t)];\n        }\n        // in-block scan (hopefully no barriers needed)\n        {\n            int32_t skip_threads_12217 = 1;\n            \n            while (slt32(skip_threads_12217, 32)) {\n                if ((squot32(local_tid_9955, 32) == 0 && slt32(local_tid_9955,\n                                                               num_groups_9877)) &&\n                    sle32(skip_threads_12217, local_tid_9955 -\n                          squot32(local_tid_9955, 32) * 32)) {\n                    // read operands\n                    {\n                        binop_param_x_12213 = *(volatile __local\n                                                int32_t *) &mem_11633[(local_tid_9955 -\n                                                                       skip_threads_12217) *\n                                                                      sizeof(int32_t)];\n                    }\n                    // perform operation\n                    {\n                        int32_t res_12215;\n                        \n                        if (thread_active_12210) {\n                            res_12215 = binop_param_x_12213 +\n                                binop_param_y_12214;\n                        }\n                        binop_param_y_12214 = res_12215;\n                    }\n                }\n                if (sle32(wave_sizze_12208, skip_threads_12217)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                if ((squot32(local_tid_9955, 32) == 0 && slt32(local_tid_9955,\n                                                               num_groups_9877)) &&\n                    sle32(skip_threa",
                   "ds_12217, local_tid_9955 -\n                          squot32(local_tid_9955, 32) * 32)) {\n                    // write result\n                    {\n                        *(volatile __local\n                          int32_t *) &mem_11633[local_tid_9955 *\n                                                sizeof(int32_t)] =\n                            binop_param_y_12214;\n                    }\n                }\n                if (sle32(wave_sizze_12208, skip_threads_12217)) {\n                    barrier(CLK_LOCAL_MEM_FENCE);\n                }\n                skip_threads_12217 *= 2;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // carry-in for every block except the first\n    {\n        if (!(squot32(local_tid_9955, 32) == 0 || !slt32(local_tid_9955,\n                                                         num_groups_9877))) {\n            // read operands\n            {\n                binop_param_x_9951 = *(volatile __local\n                                       int32_t *) &mem_11633[(squot32(local_tid_9955,\n                                                                      32) - 1) *\n                                                             sizeof(int32_t)];\n            }\n            // perform operation\n            {\n                int32_t res_9953;\n                \n                if (thread_active_12210) {\n                    res_9953 = binop_param_x_9951 + binop_param_y_9952;\n                }\n                binop_param_y_9952 = res_9953;\n            }\n            // write final result\n            {\n                *(volatile __local int32_t *) &mem_11633[local_tid_9955 *\n                                                         sizeof(int32_t)] =\n                    binop_param_y_9952;\n            }\n        }\n    }\n    barrier(CLK_LOCAL_MEM_FENCE);\n    // restore correct values for first block\n    {\n        if (squot32(local_tid_9955, 32) == 0) {\n            *(volatile __local int32_t *) &mem_11633[local_tid_9955 *\n                   ",
                   "                                  sizeof(int32_t)] =\n                binop_param_y_9952;\n        }\n    }\n    \n    int32_t scanned_elem_9960;\n    \n    if (thread_active_12210) {\n        scanned_elem_9960 = *(__local int32_t *) &mem_11633[local_tid_9955 * 4];\n    }\n    *(__global int32_t *) &mem_11636[global_tid_9954 * 4] = scanned_elem_9960;\n}\n",
                   NULL};
static cl_kernel chunked_reduce_kernel_10338;
static int chunked_reduce_kernel_10338total_runtime = 0;
static int chunked_reduce_kernel_10338runs = 0;
static cl_kernel chunked_reduce_kernel_9116;
static int chunked_reduce_kernel_9116total_runtime = 0;
static int chunked_reduce_kernel_9116runs = 0;
static cl_kernel chunked_reduce_kernel_9818;
static int chunked_reduce_kernel_9818total_runtime = 0;
static int chunked_reduce_kernel_9818runs = 0;
static cl_kernel fut_kernel_map_transpose_i32;
static int fut_kernel_map_transpose_i32total_runtime = 0;
static int fut_kernel_map_transpose_i32runs = 0;
static cl_kernel fut_kernel_map_transpose_lowheight_i32;
static int fut_kernel_map_transpose_lowheight_i32total_runtime = 0;
static int fut_kernel_map_transpose_lowheight_i32runs = 0;
static cl_kernel fut_kernel_map_transpose_lowwidth_i32;
static int fut_kernel_map_transpose_lowwidth_i32total_runtime = 0;
static int fut_kernel_map_transpose_lowwidth_i32runs = 0;
static cl_kernel kernel_replicate_12564;
static int kernel_replicate_12564total_runtime = 0;
static int kernel_replicate_12564runs = 0;
static cl_kernel kernel_replicate_12569;
static int kernel_replicate_12569total_runtime = 0;
static int kernel_replicate_12569runs = 0;
static cl_kernel map_kernel_10129;
static int map_kernel_10129total_runtime = 0;
static int map_kernel_10129runs = 0;
static cl_kernel map_kernel_10220;
static int map_kernel_10220total_runtime = 0;
static int map_kernel_10220runs = 0;
static cl_kernel map_kernel_10310;
static int map_kernel_10310total_runtime = 0;
static int map_kernel_10310runs = 0;
static cl_kernel map_kernel_10380;
static int map_kernel_10380total_runtime = 0;
static int map_kernel_10380runs = 0;
static cl_kernel map_kernel_10484;
static int map_kernel_10484total_runtime = 0;
static int map_kernel_10484runs = 0;
static cl_kernel map_kernel_10494;
static int map_kernel_10494total_runtime = 0;
static int map_kernel_10494runs = 0;
static cl_kernel map_kernel_10590;
static int map_kernel_10590total_runtime = 0;
static int map_kernel_10590runs = 0;
static cl_kernel map_kernel_10600;
static int map_kernel_10600total_runtime = 0;
static int map_kernel_10600runs = 0;
static cl_kernel map_kernel_10696;
static int map_kernel_10696total_runtime = 0;
static int map_kernel_10696runs = 0;
static cl_kernel map_kernel_10706;
static int map_kernel_10706total_runtime = 0;
static int map_kernel_10706runs = 0;
static cl_kernel map_kernel_10802;
static int map_kernel_10802total_runtime = 0;
static int map_kernel_10802runs = 0;
static cl_kernel map_kernel_10812;
static int map_kernel_10812total_runtime = 0;
static int map_kernel_10812runs = 0;
static cl_kernel map_kernel_10985;
static int map_kernel_10985total_runtime = 0;
static int map_kernel_10985runs = 0;
static cl_kernel map_kernel_10995;
static int map_kernel_10995total_runtime = 0;
static int map_kernel_10995runs = 0;
static cl_kernel map_kernel_11168;
static int map_kernel_11168total_runtime = 0;
static int map_kernel_11168runs = 0;
static cl_kernel map_kernel_11178;
static int map_kernel_11178total_runtime = 0;
static int map_kernel_11178runs = 0;
static cl_kernel map_kernel_11512;
static int map_kernel_11512total_runtime = 0;
static int map_kernel_11512runs = 0;
static cl_kernel map_kernel_11522;
static int map_kernel_11522total_runtime = 0;
static int map_kernel_11522runs = 0;
static cl_kernel map_kernel_11535;
static int map_kernel_11535total_runtime = 0;
static int map_kernel_11535runs = 0;
static cl_kernel map_kernel_11556;
static int map_kernel_11556total_runtime = 0;
static int map_kernel_11556runs = 0;
static cl_kernel map_kernel_9072;
static int map_kernel_9072total_runtime = 0;
static int map_kernel_9072runs = 0;
static cl_kernel map_kernel_9082;
static int map_kernel_9082total_runtime = 0;
static int map_kernel_9082runs = 0;
static cl_kernel map_kernel_9184;
static int map_kernel_9184total_runtime = 0;
static int map_kernel_9184runs = 0;
static cl_kernel map_kernel_9288;
static int map_kernel_9288total_runtime = 0;
static int map_kernel_9288runs = 0;
static cl_kernel map_kernel_9298;
static int map_kernel_9298total_runtime = 0;
static int map_kernel_9298runs = 0;
static cl_kernel map_kernel_9308;
static int map_kernel_9308total_runtime = 0;
static int map_kernel_9308runs = 0;
static cl_kernel map_kernel_9322;
static int map_kernel_9322total_runtime = 0;
static int map_kernel_9322runs = 0;
static cl_kernel map_kernel_9340;
static int map_kernel_9340total_runtime = 0;
static int map_kernel_9340runs = 0;
static cl_kernel map_kernel_9460;
static int map_kernel_9460total_runtime = 0;
static int map_kernel_9460runs = 0;
static cl_kernel map_kernel_9609;
static int map_kernel_9609total_runtime = 0;
static int map_kernel_9609runs = 0;
static cl_kernel map_kernel_9700;
static int map_kernel_9700total_runtime = 0;
static int map_kernel_9700runs = 0;
static cl_kernel map_kernel_9790;
static int map_kernel_9790total_runtime = 0;
static int map_kernel_9790runs = 0;
static cl_kernel map_kernel_9860;
static int map_kernel_9860total_runtime = 0;
static int map_kernel_9860runs = 0;
static cl_kernel map_kernel_9980;
static int map_kernel_9980total_runtime = 0;
static int map_kernel_9980runs = 0;
static cl_kernel reduce_kernel_10365;
static int reduce_kernel_10365total_runtime = 0;
static int reduce_kernel_10365runs = 0;
static cl_kernel reduce_kernel_9169;
static int reduce_kernel_9169total_runtime = 0;
static int reduce_kernel_9169runs = 0;
static cl_kernel reduce_kernel_9845;
static int reduce_kernel_9845total_runtime = 0;
static int reduce_kernel_9845runs = 0;
static cl_kernel scan1_kernel_10040;
static int scan1_kernel_10040total_runtime = 0;
static int scan1_kernel_10040runs = 0;
static cl_kernel scan1_kernel_10162;
static int scan1_kernel_10162total_runtime = 0;
static int scan1_kernel_10162runs = 0;
static cl_kernel scan1_kernel_10252;
static int scan1_kernel_10252total_runtime = 0;
static int scan1_kernel_10252runs = 0;
static cl_kernel scan1_kernel_10423;
static int scan1_kernel_10423total_runtime = 0;
static int scan1_kernel_10423runs = 0;
static cl_kernel scan1_kernel_10529;
static int scan1_kernel_10529total_runtime = 0;
static int scan1_kernel_10529runs = 0;
static cl_kernel scan1_kernel_10635;
static int scan1_kernel_10635total_runtime = 0;
static int scan1_kernel_10635runs = 0;
static cl_kernel scan1_kernel_10741;
static int scan1_kernel_10741total_runtime = 0;
static int scan1_kernel_10741runs = 0;
static cl_kernel scan1_kernel_10922;
static int scan1_kernel_10922total_runtime = 0;
static int scan1_kernel_10922runs = 0;
static cl_kernel scan1_kernel_11105;
static int scan1_kernel_11105total_runtime = 0;
static int scan1_kernel_11105runs = 0;
static cl_kernel scan1_kernel_11445;
static int scan1_kernel_11445total_runtime = 0;
static int scan1_kernel_11445runs = 0;
static cl_kernel scan1_kernel_9011;
static int scan1_kernel_9011total_runtime = 0;
static int scan1_kernel_9011runs = 0;
static cl_kernel scan1_kernel_9227;
static int scan1_kernel_9227total_runtime = 0;
static int scan1_kernel_9227runs = 0;
static cl_kernel scan1_kernel_9396;
static int scan1_kernel_9396total_runtime = 0;
static int scan1_kernel_9396runs = 0;
static cl_kernel scan1_kernel_9520;
static int scan1_kernel_9520total_runtime = 0;
static int scan1_kernel_9520runs = 0;
static cl_kernel scan1_kernel_9642;
static int scan1_kernel_9642total_runtime = 0;
static int scan1_kernel_9642runs = 0;
static cl_kernel scan1_kernel_9732;
static int scan1_kernel_9732total_runtime = 0;
static int scan1_kernel_9732runs = 0;
static cl_kernel scan1_kernel_9916;
static int scan1_kernel_9916total_runtime = 0;
static int scan1_kernel_9916runs = 0;
static cl_kernel scan2_kernel_10093;
static int scan2_kernel_10093total_runtime = 0;
static int scan2_kernel_10093runs = 0;
static cl_kernel scan2_kernel_10194;
static int scan2_kernel_10194total_runtime = 0;
static int scan2_kernel_10194runs = 0;
static cl_kernel scan2_kernel_10284;
static int scan2_kernel_10284total_runtime = 0;
static int scan2_kernel_10284runs = 0;
static cl_kernel scan2_kernel_10458;
static int scan2_kernel_10458total_runtime = 0;
static int scan2_kernel_10458runs = 0;
static cl_kernel scan2_kernel_10564;
static int scan2_kernel_10564total_runtime = 0;
static int scan2_kernel_10564runs = 0;
static cl_kernel scan2_kernel_10670;
static int scan2_kernel_10670total_runtime = 0;
static int scan2_kernel_10670runs = 0;
static cl_kernel scan2_kernel_10776;
static int scan2_kernel_10776total_runtime = 0;
static int scan2_kernel_10776runs = 0;
static cl_kernel scan2_kernel_10959;
static int scan2_kernel_10959total_runtime = 0;
static int scan2_kernel_10959runs = 0;
static cl_kernel scan2_kernel_11142;
static int scan2_kernel_11142total_runtime = 0;
static int scan2_kernel_11142runs = 0;
static cl_kernel scan2_kernel_11486;
static int scan2_kernel_11486total_runtime = 0;
static int scan2_kernel_11486runs = 0;
static cl_kernel scan2_kernel_9046;
static int scan2_kernel_9046total_runtime = 0;
static int scan2_kernel_9046runs = 0;
static cl_kernel scan2_kernel_9262;
static int scan2_kernel_9262total_runtime = 0;
static int scan2_kernel_9262runs = 0;
static cl_kernel scan2_kernel_9434;
static int scan2_kernel_9434total_runtime = 0;
static int scan2_kernel_9434runs = 0;
static cl_kernel scan2_kernel_9573;
static int scan2_kernel_9573total_runtime = 0;
static int scan2_kernel_9573runs = 0;
static cl_kernel scan2_kernel_9674;
static int scan2_kernel_9674total_runtime = 0;
static int scan2_kernel_9674runs = 0;
static cl_kernel scan2_kernel_9764;
static int scan2_kernel_9764total_runtime = 0;
static int scan2_kernel_9764runs = 0;
static cl_kernel scan2_kernel_9954;
static int scan2_kernel_9954total_runtime = 0;
static int scan2_kernel_9954runs = 0;
void setup_opencl_and_load_kernels()
{
    cl_int error;
    cl_program prog = setup_opencl(fut_opencl_program);
    
    {
        chunked_reduce_kernel_10338 = clCreateKernel(prog,
                                                     "chunked_reduce_kernel_10338",
                                                     &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n",
                    "chunked_reduce_kernel_10338");
    }
    {
        chunked_reduce_kernel_9116 = clCreateKernel(prog,
                                                    "chunked_reduce_kernel_9116",
                                                    &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n",
                    "chunked_reduce_kernel_9116");
    }
    {
        chunked_reduce_kernel_9818 = clCreateKernel(prog,
                                                    "chunked_reduce_kernel_9818",
                                                    &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n",
                    "chunked_reduce_kernel_9818");
    }
    {
        fut_kernel_map_transpose_i32 = clCreateKernel(prog,
                                                      "fut_kernel_map_transpose_i32",
                                                      &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n",
                    "fut_kernel_map_transpose_i32");
    }
    {
        fut_kernel_map_transpose_lowheight_i32 = clCreateKernel(prog,
                                                                "fut_kernel_map_transpose_lowheight_i32",
                                                                &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n",
                    "fut_kernel_map_transpose_lowheight_i32");
    }
    {
        fut_kernel_map_transpose_lowwidth_i32 = clCreateKernel(prog,
                                                               "fut_kernel_map_transpose_lowwidth_i32",
                                                               &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n",
                    "fut_kernel_map_transpose_lowwidth_i32");
    }
    {
        kernel_replicate_12564 = clCreateKernel(prog, "kernel_replicate_12564",
                                                &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "kernel_replicate_12564");
    }
    {
        kernel_replicate_12569 = clCreateKernel(prog, "kernel_replicate_12569",
                                                &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "kernel_replicate_12569");
    }
    {
        map_kernel_10129 = clCreateKernel(prog, "map_kernel_10129", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10129");
    }
    {
        map_kernel_10220 = clCreateKernel(prog, "map_kernel_10220", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10220");
    }
    {
        map_kernel_10310 = clCreateKernel(prog, "map_kernel_10310", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10310");
    }
    {
        map_kernel_10380 = clCreateKernel(prog, "map_kernel_10380", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10380");
    }
    {
        map_kernel_10484 = clCreateKernel(prog, "map_kernel_10484", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10484");
    }
    {
        map_kernel_10494 = clCreateKernel(prog, "map_kernel_10494", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10494");
    }
    {
        map_kernel_10590 = clCreateKernel(prog, "map_kernel_10590", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10590");
    }
    {
        map_kernel_10600 = clCreateKernel(prog, "map_kernel_10600", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10600");
    }
    {
        map_kernel_10696 = clCreateKernel(prog, "map_kernel_10696", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10696");
    }
    {
        map_kernel_10706 = clCreateKernel(prog, "map_kernel_10706", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10706");
    }
    {
        map_kernel_10802 = clCreateKernel(prog, "map_kernel_10802", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10802");
    }
    {
        map_kernel_10812 = clCreateKernel(prog, "map_kernel_10812", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10812");
    }
    {
        map_kernel_10985 = clCreateKernel(prog, "map_kernel_10985", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10985");
    }
    {
        map_kernel_10995 = clCreateKernel(prog, "map_kernel_10995", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_10995");
    }
    {
        map_kernel_11168 = clCreateKernel(prog, "map_kernel_11168", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_11168");
    }
    {
        map_kernel_11178 = clCreateKernel(prog, "map_kernel_11178", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_11178");
    }
    {
        map_kernel_11512 = clCreateKernel(prog, "map_kernel_11512", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_11512");
    }
    {
        map_kernel_11522 = clCreateKernel(prog, "map_kernel_11522", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_11522");
    }
    {
        map_kernel_11535 = clCreateKernel(prog, "map_kernel_11535", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_11535");
    }
    {
        map_kernel_11556 = clCreateKernel(prog, "map_kernel_11556", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_11556");
    }
    {
        map_kernel_9072 = clCreateKernel(prog, "map_kernel_9072", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9072");
    }
    {
        map_kernel_9082 = clCreateKernel(prog, "map_kernel_9082", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9082");
    }
    {
        map_kernel_9184 = clCreateKernel(prog, "map_kernel_9184", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9184");
    }
    {
        map_kernel_9288 = clCreateKernel(prog, "map_kernel_9288", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9288");
    }
    {
        map_kernel_9298 = clCreateKernel(prog, "map_kernel_9298", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9298");
    }
    {
        map_kernel_9308 = clCreateKernel(prog, "map_kernel_9308", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9308");
    }
    {
        map_kernel_9322 = clCreateKernel(prog, "map_kernel_9322", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9322");
    }
    {
        map_kernel_9340 = clCreateKernel(prog, "map_kernel_9340", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9340");
    }
    {
        map_kernel_9460 = clCreateKernel(prog, "map_kernel_9460", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9460");
    }
    {
        map_kernel_9609 = clCreateKernel(prog, "map_kernel_9609", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9609");
    }
    {
        map_kernel_9700 = clCreateKernel(prog, "map_kernel_9700", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9700");
    }
    {
        map_kernel_9790 = clCreateKernel(prog, "map_kernel_9790", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9790");
    }
    {
        map_kernel_9860 = clCreateKernel(prog, "map_kernel_9860", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9860");
    }
    {
        map_kernel_9980 = clCreateKernel(prog, "map_kernel_9980", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "map_kernel_9980");
    }
    {
        reduce_kernel_10365 = clCreateKernel(prog, "reduce_kernel_10365",
                                             &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "reduce_kernel_10365");
    }
    {
        reduce_kernel_9169 = clCreateKernel(prog, "reduce_kernel_9169", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "reduce_kernel_9169");
    }
    {
        reduce_kernel_9845 = clCreateKernel(prog, "reduce_kernel_9845", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "reduce_kernel_9845");
    }
    {
        scan1_kernel_10040 = clCreateKernel(prog, "scan1_kernel_10040", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10040");
    }
    {
        scan1_kernel_10162 = clCreateKernel(prog, "scan1_kernel_10162", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10162");
    }
    {
        scan1_kernel_10252 = clCreateKernel(prog, "scan1_kernel_10252", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10252");
    }
    {
        scan1_kernel_10423 = clCreateKernel(prog, "scan1_kernel_10423", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10423");
    }
    {
        scan1_kernel_10529 = clCreateKernel(prog, "scan1_kernel_10529", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10529");
    }
    {
        scan1_kernel_10635 = clCreateKernel(prog, "scan1_kernel_10635", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10635");
    }
    {
        scan1_kernel_10741 = clCreateKernel(prog, "scan1_kernel_10741", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10741");
    }
    {
        scan1_kernel_10922 = clCreateKernel(prog, "scan1_kernel_10922", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_10922");
    }
    {
        scan1_kernel_11105 = clCreateKernel(prog, "scan1_kernel_11105", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_11105");
    }
    {
        scan1_kernel_11445 = clCreateKernel(prog, "scan1_kernel_11445", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_11445");
    }
    {
        scan1_kernel_9011 = clCreateKernel(prog, "scan1_kernel_9011", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_9011");
    }
    {
        scan1_kernel_9227 = clCreateKernel(prog, "scan1_kernel_9227", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_9227");
    }
    {
        scan1_kernel_9396 = clCreateKernel(prog, "scan1_kernel_9396", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_9396");
    }
    {
        scan1_kernel_9520 = clCreateKernel(prog, "scan1_kernel_9520", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_9520");
    }
    {
        scan1_kernel_9642 = clCreateKernel(prog, "scan1_kernel_9642", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_9642");
    }
    {
        scan1_kernel_9732 = clCreateKernel(prog, "scan1_kernel_9732", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_9732");
    }
    {
        scan1_kernel_9916 = clCreateKernel(prog, "scan1_kernel_9916", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan1_kernel_9916");
    }
    {
        scan2_kernel_10093 = clCreateKernel(prog, "scan2_kernel_10093", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10093");
    }
    {
        scan2_kernel_10194 = clCreateKernel(prog, "scan2_kernel_10194", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10194");
    }
    {
        scan2_kernel_10284 = clCreateKernel(prog, "scan2_kernel_10284", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10284");
    }
    {
        scan2_kernel_10458 = clCreateKernel(prog, "scan2_kernel_10458", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10458");
    }
    {
        scan2_kernel_10564 = clCreateKernel(prog, "scan2_kernel_10564", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10564");
    }
    {
        scan2_kernel_10670 = clCreateKernel(prog, "scan2_kernel_10670", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10670");
    }
    {
        scan2_kernel_10776 = clCreateKernel(prog, "scan2_kernel_10776", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10776");
    }
    {
        scan2_kernel_10959 = clCreateKernel(prog, "scan2_kernel_10959", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_10959");
    }
    {
        scan2_kernel_11142 = clCreateKernel(prog, "scan2_kernel_11142", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_11142");
    }
    {
        scan2_kernel_11486 = clCreateKernel(prog, "scan2_kernel_11486", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_11486");
    }
    {
        scan2_kernel_9046 = clCreateKernel(prog, "scan2_kernel_9046", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_9046");
    }
    {
        scan2_kernel_9262 = clCreateKernel(prog, "scan2_kernel_9262", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_9262");
    }
    {
        scan2_kernel_9434 = clCreateKernel(prog, "scan2_kernel_9434", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_9434");
    }
    {
        scan2_kernel_9573 = clCreateKernel(prog, "scan2_kernel_9573", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_9573");
    }
    {
        scan2_kernel_9674 = clCreateKernel(prog, "scan2_kernel_9674", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_9674");
    }
    {
        scan2_kernel_9764 = clCreateKernel(prog, "scan2_kernel_9764", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_9764");
    }
    {
        scan2_kernel_9954 = clCreateKernel(prog, "scan2_kernel_9954", &error);
        assert(error == 0);
        if (debugging)
            fprintf(stderr, "Created kernel %s.\n", "scan2_kernel_9954");
    }
}
void post_opencl_setup(struct opencl_device_option *option)
{
    if (strcmp(option->platform_name, "NVIDIA CUDA") == 0 &&
        option->device_type == CL_DEVICE_TYPE_GPU) {
        cl_lockstep_width = 32;
        if (debugging)
            fprintf(stderr, "Setting lockstep width to: %d\n",
                    cl_lockstep_width);
    }
    if (strcmp(option->platform_name, "AMD Accelerated Parallel Processing") ==
        0 && option->device_type == CL_DEVICE_TYPE_GPU) {
        cl_lockstep_width = 64;
        if (debugging)
            fprintf(stderr, "Setting lockstep width to: %d\n",
                    cl_lockstep_width);
    }
}
int64_t peak_mem_usage_device = 0;
int64_t cur_mem_usage_device = 0;
struct memblock_device {
    int *references;
    cl_mem mem;
    int64_t size;
} ;
static void memblock_unref_device(struct memblock_device *block)
{
    if (block->references != NULL) {
        *block->references -= 1;
        if (detail_memory)
            fprintf(stderr,
                    "Unreferencing block in space 'device': %d references remaining.\n",
                    *block->references);
        if (*block->references == 0) {
            cur_mem_usage_device -= block->size;
            OPENCL_SUCCEED(clReleaseMemObject(block->mem));
            free(block->references);
            block->references = NULL;
            if (detail_memory)
                fprintf(stderr, "%ld bytes freed (now allocated: %ld bytes)\n",
                        block->size, cur_mem_usage_device);
        }
    }
}
static void memblock_alloc_device(struct memblock_device *block, int32_t size)
{
    memblock_unref_device(block);
    
    cl_int clCreateBuffer_succeeded_13030;
    
    block->mem = clCreateBuffer(fut_cl_context, CL_MEM_READ_WRITE, size >
                                0 ? size : 1, NULL,
                                &clCreateBuffer_succeeded_13030);
    OPENCL_SUCCEED(clCreateBuffer_succeeded_13030);
    block->references = (int *) malloc(sizeof(int));
    *block->references = 1;
    block->size = size;
    cur_mem_usage_device += size;
    if (detail_memory)
        fprintf(stderr,
                "Allocated %d bytes in space 'device' (now allocated: %ld bytes)",
                size, cur_mem_usage_device);
    if (cur_mem_usage_device > peak_mem_usage_device) {
        peak_mem_usage_device = cur_mem_usage_device;
        if (detail_memory)
            fprintf(stderr, " (new peak).\n", peak_mem_usage_device);
    } else if (detail_memory)
        fprintf(stderr, ".\n");
}
static void memblock_set_device(struct memblock_device *lhs,
                                struct memblock_device *rhs)
{
    memblock_unref_device(lhs);
    (*rhs->references)++;
    *lhs = *rhs;
}
int64_t peak_mem_usage_local = 0;
int64_t cur_mem_usage_local = 0;
struct memblock_local {
    int *references;
    unsigned char mem;
    int64_t size;
} ;
static void memblock_unref_local(struct memblock_local *block)
{
    if (block->references != NULL) {
        *block->references -= 1;
        if (detail_memory)
            fprintf(stderr,
                    "Unreferencing block in space 'local': %d references remaining.\n",
                    *block->references);
        if (*block->references == 0) {
            cur_mem_usage_local -= block->size;
            free(block->references);
            block->references = NULL;
            if (detail_memory)
                fprintf(stderr, "%ld bytes freed (now allocated: %ld bytes)\n",
                        block->size, cur_mem_usage_local);
        }
    }
}
static void memblock_alloc_local(struct memblock_local *block, int32_t size)
{
    memblock_unref_local(block);
    block->references = (int *) malloc(sizeof(int));
    *block->references = 1;
    block->size = size;
    cur_mem_usage_local += size;
    if (detail_memory)
        fprintf(stderr,
                "Allocated %d bytes in space 'local' (now allocated: %ld bytes)",
                size, cur_mem_usage_local);
    if (cur_mem_usage_local > peak_mem_usage_local) {
        peak_mem_usage_local = cur_mem_usage_local;
        if (detail_memory)
            fprintf(stderr, " (new peak).\n", peak_mem_usage_local);
    } else if (detail_memory)
        fprintf(stderr, ".\n");
}
static void memblock_set_local(struct memblock_local *lhs,
                               struct memblock_local *rhs)
{
    memblock_unref_local(lhs);
    (*rhs->references)++;
    *lhs = *rhs;
}
int64_t peak_mem_usage_default = 0;
int64_t cur_mem_usage_default = 0;
struct memblock {
    int *references;
    char *mem;
    int64_t size;
} ;
static void memblock_unref(struct memblock *block)
{
    if (block->references != NULL) {
        *block->references -= 1;
        if (detail_memory)
            fprintf(stderr,
                    "Unreferencing block in default space: %d references remaining.\n",
                    *block->references);
        if (*block->references == 0) {
            cur_mem_usage_default -= block->size;
            free(block->mem);
            free(block->references);
            block->references = NULL;
            if (detail_memory)
                fprintf(stderr, "%ld bytes freed (now allocated: %ld bytes)\n",
                        block->size, cur_mem_usage_default);
        }
    }
}
static void memblock_alloc(struct memblock *block, int32_t size)
{
    memblock_unref(block);
    block->mem = (char *) malloc(size);
    block->references = (int *) malloc(sizeof(int));
    *block->references = 1;
    block->size = size;
    cur_mem_usage_default += size;
    if (detail_memory)
        fprintf(stderr,
                "Allocated %d bytes in default space (now allocated: %ld bytes)",
                size, cur_mem_usage_default);
    if (cur_mem_usage_default > peak_mem_usage_default) {
        peak_mem_usage_default = cur_mem_usage_default;
        if (detail_memory)
            fprintf(stderr, " (new peak).\n", peak_mem_usage_default);
    } else if (detail_memory)
        fprintf(stderr, ".\n");
}
static void memblock_set(struct memblock *lhs, struct memblock *rhs)
{
    memblock_unref(lhs);
    (*rhs->references)++;
    *lhs = *rhs;
}
struct tuple_ { } ;
struct tuple_int32_t_device_mem_int32_t_int32_t_device_mem_int32_t_device_mem {
    int32_t elem_0;
    struct memblock_device elem_1;
    int32_t elem_2;
    int32_t elem_3;
    struct memblock_device elem_4;
    int32_t elem_5;
    struct memblock_device elem_6;
} ;
struct tuple_int32_t_device_mem_int32_t {
    int32_t elem_0;
    struct memblock_device elem_1;
    int32_t elem_2;
} ;
struct tuple_int32_t_device_mem_int32_t_int32_t {
    int32_t elem_0;
    struct memblock_device elem_1;
    int32_t elem_2;
    int32_t elem_3;
} ;
static struct tuple_
futhark_map_transpose_opencl_i32(struct memblock_device destmem_0,
                                 int32_t destoffset_1,
                                 struct memblock_device srcmem_2,
                                 int32_t srcoffset_3, int32_t num_arrays_4,
                                 int32_t x_elems_5, int32_t y_elems_6,
                                 int32_t in_elems_7, int32_t out_elems_8);
static
struct tuple_int32_t_device_mem_int32_t_int32_t_device_mem_int32_t_device_mem
futhark_generatePolynomials(int32_t degree_7928, int32_t prime_7929);
static char futhark_isZZeroable(int64_t pol_mem_sizze_11612,
                                struct memblock_device pol_mem_11613,
                                int32_t sizze_7966, int32_t p_7968);
static struct tuple_int32_t_device_mem_int32_t
futhark_translate(int32_t padding_8004, int32_t d_8005, int32_t p_8006,
                  int32_t x_8007);
static struct tuple_int32_t_device_mem_int32_t_int32_t
futhark_generatePolynomialsTest(int32_t degree_8025, int32_t prime_8026);
static struct tuple_int32_t_device_mem_int32_t
futhark_translateTestTight(int32_t d_8068, int32_t p_8069, int32_t x_8070);
static struct tuple_int32_t_device_mem_int32_t
futhark_translateTestLoose(int32_t d_8087, int32_t p_8088, int32_t x_8089);
static char futhark_isIrreducible(int32_t bb_8107, int32_t bb_8108,
                                  int32_t bb_8109, int32_t aa_8110,
                                  int32_t aa_8111, int32_t aa_8112,
                                  int32_t prime_8113, int32_t padding_8114);
static char futhark_isIrreducibleTest(int64_t b_mem_sizze_11612,
                                      int64_t a_mem_sizze_11614,
                                      struct memblock_device b_mem_11613,
                                      struct memblock_device a_mem_11615,
                                      int32_t sizze_8235, int32_t sizze_8236,
                                      int32_t prime_8239, int32_t padding_8240);
static struct tuple_int32_t_device_mem_int32_t_int32_t
futhark_main(int32_t prime_8379, int32_t maxDegree_8380);
static inline float futhark_log32(float x)
{
    return log(x);
}
static inline float futhark_sqrt32(float x)
{
    return sqrt(x);
}
static inline float futhark_exp32(float x)
{
    return exp(x);
}
static inline float futhark_cos32(float x)
{
    return cos(x);
}
static inline float futhark_sin32(float x)
{
    return sin(x);
}
static inline float futhark_acos32(float x)
{
    return acos(x);
}
static inline float futhark_asin32(float x)
{
    return asin(x);
}
static inline double futhark_atan32(float x)
{
    return atan(x);
}
static inline float futhark_atan2_32(float x, float y)
{
    return atan2(x, y);
}
static inline char futhark_isnan32(float x)
{
    return isnan(x);
}
static inline char futhark_isinf32(float x)
{
    return isinf(x);
}
static inline double futhark_log64(double x)
{
    return log(x);
}
static inline double futhark_sqrt64(double x)
{
    return sqrt(x);
}
static inline double futhark_exp64(double x)
{
    return exp(x);
}
static inline double futhark_cos64(double x)
{
    return cos(x);
}
static inline double futhark_sin64(double x)
{
    return sin(x);
}
static inline double futhark_acos64(double x)
{
    return acos(x);
}
static inline double futhark_asin64(double x)
{
    return asin(x);
}
static inline double futhark_atan64(double x)
{
    return atan(x);
}
static inline double futhark_atan2_64(double x, double y)
{
    return atan2(x, y);
}
static inline char futhark_isnan64(double x)
{
    return isnan(x);
}
static inline char futhark_isinf64(double x)
{
    return isinf(x);
}
static inline int8_t add8(int8_t x, int8_t y)
{
    return x + y;
}
static inline int16_t add16(int16_t x, int16_t y)
{
    return x + y;
}
static inline int32_t add32(int32_t x, int32_t y)
{
    return x + y;
}
static inline int64_t add64(int64_t x, int64_t y)
{
    return x + y;
}
static inline int8_t sub8(int8_t x, int8_t y)
{
    return x - y;
}
static inline int16_t sub16(int16_t x, int16_t y)
{
    return x - y;
}
static inline int32_t sub32(int32_t x, int32_t y)
{
    return x - y;
}
static inline int64_t sub64(int64_t x, int64_t y)
{
    return x - y;
}
static inline int8_t mul8(int8_t x, int8_t y)
{
    return x * y;
}
static inline int16_t mul16(int16_t x, int16_t y)
{
    return x * y;
}
static inline int32_t mul32(int32_t x, int32_t y)
{
    return x * y;
}
static inline int64_t mul64(int64_t x, int64_t y)
{
    return x * y;
}
static inline uint8_t udiv8(uint8_t x, uint8_t y)
{
    return x / y;
}
static inline uint16_t udiv16(uint16_t x, uint16_t y)
{
    return x / y;
}
static inline uint32_t udiv32(uint32_t x, uint32_t y)
{
    return x / y;
}
static inline uint64_t udiv64(uint64_t x, uint64_t y)
{
    return x / y;
}
static inline uint8_t umod8(uint8_t x, uint8_t y)
{
    return x % y;
}
static inline uint16_t umod16(uint16_t x, uint16_t y)
{
    return x % y;
}
static inline uint32_t umod32(uint32_t x, uint32_t y)
{
    return x % y;
}
static inline uint64_t umod64(uint64_t x, uint64_t y)
{
    return x % y;
}
static inline int8_t sdiv8(int8_t x, int8_t y)
{
    int8_t q = x / y;
    int8_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int16_t sdiv16(int16_t x, int16_t y)
{
    int16_t q = x / y;
    int16_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int32_t sdiv32(int32_t x, int32_t y)
{
    int32_t q = x / y;
    int32_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int64_t sdiv64(int64_t x, int64_t y)
{
    int64_t q = x / y;
    int64_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int8_t smod8(int8_t x, int8_t y)
{
    int8_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int16_t smod16(int16_t x, int16_t y)
{
    int16_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int32_t smod32(int32_t x, int32_t y)
{
    int32_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int64_t smod64(int64_t x, int64_t y)
{
    int64_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int8_t squot8(int8_t x, int8_t y)
{
    return x / y;
}
static inline int16_t squot16(int16_t x, int16_t y)
{
    return x / y;
}
static inline int32_t squot32(int32_t x, int32_t y)
{
    return x / y;
}
static inline int64_t squot64(int64_t x, int64_t y)
{
    return x / y;
}
static inline int8_t srem8(int8_t x, int8_t y)
{
    return x % y;
}
static inline int16_t srem16(int16_t x, int16_t y)
{
    return x % y;
}
static inline int32_t srem32(int32_t x, int32_t y)
{
    return x % y;
}
static inline int64_t srem64(int64_t x, int64_t y)
{
    return x % y;
}
static inline int8_t smin8(int8_t x, int8_t y)
{
    return x < y ? x : y;
}
static inline int16_t smin16(int16_t x, int16_t y)
{
    return x < y ? x : y;
}
static inline int32_t smin32(int32_t x, int32_t y)
{
    return x < y ? x : y;
}
static inline int64_t smin64(int64_t x, int64_t y)
{
    return x < y ? x : y;
}
static inline uint8_t umin8(uint8_t x, uint8_t y)
{
    return x < y ? x : y;
}
static inline uint16_t umin16(uint16_t x, uint16_t y)
{
    return x < y ? x : y;
}
static inline uint32_t umin32(uint32_t x, uint32_t y)
{
    return x < y ? x : y;
}
static inline uint64_t umin64(uint64_t x, uint64_t y)
{
    return x < y ? x : y;
}
static inline int8_t smax8(int8_t x, int8_t y)
{
    return x < y ? y : x;
}
static inline int16_t smax16(int16_t x, int16_t y)
{
    return x < y ? y : x;
}
static inline int32_t smax32(int32_t x, int32_t y)
{
    return x < y ? y : x;
}
static inline int64_t smax64(int64_t x, int64_t y)
{
    return x < y ? y : x;
}
static inline uint8_t umax8(uint8_t x, uint8_t y)
{
    return x < y ? y : x;
}
static inline uint16_t umax16(uint16_t x, uint16_t y)
{
    return x < y ? y : x;
}
static inline uint32_t umax32(uint32_t x, uint32_t y)
{
    return x < y ? y : x;
}
static inline uint64_t umax64(uint64_t x, uint64_t y)
{
    return x < y ? y : x;
}
static inline uint8_t shl8(uint8_t x, uint8_t y)
{
    return x << y;
}
static inline uint16_t shl16(uint16_t x, uint16_t y)
{
    return x << y;
}
static inline uint32_t shl32(uint32_t x, uint32_t y)
{
    return x << y;
}
static inline uint64_t shl64(uint64_t x, uint64_t y)
{
    return x << y;
}
static inline uint8_t lshr8(uint8_t x, uint8_t y)
{
    return x >> y;
}
static inline uint16_t lshr16(uint16_t x, uint16_t y)
{
    return x >> y;
}
static inline uint32_t lshr32(uint32_t x, uint32_t y)
{
    return x >> y;
}
static inline uint64_t lshr64(uint64_t x, uint64_t y)
{
    return x >> y;
}
static inline int8_t ashr8(int8_t x, int8_t y)
{
    return x >> y;
}
static inline int16_t ashr16(int16_t x, int16_t y)
{
    return x >> y;
}
static inline int32_t ashr32(int32_t x, int32_t y)
{
    return x >> y;
}
static inline int64_t ashr64(int64_t x, int64_t y)
{
    return x >> y;
}
static inline uint8_t and8(uint8_t x, uint8_t y)
{
    return x & y;
}
static inline uint16_t and16(uint16_t x, uint16_t y)
{
    return x & y;
}
static inline uint32_t and32(uint32_t x, uint32_t y)
{
    return x & y;
}
static inline uint64_t and64(uint64_t x, uint64_t y)
{
    return x & y;
}
static inline uint8_t or8(uint8_t x, uint8_t y)
{
    return x | y;
}
static inline uint16_t or16(uint16_t x, uint16_t y)
{
    return x | y;
}
static inline uint32_t or32(uint32_t x, uint32_t y)
{
    return x | y;
}
static inline uint64_t or64(uint64_t x, uint64_t y)
{
    return x | y;
}
static inline uint8_t xor8(uint8_t x, uint8_t y)
{
    return x ^ y;
}
static inline uint16_t xor16(uint16_t x, uint16_t y)
{
    return x ^ y;
}
static inline uint32_t xor32(uint32_t x, uint32_t y)
{
    return x ^ y;
}
static inline uint64_t xor64(uint64_t x, uint64_t y)
{
    return x ^ y;
}
static inline char ult8(uint8_t x, uint8_t y)
{
    return x < y;
}
static inline char ult16(uint16_t x, uint16_t y)
{
    return x < y;
}
static inline char ult32(uint32_t x, uint32_t y)
{
    return x < y;
}
static inline char ult64(uint64_t x, uint64_t y)
{
    return x < y;
}
static inline char ule8(uint8_t x, uint8_t y)
{
    return x <= y;
}
static inline char ule16(uint16_t x, uint16_t y)
{
    return x <= y;
}
static inline char ule32(uint32_t x, uint32_t y)
{
    return x <= y;
}
static inline char ule64(uint64_t x, uint64_t y)
{
    return x <= y;
}
static inline char slt8(int8_t x, int8_t y)
{
    return x < y;
}
static inline char slt16(int16_t x, int16_t y)
{
    return x < y;
}
static inline char slt32(int32_t x, int32_t y)
{
    return x < y;
}
static inline char slt64(int64_t x, int64_t y)
{
    return x < y;
}
static inline char sle8(int8_t x, int8_t y)
{
    return x <= y;
}
static inline char sle16(int16_t x, int16_t y)
{
    return x <= y;
}
static inline char sle32(int32_t x, int32_t y)
{
    return x <= y;
}
static inline char sle64(int64_t x, int64_t y)
{
    return x <= y;
}
static inline int8_t pow8(int8_t x, int8_t y)
{
    int8_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int16_t pow16(int16_t x, int16_t y)
{
    int16_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int32_t pow32(int32_t x, int32_t y)
{
    int32_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int64_t pow64(int64_t x, int64_t y)
{
    int64_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int8_t sext_i8_i8(int8_t x)
{
    return x;
}
static inline int16_t sext_i8_i16(int8_t x)
{
    return x;
}
static inline int32_t sext_i8_i32(int8_t x)
{
    return x;
}
static inline int64_t sext_i8_i64(int8_t x)
{
    return x;
}
static inline int8_t sext_i16_i8(int16_t x)
{
    return x;
}
static inline int16_t sext_i16_i16(int16_t x)
{
    return x;
}
static inline int32_t sext_i16_i32(int16_t x)
{
    return x;
}
static inline int64_t sext_i16_i64(int16_t x)
{
    return x;
}
static inline int8_t sext_i32_i8(int32_t x)
{
    return x;
}
static inline int16_t sext_i32_i16(int32_t x)
{
    return x;
}
static inline int32_t sext_i32_i32(int32_t x)
{
    return x;
}
static inline int64_t sext_i32_i64(int32_t x)
{
    return x;
}
static inline int8_t sext_i64_i8(int64_t x)
{
    return x;
}
static inline int16_t sext_i64_i16(int64_t x)
{
    return x;
}
static inline int32_t sext_i64_i32(int64_t x)
{
    return x;
}
static inline int64_t sext_i64_i64(int64_t x)
{
    return x;
}
static inline uint8_t zext_i8_i8(uint8_t x)
{
    return x;
}
static inline uint16_t zext_i8_i16(uint8_t x)
{
    return x;
}
static inline uint32_t zext_i8_i32(uint8_t x)
{
    return x;
}
static inline uint64_t zext_i8_i64(uint8_t x)
{
    return x;
}
static inline uint8_t zext_i16_i8(uint16_t x)
{
    return x;
}
static inline uint16_t zext_i16_i16(uint16_t x)
{
    return x;
}
static inline uint32_t zext_i16_i32(uint16_t x)
{
    return x;
}
static inline uint64_t zext_i16_i64(uint16_t x)
{
    return x;
}
static inline uint8_t zext_i32_i8(uint32_t x)
{
    return x;
}
static inline uint16_t zext_i32_i16(uint32_t x)
{
    return x;
}
static inline uint32_t zext_i32_i32(uint32_t x)
{
    return x;
}
static inline uint64_t zext_i32_i64(uint32_t x)
{
    return x;
}
static inline uint8_t zext_i64_i8(uint64_t x)
{
    return x;
}
static inline uint16_t zext_i64_i16(uint64_t x)
{
    return x;
}
static inline uint32_t zext_i64_i32(uint64_t x)
{
    return x;
}
static inline uint64_t zext_i64_i64(uint64_t x)
{
    return x;
}
static inline float fdiv32(float x, float y)
{
    return x / y;
}
static inline float fadd32(float x, float y)
{
    return x + y;
}
static inline float fsub32(float x, float y)
{
    return x - y;
}
static inline float fmul32(float x, float y)
{
    return x * y;
}
static inline float fmin32(float x, float y)
{
    return x < y ? x : y;
}
static inline float fmax32(float x, float y)
{
    return x < y ? y : x;
}
static inline float fpow32(float x, float y)
{
    return pow(x, y);
}
static inline char cmplt32(float x, float y)
{
    return x < y;
}
static inline char cmple32(float x, float y)
{
    return x <= y;
}
static inline float sitofp_i8_f32(int8_t x)
{
    return x;
}
static inline float sitofp_i16_f32(int16_t x)
{
    return x;
}
static inline float sitofp_i32_f32(int32_t x)
{
    return x;
}
static inline float sitofp_i64_f32(int64_t x)
{
    return x;
}
static inline float uitofp_i8_f32(uint8_t x)
{
    return x;
}
static inline float uitofp_i16_f32(uint16_t x)
{
    return x;
}
static inline float uitofp_i32_f32(uint32_t x)
{
    return x;
}
static inline float uitofp_i64_f32(uint64_t x)
{
    return x;
}
static inline int8_t fptosi_f32_i8(float x)
{
    return x;
}
static inline int16_t fptosi_f32_i16(float x)
{
    return x;
}
static inline int32_t fptosi_f32_i32(float x)
{
    return x;
}
static inline int64_t fptosi_f32_i64(float x)
{
    return x;
}
static inline uint8_t fptoui_f32_i8(float x)
{
    return x;
}
static inline uint16_t fptoui_f32_i16(float x)
{
    return x;
}
static inline uint32_t fptoui_f32_i32(float x)
{
    return x;
}
static inline uint64_t fptoui_f32_i64(float x)
{
    return x;
}
static inline double fdiv64(double x, double y)
{
    return x / y;
}
static inline double fadd64(double x, double y)
{
    return x + y;
}
static inline double fsub64(double x, double y)
{
    return x - y;
}
static inline double fmul64(double x, double y)
{
    return x * y;
}
static inline double fmin64(double x, double y)
{
    return x < y ? x : y;
}
static inline double fmax64(double x, double y)
{
    return x < y ? y : x;
}
static inline double fpow64(double x, double y)
{
    return pow(x, y);
}
static inline char cmplt64(double x, double y)
{
    return x < y;
}
static inline char cmple64(double x, double y)
{
    return x <= y;
}
static inline double sitofp_i8_f64(int8_t x)
{
    return x;
}
static inline double sitofp_i16_f64(int16_t x)
{
    return x;
}
static inline double sitofp_i32_f64(int32_t x)
{
    return x;
}
static inline double sitofp_i64_f64(int64_t x)
{
    return x;
}
static inline double uitofp_i8_f64(uint8_t x)
{
    return x;
}
static inline double uitofp_i16_f64(uint16_t x)
{
    return x;
}
static inline double uitofp_i32_f64(uint32_t x)
{
    return x;
}
static inline double uitofp_i64_f64(uint64_t x)
{
    return x;
}
static inline int8_t fptosi_f64_i8(double x)
{
    return x;
}
static inline int16_t fptosi_f64_i16(double x)
{
    return x;
}
static inline int32_t fptosi_f64_i32(double x)
{
    return x;
}
static inline int64_t fptosi_f64_i64(double x)
{
    return x;
}
static inline uint8_t fptoui_f64_i8(double x)
{
    return x;
}
static inline uint16_t fptoui_f64_i16(double x)
{
    return x;
}
static inline uint32_t fptoui_f64_i32(double x)
{
    return x;
}
static inline uint64_t fptoui_f64_i64(double x)
{
    return x;
}
static inline float fpconv_f32_f32(float x)
{
    return x;
}
static inline double fpconv_f32_f64(float x)
{
    return x;
}
static inline float fpconv_f64_f32(double x)
{
    return x;
}
static inline double fpconv_f64_f64(double x)
{
    return x;
}
static int detail_timing = 0;
static
struct tuple_ futhark_map_transpose_opencl_i32(struct memblock_device destmem_0,
                                               int32_t destoffset_1,
                                               struct memblock_device srcmem_2,
                                               int32_t srcoffset_3,
                                               int32_t num_arrays_4,
                                               int32_t x_elems_5,
                                               int32_t y_elems_6,
                                               int32_t in_elems_7,
                                               int32_t out_elems_8)
{
    if (in_elems_7 == out_elems_8 && ((num_arrays_4 == 1 || x_elems_5 *
                                       y_elems_6 == in_elems_7) && (x_elems_5 ==
                                                                    1 ||
                                                                    y_elems_6 ==
                                                                    1))) {
        if (in_elems_7 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, srcmem_2.mem,
                                               destmem_0.mem, srcoffset_3,
                                               destoffset_1, in_elems_7 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
    } else {
        if (sle32(x_elems_5, squot32(16, 2)) && slt32(16, y_elems_6)) {
            int32_t muly_9 = squot32(16, x_elems_5);
            int32_t new_height_10;
            
            new_height_10 = squot32(y_elems_6 + muly_9 - 1, muly_9);
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          0, sizeof(destmem_0.mem),
                                          &destmem_0.mem));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          1, sizeof(destoffset_1),
                                          &destoffset_1));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          2, sizeof(srcmem_2.mem),
                                          &srcmem_2.mem));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          3, sizeof(srcoffset_3),
                                          &srcoffset_3));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          4, sizeof(x_elems_5), &x_elems_5));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          5, sizeof(y_elems_6), &y_elems_6));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          6, sizeof(in_elems_7), &in_elems_7));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          7, sizeof(out_elems_8),
                                          &out_elems_8));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          8, sizeof(muly_9), &muly_9));
            OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowwidth_i32,
                                          9, 272 * sizeof(int32_t), NULL));
            if (1 * (x_elems_5 + srem32(16 - srem32(x_elems_5, 16), 16)) *
                (new_height_10 + srem32(16 - srem32(new_height_10, 16), 16)) *
                num_arrays_4 != 0) {
                const size_t global_work_sizze_12584[3] = {x_elems_5 +
                                                           srem32(16 -
                                                                  srem32(x_elems_5,
                                                                         16),
                                                                  16),
                                                           new_height_10 +
                                                           srem32(16 -
                                                                  srem32(new_height_10,
                                                                         16),
                                                                  16),
                                                           num_arrays_4};
                const size_t local_work_sizze_12588[3] = {16, 16, 1};
                int64_t time_start_12585, time_end_12586;
                
                if (debugging) {
                    fprintf(stderr, "Launching %s with global work size [",
                            "fut_kernel_map_transpose_lowwidth_i32");
                    fprintf(stderr, "%zu", global_work_sizze_12584[0]);
                    fprintf(stderr, ", ");
                    fprintf(stderr, "%zu", global_work_sizze_12584[1]);
                    fprintf(stderr, ", ");
                    fprintf(stderr, "%zu", global_work_sizze_12584[2]);
                    fprintf(stderr, "].\n");
                    time_start_12585 = get_wall_time();
                }
                OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                      fut_kernel_map_transpose_lowwidth_i32,
                                                      3, NULL,
                                                      global_work_sizze_12584,
                                                      local_work_sizze_12588, 0,
                                                      NULL, NULL));
                if (debugging) {
                    OPENCL_SUCCEED(clFinish(fut_cl_queue));
                    time_end_12586 = get_wall_time();
                    
                    long time_diff_12587 = time_end_12586 - time_start_12585;
                    
                    if (detail_timing) {
                        fut_kernel_map_transpose_lowwidth_i32total_runtime +=
                            time_diff_12587;
                        fut_kernel_map_transpose_lowwidth_i32runs++;
                        fprintf(stderr, "kernel %s runtime: %ldus\n",
                                "fut_kernel_map_transpose_lowwidth_i32",
                                (int) time_diff_12587);
                    }
                }
            }
        } else {
            if (sle32(y_elems_6, squot32(16, 2)) && slt32(16, x_elems_5)) {
                int32_t mulx_11 = squot32(16, y_elems_6);
                int32_t new_width_12;
                
                new_width_12 = squot32(x_elems_5 + mulx_11 - 1, mulx_11);
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              0, sizeof(destmem_0.mem),
                                              &destmem_0.mem));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              1, sizeof(destoffset_1),
                                              &destoffset_1));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              2, sizeof(srcmem_2.mem),
                                              &srcmem_2.mem));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              3, sizeof(srcoffset_3),
                                              &srcoffset_3));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              4, sizeof(x_elems_5),
                                              &x_elems_5));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              5, sizeof(y_elems_6),
                                              &y_elems_6));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              6, sizeof(in_elems_7),
                                              &in_elems_7));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              7, sizeof(out_elems_8),
                                              &out_elems_8));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              8, sizeof(mulx_11), &mulx_11));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_lowheight_i32,
                                              9, 272 * sizeof(int32_t), NULL));
                if (1 * (new_width_12 + srem32(16 - srem32(new_width_12, 16),
                                               16)) * (y_elems_6 + srem32(16 -
                                                                          srem32(y_elems_6,
                                                                                 16),
                                                                          16)) *
                    num_arrays_4 != 0) {
                    const size_t global_work_sizze_12589[3] = {new_width_12 +
                                                               srem32(16 -
                                                                      srem32(new_width_12,
                                                                             16),
                                                                      16),
                                                               y_elems_6 +
                                                               srem32(16 -
                                                                      srem32(y_elems_6,
                                                                             16),
                                                                      16),
                                                               num_arrays_4};
                    const size_t local_work_sizze_12593[3] = {16, 16, 1};
                    int64_t time_start_12590, time_end_12591;
                    
                    if (debugging) {
                        fprintf(stderr, "Launching %s with global work size [",
                                "fut_kernel_map_transpose_lowheight_i32");
                        fprintf(stderr, "%zu", global_work_sizze_12589[0]);
                        fprintf(stderr, ", ");
                        fprintf(stderr, "%zu", global_work_sizze_12589[1]);
                        fprintf(stderr, ", ");
                        fprintf(stderr, "%zu", global_work_sizze_12589[2]);
                        fprintf(stderr, "].\n");
                        time_start_12590 = get_wall_time();
                    }
                    OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                          fut_kernel_map_transpose_lowheight_i32,
                                                          3, NULL,
                                                          global_work_sizze_12589,
                                                          local_work_sizze_12593,
                                                          0, NULL, NULL));
                    if (debugging) {
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                        time_end_12591 = get_wall_time();
                        
                        long time_diff_12592 = time_end_12591 -
                             time_start_12590;
                        
                        if (detail_timing) {
                            fut_kernel_map_transpose_lowheight_i32total_runtime +=
                                time_diff_12592;
                            fut_kernel_map_transpose_lowheight_i32runs++;
                            fprintf(stderr, "kernel %s runtime: %ldus\n",
                                    "fut_kernel_map_transpose_lowheight_i32",
                                    (int) time_diff_12592);
                        }
                    }
                }
            } else {
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 0,
                                              sizeof(destmem_0.mem),
                                              &destmem_0.mem));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 1,
                                              sizeof(destoffset_1),
                                              &destoffset_1));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 2,
                                              sizeof(srcmem_2.mem),
                                              &srcmem_2.mem));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 3,
                                              sizeof(srcoffset_3),
                                              &srcoffset_3));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 4,
                                              sizeof(x_elems_5), &x_elems_5));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 5,
                                              sizeof(y_elems_6), &y_elems_6));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 6,
                                              sizeof(in_elems_7), &in_elems_7));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 7,
                                              sizeof(out_elems_8),
                                              &out_elems_8));
                OPENCL_SUCCEED(clSetKernelArg(fut_kernel_map_transpose_i32, 8,
                                              272 * sizeof(int32_t), NULL));
                if (1 * (x_elems_5 + srem32(16 - srem32(x_elems_5, 16), 16)) *
                    (y_elems_6 + srem32(16 - srem32(y_elems_6, 16), 16)) *
                    num_arrays_4 != 0) {
                    const size_t global_work_sizze_12594[3] = {x_elems_5 +
                                                               srem32(16 -
                                                                      srem32(x_elems_5,
                                                                             16),
                                                                      16),
                                                               y_elems_6 +
                                                               srem32(16 -
                                                                      srem32(y_elems_6,
                                                                             16),
                                                                      16),
                                                               num_arrays_4};
                    const size_t local_work_sizze_12598[3] = {16, 16, 1};
                    int64_t time_start_12595, time_end_12596;
                    
                    if (debugging) {
                        fprintf(stderr, "Launching %s with global work size [",
                                "fut_kernel_map_transpose_i32");
                        fprintf(stderr, "%zu", global_work_sizze_12594[0]);
                        fprintf(stderr, ", ");
                        fprintf(stderr, "%zu", global_work_sizze_12594[1]);
                        fprintf(stderr, ", ");
                        fprintf(stderr, "%zu", global_work_sizze_12594[2]);
                        fprintf(stderr, "].\n");
                        time_start_12595 = get_wall_time();
                    }
                    OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                          fut_kernel_map_transpose_i32,
                                                          3, NULL,
                                                          global_work_sizze_12594,
                                                          local_work_sizze_12598,
                                                          0, NULL, NULL));
                    if (debugging) {
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                        time_end_12596 = get_wall_time();
                        
                        long time_diff_12597 = time_end_12596 -
                             time_start_12595;
                        
                        if (detail_timing) {
                            fut_kernel_map_transpose_i32total_runtime +=
                                time_diff_12597;
                            fut_kernel_map_transpose_i32runs++;
                            fprintf(stderr, "kernel %s runtime: %ldus\n",
                                    "fut_kernel_map_transpose_i32",
                                    (int) time_diff_12597);
                        }
                    }
                }
            }
        }
    }
    
    struct tuple_ retval_12583;
    
    return retval_12583;
}
static
struct tuple_int32_t_device_mem_int32_t_int32_t_device_mem_int32_t_device_mem futhark_generatePolynomials(int32_t degree_7928,
                                                                                                          int32_t prime_7929)
{
    int32_t out_memsizze_11965;
    struct memblock_device out_mem_11964;
    
    out_mem_11964.references = NULL;
    
    int32_t out_arrsizze_11966;
    int32_t out_memsizze_11968;
    struct memblock_device out_mem_11967;
    
    out_mem_11967.references = NULL;
    
    int32_t out_memsizze_11970;
    struct memblock_device out_mem_11969;
    
    out_mem_11969.references = NULL;
    
    int32_t y_7930 = degree_7928 + 1;
    int32_t arg_7931 = pow32(prime_7929, y_7930);
    int32_t y_7938 = pow32(prime_7929, degree_7928);
    int32_t group_sizze_8979;
    
    group_sizze_8979 = cl_group_size;
    
    int32_t max_num_groups_8980;
    
    max_num_groups_8980 = cl_num_groups;
    
    int32_t y_8981 = group_sizze_8979 - 1;
    int32_t x_8982 = arg_7931 + y_8981;
    int32_t w_div_group_sizze_8983 = squot32(x_8982, group_sizze_8979);
    int32_t num_groups_maybe_zzero_8984 = smin32(w_div_group_sizze_8983,
                                                 max_num_groups_8980);
    int32_t num_groups_8985 = smax32(1, num_groups_maybe_zzero_8984);
    int32_t num_threads_8986 = num_groups_8985 * group_sizze_8979;
    int64_t binop_x_11613 = sext_i32_i64(arg_7931);
    int64_t bytes_11612 = binop_x_11613 * 4;
    struct memblock_device mem_11614;
    
    mem_11614.references = NULL;
    memblock_alloc_device(&mem_11614, bytes_11612);
    
    struct memblock_device mem_11617;
    
    mem_11617.references = NULL;
    memblock_alloc_device(&mem_11617, bytes_11612);
    
    int32_t y_9014 = num_threads_8986 - 1;
    int32_t x_9015 = arg_7931 + y_9014;
    int32_t num_iterations_9016 = squot32(x_9015, num_threads_8986);
    int32_t y_9019 = num_iterations_9016 * group_sizze_8979;
    int64_t binop_x_11622 = sext_i32_i64(num_groups_8985);
    int64_t bytes_11621 = binop_x_11622 * 4;
    struct memblock_device mem_11623;
    
    mem_11623.references = NULL;
    memblock_alloc_device(&mem_11623, bytes_11621);
    
    int64_t binop_y_11619 = sext_i32_i64(group_sizze_8979);
    int64_t bytes_11618 = 4 * binop_y_11619;
    struct memblock_local mem_11620;
    
    mem_11620.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 1, sizeof(arg_7931),
                                  &arg_7931));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 2, sizeof(y_7938),
                                  &y_7938));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 3,
                                  sizeof(num_iterations_9016),
                                  &num_iterations_9016));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 4, sizeof(y_9019),
                                  &y_9019));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 5, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 6, sizeof(mem_11617.mem),
                                  &mem_11617.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9011, 7, sizeof(mem_11623.mem),
                                  &mem_11623.mem));
    if (1 * (num_groups_8985 * group_sizze_8979) != 0) {
        const size_t global_work_sizze_12600[1] = {num_groups_8985 *
                     group_sizze_8979};
        const size_t local_work_sizze_12604[1] = {group_sizze_8979};
        int64_t time_start_12601, time_end_12602;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_9011");
            fprintf(stderr, "%zu", global_work_sizze_12600[0]);
            fprintf(stderr, "].\n");
            time_start_12601 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_9011,
                                              1, NULL, global_work_sizze_12600,
                                              local_work_sizze_12604, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12602 = get_wall_time();
            
            long time_diff_12603 = time_end_12602 - time_start_12601;
            
            if (detail_timing) {
                scan1_kernel_9011total_runtime += time_diff_12603;
                scan1_kernel_9011runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_9011", (int) time_diff_12603);
            }
        }
    }
    
    struct memblock_device mem_11629;
    
    mem_11629.references = NULL;
    memblock_alloc_device(&mem_11629, bytes_11621);
    
    int64_t bytes_11624 = 4 * binop_x_11622;
    struct memblock_local mem_11626;
    
    mem_11626.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9046, 0, bytes_11624, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9046, 1, sizeof(num_groups_8985),
                                  &num_groups_8985));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9046, 2, sizeof(mem_11623.mem),
                                  &mem_11623.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9046, 3, sizeof(mem_11629.mem),
                                  &mem_11629.mem));
    if (1 * num_groups_8985 != 0) {
        const size_t global_work_sizze_12605[1] = {num_groups_8985};
        const size_t local_work_sizze_12609[1] = {num_groups_8985};
        int64_t time_start_12606, time_end_12607;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_9046");
            fprintf(stderr, "%zu", global_work_sizze_12605[0]);
            fprintf(stderr, "].\n");
            time_start_12606 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_9046,
                                              1, NULL, global_work_sizze_12605,
                                              local_work_sizze_12609, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12607 = get_wall_time();
            
            long time_diff_12608 = time_end_12607 - time_start_12606;
            
            if (detail_timing) {
                scan2_kernel_9046total_runtime += time_diff_12608;
                scan2_kernel_9046runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_9046", (int) time_diff_12608);
            }
        }
    }
    
    int32_t num_threads_9071 = w_div_group_sizze_8983 * group_sizze_8979;
    struct memblock_device mem_11632;
    
    mem_11632.references = NULL;
    memblock_alloc_device(&mem_11632, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9072, 0, sizeof(arg_7931),
                                  &arg_7931));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9072, 1, sizeof(y_9019), &y_9019));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9072, 2, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9072, 3, sizeof(mem_11629.mem),
                                  &mem_11629.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9072, 4, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    if (1 * (w_div_group_sizze_8983 * group_sizze_8979) != 0) {
        const size_t global_work_sizze_12610[1] = {w_div_group_sizze_8983 *
                     group_sizze_8979};
        const size_t local_work_sizze_12614[1] = {group_sizze_8979};
        int64_t time_start_12611, time_end_12612;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9072");
            fprintf(stderr, "%zu", global_work_sizze_12610[0]);
            fprintf(stderr, "].\n");
            time_start_12611 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9072, 1,
                                              NULL, global_work_sizze_12610,
                                              local_work_sizze_12614, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12612 = get_wall_time();
            
            long time_diff_12613 = time_end_12612 - time_start_12611;
            
            if (detail_timing) {
                map_kernel_9072total_runtime += time_diff_12613;
                map_kernel_9072runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9072",
                        (int) time_diff_12613);
            }
        }
    }
    
    int32_t last_index_7950 = arg_7931 - 1;
    char is_empty_7951 = arg_7931 == 0;
    int32_t partition_sizze_7952;
    
    if (is_empty_7951) {
        partition_sizze_7952 = 0;
    } else {
        int32_t read_res_12615;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11632.mem, CL_TRUE,
                                           last_index_7950 * 4, sizeof(int32_t),
                                           &read_res_12615, 0, NULL, NULL));
        
        int32_t last_offset_7953 = read_res_12615;
        
        partition_sizze_7952 = last_offset_7953;
    }
    
    int64_t binop_x_11634 = sext_i32_i64(partition_sizze_7952);
    int64_t bytes_11633 = binop_x_11634 * 4;
    struct memblock_device mem_11635;
    
    mem_11635.references = NULL;
    memblock_alloc_device(&mem_11635, bytes_11633);
    
    struct memblock_device mem_11638;
    
    mem_11638.references = NULL;
    memblock_alloc_device(&mem_11638, bytes_11633);
    
    struct memblock_device mem_11641;
    
    mem_11641.references = NULL;
    memblock_alloc_device(&mem_11641, bytes_11633);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 0, sizeof(degree_7928),
                                  &degree_7928));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 1, sizeof(prime_7929),
                                  &prime_7929));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 2, sizeof(arg_7931),
                                  &arg_7931));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 3,
                                  sizeof(partition_sizze_7952),
                                  &partition_sizze_7952));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 4, sizeof(mem_11617.mem),
                                  &mem_11617.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 5, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 6, sizeof(mem_11635.mem),
                                  &mem_11635.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 7, sizeof(mem_11638.mem),
                                  &mem_11638.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9082, 8, sizeof(mem_11641.mem),
                                  &mem_11641.mem));
    if (1 * (w_div_group_sizze_8983 * group_sizze_8979) != 0) {
        const size_t global_work_sizze_12616[1] = {w_div_group_sizze_8983 *
                     group_sizze_8979};
        const size_t local_work_sizze_12620[1] = {group_sizze_8979};
        int64_t time_start_12617, time_end_12618;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9082");
            fprintf(stderr, "%zu", global_work_sizze_12616[0]);
            fprintf(stderr, "].\n");
            time_start_12617 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9082, 1,
                                              NULL, global_work_sizze_12616,
                                              local_work_sizze_12620, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12618 = get_wall_time();
            
            long time_diff_12619 = time_end_12618 - time_start_12617;
            
            if (detail_timing) {
                map_kernel_9082total_runtime += time_diff_12619;
                map_kernel_9082runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9082",
                        (int) time_diff_12619);
            }
        }
    }
    memblock_set_device(&out_mem_11964, &mem_11635);
    out_arrsizze_11966 = partition_sizze_7952;
    out_memsizze_11965 = bytes_11633;
    memblock_set_device(&out_mem_11967, &mem_11638);
    out_memsizze_11968 = bytes_11633;
    memblock_set_device(&out_mem_11969, &mem_11641);
    out_memsizze_11970 = bytes_11633;
    
    struct tuple_int32_t_device_mem_int32_t_int32_t_device_mem_int32_t_device_mem
    retval_12599;
    
    retval_12599.elem_0 = out_memsizze_11965;
    retval_12599.elem_1.references = NULL;
    memblock_set_device(&retval_12599.elem_1, &out_mem_11964);
    retval_12599.elem_2 = out_arrsizze_11966;
    retval_12599.elem_3 = out_memsizze_11968;
    retval_12599.elem_4.references = NULL;
    memblock_set_device(&retval_12599.elem_4, &out_mem_11967);
    retval_12599.elem_5 = out_memsizze_11970;
    retval_12599.elem_6.references = NULL;
    memblock_set_device(&retval_12599.elem_6, &out_mem_11969);
    memblock_unref_device(&out_mem_11964);
    memblock_unref_device(&out_mem_11967);
    memblock_unref_device(&out_mem_11969);
    memblock_unref_device(&mem_11614);
    memblock_unref_device(&mem_11617);
    memblock_unref_device(&mem_11623);
    memblock_unref_local(&mem_11620);
    memblock_unref_device(&mem_11629);
    memblock_unref_local(&mem_11626);
    memblock_unref_device(&mem_11632);
    memblock_unref_device(&mem_11635);
    memblock_unref_device(&mem_11638);
    memblock_unref_device(&mem_11641);
    return retval_12599;
}
static char futhark_isZZeroable(int64_t pol_mem_sizze_11612,
                                struct memblock_device pol_mem_11613,
                                int32_t sizze_7966, int32_t p_7968)
{
    char scalar_out_12000;
    int32_t y_7975 = sizze_7966 - 1;
    int32_t group_sizze_9101;
    
    group_sizze_9101 = cl_group_size;
    
    int32_t max_num_groups_9102;
    
    max_num_groups_9102 = cl_num_groups;
    
    int32_t y_9103 = group_sizze_9101 - 1;
    int32_t x_9104 = p_7968 + y_9103;
    int32_t w_div_group_sizze_9105 = squot32(x_9104, group_sizze_9101);
    int32_t num_groups_maybe_zzero_9106 = smin32(w_div_group_sizze_9105,
                                                 max_num_groups_9102);
    int32_t num_groups_9107 = smax32(1, num_groups_maybe_zzero_9106);
    int32_t num_threads_9108 = num_groups_9107 * group_sizze_9101;
    int32_t y_9109 = num_threads_9108 - 1;
    int32_t x_9110 = p_7968 + y_9109;
    int32_t per_thread_elements_9111 = squot32(x_9110, num_threads_9108);
    int64_t binop_x_11618 = sext_i32_i64(num_groups_9107);
    int64_t bytes_11617 = binop_x_11618 * 4;
    struct memblock_device mem_11619;
    
    mem_11619.references = NULL;
    memblock_alloc_device(&mem_11619, bytes_11617);
    
    int64_t binop_y_11615 = sext_i32_i64(group_sizze_9101);
    int64_t bytes_11614 = 4 * binop_y_11615;
    struct memblock_local mem_11616;
    
    mem_11616.references = NULL;
    if (debugging) {
        int binary_output = 0;
        int32_t x_12622 = p_7968;
        
        fprintf(stderr, "%s: ", "input size");
        write_scalar(stderr, binary_output, &i32, &x_12622);
        fprintf(stderr, "\n");
    }
    OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9116, 0, bytes_11614,
                                  NULL));
    OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9116, 1,
                                  sizeof(sizze_7966), &sizze_7966));
    OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9116, 2, sizeof(p_7968),
                                  &p_7968));
    OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9116, 3, sizeof(y_7975),
                                  &y_7975));
    OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9116, 4,
                                  sizeof(per_thread_elements_9111),
                                  &per_thread_elements_9111));
    OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9116, 5,
                                  sizeof(pol_mem_11613.mem),
                                  &pol_mem_11613.mem));
    OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9116, 6,
                                  sizeof(mem_11619.mem), &mem_11619.mem));
    if (1 * (num_groups_9107 * group_sizze_9101) != 0) {
        const size_t global_work_sizze_12623[1] = {num_groups_9107 *
                     group_sizze_9101};
        const size_t local_work_sizze_12627[1] = {group_sizze_9101};
        int64_t time_start_12624, time_end_12625;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "chunked_reduce_kernel_9116");
            fprintf(stderr, "%zu", global_work_sizze_12623[0]);
            fprintf(stderr, "].\n");
            time_start_12624 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                              chunked_reduce_kernel_9116, 1,
                                              NULL, global_work_sizze_12623,
                                              local_work_sizze_12627, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12625 = get_wall_time();
            
            long time_diff_12626 = time_end_12625 - time_start_12624;
            
            if (detail_timing) {
                chunked_reduce_kernel_9116total_runtime += time_diff_12626;
                chunked_reduce_kernel_9116runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "chunked_reduce_kernel_9116", (int) time_diff_12626);
            }
        }
    }
    
    struct memblock_device mem_11625;
    
    mem_11625.references = NULL;
    memblock_alloc_device(&mem_11625, 4);
    
    struct memblock_local mem_11622;
    
    mem_11622.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9169, 0, bytes_11614, NULL));
    OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9169, 1,
                                  sizeof(num_groups_9107), &num_groups_9107));
    OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9169, 2, sizeof(mem_11619.mem),
                                  &mem_11619.mem));
    OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9169, 3, sizeof(mem_11625.mem),
                                  &mem_11625.mem));
    if (1 * group_sizze_9101 != 0) {
        const size_t global_work_sizze_12628[1] = {group_sizze_9101};
        const size_t local_work_sizze_12632[1] = {group_sizze_9101};
        int64_t time_start_12629, time_end_12630;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "reduce_kernel_9169");
            fprintf(stderr, "%zu", global_work_sizze_12628[0]);
            fprintf(stderr, "].\n");
            time_start_12629 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, reduce_kernel_9169,
                                              1, NULL, global_work_sizze_12628,
                                              local_work_sizze_12632, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12630 = get_wall_time();
            
            long time_diff_12631 = time_end_12630 - time_start_12629;
            
            if (detail_timing) {
                reduce_kernel_9169total_runtime += time_diff_12631;
                reduce_kernel_9169runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "reduce_kernel_9169", (int) time_diff_12631);
            }
        }
    }
    
    int32_t read_res_12633;
    
    OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11625.mem, CL_TRUE, 0,
                                       sizeof(int32_t), &read_res_12633, 0,
                                       NULL, NULL));
    
    int32_t x_7977 = read_res_12633;
    char res_8003 = x_7977 == 0;
    
    scalar_out_12000 = res_8003;
    
    char retval_12621;
    
    retval_12621 = scalar_out_12000;
    memblock_unref_device(&mem_11619);
    memblock_unref_local(&mem_11616);
    memblock_unref_device(&mem_11625);
    memblock_unref_local(&mem_11622);
    return retval_12621;
}
static
struct tuple_int32_t_device_mem_int32_t futhark_translate(int32_t padding_8004,
                                                          int32_t d_8005,
                                                          int32_t p_8006,
                                                          int32_t x_8007)
{
    int32_t out_memsizze_12012;
    struct memblock_device out_mem_12011;
    
    out_mem_12011.references = NULL;
    
    int32_t out_arrsizze_12013;
    int32_t x_8014 = padding_8004 - d_8005;
    int32_t y_8015 = x_8014 - 1;
    int32_t group_sizze_9179;
    
    group_sizze_9179 = cl_group_size;
    
    int32_t y_9180 = group_sizze_9179 - 1;
    int32_t x_9181 = padding_8004 + y_9180;
    int32_t num_groups_9182 = squot32(x_9181, group_sizze_9179);
    int32_t num_threads_9183 = num_groups_9182 * group_sizze_9179;
    int64_t binop_x_11613 = sext_i32_i64(padding_8004);
    int64_t bytes_11612 = binop_x_11613 * 4;
    struct memblock_device mem_11614;
    
    mem_11614.references = NULL;
    memblock_alloc_device(&mem_11614, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9184, 0, sizeof(padding_8004),
                                  &padding_8004));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9184, 1, sizeof(p_8006), &p_8006));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9184, 2, sizeof(x_8007), &x_8007));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9184, 3, sizeof(y_8015), &y_8015));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9184, 4, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    if (1 * (num_groups_9182 * group_sizze_9179) != 0) {
        const size_t global_work_sizze_12635[1] = {num_groups_9182 *
                     group_sizze_9179};
        const size_t local_work_sizze_12639[1] = {group_sizze_9179};
        int64_t time_start_12636, time_end_12637;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9184");
            fprintf(stderr, "%zu", global_work_sizze_12635[0]);
            fprintf(stderr, "].\n");
            time_start_12636 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9184, 1,
                                              NULL, global_work_sizze_12635,
                                              local_work_sizze_12639, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12637 = get_wall_time();
            
            long time_diff_12638 = time_end_12637 - time_start_12636;
            
            if (detail_timing) {
                map_kernel_9184total_runtime += time_diff_12638;
                map_kernel_9184runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9184",
                        (int) time_diff_12638);
            }
        }
    }
    memblock_set_device(&out_mem_12011, &mem_11614);
    out_arrsizze_12013 = padding_8004;
    out_memsizze_12012 = bytes_11612;
    
    struct tuple_int32_t_device_mem_int32_t retval_12634;
    
    retval_12634.elem_0 = out_memsizze_12012;
    retval_12634.elem_1.references = NULL;
    memblock_set_device(&retval_12634.elem_1, &out_mem_12011);
    retval_12634.elem_2 = out_arrsizze_12013;
    memblock_unref_device(&out_mem_12011);
    memblock_unref_device(&mem_11614);
    return retval_12634;
}
static
struct tuple_int32_t_device_mem_int32_t_int32_t futhark_generatePolynomialsTest(int32_t degree_8025,
                                                                                int32_t prime_8026)
{
    int32_t out_memsizze_12018;
    struct memblock_device out_mem_12017;
    
    out_mem_12017.references = NULL;
    
    int32_t out_arrsizze_12019;
    int32_t out_arrsizze_12020;
    int32_t y_8027 = degree_8025 + 1;
    int32_t arg_8028 = pow32(prime_8026, y_8027);
    int32_t y_8035 = pow32(prime_8026, degree_8025);
    int32_t group_sizze_9195;
    
    group_sizze_9195 = cl_group_size;
    
    int32_t max_num_groups_9196;
    
    max_num_groups_9196 = cl_num_groups;
    
    int32_t y_9197 = group_sizze_9195 - 1;
    int32_t x_9198 = arg_8028 + y_9197;
    int32_t w_div_group_sizze_9199 = squot32(x_9198, group_sizze_9195);
    int32_t num_groups_maybe_zzero_9200 = smin32(w_div_group_sizze_9199,
                                                 max_num_groups_9196);
    int32_t num_groups_9201 = smax32(1, num_groups_maybe_zzero_9200);
    int32_t num_threads_9202 = num_groups_9201 * group_sizze_9195;
    int64_t binop_x_11613 = sext_i32_i64(arg_8028);
    int64_t bytes_11612 = binop_x_11613 * 4;
    struct memblock_device mem_11614;
    
    mem_11614.references = NULL;
    memblock_alloc_device(&mem_11614, bytes_11612);
    
    struct memblock_device mem_11617;
    
    mem_11617.references = NULL;
    memblock_alloc_device(&mem_11617, bytes_11612);
    
    int32_t y_9230 = num_threads_9202 - 1;
    int32_t x_9231 = arg_8028 + y_9230;
    int32_t num_iterations_9232 = squot32(x_9231, num_threads_9202);
    int32_t y_9235 = num_iterations_9232 * group_sizze_9195;
    int64_t binop_x_11622 = sext_i32_i64(num_groups_9201);
    int64_t bytes_11621 = binop_x_11622 * 4;
    struct memblock_device mem_11623;
    
    mem_11623.references = NULL;
    memblock_alloc_device(&mem_11623, bytes_11621);
    
    int64_t binop_y_11619 = sext_i32_i64(group_sizze_9195);
    int64_t bytes_11618 = 4 * binop_y_11619;
    struct memblock_local mem_11620;
    
    mem_11620.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 1, sizeof(arg_8028),
                                  &arg_8028));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 2, sizeof(y_8035),
                                  &y_8035));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 3,
                                  sizeof(num_iterations_9232),
                                  &num_iterations_9232));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 4, sizeof(y_9235),
                                  &y_9235));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 5, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 6, sizeof(mem_11617.mem),
                                  &mem_11617.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9227, 7, sizeof(mem_11623.mem),
                                  &mem_11623.mem));
    if (1 * (num_groups_9201 * group_sizze_9195) != 0) {
        const size_t global_work_sizze_12641[1] = {num_groups_9201 *
                     group_sizze_9195};
        const size_t local_work_sizze_12645[1] = {group_sizze_9195};
        int64_t time_start_12642, time_end_12643;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_9227");
            fprintf(stderr, "%zu", global_work_sizze_12641[0]);
            fprintf(stderr, "].\n");
            time_start_12642 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_9227,
                                              1, NULL, global_work_sizze_12641,
                                              local_work_sizze_12645, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12643 = get_wall_time();
            
            long time_diff_12644 = time_end_12643 - time_start_12642;
            
            if (detail_timing) {
                scan1_kernel_9227total_runtime += time_diff_12644;
                scan1_kernel_9227runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_9227", (int) time_diff_12644);
            }
        }
    }
    
    struct memblock_device mem_11629;
    
    mem_11629.references = NULL;
    memblock_alloc_device(&mem_11629, bytes_11621);
    
    int64_t bytes_11624 = 4 * binop_x_11622;
    struct memblock_local mem_11626;
    
    mem_11626.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9262, 0, bytes_11624, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9262, 1, sizeof(num_groups_9201),
                                  &num_groups_9201));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9262, 2, sizeof(mem_11623.mem),
                                  &mem_11623.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9262, 3, sizeof(mem_11629.mem),
                                  &mem_11629.mem));
    if (1 * num_groups_9201 != 0) {
        const size_t global_work_sizze_12646[1] = {num_groups_9201};
        const size_t local_work_sizze_12650[1] = {num_groups_9201};
        int64_t time_start_12647, time_end_12648;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_9262");
            fprintf(stderr, "%zu", global_work_sizze_12646[0]);
            fprintf(stderr, "].\n");
            time_start_12647 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_9262,
                                              1, NULL, global_work_sizze_12646,
                                              local_work_sizze_12650, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12648 = get_wall_time();
            
            long time_diff_12649 = time_end_12648 - time_start_12647;
            
            if (detail_timing) {
                scan2_kernel_9262total_runtime += time_diff_12649;
                scan2_kernel_9262runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_9262", (int) time_diff_12649);
            }
        }
    }
    
    int32_t num_threads_9287 = w_div_group_sizze_9199 * group_sizze_9195;
    struct memblock_device mem_11632;
    
    mem_11632.references = NULL;
    memblock_alloc_device(&mem_11632, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9288, 0, sizeof(arg_8028),
                                  &arg_8028));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9288, 1, sizeof(y_9235), &y_9235));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9288, 2, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9288, 3, sizeof(mem_11629.mem),
                                  &mem_11629.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9288, 4, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    if (1 * (w_div_group_sizze_9199 * group_sizze_9195) != 0) {
        const size_t global_work_sizze_12651[1] = {w_div_group_sizze_9199 *
                     group_sizze_9195};
        const size_t local_work_sizze_12655[1] = {group_sizze_9195};
        int64_t time_start_12652, time_end_12653;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9288");
            fprintf(stderr, "%zu", global_work_sizze_12651[0]);
            fprintf(stderr, "].\n");
            time_start_12652 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9288, 1,
                                              NULL, global_work_sizze_12651,
                                              local_work_sizze_12655, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12653 = get_wall_time();
            
            long time_diff_12654 = time_end_12653 - time_start_12652;
            
            if (detail_timing) {
                map_kernel_9288total_runtime += time_diff_12654;
                map_kernel_9288runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9288",
                        (int) time_diff_12654);
            }
        }
    }
    
    int32_t last_index_8047 = arg_8028 - 1;
    char is_empty_8048 = arg_8028 == 0;
    int32_t partition_sizze_8049;
    
    if (is_empty_8048) {
        partition_sizze_8049 = 0;
    } else {
        int32_t read_res_12656;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11632.mem, CL_TRUE,
                                           last_index_8047 * 4, sizeof(int32_t),
                                           &read_res_12656, 0, NULL, NULL));
        
        int32_t last_offset_8050 = read_res_12656;
        
        partition_sizze_8049 = last_offset_8050;
    }
    
    int64_t binop_x_11634 = sext_i32_i64(partition_sizze_8049);
    int64_t bytes_11633 = binop_x_11634 * 4;
    struct memblock_device mem_11635;
    
    mem_11635.references = NULL;
    memblock_alloc_device(&mem_11635, bytes_11633);
    
    struct memblock_device mem_11638;
    
    mem_11638.references = NULL;
    memblock_alloc_device(&mem_11638, bytes_11633);
    
    struct memblock_device mem_11641;
    
    mem_11641.references = NULL;
    memblock_alloc_device(&mem_11641, bytes_11633);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 0, sizeof(degree_8025),
                                  &degree_8025));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 1, sizeof(prime_8026),
                                  &prime_8026));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 2, sizeof(arg_8028),
                                  &arg_8028));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 3,
                                  sizeof(partition_sizze_8049),
                                  &partition_sizze_8049));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 4, sizeof(mem_11617.mem),
                                  &mem_11617.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 5, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 6, sizeof(mem_11635.mem),
                                  &mem_11635.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 7, sizeof(mem_11638.mem),
                                  &mem_11638.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9298, 8, sizeof(mem_11641.mem),
                                  &mem_11641.mem));
    if (1 * (w_div_group_sizze_9199 * group_sizze_9195) != 0) {
        const size_t global_work_sizze_12657[1] = {w_div_group_sizze_9199 *
                     group_sizze_9195};
        const size_t local_work_sizze_12661[1] = {group_sizze_9195};
        int64_t time_start_12658, time_end_12659;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9298");
            fprintf(stderr, "%zu", global_work_sizze_12657[0]);
            fprintf(stderr, "].\n");
            time_start_12658 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9298, 1,
                                              NULL, global_work_sizze_12657,
                                              local_work_sizze_12661, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12659 = get_wall_time();
            
            long time_diff_12660 = time_end_12659 - time_start_12658;
            
            if (detail_timing) {
                map_kernel_9298total_runtime += time_diff_12660;
                map_kernel_9298runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9298",
                        (int) time_diff_12660);
            }
        }
    }
    
    int32_t x_9305 = partition_sizze_8049 + y_9197;
    int32_t num_groups_9306 = squot32(x_9305, group_sizze_9195);
    int32_t num_threads_9307 = num_groups_9306 * group_sizze_9195;
    int64_t bytes_11645 = 12 * binop_x_11634;
    struct memblock_device mem_11649;
    
    mem_11649.references = NULL;
    memblock_alloc_device(&mem_11649, bytes_11645);
    
    int64_t num_threads64_11928 = sext_i32_i64(num_threads_9307);
    int64_t total_sizze_11929 = num_threads64_11928 * 12;
    struct memblock_device mem_11644;
    
    mem_11644.references = NULL;
    memblock_alloc_device(&mem_11644, total_sizze_11929);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9308, 0,
                                  sizeof(partition_sizze_8049),
                                  &partition_sizze_8049));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9308, 1, sizeof(mem_11635.mem),
                                  &mem_11635.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9308, 2, sizeof(mem_11638.mem),
                                  &mem_11638.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9308, 3, sizeof(mem_11641.mem),
                                  &mem_11641.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9308, 4, sizeof(mem_11644.mem),
                                  &mem_11644.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9308, 5, sizeof(mem_11649.mem),
                                  &mem_11649.mem));
    if (1 * (num_groups_9306 * group_sizze_9195) != 0) {
        const size_t global_work_sizze_12662[1] = {num_groups_9306 *
                     group_sizze_9195};
        const size_t local_work_sizze_12666[1] = {group_sizze_9195};
        int64_t time_start_12663, time_end_12664;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9308");
            fprintf(stderr, "%zu", global_work_sizze_12662[0]);
            fprintf(stderr, "].\n");
            time_start_12663 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9308, 1,
                                              NULL, global_work_sizze_12662,
                                              local_work_sizze_12666, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12664 = get_wall_time();
            
            long time_diff_12665 = time_end_12664 - time_start_12663;
            
            if (detail_timing) {
                map_kernel_9308total_runtime += time_diff_12665;
                map_kernel_9308runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9308",
                        (int) time_diff_12665);
            }
        }
    }
    
    int32_t convop_x_11651 = partition_sizze_8049 * 3;
    int64_t binop_x_11652 = sext_i32_i64(convop_x_11651);
    int64_t bytes_11650 = binop_x_11652 * 4;
    struct memblock_device mem_11653;
    
    mem_11653.references = NULL;
    memblock_alloc_device(&mem_11653, bytes_11650);
    
    struct tuple_ call_ret_12667;
    
    call_ret_12667 = futhark_map_transpose_opencl_i32(mem_11653, 0, mem_11649,
                                                      0, 1,
                                                      partition_sizze_8049, 3,
                                                      partition_sizze_8049 * 3,
                                                      partition_sizze_8049 * 3);
    memblock_set_device(&out_mem_12017, &mem_11653);
    out_arrsizze_12019 = partition_sizze_8049;
    out_arrsizze_12020 = 3;
    out_memsizze_12018 = bytes_11650;
    
    struct tuple_int32_t_device_mem_int32_t_int32_t retval_12640;
    
    retval_12640.elem_0 = out_memsizze_12018;
    retval_12640.elem_1.references = NULL;
    memblock_set_device(&retval_12640.elem_1, &out_mem_12017);
    retval_12640.elem_2 = out_arrsizze_12019;
    retval_12640.elem_3 = out_arrsizze_12020;
    memblock_unref_device(&out_mem_12017);
    memblock_unref_device(&mem_11614);
    memblock_unref_device(&mem_11617);
    memblock_unref_device(&mem_11623);
    memblock_unref_local(&mem_11620);
    memblock_unref_device(&mem_11629);
    memblock_unref_local(&mem_11626);
    memblock_unref_device(&mem_11632);
    memblock_unref_device(&mem_11635);
    memblock_unref_device(&mem_11638);
    memblock_unref_device(&mem_11641);
    memblock_unref_device(&mem_11649);
    memblock_unref_device(&mem_11644);
    memblock_unref_device(&mem_11653);
    return retval_12640;
}
static
struct tuple_int32_t_device_mem_int32_t futhark_translateTestTight(int32_t d_8068,
                                                                   int32_t p_8069,
                                                                   int32_t x_8070)
{
    int32_t out_memsizze_12055;
    struct memblock_device out_mem_12054;
    
    out_mem_12054.references = NULL;
    
    int32_t out_arrsizze_12056;
    int32_t arg_8071 = d_8068 + 1;
    int32_t group_sizze_9317;
    
    group_sizze_9317 = cl_group_size;
    
    int32_t y_9318 = group_sizze_9317 - 1;
    int32_t x_9319 = arg_8071 + y_9318;
    int32_t num_groups_9320 = squot32(x_9319, group_sizze_9317);
    int32_t num_threads_9321 = num_groups_9320 * group_sizze_9317;
    int64_t binop_x_11613 = sext_i32_i64(arg_8071);
    int64_t bytes_11612 = binop_x_11613 * 4;
    struct memblock_device mem_11614;
    
    mem_11614.references = NULL;
    memblock_alloc_device(&mem_11614, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9322, 0, sizeof(p_8069), &p_8069));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9322, 1, sizeof(x_8070), &x_8070));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9322, 2, sizeof(arg_8071),
                                  &arg_8071));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9322, 3, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    if (1 * (num_groups_9320 * group_sizze_9317) != 0) {
        const size_t global_work_sizze_12669[1] = {num_groups_9320 *
                     group_sizze_9317};
        const size_t local_work_sizze_12673[1] = {group_sizze_9317};
        int64_t time_start_12670, time_end_12671;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9322");
            fprintf(stderr, "%zu", global_work_sizze_12669[0]);
            fprintf(stderr, "].\n");
            time_start_12670 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9322, 1,
                                              NULL, global_work_sizze_12669,
                                              local_work_sizze_12673, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12671 = get_wall_time();
            
            long time_diff_12672 = time_end_12671 - time_start_12670;
            
            if (detail_timing) {
                map_kernel_9322total_runtime += time_diff_12672;
                map_kernel_9322runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9322",
                        (int) time_diff_12672);
            }
        }
    }
    memblock_set_device(&out_mem_12054, &mem_11614);
    out_arrsizze_12056 = arg_8071;
    out_memsizze_12055 = bytes_11612;
    
    struct tuple_int32_t_device_mem_int32_t retval_12668;
    
    retval_12668.elem_0 = out_memsizze_12055;
    retval_12668.elem_1.references = NULL;
    memblock_set_device(&retval_12668.elem_1, &out_mem_12054);
    retval_12668.elem_2 = out_arrsizze_12056;
    memblock_unref_device(&out_mem_12054);
    memblock_unref_device(&mem_11614);
    return retval_12668;
}
static
struct tuple_int32_t_device_mem_int32_t futhark_translateTestLoose(int32_t d_8087,
                                                                   int32_t p_8088,
                                                                   int32_t x_8089)
{
    int32_t out_memsizze_12061;
    struct memblock_device out_mem_12060;
    
    out_mem_12060.references = NULL;
    
    int32_t out_arrsizze_12062;
    int32_t arg_8090 = d_8087 * 2;
    int32_t y_8097 = d_8087 - 1;
    int32_t group_sizze_9335;
    
    group_sizze_9335 = cl_group_size;
    
    int32_t y_9336 = group_sizze_9335 - 1;
    int32_t x_9337 = arg_8090 + y_9336;
    int32_t num_groups_9338 = squot32(x_9337, group_sizze_9335);
    int32_t num_threads_9339 = num_groups_9338 * group_sizze_9335;
    int64_t binop_x_11613 = sext_i32_i64(arg_8090);
    int64_t bytes_11612 = binop_x_11613 * 4;
    struct memblock_device mem_11614;
    
    mem_11614.references = NULL;
    memblock_alloc_device(&mem_11614, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9340, 0, sizeof(p_8088), &p_8088));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9340, 1, sizeof(x_8089), &x_8089));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9340, 2, sizeof(arg_8090),
                                  &arg_8090));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9340, 3, sizeof(y_8097), &y_8097));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9340, 4, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    if (1 * (num_groups_9338 * group_sizze_9335) != 0) {
        const size_t global_work_sizze_12675[1] = {num_groups_9338 *
                     group_sizze_9335};
        const size_t local_work_sizze_12679[1] = {group_sizze_9335};
        int64_t time_start_12676, time_end_12677;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9340");
            fprintf(stderr, "%zu", global_work_sizze_12675[0]);
            fprintf(stderr, "].\n");
            time_start_12676 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9340, 1,
                                              NULL, global_work_sizze_12675,
                                              local_work_sizze_12679, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12677 = get_wall_time();
            
            long time_diff_12678 = time_end_12677 - time_start_12676;
            
            if (detail_timing) {
                map_kernel_9340total_runtime += time_diff_12678;
                map_kernel_9340runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9340",
                        (int) time_diff_12678);
            }
        }
    }
    memblock_set_device(&out_mem_12060, &mem_11614);
    out_arrsizze_12062 = arg_8090;
    out_memsizze_12061 = bytes_11612;
    
    struct tuple_int32_t_device_mem_int32_t retval_12674;
    
    retval_12674.elem_0 = out_memsizze_12061;
    retval_12674.elem_1.references = NULL;
    memblock_set_device(&retval_12674.elem_1, &out_mem_12060);
    retval_12674.elem_2 = out_arrsizze_12062;
    memblock_unref_device(&out_mem_12060);
    memblock_unref_device(&mem_11614);
    return retval_12674;
}
static char futhark_isIrreducible(int32_t bb_8107, int32_t bb_8108,
                                  int32_t bb_8109, int32_t aa_8110,
                                  int32_t aa_8111, int32_t aa_8112,
                                  int32_t prime_8113, int32_t padding_8114)
{
    char scalar_out_12066;
    int32_t x_8120 = padding_8114 - aa_8110;
    int32_t y_8121 = x_8120 - 1;
    int32_t x_8122 = padding_8114 - bb_8107;
    int32_t y_8123 = x_8122 - 1;
    int32_t group_sizze_9351;
    
    group_sizze_9351 = cl_group_size;
    
    int32_t max_num_groups_9352;
    
    max_num_groups_9352 = cl_num_groups;
    
    int32_t y_9353 = group_sizze_9351 - 1;
    int32_t x_9354 = padding_8114 + y_9353;
    int32_t w_div_group_sizze_9355 = squot32(x_9354, group_sizze_9351);
    int32_t num_groups_maybe_zzero_9356 = smin32(w_div_group_sizze_9355,
                                                 max_num_groups_9352);
    int32_t num_groups_9357 = smax32(1, num_groups_maybe_zzero_9356);
    int32_t num_threads_9358 = num_groups_9357 * group_sizze_9351;
    int64_t binop_x_11613 = sext_i32_i64(padding_8114);
    int64_t bytes_11612 = binop_x_11613 * 4;
    struct memblock_device mem_11614;
    
    mem_11614.references = NULL;
    memblock_alloc_device(&mem_11614, bytes_11612);
    
    struct memblock_device mem_11617;
    
    mem_11617.references = NULL;
    memblock_alloc_device(&mem_11617, bytes_11612);
    
    struct memblock_device mem_11620;
    
    mem_11620.references = NULL;
    memblock_alloc_device(&mem_11620, bytes_11612);
    
    int32_t y_9399 = num_threads_9358 - 1;
    int32_t x_9400 = padding_8114 + y_9399;
    int32_t num_iterations_9401 = squot32(x_9400, num_threads_9358);
    int32_t y_9404 = num_iterations_9401 * group_sizze_9351;
    int64_t binop_x_11625 = sext_i32_i64(num_groups_9357);
    int64_t bytes_11624 = binop_x_11625 * 4;
    struct memblock_device mem_11626;
    
    mem_11626.references = NULL;
    memblock_alloc_device(&mem_11626, bytes_11624);
    
    int64_t binop_y_11622 = sext_i32_i64(group_sizze_9351);
    int64_t bytes_11621 = 4 * binop_y_11622;
    struct memblock_local mem_11623;
    
    mem_11623.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 0, bytes_11621, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 1, sizeof(bb_8108),
                                  &bb_8108));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 2, sizeof(bb_8109),
                                  &bb_8109));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 3, sizeof(aa_8111),
                                  &aa_8111));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 4, sizeof(aa_8112),
                                  &aa_8112));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 5, sizeof(padding_8114),
                                  &padding_8114));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 6, sizeof(y_8121),
                                  &y_8121));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 7, sizeof(y_8123),
                                  &y_8123));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 8,
                                  sizeof(num_iterations_9401),
                                  &num_iterations_9401));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 9, sizeof(y_9404),
                                  &y_9404));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 10, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 11, sizeof(mem_11617.mem),
                                  &mem_11617.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 12, sizeof(mem_11620.mem),
                                  &mem_11620.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9396, 13, sizeof(mem_11626.mem),
                                  &mem_11626.mem));
    if (1 * (num_groups_9357 * group_sizze_9351) != 0) {
        const size_t global_work_sizze_12681[1] = {num_groups_9357 *
                     group_sizze_9351};
        const size_t local_work_sizze_12685[1] = {group_sizze_9351};
        int64_t time_start_12682, time_end_12683;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_9396");
            fprintf(stderr, "%zu", global_work_sizze_12681[0]);
            fprintf(stderr, "].\n");
            time_start_12682 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_9396,
                                              1, NULL, global_work_sizze_12681,
                                              local_work_sizze_12685, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12683 = get_wall_time();
            
            long time_diff_12684 = time_end_12683 - time_start_12682;
            
            if (detail_timing) {
                scan1_kernel_9396total_runtime += time_diff_12684;
                scan1_kernel_9396runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_9396", (int) time_diff_12684);
            }
        }
    }
    
    struct memblock_device mem_11632;
    
    mem_11632.references = NULL;
    memblock_alloc_device(&mem_11632, bytes_11624);
    
    int64_t bytes_11627 = 4 * binop_x_11625;
    struct memblock_local mem_11629;
    
    mem_11629.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9434, 0, bytes_11627, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9434, 1, sizeof(num_groups_9357),
                                  &num_groups_9357));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9434, 2, sizeof(mem_11626.mem),
                                  &mem_11626.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9434, 3, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    if (1 * num_groups_9357 != 0) {
        const size_t global_work_sizze_12686[1] = {num_groups_9357};
        const size_t local_work_sizze_12690[1] = {num_groups_9357};
        int64_t time_start_12687, time_end_12688;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_9434");
            fprintf(stderr, "%zu", global_work_sizze_12686[0]);
            fprintf(stderr, "].\n");
            time_start_12687 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_9434,
                                              1, NULL, global_work_sizze_12686,
                                              local_work_sizze_12690, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12688 = get_wall_time();
            
            long time_diff_12689 = time_end_12688 - time_start_12687;
            
            if (detail_timing) {
                scan2_kernel_9434total_runtime += time_diff_12689;
                scan2_kernel_9434runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_9434", (int) time_diff_12689);
            }
        }
    }
    
    int32_t num_threads_9459 = w_div_group_sizze_9355 * group_sizze_9351;
    struct memblock_device mem_11635;
    
    mem_11635.references = NULL;
    memblock_alloc_device(&mem_11635, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9460, 0, sizeof(padding_8114),
                                  &padding_8114));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9460, 1, sizeof(y_9404), &y_9404));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9460, 2, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9460, 3, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9460, 4, sizeof(mem_11635.mem),
                                  &mem_11635.mem));
    if (1 * (w_div_group_sizze_9355 * group_sizze_9351) != 0) {
        const size_t global_work_sizze_12691[1] = {w_div_group_sizze_9355 *
                     group_sizze_9351};
        const size_t local_work_sizze_12695[1] = {group_sizze_9351};
        int64_t time_start_12692, time_end_12693;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9460");
            fprintf(stderr, "%zu", global_work_sizze_12691[0]);
            fprintf(stderr, "].\n");
            time_start_12692 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9460, 1,
                                              NULL, global_work_sizze_12691,
                                              local_work_sizze_12695, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12693 = get_wall_time();
            
            long time_diff_12694 = time_end_12693 - time_start_12692;
            
            if (detail_timing) {
                map_kernel_9460total_runtime += time_diff_12694;
                map_kernel_9460runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9460",
                        (int) time_diff_12694);
            }
        }
    }
    
    struct memblock_device mem_11638;
    
    mem_11638.references = NULL;
    memblock_alloc_device(&mem_11638, bytes_11612);
    
    struct memblock_device mem_11641;
    
    mem_11641.references = NULL;
    memblock_alloc_device(&mem_11641, bytes_11612);
    
    struct memblock_device mem_11644;
    
    mem_11644.references = NULL;
    memblock_alloc_device(&mem_11644, bytes_11612);
    
    struct memblock_device mem_11647;
    
    mem_11647.references = NULL;
    memblock_alloc_device(&mem_11647, bytes_11612);
    
    struct memblock_device mem_11656;
    
    mem_11656.references = NULL;
    memblock_alloc_device(&mem_11656, bytes_11624);
    
    struct memblock_device mem_11659;
    
    mem_11659.references = NULL;
    memblock_alloc_device(&mem_11659, bytes_11624);
    
    struct memblock_local mem_11650;
    
    mem_11650.references = NULL;
    
    struct memblock_local mem_11653;
    
    mem_11653.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 0, bytes_11621, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 1, bytes_11621, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 2, sizeof(bb_8108),
                                  &bb_8108));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 3, sizeof(bb_8109),
                                  &bb_8109));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 4, sizeof(aa_8111),
                                  &aa_8111));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 5, sizeof(aa_8112),
                                  &aa_8112));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 6, sizeof(padding_8114),
                                  &padding_8114));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 7, sizeof(y_8121),
                                  &y_8121));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 8, sizeof(y_8123),
                                  &y_8123));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 9,
                                  sizeof(num_iterations_9401),
                                  &num_iterations_9401));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 10, sizeof(y_9404),
                                  &y_9404));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 11, sizeof(mem_11635.mem),
                                  &mem_11635.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 12, sizeof(mem_11638.mem),
                                  &mem_11638.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 13, sizeof(mem_11641.mem),
                                  &mem_11641.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 14, sizeof(mem_11644.mem),
                                  &mem_11644.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 15, sizeof(mem_11647.mem),
                                  &mem_11647.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 16, sizeof(mem_11656.mem),
                                  &mem_11656.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9520, 17, sizeof(mem_11659.mem),
                                  &mem_11659.mem));
    if (1 * (num_groups_9357 * group_sizze_9351) != 0) {
        const size_t global_work_sizze_12696[1] = {num_groups_9357 *
                     group_sizze_9351};
        const size_t local_work_sizze_12700[1] = {group_sizze_9351};
        int64_t time_start_12697, time_end_12698;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_9520");
            fprintf(stderr, "%zu", global_work_sizze_12696[0]);
            fprintf(stderr, "].\n");
            time_start_12697 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_9520,
                                              1, NULL, global_work_sizze_12696,
                                              local_work_sizze_12700, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12698 = get_wall_time();
            
            long time_diff_12699 = time_end_12698 - time_start_12697;
            
            if (detail_timing) {
                scan1_kernel_9520total_runtime += time_diff_12699;
                scan1_kernel_9520runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_9520", (int) time_diff_12699);
            }
        }
    }
    
    struct memblock_device mem_11668;
    
    mem_11668.references = NULL;
    memblock_alloc_device(&mem_11668, bytes_11624);
    
    struct memblock_device mem_11671;
    
    mem_11671.references = NULL;
    memblock_alloc_device(&mem_11671, bytes_11624);
    
    struct memblock_local mem_11662;
    
    mem_11662.references = NULL;
    
    struct memblock_local mem_11665;
    
    mem_11665.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9573, 0, bytes_11627, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9573, 1, bytes_11627, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9573, 2, sizeof(num_groups_9357),
                                  &num_groups_9357));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9573, 3, sizeof(mem_11656.mem),
                                  &mem_11656.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9573, 4, sizeof(mem_11659.mem),
                                  &mem_11659.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9573, 5, sizeof(mem_11668.mem),
                                  &mem_11668.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9573, 6, sizeof(mem_11671.mem),
                                  &mem_11671.mem));
    if (1 * num_groups_9357 != 0) {
        const size_t global_work_sizze_12701[1] = {num_groups_9357};
        const size_t local_work_sizze_12705[1] = {num_groups_9357};
        int64_t time_start_12702, time_end_12703;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_9573");
            fprintf(stderr, "%zu", global_work_sizze_12701[0]);
            fprintf(stderr, "].\n");
            time_start_12702 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_9573,
                                              1, NULL, global_work_sizze_12701,
                                              local_work_sizze_12705, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12703 = get_wall_time();
            
            long time_diff_12704 = time_end_12703 - time_start_12702;
            
            if (detail_timing) {
                scan2_kernel_9573total_runtime += time_diff_12704;
                scan2_kernel_9573runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_9573", (int) time_diff_12704);
            }
        }
    }
    
    struct memblock_device mem_11674;
    
    mem_11674.references = NULL;
    memblock_alloc_device(&mem_11674, bytes_11612);
    
    struct memblock_device mem_11677;
    
    mem_11677.references = NULL;
    memblock_alloc_device(&mem_11677, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 0, sizeof(padding_8114),
                                  &padding_8114));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 1, sizeof(y_9404), &y_9404));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 2, sizeof(mem_11638.mem),
                                  &mem_11638.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 3, sizeof(mem_11641.mem),
                                  &mem_11641.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 4, sizeof(mem_11668.mem),
                                  &mem_11668.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 5, sizeof(mem_11671.mem),
                                  &mem_11671.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 6, sizeof(mem_11674.mem),
                                  &mem_11674.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9609, 7, sizeof(mem_11677.mem),
                                  &mem_11677.mem));
    if (1 * (w_div_group_sizze_9355 * group_sizze_9351) != 0) {
        const size_t global_work_sizze_12706[1] = {w_div_group_sizze_9355 *
                     group_sizze_9351};
        const size_t local_work_sizze_12710[1] = {group_sizze_9351};
        int64_t time_start_12707, time_end_12708;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9609");
            fprintf(stderr, "%zu", global_work_sizze_12706[0]);
            fprintf(stderr, "].\n");
            time_start_12707 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9609, 1,
                                              NULL, global_work_sizze_12706,
                                              local_work_sizze_12710, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12708 = get_wall_time();
            
            long time_diff_12709 = time_end_12708 - time_start_12707;
            
            if (detail_timing) {
                map_kernel_9609total_runtime += time_diff_12709;
                map_kernel_9609runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9609",
                        (int) time_diff_12709);
            }
        }
    }
    
    int32_t last_index_8185 = padding_8114 - 1;
    char is_empty_8186 = padding_8114 == 0;
    int32_t partition_sizze_8187;
    
    if (is_empty_8186) {
        partition_sizze_8187 = 0;
    } else {
        int32_t read_res_12711;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11677.mem, CL_TRUE,
                                           last_index_8185 * 4, sizeof(int32_t),
                                           &read_res_12711, 0, NULL, NULL));
        
        int32_t last_offset_8188 = read_res_12711;
        
        partition_sizze_8187 = last_offset_8188;
    }
    
    char cond_8189 = bb_8108 == aa_8111;
    int32_t res_8190;
    
    if (cond_8189) {
        res_8190 = padding_8114;
    } else {
        res_8190 = 0;
    }
    
    struct memblock_device mem_11682;
    
    mem_11682.references = NULL;
    memblock_alloc_device(&mem_11682, bytes_11612);
    
    struct memblock_device mem_11688;
    
    mem_11688.references = NULL;
    memblock_alloc_device(&mem_11688, bytes_11624);
    
    struct memblock_device mem_11694;
    
    mem_11694.references = NULL;
    memblock_alloc_device(&mem_11694, bytes_11624);
    
    struct memblock_device mem_11697;
    
    mem_11697.references = NULL;
    memblock_alloc_device(&mem_11697, bytes_11612);
    
    struct memblock_device mem_11700;
    
    mem_11700.references = NULL;
    memblock_alloc_device(&mem_11700, bytes_11612);
    
    struct memblock_device mem_11706;
    
    mem_11706.references = NULL;
    memblock_alloc_device(&mem_11706, bytes_11624);
    
    struct memblock_device mem_11712;
    
    mem_11712.references = NULL;
    memblock_alloc_device(&mem_11712, bytes_11624);
    
    struct memblock_device mem_11715;
    
    mem_11715.references = NULL;
    memblock_alloc_device(&mem_11715, bytes_11612);
    
    struct memblock_device mem_11721;
    
    mem_11721.references = NULL;
    memblock_alloc_device(&mem_11721, bytes_11624);
    
    struct memblock_device mem_11727;
    
    mem_11727.references = NULL;
    memblock_alloc_device(&mem_11727, 4);
    
    struct memblock_device double_buffer_mem_11922;
    
    double_buffer_mem_11922.references = NULL;
    memblock_alloc_device(&double_buffer_mem_11922, bytes_11612);
    
    struct memblock_device mem_11730;
    
    mem_11730.references = NULL;
    memblock_alloc_device(&mem_11730, bytes_11612);
    
    struct memblock_local mem_11685;
    
    mem_11685.references = NULL;
    
    struct memblock_local mem_11691;
    
    mem_11691.references = NULL;
    
    struct memblock_local mem_11703;
    
    mem_11703.references = NULL;
    
    struct memblock_local mem_11709;
    
    mem_11709.references = NULL;
    
    struct memblock_local mem_11718;
    
    mem_11718.references = NULL;
    
    struct memblock_local mem_11724;
    
    mem_11724.references = NULL;
    
    struct memblock_device res_mem_11732;
    
    res_mem_11732.references = NULL;
    
    char res_8192;
    char nameless_8194;
    struct memblock_device b_mem_11679;
    
    b_mem_11679.references = NULL;
    memblock_set_device(&b_mem_11679, &mem_11644);
    nameless_8194 = 1;
    for (int32_t i_8195 = 0; i_8195 < res_8190; i_8195++) {
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9642, 0, bytes_11621, NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9642, 1,
                                      sizeof(padding_8114), &padding_8114));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9642, 2,
                                      sizeof(num_iterations_9401),
                                      &num_iterations_9401));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9642, 3, sizeof(y_9404),
                                      &y_9404));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9642, 4,
                                      sizeof(b_mem_11679.mem),
                                      &b_mem_11679.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9642, 5,
                                      sizeof(mem_11682.mem), &mem_11682.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9642, 6,
                                      sizeof(mem_11688.mem), &mem_11688.mem));
        if (1 * (num_groups_9357 * group_sizze_9351) != 0) {
            const size_t global_work_sizze_12712[1] = {num_groups_9357 *
                         group_sizze_9351};
            const size_t local_work_sizze_12716[1] = {group_sizze_9351};
            int64_t time_start_12713, time_end_12714;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan1_kernel_9642");
                fprintf(stderr, "%zu", global_work_sizze_12712[0]);
                fprintf(stderr, "].\n");
                time_start_12713 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan1_kernel_9642, 1, NULL,
                                                  global_work_sizze_12712,
                                                  local_work_sizze_12716, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12714 = get_wall_time();
                
                long time_diff_12715 = time_end_12714 - time_start_12713;
                
                if (detail_timing) {
                    scan1_kernel_9642total_runtime += time_diff_12715;
                    scan1_kernel_9642runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan1_kernel_9642", (int) time_diff_12715);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9674, 0, bytes_11627, NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9674, 1,
                                      sizeof(num_groups_9357),
                                      &num_groups_9357));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9674, 2,
                                      sizeof(mem_11688.mem), &mem_11688.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9674, 3,
                                      sizeof(mem_11694.mem), &mem_11694.mem));
        if (1 * num_groups_9357 != 0) {
            const size_t global_work_sizze_12717[1] = {num_groups_9357};
            const size_t local_work_sizze_12721[1] = {num_groups_9357};
            int64_t time_start_12718, time_end_12719;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan2_kernel_9674");
                fprintf(stderr, "%zu", global_work_sizze_12717[0]);
                fprintf(stderr, "].\n");
                time_start_12718 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan2_kernel_9674, 1, NULL,
                                                  global_work_sizze_12717,
                                                  local_work_sizze_12721, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12719 = get_wall_time();
                
                long time_diff_12720 = time_end_12719 - time_start_12718;
                
                if (detail_timing) {
                    scan2_kernel_9674total_runtime += time_diff_12720;
                    scan2_kernel_9674runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan2_kernel_9674", (int) time_diff_12720);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9700, 0, sizeof(padding_8114),
                                      &padding_8114));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9700, 1, sizeof(y_9404),
                                      &y_9404));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9700, 2, sizeof(mem_11682.mem),
                                      &mem_11682.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9700, 3, sizeof(mem_11694.mem),
                                      &mem_11694.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9700, 4, sizeof(mem_11697.mem),
                                      &mem_11697.mem));
        if (1 * (w_div_group_sizze_9355 * group_sizze_9351) != 0) {
            const size_t global_work_sizze_12722[1] = {w_div_group_sizze_9355 *
                         group_sizze_9351};
            const size_t local_work_sizze_12726[1] = {group_sizze_9351};
            int64_t time_start_12723, time_end_12724;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_9700");
                fprintf(stderr, "%zu", global_work_sizze_12722[0]);
                fprintf(stderr, "].\n");
                time_start_12723 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9700,
                                                  1, NULL,
                                                  global_work_sizze_12722,
                                                  local_work_sizze_12726, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12724 = get_wall_time();
                
                long time_diff_12725 = time_end_12724 - time_start_12723;
                
                if (detail_timing) {
                    map_kernel_9700total_runtime += time_diff_12725;
                    map_kernel_9700runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_9700", (int) time_diff_12725);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9732, 0, bytes_11621, NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9732, 1,
                                      sizeof(padding_8114), &padding_8114));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9732, 2,
                                      sizeof(num_iterations_9401),
                                      &num_iterations_9401));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9732, 3, sizeof(y_9404),
                                      &y_9404));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9732, 4,
                                      sizeof(mem_11697.mem), &mem_11697.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9732, 5,
                                      sizeof(mem_11700.mem), &mem_11700.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9732, 6,
                                      sizeof(mem_11706.mem), &mem_11706.mem));
        if (1 * (num_groups_9357 * group_sizze_9351) != 0) {
            const size_t global_work_sizze_12727[1] = {num_groups_9357 *
                         group_sizze_9351};
            const size_t local_work_sizze_12731[1] = {group_sizze_9351};
            int64_t time_start_12728, time_end_12729;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan1_kernel_9732");
                fprintf(stderr, "%zu", global_work_sizze_12727[0]);
                fprintf(stderr, "].\n");
                time_start_12728 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan1_kernel_9732, 1, NULL,
                                                  global_work_sizze_12727,
                                                  local_work_sizze_12731, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12729 = get_wall_time();
                
                long time_diff_12730 = time_end_12729 - time_start_12728;
                
                if (detail_timing) {
                    scan1_kernel_9732total_runtime += time_diff_12730;
                    scan1_kernel_9732runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan1_kernel_9732", (int) time_diff_12730);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9764, 0, bytes_11627, NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9764, 1,
                                      sizeof(num_groups_9357),
                                      &num_groups_9357));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9764, 2,
                                      sizeof(mem_11706.mem), &mem_11706.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9764, 3,
                                      sizeof(mem_11712.mem), &mem_11712.mem));
        if (1 * num_groups_9357 != 0) {
            const size_t global_work_sizze_12732[1] = {num_groups_9357};
            const size_t local_work_sizze_12736[1] = {num_groups_9357};
            int64_t time_start_12733, time_end_12734;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan2_kernel_9764");
                fprintf(stderr, "%zu", global_work_sizze_12732[0]);
                fprintf(stderr, "].\n");
                time_start_12733 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan2_kernel_9764, 1, NULL,
                                                  global_work_sizze_12732,
                                                  local_work_sizze_12736, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12734 = get_wall_time();
                
                long time_diff_12735 = time_end_12734 - time_start_12733;
                
                if (detail_timing) {
                    scan2_kernel_9764total_runtime += time_diff_12735;
                    scan2_kernel_9764runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan2_kernel_9764", (int) time_diff_12735);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9790, 0, sizeof(padding_8114),
                                      &padding_8114));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9790, 1, sizeof(y_9404),
                                      &y_9404));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9790, 2, sizeof(mem_11700.mem),
                                      &mem_11700.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9790, 3, sizeof(mem_11712.mem),
                                      &mem_11712.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9790, 4, sizeof(mem_11715.mem),
                                      &mem_11715.mem));
        if (1 * (w_div_group_sizze_9355 * group_sizze_9351) != 0) {
            const size_t global_work_sizze_12737[1] = {w_div_group_sizze_9355 *
                         group_sizze_9351};
            const size_t local_work_sizze_12741[1] = {group_sizze_9351};
            int64_t time_start_12738, time_end_12739;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_9790");
                fprintf(stderr, "%zu", global_work_sizze_12737[0]);
                fprintf(stderr, "].\n");
                time_start_12738 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9790,
                                                  1, NULL,
                                                  global_work_sizze_12737,
                                                  local_work_sizze_12741, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12739 = get_wall_time();
                
                long time_diff_12740 = time_end_12739 - time_start_12738;
                
                if (detail_timing) {
                    map_kernel_9790total_runtime += time_diff_12740;
                    map_kernel_9790runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_9790", (int) time_diff_12740);
                }
            }
        }
        
        int32_t partition_sizze_8209;
        
        if (is_empty_8186) {
            partition_sizze_8209 = 0;
        } else {
            int32_t read_res_12742;
            
            OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11715.mem,
                                               CL_TRUE, last_index_8185 * 4,
                                               sizeof(int32_t), &read_res_12742,
                                               0, NULL, NULL));
            
            int32_t last_offset_8210 = read_res_12742;
            
            partition_sizze_8209 = last_offset_8210;
        }
        
        char res_8211 = slt32(partition_sizze_8187, partition_sizze_8209);
        
        if (debugging) {
            int binary_output = 0;
            int32_t x_12743 = padding_8114;
            
            fprintf(stderr, "%s: ", "input size");
            write_scalar(stderr, binary_output, &i32, &x_12743);
            fprintf(stderr, "\n");
        }
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9818, 0,
                                      bytes_11621, NULL));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9818, 1,
                                      sizeof(padding_8114), &padding_8114));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9818, 2,
                                      sizeof(num_threads_9358),
                                      &num_threads_9358));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9818, 3,
                                      sizeof(num_iterations_9401),
                                      &num_iterations_9401));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9818, 4,
                                      sizeof(b_mem_11679.mem),
                                      &b_mem_11679.mem));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_9818, 5,
                                      sizeof(mem_11721.mem), &mem_11721.mem));
        if (1 * (num_groups_9357 * group_sizze_9351) != 0) {
            const size_t global_work_sizze_12744[1] = {num_groups_9357 *
                         group_sizze_9351};
            const size_t local_work_sizze_12748[1] = {group_sizze_9351};
            int64_t time_start_12745, time_end_12746;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "chunked_reduce_kernel_9818");
                fprintf(stderr, "%zu", global_work_sizze_12744[0]);
                fprintf(stderr, "].\n");
                time_start_12745 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  chunked_reduce_kernel_9818, 1,
                                                  NULL, global_work_sizze_12744,
                                                  local_work_sizze_12748, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12746 = get_wall_time();
                
                long time_diff_12747 = time_end_12746 - time_start_12745;
                
                if (detail_timing) {
                    chunked_reduce_kernel_9818total_runtime += time_diff_12747;
                    chunked_reduce_kernel_9818runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "chunked_reduce_kernel_9818",
                            (int) time_diff_12747);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9845, 0, bytes_11621,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9845, 1,
                                      sizeof(num_groups_9357),
                                      &num_groups_9357));
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9845, 2,
                                      sizeof(mem_11721.mem), &mem_11721.mem));
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_9845, 3,
                                      sizeof(mem_11727.mem), &mem_11727.mem));
        if (1 * group_sizze_9351 != 0) {
            const size_t global_work_sizze_12749[1] = {group_sizze_9351};
            const size_t local_work_sizze_12753[1] = {group_sizze_9351};
            int64_t time_start_12750, time_end_12751;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "reduce_kernel_9845");
                fprintf(stderr, "%zu", global_work_sizze_12749[0]);
                fprintf(stderr, "].\n");
                time_start_12750 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  reduce_kernel_9845, 1, NULL,
                                                  global_work_sizze_12749,
                                                  local_work_sizze_12753, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12751 = get_wall_time();
                
                long time_diff_12752 = time_end_12751 - time_start_12750;
                
                if (detail_timing) {
                    reduce_kernel_9845total_runtime += time_diff_12752;
                    reduce_kernel_9845runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "reduce_kernel_9845", (int) time_diff_12752);
                }
            }
        }
        
        int32_t read_res_12754;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11727.mem, CL_TRUE,
                                           0, sizeof(int32_t), &read_res_12754,
                                           0, NULL, NULL));
        
        int32_t x_8212 = read_res_12754;
        char res_8216 = x_8212 == 0;
        int32_t res_8217 = partition_sizze_8187 - partition_sizze_8209;
        int32_t res_8218;
        
        if (res_8216) {
            res_8218 = 0;
        } else {
            int32_t read_res_12755;
            
            OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, b_mem_11679.mem,
                                               CL_TRUE, partition_sizze_8209 *
                                               4, sizeof(int32_t),
                                               &read_res_12755, 0, NULL, NULL));
            
            int32_t res_8219 = read_res_12755;
            
            res_8218 = res_8219;
        }
        
        int32_t read_res_12756;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11647.mem, CL_TRUE,
                                           partition_sizze_8187 * 4,
                                           sizeof(int32_t), &read_res_12756, 0,
                                           NULL, NULL));
        
        int32_t y_8220 = read_res_12756;
        int32_t res_8221 = sdiv32(res_8218, y_8220);
        int32_t res_8222;
        
        if (res_8211) {
            res_8222 = 0;
        } else {
            res_8222 = res_8221;
        }
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9860, 0, sizeof(prime_8113),
                                      &prime_8113));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9860, 1, sizeof(padding_8114),
                                      &padding_8114));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9860, 2, sizeof(res_8217),
                                      &res_8217));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9860, 3, sizeof(res_8222),
                                      &res_8222));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9860, 4, sizeof(mem_11647.mem),
                                      &mem_11647.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9860, 5,
                                      sizeof(b_mem_11679.mem),
                                      &b_mem_11679.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_9860, 6, sizeof(mem_11730.mem),
                                      &mem_11730.mem));
        if (1 * (w_div_group_sizze_9355 * group_sizze_9351) != 0) {
            const size_t global_work_sizze_12757[1] = {w_div_group_sizze_9355 *
                         group_sizze_9351};
            const size_t local_work_sizze_12761[1] = {group_sizze_9351};
            int64_t time_start_12758, time_end_12759;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_9860");
                fprintf(stderr, "%zu", global_work_sizze_12757[0]);
                fprintf(stderr, "].\n");
                time_start_12758 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9860,
                                                  1, NULL,
                                                  global_work_sizze_12757,
                                                  local_work_sizze_12761, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12759 = get_wall_time();
                
                long time_diff_12760 = time_end_12759 - time_start_12758;
                
                if (detail_timing) {
                    map_kernel_9860total_runtime += time_diff_12760;
                    map_kernel_9860runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_9860", (int) time_diff_12760);
                }
            }
        }
        
        char res_8233 = !res_8216;
        char x_8234 = res_8211 && res_8233;
        
        if (padding_8114 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11730.mem,
                                               double_buffer_mem_11922.mem, 0,
                                               0, padding_8114 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        
        struct memblock_device b_mem_tmp_12129;
        
        b_mem_tmp_12129.references = NULL;
        memblock_set_device(&b_mem_tmp_12129, &double_buffer_mem_11922);
        
        char nameless_tmp_12131;
        
        nameless_tmp_12131 = x_8234;
        memblock_set_device(&b_mem_11679, &b_mem_tmp_12129);
        nameless_8194 = nameless_tmp_12131;
        memblock_unref_device(&b_mem_tmp_12129);
    }
    memblock_set_device(&res_mem_11732, &b_mem_11679);
    res_8192 = nameless_8194;
    scalar_out_12066 = res_8192;
    
    char retval_12680;
    
    retval_12680 = scalar_out_12066;
    memblock_unref_device(&mem_11614);
    memblock_unref_device(&mem_11617);
    memblock_unref_device(&mem_11620);
    memblock_unref_device(&mem_11626);
    memblock_unref_local(&mem_11623);
    memblock_unref_device(&mem_11632);
    memblock_unref_local(&mem_11629);
    memblock_unref_device(&mem_11635);
    memblock_unref_device(&mem_11638);
    memblock_unref_device(&mem_11641);
    memblock_unref_device(&mem_11644);
    memblock_unref_device(&mem_11647);
    memblock_unref_device(&mem_11656);
    memblock_unref_device(&mem_11659);
    memblock_unref_local(&mem_11650);
    memblock_unref_local(&mem_11653);
    memblock_unref_device(&mem_11668);
    memblock_unref_device(&mem_11671);
    memblock_unref_local(&mem_11662);
    memblock_unref_local(&mem_11665);
    memblock_unref_device(&mem_11674);
    memblock_unref_device(&mem_11677);
    memblock_unref_device(&mem_11682);
    memblock_unref_device(&mem_11688);
    memblock_unref_device(&mem_11694);
    memblock_unref_device(&mem_11697);
    memblock_unref_device(&mem_11700);
    memblock_unref_device(&mem_11706);
    memblock_unref_device(&mem_11712);
    memblock_unref_device(&mem_11715);
    memblock_unref_device(&mem_11721);
    memblock_unref_device(&mem_11727);
    memblock_unref_device(&double_buffer_mem_11922);
    memblock_unref_device(&mem_11730);
    memblock_unref_local(&mem_11685);
    memblock_unref_local(&mem_11691);
    memblock_unref_local(&mem_11703);
    memblock_unref_local(&mem_11709);
    memblock_unref_local(&mem_11718);
    memblock_unref_local(&mem_11724);
    memblock_unref_device(&res_mem_11732);
    memblock_unref_device(&b_mem_11679);
    return retval_12680;
}
static char futhark_isIrreducibleTest(int64_t b_mem_sizze_11612,
                                      int64_t a_mem_sizze_11614,
                                      struct memblock_device b_mem_11613,
                                      struct memblock_device a_mem_11615,
                                      int32_t sizze_8235, int32_t sizze_8236,
                                      int32_t prime_8239, int32_t padding_8240)
{
    char scalar_out_12193;
    char y_8241 = slt32(0, sizze_8235);
    char bounds_check_8242;
    
    if (!y_8241) {
        fprintf(stderr, "Assertion failed at %s: %s\n",
                "././isIrreducible.fut:47:20-47:20", "index out of bounds");
        exit(1);
    }
    
    int32_t read_res_12763;
    
    OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, b_mem_11613.mem, CL_TRUE,
                                       0, sizeof(int32_t), &read_res_12763, 0,
                                       NULL, NULL));
    
    int32_t arg_8243 = read_res_12763;
    char y_8244 = slt32(1, sizze_8235);
    char bounds_check_8245;
    
    if (!y_8244) {
        fprintf(stderr, "Assertion failed at %s: %s\n",
                "././isIrreducible.fut:47:26-47:26", "index out of bounds");
        exit(1);
    }
    
    int32_t read_res_12764;
    
    OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, b_mem_11613.mem, CL_TRUE,
                                       4, sizeof(int32_t), &read_res_12764, 0,
                                       NULL, NULL));
    
    int32_t arg_8246 = read_res_12764;
    char y_8247 = slt32(2, sizze_8235);
    char bounds_check_8248;
    
    if (!y_8247) {
        fprintf(stderr, "Assertion failed at %s: %s\n",
                "././isIrreducible.fut:47:32-47:32", "index out of bounds");
        exit(1);
    }
    
    int32_t read_res_12765;
    
    OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, b_mem_11613.mem, CL_TRUE,
                                       8, sizeof(int32_t), &read_res_12765, 0,
                                       NULL, NULL));
    
    int32_t arg_8249 = read_res_12765;
    char y_8250 = slt32(0, sizze_8236);
    char bounds_check_8251;
    
    if (!y_8250) {
        fprintf(stderr, "Assertion failed at %s: %s\n",
                "././isIrreducible.fut:47:39-47:39", "index out of bounds");
        exit(1);
    }
    
    int32_t read_res_12766;
    
    OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, a_mem_11615.mem, CL_TRUE,
                                       0, sizeof(int32_t), &read_res_12766, 0,
                                       NULL, NULL));
    
    int32_t arg_8252 = read_res_12766;
    char y_8253 = slt32(1, sizze_8236);
    char bounds_check_8254;
    
    if (!y_8253) {
        fprintf(stderr, "Assertion failed at %s: %s\n",
                "././isIrreducible.fut:47:45-47:45", "index out of bounds");
        exit(1);
    }
    
    int32_t read_res_12767;
    
    OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, a_mem_11615.mem, CL_TRUE,
                                       4, sizeof(int32_t), &read_res_12767, 0,
                                       NULL, NULL));
    
    int32_t arg_8255 = read_res_12767;
    char y_8256 = slt32(2, sizze_8236);
    char bounds_check_8257;
    
    if (!y_8256) {
        fprintf(stderr, "Assertion failed at %s: %s\n",
                "././isIrreducible.fut:47:51-47:51", "index out of bounds");
        exit(1);
    }
    
    int32_t read_res_12768;
    
    OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, a_mem_11615.mem, CL_TRUE,
                                       8, sizeof(int32_t), &read_res_12768, 0,
                                       NULL, NULL));
    
    int32_t arg_8258 = read_res_12768;
    int32_t x_8264 = padding_8240 - arg_8252;
    int32_t y_8265 = x_8264 - 1;
    int32_t x_8266 = padding_8240 - arg_8243;
    int32_t y_8267 = x_8266 - 1;
    int32_t group_sizze_9871;
    
    group_sizze_9871 = cl_group_size;
    
    int32_t max_num_groups_9872;
    
    max_num_groups_9872 = cl_num_groups;
    
    int32_t y_9873 = group_sizze_9871 - 1;
    int32_t x_9874 = padding_8240 + y_9873;
    int32_t w_div_group_sizze_9875 = squot32(x_9874, group_sizze_9871);
    int32_t num_groups_maybe_zzero_9876 = smin32(w_div_group_sizze_9875,
                                                 max_num_groups_9872);
    int32_t num_groups_9877 = smax32(1, num_groups_maybe_zzero_9876);
    int32_t num_threads_9878 = num_groups_9877 * group_sizze_9871;
    int64_t binop_x_11617 = sext_i32_i64(padding_8240);
    int64_t bytes_11616 = binop_x_11617 * 4;
    struct memblock_device mem_11618;
    
    mem_11618.references = NULL;
    memblock_alloc_device(&mem_11618, bytes_11616);
    
    struct memblock_device mem_11621;
    
    mem_11621.references = NULL;
    memblock_alloc_device(&mem_11621, bytes_11616);
    
    struct memblock_device mem_11624;
    
    mem_11624.references = NULL;
    memblock_alloc_device(&mem_11624, bytes_11616);
    
    int32_t y_9919 = num_threads_9878 - 1;
    int32_t x_9920 = padding_8240 + y_9919;
    int32_t num_iterations_9921 = squot32(x_9920, num_threads_9878);
    int32_t y_9924 = num_iterations_9921 * group_sizze_9871;
    int64_t binop_x_11629 = sext_i32_i64(num_groups_9877);
    int64_t bytes_11628 = binop_x_11629 * 4;
    struct memblock_device mem_11630;
    
    mem_11630.references = NULL;
    memblock_alloc_device(&mem_11630, bytes_11628);
    
    int64_t binop_y_11626 = sext_i32_i64(group_sizze_9871);
    int64_t bytes_11625 = 4 * binop_y_11626;
    struct memblock_local mem_11627;
    
    mem_11627.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 0, bytes_11625, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 1, sizeof(padding_8240),
                                  &padding_8240));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 2, sizeof(arg_8246),
                                  &arg_8246));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 3, sizeof(arg_8249),
                                  &arg_8249));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 4, sizeof(arg_8255),
                                  &arg_8255));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 5, sizeof(arg_8258),
                                  &arg_8258));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 6, sizeof(y_8265),
                                  &y_8265));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 7, sizeof(y_8267),
                                  &y_8267));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 8,
                                  sizeof(num_iterations_9921),
                                  &num_iterations_9921));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 9, sizeof(y_9924),
                                  &y_9924));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 10, sizeof(mem_11618.mem),
                                  &mem_11618.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 11, sizeof(mem_11621.mem),
                                  &mem_11621.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 12, sizeof(mem_11624.mem),
                                  &mem_11624.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_9916, 13, sizeof(mem_11630.mem),
                                  &mem_11630.mem));
    if (1 * (num_groups_9877 * group_sizze_9871) != 0) {
        const size_t global_work_sizze_12769[1] = {num_groups_9877 *
                     group_sizze_9871};
        const size_t local_work_sizze_12773[1] = {group_sizze_9871};
        int64_t time_start_12770, time_end_12771;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_9916");
            fprintf(stderr, "%zu", global_work_sizze_12769[0]);
            fprintf(stderr, "].\n");
            time_start_12770 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_9916,
                                              1, NULL, global_work_sizze_12769,
                                              local_work_sizze_12773, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12771 = get_wall_time();
            
            long time_diff_12772 = time_end_12771 - time_start_12770;
            
            if (detail_timing) {
                scan1_kernel_9916total_runtime += time_diff_12772;
                scan1_kernel_9916runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_9916", (int) time_diff_12772);
            }
        }
    }
    
    struct memblock_device mem_11636;
    
    mem_11636.references = NULL;
    memblock_alloc_device(&mem_11636, bytes_11628);
    
    int64_t bytes_11631 = 4 * binop_x_11629;
    struct memblock_local mem_11633;
    
    mem_11633.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9954, 0, bytes_11631, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9954, 1, sizeof(num_groups_9877),
                                  &num_groups_9877));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9954, 2, sizeof(mem_11630.mem),
                                  &mem_11630.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_9954, 3, sizeof(mem_11636.mem),
                                  &mem_11636.mem));
    if (1 * num_groups_9877 != 0) {
        const size_t global_work_sizze_12774[1] = {num_groups_9877};
        const size_t local_work_sizze_12778[1] = {num_groups_9877};
        int64_t time_start_12775, time_end_12776;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_9954");
            fprintf(stderr, "%zu", global_work_sizze_12774[0]);
            fprintf(stderr, "].\n");
            time_start_12775 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_9954,
                                              1, NULL, global_work_sizze_12774,
                                              local_work_sizze_12778, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12776 = get_wall_time();
            
            long time_diff_12777 = time_end_12776 - time_start_12775;
            
            if (detail_timing) {
                scan2_kernel_9954total_runtime += time_diff_12777;
                scan2_kernel_9954runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_9954", (int) time_diff_12777);
            }
        }
    }
    
    int32_t num_threads_9979 = w_div_group_sizze_9875 * group_sizze_9871;
    struct memblock_device mem_11639;
    
    mem_11639.references = NULL;
    memblock_alloc_device(&mem_11639, bytes_11616);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9980, 0, sizeof(padding_8240),
                                  &padding_8240));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9980, 1, sizeof(y_9924), &y_9924));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9980, 2, sizeof(mem_11618.mem),
                                  &mem_11618.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9980, 3, sizeof(mem_11636.mem),
                                  &mem_11636.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_9980, 4, sizeof(mem_11639.mem),
                                  &mem_11639.mem));
    if (1 * (w_div_group_sizze_9875 * group_sizze_9871) != 0) {
        const size_t global_work_sizze_12779[1] = {w_div_group_sizze_9875 *
                     group_sizze_9871};
        const size_t local_work_sizze_12783[1] = {group_sizze_9871};
        int64_t time_start_12780, time_end_12781;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_9980");
            fprintf(stderr, "%zu", global_work_sizze_12779[0]);
            fprintf(stderr, "].\n");
            time_start_12780 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_9980, 1,
                                              NULL, global_work_sizze_12779,
                                              local_work_sizze_12783, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12781 = get_wall_time();
            
            long time_diff_12782 = time_end_12781 - time_start_12780;
            
            if (detail_timing) {
                map_kernel_9980total_runtime += time_diff_12782;
                map_kernel_9980runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n", "map_kernel_9980",
                        (int) time_diff_12782);
            }
        }
    }
    
    struct memblock_device mem_11642;
    
    mem_11642.references = NULL;
    memblock_alloc_device(&mem_11642, bytes_11616);
    
    struct memblock_device mem_11645;
    
    mem_11645.references = NULL;
    memblock_alloc_device(&mem_11645, bytes_11616);
    
    struct memblock_device mem_11648;
    
    mem_11648.references = NULL;
    memblock_alloc_device(&mem_11648, bytes_11616);
    
    struct memblock_device mem_11651;
    
    mem_11651.references = NULL;
    memblock_alloc_device(&mem_11651, bytes_11616);
    
    struct memblock_device mem_11660;
    
    mem_11660.references = NULL;
    memblock_alloc_device(&mem_11660, bytes_11628);
    
    struct memblock_device mem_11663;
    
    mem_11663.references = NULL;
    memblock_alloc_device(&mem_11663, bytes_11628);
    
    struct memblock_local mem_11654;
    
    mem_11654.references = NULL;
    
    struct memblock_local mem_11657;
    
    mem_11657.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 0, bytes_11625, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 1, bytes_11625, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 2, sizeof(padding_8240),
                                  &padding_8240));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 3, sizeof(arg_8246),
                                  &arg_8246));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 4, sizeof(arg_8249),
                                  &arg_8249));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 5, sizeof(arg_8255),
                                  &arg_8255));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 6, sizeof(arg_8258),
                                  &arg_8258));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 7, sizeof(y_8265),
                                  &y_8265));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 8, sizeof(y_8267),
                                  &y_8267));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 9,
                                  sizeof(num_iterations_9921),
                                  &num_iterations_9921));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 10, sizeof(y_9924),
                                  &y_9924));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 11, sizeof(mem_11639.mem),
                                  &mem_11639.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 12, sizeof(mem_11642.mem),
                                  &mem_11642.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 13, sizeof(mem_11645.mem),
                                  &mem_11645.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 14, sizeof(mem_11648.mem),
                                  &mem_11648.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 15, sizeof(mem_11651.mem),
                                  &mem_11651.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 16, sizeof(mem_11660.mem),
                                  &mem_11660.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10040, 17, sizeof(mem_11663.mem),
                                  &mem_11663.mem));
    if (1 * (num_groups_9877 * group_sizze_9871) != 0) {
        const size_t global_work_sizze_12784[1] = {num_groups_9877 *
                     group_sizze_9871};
        const size_t local_work_sizze_12788[1] = {group_sizze_9871};
        int64_t time_start_12785, time_end_12786;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_10040");
            fprintf(stderr, "%zu", global_work_sizze_12784[0]);
            fprintf(stderr, "].\n");
            time_start_12785 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_10040,
                                              1, NULL, global_work_sizze_12784,
                                              local_work_sizze_12788, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12786 = get_wall_time();
            
            long time_diff_12787 = time_end_12786 - time_start_12785;
            
            if (detail_timing) {
                scan1_kernel_10040total_runtime += time_diff_12787;
                scan1_kernel_10040runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_10040", (int) time_diff_12787);
            }
        }
    }
    
    struct memblock_device mem_11672;
    
    mem_11672.references = NULL;
    memblock_alloc_device(&mem_11672, bytes_11628);
    
    struct memblock_device mem_11675;
    
    mem_11675.references = NULL;
    memblock_alloc_device(&mem_11675, bytes_11628);
    
    struct memblock_local mem_11666;
    
    mem_11666.references = NULL;
    
    struct memblock_local mem_11669;
    
    mem_11669.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10093, 0, bytes_11631, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10093, 1, bytes_11631, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10093, 2,
                                  sizeof(num_groups_9877), &num_groups_9877));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10093, 3, sizeof(mem_11660.mem),
                                  &mem_11660.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10093, 4, sizeof(mem_11663.mem),
                                  &mem_11663.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10093, 5, sizeof(mem_11672.mem),
                                  &mem_11672.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10093, 6, sizeof(mem_11675.mem),
                                  &mem_11675.mem));
    if (1 * num_groups_9877 != 0) {
        const size_t global_work_sizze_12789[1] = {num_groups_9877};
        const size_t local_work_sizze_12793[1] = {num_groups_9877};
        int64_t time_start_12790, time_end_12791;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_10093");
            fprintf(stderr, "%zu", global_work_sizze_12789[0]);
            fprintf(stderr, "].\n");
            time_start_12790 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_10093,
                                              1, NULL, global_work_sizze_12789,
                                              local_work_sizze_12793, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12791 = get_wall_time();
            
            long time_diff_12792 = time_end_12791 - time_start_12790;
            
            if (detail_timing) {
                scan2_kernel_10093total_runtime += time_diff_12792;
                scan2_kernel_10093runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_10093", (int) time_diff_12792);
            }
        }
    }
    
    struct memblock_device mem_11678;
    
    mem_11678.references = NULL;
    memblock_alloc_device(&mem_11678, bytes_11616);
    
    struct memblock_device mem_11681;
    
    mem_11681.references = NULL;
    memblock_alloc_device(&mem_11681, bytes_11616);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 0, sizeof(padding_8240),
                                  &padding_8240));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 1, sizeof(y_9924),
                                  &y_9924));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 2, sizeof(mem_11642.mem),
                                  &mem_11642.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 3, sizeof(mem_11645.mem),
                                  &mem_11645.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 4, sizeof(mem_11672.mem),
                                  &mem_11672.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 5, sizeof(mem_11675.mem),
                                  &mem_11675.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 6, sizeof(mem_11678.mem),
                                  &mem_11678.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10129, 7, sizeof(mem_11681.mem),
                                  &mem_11681.mem));
    if (1 * (w_div_group_sizze_9875 * group_sizze_9871) != 0) {
        const size_t global_work_sizze_12794[1] = {w_div_group_sizze_9875 *
                     group_sizze_9871};
        const size_t local_work_sizze_12798[1] = {group_sizze_9871};
        int64_t time_start_12795, time_end_12796;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10129");
            fprintf(stderr, "%zu", global_work_sizze_12794[0]);
            fprintf(stderr, "].\n");
            time_start_12795 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10129, 1,
                                              NULL, global_work_sizze_12794,
                                              local_work_sizze_12798, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12796 = get_wall_time();
            
            long time_diff_12797 = time_end_12796 - time_start_12795;
            
            if (detail_timing) {
                map_kernel_10129total_runtime += time_diff_12797;
                map_kernel_10129runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10129", (int) time_diff_12797);
            }
        }
    }
    
    int32_t last_index_8329 = padding_8240 - 1;
    char is_empty_8330 = padding_8240 == 0;
    int32_t partition_sizze_8331;
    
    if (is_empty_8330) {
        partition_sizze_8331 = 0;
    } else {
        int32_t read_res_12799;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11681.mem, CL_TRUE,
                                           last_index_8329 * 4, sizeof(int32_t),
                                           &read_res_12799, 0, NULL, NULL));
        
        int32_t last_offset_8332 = read_res_12799;
        
        partition_sizze_8331 = last_offset_8332;
    }
    
    char cond_8333 = arg_8246 == arg_8255;
    int32_t res_8334;
    
    if (cond_8333) {
        res_8334 = padding_8240;
    } else {
        res_8334 = 0;
    }
    
    struct memblock_device mem_11686;
    
    mem_11686.references = NULL;
    memblock_alloc_device(&mem_11686, bytes_11616);
    
    struct memblock_device mem_11692;
    
    mem_11692.references = NULL;
    memblock_alloc_device(&mem_11692, bytes_11628);
    
    struct memblock_device mem_11698;
    
    mem_11698.references = NULL;
    memblock_alloc_device(&mem_11698, bytes_11628);
    
    struct memblock_device mem_11701;
    
    mem_11701.references = NULL;
    memblock_alloc_device(&mem_11701, bytes_11616);
    
    struct memblock_device mem_11704;
    
    mem_11704.references = NULL;
    memblock_alloc_device(&mem_11704, bytes_11616);
    
    struct memblock_device mem_11710;
    
    mem_11710.references = NULL;
    memblock_alloc_device(&mem_11710, bytes_11628);
    
    struct memblock_device mem_11716;
    
    mem_11716.references = NULL;
    memblock_alloc_device(&mem_11716, bytes_11628);
    
    struct memblock_device mem_11719;
    
    mem_11719.references = NULL;
    memblock_alloc_device(&mem_11719, bytes_11616);
    
    struct memblock_device mem_11725;
    
    mem_11725.references = NULL;
    memblock_alloc_device(&mem_11725, bytes_11628);
    
    struct memblock_device mem_11731;
    
    mem_11731.references = NULL;
    memblock_alloc_device(&mem_11731, 4);
    
    struct memblock_device double_buffer_mem_11922;
    
    double_buffer_mem_11922.references = NULL;
    memblock_alloc_device(&double_buffer_mem_11922, bytes_11616);
    
    struct memblock_device mem_11734;
    
    mem_11734.references = NULL;
    memblock_alloc_device(&mem_11734, bytes_11616);
    
    struct memblock_local mem_11689;
    
    mem_11689.references = NULL;
    
    struct memblock_local mem_11695;
    
    mem_11695.references = NULL;
    
    struct memblock_local mem_11707;
    
    mem_11707.references = NULL;
    
    struct memblock_local mem_11713;
    
    mem_11713.references = NULL;
    
    struct memblock_local mem_11722;
    
    mem_11722.references = NULL;
    
    struct memblock_local mem_11728;
    
    mem_11728.references = NULL;
    
    struct memblock_device res_mem_11736;
    
    res_mem_11736.references = NULL;
    
    char res_8336;
    char nameless_8338;
    struct memblock_device b_mem_11683;
    
    b_mem_11683.references = NULL;
    memblock_set_device(&b_mem_11683, &mem_11648);
    nameless_8338 = 1;
    for (int32_t i_8339 = 0; i_8339 < res_8334; i_8339++) {
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10162, 0, bytes_11625,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10162, 1,
                                      sizeof(padding_8240), &padding_8240));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10162, 2,
                                      sizeof(num_iterations_9921),
                                      &num_iterations_9921));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10162, 3, sizeof(y_9924),
                                      &y_9924));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10162, 4,
                                      sizeof(b_mem_11683.mem),
                                      &b_mem_11683.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10162, 5,
                                      sizeof(mem_11686.mem), &mem_11686.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10162, 6,
                                      sizeof(mem_11692.mem), &mem_11692.mem));
        if (1 * (num_groups_9877 * group_sizze_9871) != 0) {
            const size_t global_work_sizze_12800[1] = {num_groups_9877 *
                         group_sizze_9871};
            const size_t local_work_sizze_12804[1] = {group_sizze_9871};
            int64_t time_start_12801, time_end_12802;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan1_kernel_10162");
                fprintf(stderr, "%zu", global_work_sizze_12800[0]);
                fprintf(stderr, "].\n");
                time_start_12801 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan1_kernel_10162, 1, NULL,
                                                  global_work_sizze_12800,
                                                  local_work_sizze_12804, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12802 = get_wall_time();
                
                long time_diff_12803 = time_end_12802 - time_start_12801;
                
                if (detail_timing) {
                    scan1_kernel_10162total_runtime += time_diff_12803;
                    scan1_kernel_10162runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan1_kernel_10162", (int) time_diff_12803);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10194, 0, bytes_11631,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10194, 1,
                                      sizeof(num_groups_9877),
                                      &num_groups_9877));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10194, 2,
                                      sizeof(mem_11692.mem), &mem_11692.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10194, 3,
                                      sizeof(mem_11698.mem), &mem_11698.mem));
        if (1 * num_groups_9877 != 0) {
            const size_t global_work_sizze_12805[1] = {num_groups_9877};
            const size_t local_work_sizze_12809[1] = {num_groups_9877};
            int64_t time_start_12806, time_end_12807;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan2_kernel_10194");
                fprintf(stderr, "%zu", global_work_sizze_12805[0]);
                fprintf(stderr, "].\n");
                time_start_12806 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan2_kernel_10194, 1, NULL,
                                                  global_work_sizze_12805,
                                                  local_work_sizze_12809, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12807 = get_wall_time();
                
                long time_diff_12808 = time_end_12807 - time_start_12806;
                
                if (detail_timing) {
                    scan2_kernel_10194total_runtime += time_diff_12808;
                    scan2_kernel_10194runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan2_kernel_10194", (int) time_diff_12808);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10220, 0, sizeof(padding_8240),
                                      &padding_8240));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10220, 1, sizeof(y_9924),
                                      &y_9924));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10220, 2,
                                      sizeof(mem_11686.mem), &mem_11686.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10220, 3,
                                      sizeof(mem_11698.mem), &mem_11698.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10220, 4,
                                      sizeof(mem_11701.mem), &mem_11701.mem));
        if (1 * (w_div_group_sizze_9875 * group_sizze_9871) != 0) {
            const size_t global_work_sizze_12810[1] = {w_div_group_sizze_9875 *
                         group_sizze_9871};
            const size_t local_work_sizze_12814[1] = {group_sizze_9871};
            int64_t time_start_12811, time_end_12812;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_10220");
                fprintf(stderr, "%zu", global_work_sizze_12810[0]);
                fprintf(stderr, "].\n");
                time_start_12811 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  map_kernel_10220, 1, NULL,
                                                  global_work_sizze_12810,
                                                  local_work_sizze_12814, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12812 = get_wall_time();
                
                long time_diff_12813 = time_end_12812 - time_start_12811;
                
                if (detail_timing) {
                    map_kernel_10220total_runtime += time_diff_12813;
                    map_kernel_10220runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_10220", (int) time_diff_12813);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10252, 0, bytes_11625,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10252, 1,
                                      sizeof(padding_8240), &padding_8240));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10252, 2,
                                      sizeof(num_iterations_9921),
                                      &num_iterations_9921));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10252, 3, sizeof(y_9924),
                                      &y_9924));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10252, 4,
                                      sizeof(mem_11701.mem), &mem_11701.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10252, 5,
                                      sizeof(mem_11704.mem), &mem_11704.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10252, 6,
                                      sizeof(mem_11710.mem), &mem_11710.mem));
        if (1 * (num_groups_9877 * group_sizze_9871) != 0) {
            const size_t global_work_sizze_12815[1] = {num_groups_9877 *
                         group_sizze_9871};
            const size_t local_work_sizze_12819[1] = {group_sizze_9871};
            int64_t time_start_12816, time_end_12817;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan1_kernel_10252");
                fprintf(stderr, "%zu", global_work_sizze_12815[0]);
                fprintf(stderr, "].\n");
                time_start_12816 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan1_kernel_10252, 1, NULL,
                                                  global_work_sizze_12815,
                                                  local_work_sizze_12819, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12817 = get_wall_time();
                
                long time_diff_12818 = time_end_12817 - time_start_12816;
                
                if (detail_timing) {
                    scan1_kernel_10252total_runtime += time_diff_12818;
                    scan1_kernel_10252runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan1_kernel_10252", (int) time_diff_12818);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10284, 0, bytes_11631,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10284, 1,
                                      sizeof(num_groups_9877),
                                      &num_groups_9877));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10284, 2,
                                      sizeof(mem_11710.mem), &mem_11710.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10284, 3,
                                      sizeof(mem_11716.mem), &mem_11716.mem));
        if (1 * num_groups_9877 != 0) {
            const size_t global_work_sizze_12820[1] = {num_groups_9877};
            const size_t local_work_sizze_12824[1] = {num_groups_9877};
            int64_t time_start_12821, time_end_12822;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan2_kernel_10284");
                fprintf(stderr, "%zu", global_work_sizze_12820[0]);
                fprintf(stderr, "].\n");
                time_start_12821 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan2_kernel_10284, 1, NULL,
                                                  global_work_sizze_12820,
                                                  local_work_sizze_12824, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12822 = get_wall_time();
                
                long time_diff_12823 = time_end_12822 - time_start_12821;
                
                if (detail_timing) {
                    scan2_kernel_10284total_runtime += time_diff_12823;
                    scan2_kernel_10284runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan2_kernel_10284", (int) time_diff_12823);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10310, 0, sizeof(padding_8240),
                                      &padding_8240));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10310, 1, sizeof(y_9924),
                                      &y_9924));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10310, 2,
                                      sizeof(mem_11704.mem), &mem_11704.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10310, 3,
                                      sizeof(mem_11716.mem), &mem_11716.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10310, 4,
                                      sizeof(mem_11719.mem), &mem_11719.mem));
        if (1 * (w_div_group_sizze_9875 * group_sizze_9871) != 0) {
            const size_t global_work_sizze_12825[1] = {w_div_group_sizze_9875 *
                         group_sizze_9871};
            const size_t local_work_sizze_12829[1] = {group_sizze_9871};
            int64_t time_start_12826, time_end_12827;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_10310");
                fprintf(stderr, "%zu", global_work_sizze_12825[0]);
                fprintf(stderr, "].\n");
                time_start_12826 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  map_kernel_10310, 1, NULL,
                                                  global_work_sizze_12825,
                                                  local_work_sizze_12829, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12827 = get_wall_time();
                
                long time_diff_12828 = time_end_12827 - time_start_12826;
                
                if (detail_timing) {
                    map_kernel_10310total_runtime += time_diff_12828;
                    map_kernel_10310runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_10310", (int) time_diff_12828);
                }
            }
        }
        
        int32_t partition_sizze_8353;
        
        if (is_empty_8330) {
            partition_sizze_8353 = 0;
        } else {
            int32_t read_res_12830;
            
            OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11719.mem,
                                               CL_TRUE, last_index_8329 * 4,
                                               sizeof(int32_t), &read_res_12830,
                                               0, NULL, NULL));
            
            int32_t last_offset_8354 = read_res_12830;
            
            partition_sizze_8353 = last_offset_8354;
        }
        
        char res_8355 = slt32(partition_sizze_8331, partition_sizze_8353);
        
        if (debugging) {
            int binary_output = 0;
            int32_t x_12831 = padding_8240;
            
            fprintf(stderr, "%s: ", "input size");
            write_scalar(stderr, binary_output, &i32, &x_12831);
            fprintf(stderr, "\n");
        }
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_10338, 0,
                                      bytes_11625, NULL));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_10338, 1,
                                      sizeof(padding_8240), &padding_8240));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_10338, 2,
                                      sizeof(num_threads_9878),
                                      &num_threads_9878));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_10338, 3,
                                      sizeof(num_iterations_9921),
                                      &num_iterations_9921));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_10338, 4,
                                      sizeof(b_mem_11683.mem),
                                      &b_mem_11683.mem));
        OPENCL_SUCCEED(clSetKernelArg(chunked_reduce_kernel_10338, 5,
                                      sizeof(mem_11725.mem), &mem_11725.mem));
        if (1 * (num_groups_9877 * group_sizze_9871) != 0) {
            const size_t global_work_sizze_12832[1] = {num_groups_9877 *
                         group_sizze_9871};
            const size_t local_work_sizze_12836[1] = {group_sizze_9871};
            int64_t time_start_12833, time_end_12834;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "chunked_reduce_kernel_10338");
                fprintf(stderr, "%zu", global_work_sizze_12832[0]);
                fprintf(stderr, "].\n");
                time_start_12833 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  chunked_reduce_kernel_10338,
                                                  1, NULL,
                                                  global_work_sizze_12832,
                                                  local_work_sizze_12836, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12834 = get_wall_time();
                
                long time_diff_12835 = time_end_12834 - time_start_12833;
                
                if (detail_timing) {
                    chunked_reduce_kernel_10338total_runtime += time_diff_12835;
                    chunked_reduce_kernel_10338runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "chunked_reduce_kernel_10338",
                            (int) time_diff_12835);
                }
            }
        }
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_10365, 0, bytes_11625,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_10365, 1,
                                      sizeof(num_groups_9877),
                                      &num_groups_9877));
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_10365, 2,
                                      sizeof(mem_11725.mem), &mem_11725.mem));
        OPENCL_SUCCEED(clSetKernelArg(reduce_kernel_10365, 3,
                                      sizeof(mem_11731.mem), &mem_11731.mem));
        if (1 * group_sizze_9871 != 0) {
            const size_t global_work_sizze_12837[1] = {group_sizze_9871};
            const size_t local_work_sizze_12841[1] = {group_sizze_9871};
            int64_t time_start_12838, time_end_12839;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "reduce_kernel_10365");
                fprintf(stderr, "%zu", global_work_sizze_12837[0]);
                fprintf(stderr, "].\n");
                time_start_12838 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  reduce_kernel_10365, 1, NULL,
                                                  global_work_sizze_12837,
                                                  local_work_sizze_12841, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12839 = get_wall_time();
                
                long time_diff_12840 = time_end_12839 - time_start_12838;
                
                if (detail_timing) {
                    reduce_kernel_10365total_runtime += time_diff_12840;
                    reduce_kernel_10365runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "reduce_kernel_10365", (int) time_diff_12840);
                }
            }
        }
        
        int32_t read_res_12842;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11731.mem, CL_TRUE,
                                           0, sizeof(int32_t), &read_res_12842,
                                           0, NULL, NULL));
        
        int32_t x_8356 = read_res_12842;
        char res_8360 = x_8356 == 0;
        int32_t res_8361 = partition_sizze_8331 - partition_sizze_8353;
        int32_t res_8362;
        
        if (res_8360) {
            res_8362 = 0;
        } else {
            int32_t read_res_12843;
            
            OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, b_mem_11683.mem,
                                               CL_TRUE, partition_sizze_8353 *
                                               4, sizeof(int32_t),
                                               &read_res_12843, 0, NULL, NULL));
            
            int32_t res_8363 = read_res_12843;
            
            res_8362 = res_8363;
        }
        
        int32_t read_res_12844;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11651.mem, CL_TRUE,
                                           partition_sizze_8331 * 4,
                                           sizeof(int32_t), &read_res_12844, 0,
                                           NULL, NULL));
        
        int32_t y_8364 = read_res_12844;
        int32_t res_8365 = sdiv32(res_8362, y_8364);
        int32_t res_8366;
        
        if (res_8355) {
            res_8366 = 0;
        } else {
            res_8366 = res_8365;
        }
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10380, 0, sizeof(prime_8239),
                                      &prime_8239));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10380, 1, sizeof(padding_8240),
                                      &padding_8240));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10380, 2, sizeof(res_8361),
                                      &res_8361));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10380, 3, sizeof(res_8366),
                                      &res_8366));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10380, 4,
                                      sizeof(mem_11651.mem), &mem_11651.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10380, 5,
                                      sizeof(b_mem_11683.mem),
                                      &b_mem_11683.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_10380, 6,
                                      sizeof(mem_11734.mem), &mem_11734.mem));
        if (1 * (w_div_group_sizze_9875 * group_sizze_9871) != 0) {
            const size_t global_work_sizze_12845[1] = {w_div_group_sizze_9875 *
                         group_sizze_9871};
            const size_t local_work_sizze_12849[1] = {group_sizze_9871};
            int64_t time_start_12846, time_end_12847;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_10380");
                fprintf(stderr, "%zu", global_work_sizze_12845[0]);
                fprintf(stderr, "].\n");
                time_start_12846 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  map_kernel_10380, 1, NULL,
                                                  global_work_sizze_12845,
                                                  local_work_sizze_12849, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12847 = get_wall_time();
                
                long time_diff_12848 = time_end_12847 - time_start_12846;
                
                if (detail_timing) {
                    map_kernel_10380total_runtime += time_diff_12848;
                    map_kernel_10380runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_10380", (int) time_diff_12848);
                }
            }
        }
        
        char res_8377 = !res_8360;
        char x_8378 = res_8355 && res_8377;
        
        if (padding_8240 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11734.mem,
                                               double_buffer_mem_11922.mem, 0,
                                               0, padding_8240 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        
        struct memblock_device b_mem_tmp_12256;
        
        b_mem_tmp_12256.references = NULL;
        memblock_set_device(&b_mem_tmp_12256, &double_buffer_mem_11922);
        
        char nameless_tmp_12258;
        
        nameless_tmp_12258 = x_8378;
        memblock_set_device(&b_mem_11683, &b_mem_tmp_12256);
        nameless_8338 = nameless_tmp_12258;
        memblock_unref_device(&b_mem_tmp_12256);
    }
    memblock_set_device(&res_mem_11736, &b_mem_11683);
    res_8336 = nameless_8338;
    scalar_out_12193 = res_8336;
    
    char retval_12762;
    
    retval_12762 = scalar_out_12193;
    memblock_unref_device(&mem_11618);
    memblock_unref_device(&mem_11621);
    memblock_unref_device(&mem_11624);
    memblock_unref_device(&mem_11630);
    memblock_unref_local(&mem_11627);
    memblock_unref_device(&mem_11636);
    memblock_unref_local(&mem_11633);
    memblock_unref_device(&mem_11639);
    memblock_unref_device(&mem_11642);
    memblock_unref_device(&mem_11645);
    memblock_unref_device(&mem_11648);
    memblock_unref_device(&mem_11651);
    memblock_unref_device(&mem_11660);
    memblock_unref_device(&mem_11663);
    memblock_unref_local(&mem_11654);
    memblock_unref_local(&mem_11657);
    memblock_unref_device(&mem_11672);
    memblock_unref_device(&mem_11675);
    memblock_unref_local(&mem_11666);
    memblock_unref_local(&mem_11669);
    memblock_unref_device(&mem_11678);
    memblock_unref_device(&mem_11681);
    memblock_unref_device(&mem_11686);
    memblock_unref_device(&mem_11692);
    memblock_unref_device(&mem_11698);
    memblock_unref_device(&mem_11701);
    memblock_unref_device(&mem_11704);
    memblock_unref_device(&mem_11710);
    memblock_unref_device(&mem_11716);
    memblock_unref_device(&mem_11719);
    memblock_unref_device(&mem_11725);
    memblock_unref_device(&mem_11731);
    memblock_unref_device(&double_buffer_mem_11922);
    memblock_unref_device(&mem_11734);
    memblock_unref_local(&mem_11689);
    memblock_unref_local(&mem_11695);
    memblock_unref_local(&mem_11707);
    memblock_unref_local(&mem_11713);
    memblock_unref_local(&mem_11722);
    memblock_unref_local(&mem_11728);
    memblock_unref_device(&res_mem_11736);
    memblock_unref_device(&b_mem_11683);
    return retval_12762;
}
static
struct tuple_int32_t_device_mem_int32_t_int32_t futhark_main(int32_t prime_8379,
                                                             int32_t maxDegree_8380)
{
    int32_t out_memsizze_12321;
    struct memblock_device out_mem_12320;
    
    out_mem_12320.references = NULL;
    
    int32_t out_arrsizze_12322;
    int32_t out_arrsizze_12323;
    int32_t group_sizze_10391;
    
    group_sizze_10391 = cl_group_size;
    
    int32_t max_num_groups_10392;
    
    max_num_groups_10392 = cl_num_groups;
    
    int32_t y_10393 = group_sizze_10391 - 1;
    int32_t x_10394 = prime_8379 + y_10393;
    int32_t w_div_group_sizze_10395 = squot32(x_10394, group_sizze_10391);
    int32_t num_groups_maybe_zzero_10396 = smin32(w_div_group_sizze_10395,
                                                  max_num_groups_10392);
    int32_t num_groups_10397 = smax32(1, num_groups_maybe_zzero_10396);
    int32_t num_threads_10398 = num_groups_10397 * group_sizze_10391;
    int64_t binop_x_11613 = sext_i32_i64(prime_8379);
    int64_t bytes_11612 = binop_x_11613 * 4;
    struct memblock_device mem_11614;
    
    mem_11614.references = NULL;
    memblock_alloc_device(&mem_11614, bytes_11612);
    
    struct memblock_device mem_11617;
    
    mem_11617.references = NULL;
    memblock_alloc_device(&mem_11617, bytes_11612);
    
    int32_t y_10426 = num_threads_10398 - 1;
    int32_t x_10427 = prime_8379 + y_10426;
    int32_t num_iterations_10428 = squot32(x_10427, num_threads_10398);
    int32_t y_10431 = num_iterations_10428 * group_sizze_10391;
    int64_t binop_x_11622 = sext_i32_i64(num_groups_10397);
    int64_t bytes_11621 = binop_x_11622 * 4;
    struct memblock_device mem_11623;
    
    mem_11623.references = NULL;
    memblock_alloc_device(&mem_11623, bytes_11621);
    
    int64_t binop_y_11619 = sext_i32_i64(group_sizze_10391);
    int64_t bytes_11618 = 4 * binop_y_11619;
    struct memblock_local mem_11620;
    
    mem_11620.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10423, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10423, 1, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10423, 2,
                                  sizeof(num_iterations_10428),
                                  &num_iterations_10428));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10423, 3, sizeof(y_10431),
                                  &y_10431));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10423, 4, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10423, 5, sizeof(mem_11617.mem),
                                  &mem_11617.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10423, 6, sizeof(mem_11623.mem),
                                  &mem_11623.mem));
    if (1 * (num_groups_10397 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12851[1] = {num_groups_10397 *
                     group_sizze_10391};
        const size_t local_work_sizze_12855[1] = {group_sizze_10391};
        int64_t time_start_12852, time_end_12853;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_10423");
            fprintf(stderr, "%zu", global_work_sizze_12851[0]);
            fprintf(stderr, "].\n");
            time_start_12852 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_10423,
                                              1, NULL, global_work_sizze_12851,
                                              local_work_sizze_12855, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12853 = get_wall_time();
            
            long time_diff_12854 = time_end_12853 - time_start_12852;
            
            if (detail_timing) {
                scan1_kernel_10423total_runtime += time_diff_12854;
                scan1_kernel_10423runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_10423", (int) time_diff_12854);
            }
        }
    }
    
    struct memblock_device mem_11629;
    
    mem_11629.references = NULL;
    memblock_alloc_device(&mem_11629, bytes_11621);
    
    int64_t bytes_11624 = 4 * binop_x_11622;
    struct memblock_local mem_11626;
    
    mem_11626.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10458, 0, bytes_11624, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10458, 1,
                                  sizeof(num_groups_10397), &num_groups_10397));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10458, 2, sizeof(mem_11623.mem),
                                  &mem_11623.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10458, 3, sizeof(mem_11629.mem),
                                  &mem_11629.mem));
    if (1 * num_groups_10397 != 0) {
        const size_t global_work_sizze_12856[1] = {num_groups_10397};
        const size_t local_work_sizze_12860[1] = {num_groups_10397};
        int64_t time_start_12857, time_end_12858;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_10458");
            fprintf(stderr, "%zu", global_work_sizze_12856[0]);
            fprintf(stderr, "].\n");
            time_start_12857 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_10458,
                                              1, NULL, global_work_sizze_12856,
                                              local_work_sizze_12860, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12858 = get_wall_time();
            
            long time_diff_12859 = time_end_12858 - time_start_12857;
            
            if (detail_timing) {
                scan2_kernel_10458total_runtime += time_diff_12859;
                scan2_kernel_10458runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_10458", (int) time_diff_12859);
            }
        }
    }
    
    int32_t num_threads_10483 = w_div_group_sizze_10395 * group_sizze_10391;
    struct memblock_device mem_11632;
    
    mem_11632.references = NULL;
    memblock_alloc_device(&mem_11632, bytes_11612);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10484, 0, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10484, 1, sizeof(y_10431),
                                  &y_10431));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10484, 2, sizeof(mem_11614.mem),
                                  &mem_11614.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10484, 3, sizeof(mem_11629.mem),
                                  &mem_11629.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10484, 4, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    if (1 * (w_div_group_sizze_10395 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12861[1] = {w_div_group_sizze_10395 *
                     group_sizze_10391};
        const size_t local_work_sizze_12865[1] = {group_sizze_10391};
        int64_t time_start_12862, time_end_12863;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10484");
            fprintf(stderr, "%zu", global_work_sizze_12861[0]);
            fprintf(stderr, "].\n");
            time_start_12862 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10484, 1,
                                              NULL, global_work_sizze_12861,
                                              local_work_sizze_12865, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12863 = get_wall_time();
            
            long time_diff_12864 = time_end_12863 - time_start_12862;
            
            if (detail_timing) {
                map_kernel_10484total_runtime += time_diff_12864;
                map_kernel_10484runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10484", (int) time_diff_12864);
            }
        }
    }
    
    int32_t last_index_8398 = prime_8379 - 1;
    char is_empty_8399 = prime_8379 == 0;
    int32_t partition_sizze_8400;
    
    if (is_empty_8399) {
        partition_sizze_8400 = 0;
    } else {
        int32_t read_res_12866;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11632.mem, CL_TRUE,
                                           last_index_8398 * 4, sizeof(int32_t),
                                           &read_res_12866, 0, NULL, NULL));
        
        int32_t last_offset_8401 = read_res_12866;
        
        partition_sizze_8400 = last_offset_8401;
    }
    
    int64_t binop_x_11634 = sext_i32_i64(partition_sizze_8400);
    int64_t bytes_11633 = binop_x_11634 * 4;
    struct memblock_device mem_11635;
    
    mem_11635.references = NULL;
    memblock_alloc_device(&mem_11635, bytes_11633);
    
    struct memblock_device mem_11638;
    
    mem_11638.references = NULL;
    memblock_alloc_device(&mem_11638, bytes_11633);
    
    struct memblock_device mem_11641;
    
    mem_11641.references = NULL;
    memblock_alloc_device(&mem_11641, bytes_11633);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10494, 0, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10494, 1,
                                  sizeof(partition_sizze_8400),
                                  &partition_sizze_8400));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10494, 2, sizeof(mem_11617.mem),
                                  &mem_11617.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10494, 3, sizeof(mem_11632.mem),
                                  &mem_11632.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10494, 4, sizeof(mem_11635.mem),
                                  &mem_11635.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10494, 5, sizeof(mem_11638.mem),
                                  &mem_11638.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10494, 6, sizeof(mem_11641.mem),
                                  &mem_11641.mem));
    if (1 * (w_div_group_sizze_10395 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12867[1] = {w_div_group_sizze_10395 *
                     group_sizze_10391};
        const size_t local_work_sizze_12871[1] = {group_sizze_10391};
        int64_t time_start_12868, time_end_12869;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10494");
            fprintf(stderr, "%zu", global_work_sizze_12867[0]);
            fprintf(stderr, "].\n");
            time_start_12868 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10494, 1,
                                              NULL, global_work_sizze_12867,
                                              local_work_sizze_12871, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12869 = get_wall_time();
            
            long time_diff_12870 = time_end_12869 - time_start_12868;
            
            if (detail_timing) {
                map_kernel_10494total_runtime += time_diff_12870;
                map_kernel_10494runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10494", (int) time_diff_12870);
            }
        }
    }
    
    int32_t arg_8414 = pow32(prime_8379, 2);
    int32_t x_10500 = arg_8414 + y_10393;
    int32_t w_div_group_sizze_10501 = squot32(x_10500, group_sizze_10391);
    int32_t num_groups_maybe_zzero_10502 = smin32(w_div_group_sizze_10501,
                                                  max_num_groups_10392);
    int32_t num_groups_10503 = smax32(1, num_groups_maybe_zzero_10502);
    int32_t num_threads_10504 = num_groups_10503 * group_sizze_10391;
    int64_t binop_x_11643 = sext_i32_i64(arg_8414);
    int64_t bytes_11642 = binop_x_11643 * 4;
    struct memblock_device mem_11644;
    
    mem_11644.references = NULL;
    memblock_alloc_device(&mem_11644, bytes_11642);
    
    struct memblock_device mem_11647;
    
    mem_11647.references = NULL;
    memblock_alloc_device(&mem_11647, bytes_11642);
    
    int32_t y_10532 = num_threads_10504 - 1;
    int32_t x_10533 = arg_8414 + y_10532;
    int32_t num_iterations_10534 = squot32(x_10533, num_threads_10504);
    int32_t y_10537 = num_iterations_10534 * group_sizze_10391;
    int64_t binop_x_11652 = sext_i32_i64(num_groups_10503);
    int64_t bytes_11651 = binop_x_11652 * 4;
    struct memblock_device mem_11653;
    
    mem_11653.references = NULL;
    memblock_alloc_device(&mem_11653, bytes_11651);
    
    struct memblock_local mem_11650;
    
    mem_11650.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 1, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 2, sizeof(arg_8414),
                                  &arg_8414));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 3,
                                  sizeof(num_iterations_10534),
                                  &num_iterations_10534));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 4, sizeof(y_10537),
                                  &y_10537));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 5, sizeof(mem_11644.mem),
                                  &mem_11644.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 6, sizeof(mem_11647.mem),
                                  &mem_11647.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10529, 7, sizeof(mem_11653.mem),
                                  &mem_11653.mem));
    if (1 * (num_groups_10503 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12872[1] = {num_groups_10503 *
                     group_sizze_10391};
        const size_t local_work_sizze_12876[1] = {group_sizze_10391};
        int64_t time_start_12873, time_end_12874;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_10529");
            fprintf(stderr, "%zu", global_work_sizze_12872[0]);
            fprintf(stderr, "].\n");
            time_start_12873 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_10529,
                                              1, NULL, global_work_sizze_12872,
                                              local_work_sizze_12876, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12874 = get_wall_time();
            
            long time_diff_12875 = time_end_12874 - time_start_12873;
            
            if (detail_timing) {
                scan1_kernel_10529total_runtime += time_diff_12875;
                scan1_kernel_10529runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_10529", (int) time_diff_12875);
            }
        }
    }
    
    struct memblock_device mem_11659;
    
    mem_11659.references = NULL;
    memblock_alloc_device(&mem_11659, bytes_11651);
    
    int64_t bytes_11654 = 4 * binop_x_11652;
    struct memblock_local mem_11656;
    
    mem_11656.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10564, 0, bytes_11654, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10564, 1,
                                  sizeof(num_groups_10503), &num_groups_10503));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10564, 2, sizeof(mem_11653.mem),
                                  &mem_11653.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10564, 3, sizeof(mem_11659.mem),
                                  &mem_11659.mem));
    if (1 * num_groups_10503 != 0) {
        const size_t global_work_sizze_12877[1] = {num_groups_10503};
        const size_t local_work_sizze_12881[1] = {num_groups_10503};
        int64_t time_start_12878, time_end_12879;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_10564");
            fprintf(stderr, "%zu", global_work_sizze_12877[0]);
            fprintf(stderr, "].\n");
            time_start_12878 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_10564,
                                              1, NULL, global_work_sizze_12877,
                                              local_work_sizze_12881, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12879 = get_wall_time();
            
            long time_diff_12880 = time_end_12879 - time_start_12878;
            
            if (detail_timing) {
                scan2_kernel_10564total_runtime += time_diff_12880;
                scan2_kernel_10564runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_10564", (int) time_diff_12880);
            }
        }
    }
    
    int32_t num_threads_10589 = w_div_group_sizze_10501 * group_sizze_10391;
    struct memblock_device mem_11662;
    
    mem_11662.references = NULL;
    memblock_alloc_device(&mem_11662, bytes_11642);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10590, 0, sizeof(arg_8414),
                                  &arg_8414));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10590, 1, sizeof(y_10537),
                                  &y_10537));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10590, 2, sizeof(mem_11644.mem),
                                  &mem_11644.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10590, 3, sizeof(mem_11659.mem),
                                  &mem_11659.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10590, 4, sizeof(mem_11662.mem),
                                  &mem_11662.mem));
    if (1 * (w_div_group_sizze_10501 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12882[1] = {w_div_group_sizze_10501 *
                     group_sizze_10391};
        const size_t local_work_sizze_12886[1] = {group_sizze_10391};
        int64_t time_start_12883, time_end_12884;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10590");
            fprintf(stderr, "%zu", global_work_sizze_12882[0]);
            fprintf(stderr, "].\n");
            time_start_12883 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10590, 1,
                                              NULL, global_work_sizze_12882,
                                              local_work_sizze_12886, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12884 = get_wall_time();
            
            long time_diff_12885 = time_end_12884 - time_start_12883;
            
            if (detail_timing) {
                map_kernel_10590total_runtime += time_diff_12885;
                map_kernel_10590runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10590", (int) time_diff_12885);
            }
        }
    }
    
    int32_t last_index_8427 = arg_8414 - 1;
    char is_empty_8428 = arg_8414 == 0;
    int32_t partition_sizze_8429;
    
    if (is_empty_8428) {
        partition_sizze_8429 = 0;
    } else {
        int32_t read_res_12887;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11662.mem, CL_TRUE,
                                           last_index_8427 * 4, sizeof(int32_t),
                                           &read_res_12887, 0, NULL, NULL));
        
        int32_t last_offset_8430 = read_res_12887;
        
        partition_sizze_8429 = last_offset_8430;
    }
    
    int64_t binop_x_11664 = sext_i32_i64(partition_sizze_8429);
    int64_t bytes_11663 = binop_x_11664 * 4;
    struct memblock_device mem_11665;
    
    mem_11665.references = NULL;
    memblock_alloc_device(&mem_11665, bytes_11663);
    
    struct memblock_device mem_11668;
    
    mem_11668.references = NULL;
    memblock_alloc_device(&mem_11668, bytes_11663);
    
    struct memblock_device mem_11671;
    
    mem_11671.references = NULL;
    memblock_alloc_device(&mem_11671, bytes_11663);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 0, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 1, sizeof(arg_8414),
                                  &arg_8414));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 2,
                                  sizeof(partition_sizze_8429),
                                  &partition_sizze_8429));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 3, sizeof(mem_11647.mem),
                                  &mem_11647.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 4, sizeof(mem_11662.mem),
                                  &mem_11662.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 5, sizeof(mem_11665.mem),
                                  &mem_11665.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 6, sizeof(mem_11668.mem),
                                  &mem_11668.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10600, 7, sizeof(mem_11671.mem),
                                  &mem_11671.mem));
    if (1 * (w_div_group_sizze_10501 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12888[1] = {w_div_group_sizze_10501 *
                     group_sizze_10391};
        const size_t local_work_sizze_12892[1] = {group_sizze_10391};
        int64_t time_start_12889, time_end_12890;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10600");
            fprintf(stderr, "%zu", global_work_sizze_12888[0]);
            fprintf(stderr, "].\n");
            time_start_12889 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10600, 1,
                                              NULL, global_work_sizze_12888,
                                              local_work_sizze_12892, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12890 = get_wall_time();
            
            long time_diff_12891 = time_end_12890 - time_start_12889;
            
            if (detail_timing) {
                map_kernel_10600total_runtime += time_diff_12891;
                map_kernel_10600runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10600", (int) time_diff_12891);
            }
        }
    }
    
    int32_t arg_8443 = pow32(prime_8379, 3);
    int32_t x_10606 = arg_8443 + y_10393;
    int32_t w_div_group_sizze_10607 = squot32(x_10606, group_sizze_10391);
    int32_t num_groups_maybe_zzero_10608 = smin32(w_div_group_sizze_10607,
                                                  max_num_groups_10392);
    int32_t num_groups_10609 = smax32(1, num_groups_maybe_zzero_10608);
    int32_t num_threads_10610 = num_groups_10609 * group_sizze_10391;
    int64_t binop_x_11673 = sext_i32_i64(arg_8443);
    int64_t bytes_11672 = binop_x_11673 * 4;
    struct memblock_device mem_11674;
    
    mem_11674.references = NULL;
    memblock_alloc_device(&mem_11674, bytes_11672);
    
    struct memblock_device mem_11677;
    
    mem_11677.references = NULL;
    memblock_alloc_device(&mem_11677, bytes_11672);
    
    int32_t y_10638 = num_threads_10610 - 1;
    int32_t x_10639 = arg_8443 + y_10638;
    int32_t num_iterations_10640 = squot32(x_10639, num_threads_10610);
    int32_t y_10643 = num_iterations_10640 * group_sizze_10391;
    int64_t binop_x_11682 = sext_i32_i64(num_groups_10609);
    int64_t bytes_11681 = binop_x_11682 * 4;
    struct memblock_device mem_11683;
    
    mem_11683.references = NULL;
    memblock_alloc_device(&mem_11683, bytes_11681);
    
    struct memblock_local mem_11680;
    
    mem_11680.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 1, sizeof(arg_8414),
                                  &arg_8414));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 2, sizeof(arg_8443),
                                  &arg_8443));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 3,
                                  sizeof(num_iterations_10640),
                                  &num_iterations_10640));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 4, sizeof(y_10643),
                                  &y_10643));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 5, sizeof(mem_11674.mem),
                                  &mem_11674.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 6, sizeof(mem_11677.mem),
                                  &mem_11677.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10635, 7, sizeof(mem_11683.mem),
                                  &mem_11683.mem));
    if (1 * (num_groups_10609 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12893[1] = {num_groups_10609 *
                     group_sizze_10391};
        const size_t local_work_sizze_12897[1] = {group_sizze_10391};
        int64_t time_start_12894, time_end_12895;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_10635");
            fprintf(stderr, "%zu", global_work_sizze_12893[0]);
            fprintf(stderr, "].\n");
            time_start_12894 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_10635,
                                              1, NULL, global_work_sizze_12893,
                                              local_work_sizze_12897, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12895 = get_wall_time();
            
            long time_diff_12896 = time_end_12895 - time_start_12894;
            
            if (detail_timing) {
                scan1_kernel_10635total_runtime += time_diff_12896;
                scan1_kernel_10635runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_10635", (int) time_diff_12896);
            }
        }
    }
    
    struct memblock_device mem_11689;
    
    mem_11689.references = NULL;
    memblock_alloc_device(&mem_11689, bytes_11681);
    
    int64_t bytes_11684 = 4 * binop_x_11682;
    struct memblock_local mem_11686;
    
    mem_11686.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10670, 0, bytes_11684, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10670, 1,
                                  sizeof(num_groups_10609), &num_groups_10609));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10670, 2, sizeof(mem_11683.mem),
                                  &mem_11683.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10670, 3, sizeof(mem_11689.mem),
                                  &mem_11689.mem));
    if (1 * num_groups_10609 != 0) {
        const size_t global_work_sizze_12898[1] = {num_groups_10609};
        const size_t local_work_sizze_12902[1] = {num_groups_10609};
        int64_t time_start_12899, time_end_12900;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_10670");
            fprintf(stderr, "%zu", global_work_sizze_12898[0]);
            fprintf(stderr, "].\n");
            time_start_12899 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_10670,
                                              1, NULL, global_work_sizze_12898,
                                              local_work_sizze_12902, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12900 = get_wall_time();
            
            long time_diff_12901 = time_end_12900 - time_start_12899;
            
            if (detail_timing) {
                scan2_kernel_10670total_runtime += time_diff_12901;
                scan2_kernel_10670runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_10670", (int) time_diff_12901);
            }
        }
    }
    
    int32_t num_threads_10695 = w_div_group_sizze_10607 * group_sizze_10391;
    struct memblock_device mem_11692;
    
    mem_11692.references = NULL;
    memblock_alloc_device(&mem_11692, bytes_11672);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10696, 0, sizeof(arg_8443),
                                  &arg_8443));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10696, 1, sizeof(y_10643),
                                  &y_10643));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10696, 2, sizeof(mem_11674.mem),
                                  &mem_11674.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10696, 3, sizeof(mem_11689.mem),
                                  &mem_11689.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10696, 4, sizeof(mem_11692.mem),
                                  &mem_11692.mem));
    if (1 * (w_div_group_sizze_10607 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12903[1] = {w_div_group_sizze_10607 *
                     group_sizze_10391};
        const size_t local_work_sizze_12907[1] = {group_sizze_10391};
        int64_t time_start_12904, time_end_12905;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10696");
            fprintf(stderr, "%zu", global_work_sizze_12903[0]);
            fprintf(stderr, "].\n");
            time_start_12904 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10696, 1,
                                              NULL, global_work_sizze_12903,
                                              local_work_sizze_12907, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12905 = get_wall_time();
            
            long time_diff_12906 = time_end_12905 - time_start_12904;
            
            if (detail_timing) {
                map_kernel_10696total_runtime += time_diff_12906;
                map_kernel_10696runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10696", (int) time_diff_12906);
            }
        }
    }
    
    int32_t last_index_8456 = arg_8443 - 1;
    char is_empty_8457 = arg_8443 == 0;
    int32_t partition_sizze_8458;
    
    if (is_empty_8457) {
        partition_sizze_8458 = 0;
    } else {
        int32_t read_res_12908;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11692.mem, CL_TRUE,
                                           last_index_8456 * 4, sizeof(int32_t),
                                           &read_res_12908, 0, NULL, NULL));
        
        int32_t last_offset_8459 = read_res_12908;
        
        partition_sizze_8458 = last_offset_8459;
    }
    
    int64_t binop_x_11694 = sext_i32_i64(partition_sizze_8458);
    int64_t bytes_11693 = binop_x_11694 * 4;
    struct memblock_device mem_11695;
    
    mem_11695.references = NULL;
    memblock_alloc_device(&mem_11695, bytes_11693);
    
    struct memblock_device mem_11698;
    
    mem_11698.references = NULL;
    memblock_alloc_device(&mem_11698, bytes_11693);
    
    struct memblock_device mem_11701;
    
    mem_11701.references = NULL;
    memblock_alloc_device(&mem_11701, bytes_11693);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 0, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 1, sizeof(arg_8443),
                                  &arg_8443));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 2,
                                  sizeof(partition_sizze_8458),
                                  &partition_sizze_8458));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 3, sizeof(mem_11677.mem),
                                  &mem_11677.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 4, sizeof(mem_11692.mem),
                                  &mem_11692.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 5, sizeof(mem_11695.mem),
                                  &mem_11695.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 6, sizeof(mem_11698.mem),
                                  &mem_11698.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10706, 7, sizeof(mem_11701.mem),
                                  &mem_11701.mem));
    if (1 * (w_div_group_sizze_10607 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12909[1] = {w_div_group_sizze_10607 *
                     group_sizze_10391};
        const size_t local_work_sizze_12913[1] = {group_sizze_10391};
        int64_t time_start_12910, time_end_12911;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10706");
            fprintf(stderr, "%zu", global_work_sizze_12909[0]);
            fprintf(stderr, "].\n");
            time_start_12910 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10706, 1,
                                              NULL, global_work_sizze_12909,
                                              local_work_sizze_12913, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12911 = get_wall_time();
            
            long time_diff_12912 = time_end_12911 - time_start_12910;
            
            if (detail_timing) {
                map_kernel_10706total_runtime += time_diff_12912;
                map_kernel_10706runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10706", (int) time_diff_12912);
            }
        }
    }
    
    int32_t arg_8472 = pow32(prime_8379, 4);
    int32_t x_10712 = arg_8472 + y_10393;
    int32_t w_div_group_sizze_10713 = squot32(x_10712, group_sizze_10391);
    int32_t num_groups_maybe_zzero_10714 = smin32(w_div_group_sizze_10713,
                                                  max_num_groups_10392);
    int32_t num_groups_10715 = smax32(1, num_groups_maybe_zzero_10714);
    int32_t num_threads_10716 = num_groups_10715 * group_sizze_10391;
    int64_t binop_x_11703 = sext_i32_i64(arg_8472);
    int64_t bytes_11702 = binop_x_11703 * 4;
    struct memblock_device mem_11704;
    
    mem_11704.references = NULL;
    memblock_alloc_device(&mem_11704, bytes_11702);
    
    struct memblock_device mem_11707;
    
    mem_11707.references = NULL;
    memblock_alloc_device(&mem_11707, bytes_11702);
    
    int32_t y_10744 = num_threads_10716 - 1;
    int32_t x_10745 = arg_8472 + y_10744;
    int32_t num_iterations_10746 = squot32(x_10745, num_threads_10716);
    int32_t y_10749 = num_iterations_10746 * group_sizze_10391;
    int64_t binop_x_11712 = sext_i32_i64(num_groups_10715);
    int64_t bytes_11711 = binop_x_11712 * 4;
    struct memblock_device mem_11713;
    
    mem_11713.references = NULL;
    memblock_alloc_device(&mem_11713, bytes_11711);
    
    struct memblock_local mem_11710;
    
    mem_11710.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 1, sizeof(arg_8443),
                                  &arg_8443));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 2, sizeof(arg_8472),
                                  &arg_8472));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 3,
                                  sizeof(num_iterations_10746),
                                  &num_iterations_10746));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 4, sizeof(y_10749),
                                  &y_10749));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 5, sizeof(mem_11704.mem),
                                  &mem_11704.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 6, sizeof(mem_11707.mem),
                                  &mem_11707.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10741, 7, sizeof(mem_11713.mem),
                                  &mem_11713.mem));
    if (1 * (num_groups_10715 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12914[1] = {num_groups_10715 *
                     group_sizze_10391};
        const size_t local_work_sizze_12918[1] = {group_sizze_10391};
        int64_t time_start_12915, time_end_12916;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_10741");
            fprintf(stderr, "%zu", global_work_sizze_12914[0]);
            fprintf(stderr, "].\n");
            time_start_12915 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_10741,
                                              1, NULL, global_work_sizze_12914,
                                              local_work_sizze_12918, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12916 = get_wall_time();
            
            long time_diff_12917 = time_end_12916 - time_start_12915;
            
            if (detail_timing) {
                scan1_kernel_10741total_runtime += time_diff_12917;
                scan1_kernel_10741runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_10741", (int) time_diff_12917);
            }
        }
    }
    
    struct memblock_device mem_11719;
    
    mem_11719.references = NULL;
    memblock_alloc_device(&mem_11719, bytes_11711);
    
    int64_t bytes_11714 = 4 * binop_x_11712;
    struct memblock_local mem_11716;
    
    mem_11716.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10776, 0, bytes_11714, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10776, 1,
                                  sizeof(num_groups_10715), &num_groups_10715));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10776, 2, sizeof(mem_11713.mem),
                                  &mem_11713.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10776, 3, sizeof(mem_11719.mem),
                                  &mem_11719.mem));
    if (1 * num_groups_10715 != 0) {
        const size_t global_work_sizze_12919[1] = {num_groups_10715};
        const size_t local_work_sizze_12923[1] = {num_groups_10715};
        int64_t time_start_12920, time_end_12921;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_10776");
            fprintf(stderr, "%zu", global_work_sizze_12919[0]);
            fprintf(stderr, "].\n");
            time_start_12920 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_10776,
                                              1, NULL, global_work_sizze_12919,
                                              local_work_sizze_12923, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12921 = get_wall_time();
            
            long time_diff_12922 = time_end_12921 - time_start_12920;
            
            if (detail_timing) {
                scan2_kernel_10776total_runtime += time_diff_12922;
                scan2_kernel_10776runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_10776", (int) time_diff_12922);
            }
        }
    }
    
    int32_t num_threads_10801 = w_div_group_sizze_10713 * group_sizze_10391;
    struct memblock_device mem_11722;
    
    mem_11722.references = NULL;
    memblock_alloc_device(&mem_11722, bytes_11702);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10802, 0, sizeof(arg_8472),
                                  &arg_8472));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10802, 1, sizeof(y_10749),
                                  &y_10749));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10802, 2, sizeof(mem_11704.mem),
                                  &mem_11704.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10802, 3, sizeof(mem_11719.mem),
                                  &mem_11719.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10802, 4, sizeof(mem_11722.mem),
                                  &mem_11722.mem));
    if (1 * (w_div_group_sizze_10713 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12924[1] = {w_div_group_sizze_10713 *
                     group_sizze_10391};
        const size_t local_work_sizze_12928[1] = {group_sizze_10391};
        int64_t time_start_12925, time_end_12926;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10802");
            fprintf(stderr, "%zu", global_work_sizze_12924[0]);
            fprintf(stderr, "].\n");
            time_start_12925 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10802, 1,
                                              NULL, global_work_sizze_12924,
                                              local_work_sizze_12928, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12926 = get_wall_time();
            
            long time_diff_12927 = time_end_12926 - time_start_12925;
            
            if (detail_timing) {
                map_kernel_10802total_runtime += time_diff_12927;
                map_kernel_10802runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10802", (int) time_diff_12927);
            }
        }
    }
    
    int32_t last_index_8485 = arg_8472 - 1;
    char is_empty_8486 = arg_8472 == 0;
    int32_t partition_sizze_8487;
    
    if (is_empty_8486) {
        partition_sizze_8487 = 0;
    } else {
        int32_t read_res_12929;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11722.mem, CL_TRUE,
                                           last_index_8485 * 4, sizeof(int32_t),
                                           &read_res_12929, 0, NULL, NULL));
        
        int32_t last_offset_8488 = read_res_12929;
        
        partition_sizze_8487 = last_offset_8488;
    }
    
    int64_t binop_x_11724 = sext_i32_i64(partition_sizze_8487);
    int64_t bytes_11723 = binop_x_11724 * 4;
    struct memblock_device mem_11725;
    
    mem_11725.references = NULL;
    memblock_alloc_device(&mem_11725, bytes_11723);
    
    struct memblock_device mem_11728;
    
    mem_11728.references = NULL;
    memblock_alloc_device(&mem_11728, bytes_11723);
    
    struct memblock_device mem_11731;
    
    mem_11731.references = NULL;
    memblock_alloc_device(&mem_11731, bytes_11723);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 0, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 1, sizeof(arg_8472),
                                  &arg_8472));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 2,
                                  sizeof(partition_sizze_8487),
                                  &partition_sizze_8487));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 3, sizeof(mem_11707.mem),
                                  &mem_11707.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 4, sizeof(mem_11722.mem),
                                  &mem_11722.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 5, sizeof(mem_11725.mem),
                                  &mem_11725.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 6, sizeof(mem_11728.mem),
                                  &mem_11728.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10812, 7, sizeof(mem_11731.mem),
                                  &mem_11731.mem));
    if (1 * (w_div_group_sizze_10713 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12930[1] = {w_div_group_sizze_10713 *
                     group_sizze_10391};
        const size_t local_work_sizze_12934[1] = {group_sizze_10391};
        int64_t time_start_12931, time_end_12932;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10812");
            fprintf(stderr, "%zu", global_work_sizze_12930[0]);
            fprintf(stderr, "].\n");
            time_start_12931 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10812, 1,
                                              NULL, global_work_sizze_12930,
                                              local_work_sizze_12934, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12932 = get_wall_time();
            
            long time_diff_12933 = time_end_12932 - time_start_12931;
            
            if (detail_timing) {
                map_kernel_10812total_runtime += time_diff_12933;
                map_kernel_10812runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10812", (int) time_diff_12933);
            }
        }
    }
    
    int32_t x_10840 = partition_sizze_8458 + y_10393;
    int32_t w_div_group_sizze_10841 = squot32(x_10840, group_sizze_10391);
    int32_t num_groups_maybe_zzero_10842 = smin32(w_div_group_sizze_10841,
                                                  max_num_groups_10392);
    int32_t num_groups_10843 = smax32(1, num_groups_maybe_zzero_10842);
    int32_t num_threads_10844 = num_groups_10843 * group_sizze_10391;
    struct memblock_device mem_11734;
    
    mem_11734.references = NULL;
    memblock_alloc_device(&mem_11734, bytes_11693);
    
    struct memblock_device mem_11737;
    
    mem_11737.references = NULL;
    memblock_alloc_device(&mem_11737, bytes_11693);
    
    int32_t y_10925 = num_threads_10844 - 1;
    int32_t x_10926 = partition_sizze_8458 + y_10925;
    int32_t num_iterations_10927 = squot32(x_10926, num_threads_10844);
    int32_t y_10930 = num_iterations_10927 * group_sizze_10391;
    int64_t binop_x_11745 = sext_i32_i64(num_groups_10843);
    int64_t bytes_11744 = binop_x_11745 * 4;
    struct memblock_device mem_11746;
    
    mem_11746.references = NULL;
    memblock_alloc_device(&mem_11746, bytes_11744);
    
    int64_t num_threads64_11940 = sext_i32_i64(num_threads_10844);
    int64_t total_sizze_11941 = num_threads64_11940 * 12;
    struct memblock_device mem_11740;
    
    mem_11740.references = NULL;
    memblock_alloc_device(&mem_11740, total_sizze_11941);
    
    struct memblock_local mem_11743;
    
    mem_11743.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 1, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 2,
                                  sizeof(partition_sizze_8458),
                                  &partition_sizze_8458));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 3,
                                  sizeof(num_iterations_10927),
                                  &num_iterations_10927));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 4, sizeof(y_10930),
                                  &y_10930));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 5, sizeof(mem_11695.mem),
                                  &mem_11695.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 6, sizeof(mem_11698.mem),
                                  &mem_11698.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 7, sizeof(mem_11701.mem),
                                  &mem_11701.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 8, sizeof(mem_11734.mem),
                                  &mem_11734.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 9, sizeof(mem_11737.mem),
                                  &mem_11737.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 10, sizeof(mem_11740.mem),
                                  &mem_11740.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_10922, 11, sizeof(mem_11746.mem),
                                  &mem_11746.mem));
    if (1 * (num_groups_10843 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12935[1] = {num_groups_10843 *
                     group_sizze_10391};
        const size_t local_work_sizze_12939[1] = {group_sizze_10391};
        int64_t time_start_12936, time_end_12937;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_10922");
            fprintf(stderr, "%zu", global_work_sizze_12935[0]);
            fprintf(stderr, "].\n");
            time_start_12936 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_10922,
                                              1, NULL, global_work_sizze_12935,
                                              local_work_sizze_12939, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12937 = get_wall_time();
            
            long time_diff_12938 = time_end_12937 - time_start_12936;
            
            if (detail_timing) {
                scan1_kernel_10922total_runtime += time_diff_12938;
                scan1_kernel_10922runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_10922", (int) time_diff_12938);
            }
        }
    }
    
    struct memblock_device mem_11752;
    
    mem_11752.references = NULL;
    memblock_alloc_device(&mem_11752, bytes_11744);
    
    int64_t bytes_11747 = 4 * binop_x_11745;
    struct memblock_local mem_11749;
    
    mem_11749.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10959, 0, bytes_11747, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10959, 1,
                                  sizeof(num_groups_10843), &num_groups_10843));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10959, 2, sizeof(mem_11746.mem),
                                  &mem_11746.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_10959, 3, sizeof(mem_11752.mem),
                                  &mem_11752.mem));
    if (1 * num_groups_10843 != 0) {
        const size_t global_work_sizze_12940[1] = {num_groups_10843};
        const size_t local_work_sizze_12944[1] = {num_groups_10843};
        int64_t time_start_12941, time_end_12942;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_10959");
            fprintf(stderr, "%zu", global_work_sizze_12940[0]);
            fprintf(stderr, "].\n");
            time_start_12941 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_10959,
                                              1, NULL, global_work_sizze_12940,
                                              local_work_sizze_12944, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12942 = get_wall_time();
            
            long time_diff_12943 = time_end_12942 - time_start_12941;
            
            if (detail_timing) {
                scan2_kernel_10959total_runtime += time_diff_12943;
                scan2_kernel_10959runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_10959", (int) time_diff_12943);
            }
        }
    }
    
    int32_t num_threads_10984 = w_div_group_sizze_10841 * group_sizze_10391;
    struct memblock_device mem_11755;
    
    mem_11755.references = NULL;
    memblock_alloc_device(&mem_11755, bytes_11693);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10985, 0,
                                  sizeof(partition_sizze_8458),
                                  &partition_sizze_8458));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10985, 1, sizeof(y_10930),
                                  &y_10930));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10985, 2, sizeof(mem_11734.mem),
                                  &mem_11734.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10985, 3, sizeof(mem_11752.mem),
                                  &mem_11752.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10985, 4, sizeof(mem_11755.mem),
                                  &mem_11755.mem));
    if (1 * (w_div_group_sizze_10841 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12945[1] = {w_div_group_sizze_10841 *
                     group_sizze_10391};
        const size_t local_work_sizze_12949[1] = {group_sizze_10391};
        int64_t time_start_12946, time_end_12947;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10985");
            fprintf(stderr, "%zu", global_work_sizze_12945[0]);
            fprintf(stderr, "].\n");
            time_start_12946 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10985, 1,
                                              NULL, global_work_sizze_12945,
                                              local_work_sizze_12949, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12947 = get_wall_time();
            
            long time_diff_12948 = time_end_12947 - time_start_12946;
            
            if (detail_timing) {
                map_kernel_10985total_runtime += time_diff_12948;
                map_kernel_10985runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10985", (int) time_diff_12948);
            }
        }
    }
    
    int32_t last_index_8553 = partition_sizze_8458 - 1;
    char is_empty_8554 = partition_sizze_8458 == 0;
    int32_t partition_sizze_8555;
    
    if (is_empty_8554) {
        partition_sizze_8555 = 0;
    } else {
        int32_t read_res_12950;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11755.mem, CL_TRUE,
                                           last_index_8553 * 4, sizeof(int32_t),
                                           &read_res_12950, 0, NULL, NULL));
        
        int32_t last_offset_8556 = read_res_12950;
        
        partition_sizze_8555 = last_offset_8556;
    }
    
    int64_t binop_x_11757 = sext_i32_i64(partition_sizze_8555);
    int64_t bytes_11756 = binop_x_11757 * 4;
    struct memblock_device mem_11758;
    
    mem_11758.references = NULL;
    memblock_alloc_device(&mem_11758, bytes_11756);
    
    struct memblock_device mem_11761;
    
    mem_11761.references = NULL;
    memblock_alloc_device(&mem_11761, bytes_11756);
    
    struct memblock_device mem_11764;
    
    mem_11764.references = NULL;
    memblock_alloc_device(&mem_11764, bytes_11756);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 0,
                                  sizeof(partition_sizze_8458),
                                  &partition_sizze_8458));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 1,
                                  sizeof(partition_sizze_8555),
                                  &partition_sizze_8555));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 2, sizeof(mem_11695.mem),
                                  &mem_11695.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 3, sizeof(mem_11698.mem),
                                  &mem_11698.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 4, sizeof(mem_11701.mem),
                                  &mem_11701.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 5, sizeof(mem_11737.mem),
                                  &mem_11737.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 6, sizeof(mem_11755.mem),
                                  &mem_11755.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 7, sizeof(mem_11758.mem),
                                  &mem_11758.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 8, sizeof(mem_11761.mem),
                                  &mem_11761.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_10995, 9, sizeof(mem_11764.mem),
                                  &mem_11764.mem));
    if (1 * (w_div_group_sizze_10841 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12951[1] = {w_div_group_sizze_10841 *
                     group_sizze_10391};
        const size_t local_work_sizze_12955[1] = {group_sizze_10391};
        int64_t time_start_12952, time_end_12953;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_10995");
            fprintf(stderr, "%zu", global_work_sizze_12951[0]);
            fprintf(stderr, "].\n");
            time_start_12952 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_10995, 1,
                                              NULL, global_work_sizze_12951,
                                              local_work_sizze_12955, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12953 = get_wall_time();
            
            long time_diff_12954 = time_end_12953 - time_start_12952;
            
            if (detail_timing) {
                map_kernel_10995total_runtime += time_diff_12954;
                map_kernel_10995runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_10995", (int) time_diff_12954);
            }
        }
    }
    
    int32_t x_11023 = partition_sizze_8487 + y_10393;
    int32_t w_div_group_sizze_11024 = squot32(x_11023, group_sizze_10391);
    int32_t num_groups_maybe_zzero_11025 = smin32(w_div_group_sizze_11024,
                                                  max_num_groups_10392);
    int32_t num_groups_11026 = smax32(1, num_groups_maybe_zzero_11025);
    int32_t num_threads_11027 = num_groups_11026 * group_sizze_10391;
    struct memblock_device mem_11767;
    
    mem_11767.references = NULL;
    memblock_alloc_device(&mem_11767, bytes_11723);
    
    struct memblock_device mem_11770;
    
    mem_11770.references = NULL;
    memblock_alloc_device(&mem_11770, bytes_11723);
    
    int32_t y_11108 = num_threads_11027 - 1;
    int32_t x_11109 = partition_sizze_8487 + y_11108;
    int32_t num_iterations_11110 = squot32(x_11109, num_threads_11027);
    int32_t y_11113 = num_iterations_11110 * group_sizze_10391;
    int64_t binop_x_11778 = sext_i32_i64(num_groups_11026);
    int64_t bytes_11777 = binop_x_11778 * 4;
    struct memblock_device mem_11779;
    
    mem_11779.references = NULL;
    memblock_alloc_device(&mem_11779, bytes_11777);
    
    int64_t num_threads64_11945 = sext_i32_i64(num_threads_11027);
    int64_t total_sizze_11946 = num_threads64_11945 * 16;
    struct memblock_device mem_11773;
    
    mem_11773.references = NULL;
    memblock_alloc_device(&mem_11773, total_sizze_11946);
    
    struct memblock_local mem_11776;
    
    mem_11776.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 0, bytes_11618, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 1, sizeof(prime_8379),
                                  &prime_8379));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 2,
                                  sizeof(partition_sizze_8487),
                                  &partition_sizze_8487));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 3,
                                  sizeof(num_iterations_11110),
                                  &num_iterations_11110));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 4, sizeof(y_11113),
                                  &y_11113));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 5, sizeof(mem_11725.mem),
                                  &mem_11725.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 6, sizeof(mem_11728.mem),
                                  &mem_11728.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 7, sizeof(mem_11731.mem),
                                  &mem_11731.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 8, sizeof(mem_11767.mem),
                                  &mem_11767.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 9, sizeof(mem_11770.mem),
                                  &mem_11770.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 10, sizeof(mem_11773.mem),
                                  &mem_11773.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11105, 11, sizeof(mem_11779.mem),
                                  &mem_11779.mem));
    if (1 * (num_groups_11026 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12956[1] = {num_groups_11026 *
                     group_sizze_10391};
        const size_t local_work_sizze_12960[1] = {group_sizze_10391};
        int64_t time_start_12957, time_end_12958;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan1_kernel_11105");
            fprintf(stderr, "%zu", global_work_sizze_12956[0]);
            fprintf(stderr, "].\n");
            time_start_12957 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan1_kernel_11105,
                                              1, NULL, global_work_sizze_12956,
                                              local_work_sizze_12960, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12958 = get_wall_time();
            
            long time_diff_12959 = time_end_12958 - time_start_12957;
            
            if (detail_timing) {
                scan1_kernel_11105total_runtime += time_diff_12959;
                scan1_kernel_11105runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan1_kernel_11105", (int) time_diff_12959);
            }
        }
    }
    
    struct memblock_device mem_11785;
    
    mem_11785.references = NULL;
    memblock_alloc_device(&mem_11785, bytes_11777);
    
    int64_t bytes_11780 = 4 * binop_x_11778;
    struct memblock_local mem_11782;
    
    mem_11782.references = NULL;
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11142, 0, bytes_11780, NULL));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11142, 1,
                                  sizeof(num_groups_11026), &num_groups_11026));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11142, 2, sizeof(mem_11779.mem),
                                  &mem_11779.mem));
    OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11142, 3, sizeof(mem_11785.mem),
                                  &mem_11785.mem));
    if (1 * num_groups_11026 != 0) {
        const size_t global_work_sizze_12961[1] = {num_groups_11026};
        const size_t local_work_sizze_12965[1] = {num_groups_11026};
        int64_t time_start_12962, time_end_12963;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "scan2_kernel_11142");
            fprintf(stderr, "%zu", global_work_sizze_12961[0]);
            fprintf(stderr, "].\n");
            time_start_12962 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, scan2_kernel_11142,
                                              1, NULL, global_work_sizze_12961,
                                              local_work_sizze_12965, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12963 = get_wall_time();
            
            long time_diff_12964 = time_end_12963 - time_start_12962;
            
            if (detail_timing) {
                scan2_kernel_11142total_runtime += time_diff_12964;
                scan2_kernel_11142runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "scan2_kernel_11142", (int) time_diff_12964);
            }
        }
    }
    
    int32_t num_threads_11167 = w_div_group_sizze_11024 * group_sizze_10391;
    struct memblock_device mem_11788;
    
    mem_11788.references = NULL;
    memblock_alloc_device(&mem_11788, bytes_11723);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11168, 0,
                                  sizeof(partition_sizze_8487),
                                  &partition_sizze_8487));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11168, 1, sizeof(y_11113),
                                  &y_11113));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11168, 2, sizeof(mem_11767.mem),
                                  &mem_11767.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11168, 3, sizeof(mem_11785.mem),
                                  &mem_11785.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11168, 4, sizeof(mem_11788.mem),
                                  &mem_11788.mem));
    if (1 * (w_div_group_sizze_11024 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12966[1] = {w_div_group_sizze_11024 *
                     group_sizze_10391};
        const size_t local_work_sizze_12970[1] = {group_sizze_10391};
        int64_t time_start_12967, time_end_12968;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_11168");
            fprintf(stderr, "%zu", global_work_sizze_12966[0]);
            fprintf(stderr, "].\n");
            time_start_12967 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_11168, 1,
                                              NULL, global_work_sizze_12966,
                                              local_work_sizze_12970, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12968 = get_wall_time();
            
            long time_diff_12969 = time_end_12968 - time_start_12967;
            
            if (detail_timing) {
                map_kernel_11168total_runtime += time_diff_12969;
                map_kernel_11168runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_11168", (int) time_diff_12969);
            }
        }
    }
    
    int32_t last_index_8622 = partition_sizze_8487 - 1;
    char is_empty_8623 = partition_sizze_8487 == 0;
    int32_t partition_sizze_8624;
    
    if (is_empty_8623) {
        partition_sizze_8624 = 0;
    } else {
        int32_t read_res_12971;
        
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11788.mem, CL_TRUE,
                                           last_index_8622 * 4, sizeof(int32_t),
                                           &read_res_12971, 0, NULL, NULL));
        
        int32_t last_offset_8625 = read_res_12971;
        
        partition_sizze_8624 = last_offset_8625;
    }
    
    int64_t binop_x_11790 = sext_i32_i64(partition_sizze_8624);
    int64_t bytes_11789 = binop_x_11790 * 4;
    struct memblock_device mem_11791;
    
    mem_11791.references = NULL;
    memblock_alloc_device(&mem_11791, bytes_11789);
    
    struct memblock_device mem_11794;
    
    mem_11794.references = NULL;
    memblock_alloc_device(&mem_11794, bytes_11789);
    
    struct memblock_device mem_11797;
    
    mem_11797.references = NULL;
    memblock_alloc_device(&mem_11797, bytes_11789);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 0,
                                  sizeof(partition_sizze_8487),
                                  &partition_sizze_8487));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 1,
                                  sizeof(partition_sizze_8624),
                                  &partition_sizze_8624));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 2, sizeof(mem_11725.mem),
                                  &mem_11725.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 3, sizeof(mem_11728.mem),
                                  &mem_11728.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 4, sizeof(mem_11731.mem),
                                  &mem_11731.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 5, sizeof(mem_11770.mem),
                                  &mem_11770.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 6, sizeof(mem_11788.mem),
                                  &mem_11788.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 7, sizeof(mem_11791.mem),
                                  &mem_11791.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 8, sizeof(mem_11794.mem),
                                  &mem_11794.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11178, 9, sizeof(mem_11797.mem),
                                  &mem_11797.mem));
    if (1 * (w_div_group_sizze_11024 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_12972[1] = {w_div_group_sizze_11024 *
                     group_sizze_10391};
        const size_t local_work_sizze_12976[1] = {group_sizze_10391};
        int64_t time_start_12973, time_end_12974;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_11178");
            fprintf(stderr, "%zu", global_work_sizze_12972[0]);
            fprintf(stderr, "].\n");
            time_start_12973 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_11178, 1,
                                              NULL, global_work_sizze_12972,
                                              local_work_sizze_12976, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_12974 = get_wall_time();
            
            long time_diff_12975 = time_end_12974 - time_start_12973;
            
            if (detail_timing) {
                map_kernel_11178total_runtime += time_diff_12975;
                map_kernel_11178runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_11178", (int) time_diff_12975);
            }
        }
    }
    
    char cond_8640 = maxDegree_8380 == 2;
    int32_t conc_tmp_8641 = partition_sizze_8429 + partition_sizze_8555;
    char cond_8642 = maxDegree_8380 == 1;
    char cond_8643 = maxDegree_8380 == 0;
    int32_t conc_tmp_8644 = conc_tmp_8641 + partition_sizze_8624;
    int32_t branch_ctx_8645;
    
    if (cond_8643) {
        branch_ctx_8645 = partition_sizze_8400;
    } else {
        branch_ctx_8645 = conc_tmp_8644;
    }
    
    int32_t branch_ctx_8646;
    
    if (cond_8642) {
        branch_ctx_8646 = partition_sizze_8429;
    } else {
        branch_ctx_8646 = branch_ctx_8645;
    }
    
    int32_t branch_ctx_8647;
    
    if (cond_8640) {
        branch_ctx_8647 = conc_tmp_8641;
    } else {
        branch_ctx_8647 = branch_ctx_8646;
    }
    
    int64_t binop_x_11799 = sext_i32_i64(conc_tmp_8641);
    int64_t bytes_11798 = binop_x_11799 * 4;
    struct memblock_device mem_11800;
    
    mem_11800.references = NULL;
    memblock_alloc_device(&mem_11800, bytes_11798);
    
    struct memblock_device mem_11803;
    
    mem_11803.references = NULL;
    memblock_alloc_device(&mem_11803, bytes_11798);
    
    struct memblock_device mem_11806;
    
    mem_11806.references = NULL;
    memblock_alloc_device(&mem_11806, bytes_11798);
    
    int64_t binop_x_11808 = sext_i32_i64(conc_tmp_8644);
    int64_t bytes_11807 = binop_x_11808 * 4;
    struct memblock_device mem_11809;
    
    mem_11809.references = NULL;
    memblock_alloc_device(&mem_11809, bytes_11807);
    
    struct memblock_device mem_11812;
    
    mem_11812.references = NULL;
    memblock_alloc_device(&mem_11812, bytes_11807);
    
    struct memblock_device mem_11815;
    
    mem_11815.references = NULL;
    memblock_alloc_device(&mem_11815, bytes_11807);
    
    int64_t res_mem_sizze_11828;
    int64_t res_mem_sizze_11830;
    int64_t res_mem_sizze_11832;
    struct memblock_device res_mem_11829;
    
    res_mem_11829.references = NULL;
    
    struct memblock_device res_mem_11831;
    
    res_mem_11831.references = NULL;
    
    struct memblock_device res_mem_11833;
    
    res_mem_11833.references = NULL;
    if (cond_8640) {
        int32_t tmp_offs_12502 = 0;
        
        if (partition_sizze_8429 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11665.mem,
                                               mem_11800.mem, 0,
                                               tmp_offs_12502 * 4,
                                               partition_sizze_8429 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12502 += partition_sizze_8429;
        if (partition_sizze_8555 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11758.mem,
                                               mem_11800.mem, 0,
                                               tmp_offs_12502 * 4,
                                               partition_sizze_8555 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12502 += partition_sizze_8555;
        
        int32_t tmp_offs_12503 = 0;
        
        if (partition_sizze_8429 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11668.mem,
                                               mem_11803.mem, 0,
                                               tmp_offs_12503 * 4,
                                               partition_sizze_8429 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12503 += partition_sizze_8429;
        if (partition_sizze_8555 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11761.mem,
                                               mem_11803.mem, 0,
                                               tmp_offs_12503 * 4,
                                               partition_sizze_8555 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12503 += partition_sizze_8555;
        
        int32_t tmp_offs_12504 = 0;
        
        if (partition_sizze_8429 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11671.mem,
                                               mem_11806.mem, 0,
                                               tmp_offs_12504 * 4,
                                               partition_sizze_8429 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12504 += partition_sizze_8429;
        if (partition_sizze_8555 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11764.mem,
                                               mem_11806.mem, 0,
                                               tmp_offs_12504 * 4,
                                               partition_sizze_8555 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12504 += partition_sizze_8555;
        memblock_set_device(&res_mem_11829, &mem_11800);
        res_mem_sizze_11828 = bytes_11798;
        memblock_set_device(&res_mem_11831, &mem_11803);
        res_mem_sizze_11830 = bytes_11798;
        memblock_set_device(&res_mem_11833, &mem_11806);
        res_mem_sizze_11832 = bytes_11798;
    } else {
        int64_t res_mem_sizze_11822;
        int64_t res_mem_sizze_11824;
        int64_t res_mem_sizze_11826;
        struct memblock_device res_mem_11823;
        
        res_mem_11823.references = NULL;
        
        struct memblock_device res_mem_11825;
        
        res_mem_11825.references = NULL;
        
        struct memblock_device res_mem_11827;
        
        res_mem_11827.references = NULL;
        if (cond_8642) {
            memblock_set_device(&res_mem_11823, &mem_11665);
            res_mem_sizze_11822 = bytes_11663;
            memblock_set_device(&res_mem_11825, &mem_11668);
            res_mem_sizze_11824 = bytes_11663;
            memblock_set_device(&res_mem_11827, &mem_11671);
            res_mem_sizze_11826 = bytes_11663;
        } else {
            int64_t res_mem_sizze_11816;
            int64_t res_mem_sizze_11818;
            int64_t res_mem_sizze_11820;
            struct memblock_device res_mem_11817;
            
            res_mem_11817.references = NULL;
            
            struct memblock_device res_mem_11819;
            
            res_mem_11819.references = NULL;
            
            struct memblock_device res_mem_11821;
            
            res_mem_11821.references = NULL;
            if (cond_8643) {
                memblock_set_device(&res_mem_11817, &mem_11635);
                res_mem_sizze_11816 = bytes_11633;
                memblock_set_device(&res_mem_11819, &mem_11638);
                res_mem_sizze_11818 = bytes_11633;
                memblock_set_device(&res_mem_11821, &mem_11641);
                res_mem_sizze_11820 = bytes_11633;
            } else {
                int32_t tmp_offs_12505 = 0;
                
                if (partition_sizze_8429 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11665.mem,
                                                       mem_11809.mem, 0,
                                                       tmp_offs_12505 * 4,
                                                       partition_sizze_8429 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12505 += partition_sizze_8429;
                if (partition_sizze_8555 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11758.mem,
                                                       mem_11809.mem, 0,
                                                       tmp_offs_12505 * 4,
                                                       partition_sizze_8555 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12505 += partition_sizze_8555;
                if (partition_sizze_8624 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11791.mem,
                                                       mem_11809.mem, 0,
                                                       tmp_offs_12505 * 4,
                                                       partition_sizze_8624 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12505 += partition_sizze_8624;
                
                int32_t tmp_offs_12506 = 0;
                
                if (partition_sizze_8429 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11668.mem,
                                                       mem_11812.mem, 0,
                                                       tmp_offs_12506 * 4,
                                                       partition_sizze_8429 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12506 += partition_sizze_8429;
                if (partition_sizze_8555 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11761.mem,
                                                       mem_11812.mem, 0,
                                                       tmp_offs_12506 * 4,
                                                       partition_sizze_8555 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12506 += partition_sizze_8555;
                if (partition_sizze_8624 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11794.mem,
                                                       mem_11812.mem, 0,
                                                       tmp_offs_12506 * 4,
                                                       partition_sizze_8624 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12506 += partition_sizze_8624;
                
                int32_t tmp_offs_12507 = 0;
                
                if (partition_sizze_8429 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11671.mem,
                                                       mem_11815.mem, 0,
                                                       tmp_offs_12507 * 4,
                                                       partition_sizze_8429 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12507 += partition_sizze_8429;
                if (partition_sizze_8555 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11764.mem,
                                                       mem_11815.mem, 0,
                                                       tmp_offs_12507 * 4,
                                                       partition_sizze_8555 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12507 += partition_sizze_8555;
                if (partition_sizze_8624 * sizeof(int32_t) > 0) {
                    OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                                       mem_11797.mem,
                                                       mem_11815.mem, 0,
                                                       tmp_offs_12507 * 4,
                                                       partition_sizze_8624 *
                                                       sizeof(int32_t), 0, NULL,
                                                       NULL));
                    if (debugging)
                        OPENCL_SUCCEED(clFinish(fut_cl_queue));
                }
                tmp_offs_12507 += partition_sizze_8624;
                memblock_set_device(&res_mem_11817, &mem_11809);
                res_mem_sizze_11816 = bytes_11807;
                memblock_set_device(&res_mem_11819, &mem_11812);
                res_mem_sizze_11818 = bytes_11807;
                memblock_set_device(&res_mem_11821, &mem_11815);
                res_mem_sizze_11820 = bytes_11807;
            }
            memblock_set_device(&res_mem_11823, &res_mem_11817);
            res_mem_sizze_11822 = res_mem_sizze_11816;
            memblock_set_device(&res_mem_11825, &res_mem_11819);
            res_mem_sizze_11824 = res_mem_sizze_11818;
            memblock_set_device(&res_mem_11827, &res_mem_11821);
            res_mem_sizze_11826 = res_mem_sizze_11820;
            memblock_unref_device(&res_mem_11817);
            memblock_unref_device(&res_mem_11819);
            memblock_unref_device(&res_mem_11821);
        }
        memblock_set_device(&res_mem_11829, &res_mem_11823);
        res_mem_sizze_11828 = res_mem_sizze_11822;
        memblock_set_device(&res_mem_11831, &res_mem_11825);
        res_mem_sizze_11830 = res_mem_sizze_11824;
        memblock_set_device(&res_mem_11833, &res_mem_11827);
        res_mem_sizze_11832 = res_mem_sizze_11826;
        memblock_unref_device(&res_mem_11823);
        memblock_unref_device(&res_mem_11825);
        memblock_unref_device(&res_mem_11827);
    }
    
    int32_t upper_bound_8681 = maxDegree_8380 - 3;
    struct memblock_local mem_11878;
    
    mem_11878.references = NULL;
    
    int64_t res_mem_sizze_11909;
    int64_t res_mem_sizze_11911;
    int64_t res_mem_sizze_11913;
    struct memblock_device res_mem_11910;
    
    res_mem_11910.references = NULL;
    
    struct memblock_device res_mem_11912;
    
    res_mem_11912.references = NULL;
    
    struct memblock_device res_mem_11914;
    
    res_mem_11914.references = NULL;
    
    int32_t sizze_8682;
    int32_t sizze_8686;
    int64_t polys_mem_sizze_11834;
    struct memblock_device polys_mem_11835;
    
    polys_mem_11835.references = NULL;
    
    int64_t polys_mem_sizze_11836;
    struct memblock_device polys_mem_11837;
    
    polys_mem_11837.references = NULL;
    
    int64_t polys_mem_sizze_11838;
    struct memblock_device polys_mem_11839;
    
    polys_mem_11839.references = NULL;
    polys_mem_sizze_11834 = res_mem_sizze_11828;
    polys_mem_sizze_11836 = res_mem_sizze_11830;
    polys_mem_sizze_11838 = res_mem_sizze_11832;
    memblock_set_device(&polys_mem_11835, &res_mem_11829);
    memblock_set_device(&polys_mem_11837, &res_mem_11831);
    memblock_set_device(&polys_mem_11839, &res_mem_11833);
    sizze_8686 = branch_ctx_8647;
    for (int32_t i_8690 = 0; i_8690 < upper_bound_8681; i_8690++) {
        int32_t res_8691 = i_8690 + 4;
        int32_t y_8692 = res_8691 + 1;
        int32_t x_8693 = pow32(prime_8379, y_8692);
        int32_t y_8694 = pow32(prime_8379, res_8691);
        int32_t arg_8695 = x_8693 - y_8694;
        int32_t last_index_8702 = y_8692 - 1;
        int32_t x_11256 = arg_8695 + y_10393;
        int32_t w_div_group_sizze_11257 = squot32(x_11256, group_sizze_10391);
        int32_t num_groups_maybe_zzero_11258 = smin32(w_div_group_sizze_11257,
                                                      max_num_groups_10392);
        int32_t num_groups_11259 = smax32(1, num_groups_maybe_zzero_11258);
        int32_t num_threads_11260 = num_groups_11259 * group_sizze_10391;
        int64_t binop_x_11841 = sext_i32_i64(arg_8695);
        int64_t bytes_11840 = binop_x_11841 * 4;
        struct memblock_device mem_11842;
        
        mem_11842.references = NULL;
        memblock_alloc_device(&mem_11842, bytes_11840);
        
        struct memblock_device mem_11845;
        
        mem_11845.references = NULL;
        memblock_alloc_device(&mem_11845, bytes_11840);
        
        struct memblock_device mem_11847;
        
        mem_11847.references = NULL;
        memblock_alloc_device(&mem_11847, binop_x_11841);
        
        struct memblock_device mem_11850;
        
        mem_11850.references = NULL;
        memblock_alloc_device(&mem_11850, bytes_11840);
        
        int32_t y_11448 = num_threads_11260 - 1;
        int32_t x_11449 = arg_8695 + y_11448;
        int32_t num_iterations_11450 = squot32(x_11449, num_threads_11260);
        int32_t y_11453 = num_iterations_11450 * group_sizze_10391;
        int64_t binop_x_11880 = sext_i32_i64(num_groups_11259);
        int64_t bytes_11879 = binop_x_11880 * 4;
        struct memblock_device mem_11881;
        
        mem_11881.references = NULL;
        memblock_alloc_device(&mem_11881, bytes_11879);
        
        int64_t binop_x_11852 = sext_i32_i64(y_8692);
        int64_t bytes_11851 = binop_x_11852 * 4;
        int64_t num_threads64_11950 = sext_i32_i64(num_threads_11260);
        int64_t total_sizze_11951 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11853;
        
        mem_11853.references = NULL;
        memblock_alloc_device(&mem_11853, total_sizze_11951);
        
        int64_t total_sizze_11952 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11856;
        
        mem_11856.references = NULL;
        memblock_alloc_device(&mem_11856, total_sizze_11952);
        
        int64_t total_sizze_11953 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11859;
        
        mem_11859.references = NULL;
        memblock_alloc_device(&mem_11859, total_sizze_11953);
        
        int64_t total_sizze_11954 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11862;
        
        mem_11862.references = NULL;
        memblock_alloc_device(&mem_11862, total_sizze_11954);
        
        int64_t total_sizze_11955 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11865;
        
        mem_11865.references = NULL;
        memblock_alloc_device(&mem_11865, total_sizze_11955);
        
        int64_t total_sizze_11956 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11868;
        
        mem_11868.references = NULL;
        memblock_alloc_device(&mem_11868, total_sizze_11956);
        
        int64_t total_sizze_11957 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11871;
        
        mem_11871.references = NULL;
        memblock_alloc_device(&mem_11871, total_sizze_11957);
        
        int64_t total_sizze_11958 = num_threads64_11950 * bytes_11851;
        struct memblock_device mem_11874;
        
        mem_11874.references = NULL;
        memblock_alloc_device(&mem_11874, total_sizze_11958);
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 0, bytes_11618,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 1, sizeof(prime_8379),
                                      &prime_8379));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 2, sizeof(sizze_8686),
                                      &sizze_8686));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 3, sizeof(y_8692),
                                      &y_8692));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 4, sizeof(y_8694),
                                      &y_8694));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 5, sizeof(arg_8695),
                                      &arg_8695));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 6,
                                      sizeof(last_index_8702),
                                      &last_index_8702));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 7,
                                      sizeof(num_iterations_11450),
                                      &num_iterations_11450));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 8, sizeof(y_11453),
                                      &y_11453));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 9,
                                      sizeof(polys_mem_11835.mem),
                                      &polys_mem_11835.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 10,
                                      sizeof(polys_mem_11837.mem),
                                      &polys_mem_11837.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 11,
                                      sizeof(polys_mem_11839.mem),
                                      &polys_mem_11839.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 12,
                                      sizeof(mem_11842.mem), &mem_11842.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 13,
                                      sizeof(mem_11845.mem), &mem_11845.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 14,
                                      sizeof(mem_11847.mem), &mem_11847.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 15,
                                      sizeof(mem_11850.mem), &mem_11850.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 16,
                                      sizeof(mem_11853.mem), &mem_11853.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 17,
                                      sizeof(mem_11856.mem), &mem_11856.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 18,
                                      sizeof(mem_11859.mem), &mem_11859.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 19,
                                      sizeof(mem_11862.mem), &mem_11862.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 20,
                                      sizeof(mem_11865.mem), &mem_11865.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 21,
                                      sizeof(mem_11868.mem), &mem_11868.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 22,
                                      sizeof(mem_11871.mem), &mem_11871.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 23,
                                      sizeof(mem_11874.mem), &mem_11874.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan1_kernel_11445, 24,
                                      sizeof(mem_11881.mem), &mem_11881.mem));
        if (1 * (num_groups_11259 * group_sizze_10391) != 0) {
            const size_t global_work_sizze_12977[1] = {num_groups_11259 *
                         group_sizze_10391};
            const size_t local_work_sizze_12981[1] = {group_sizze_10391};
            int64_t time_start_12978, time_end_12979;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan1_kernel_11445");
                fprintf(stderr, "%zu", global_work_sizze_12977[0]);
                fprintf(stderr, "].\n");
                time_start_12978 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan1_kernel_11445, 1, NULL,
                                                  global_work_sizze_12977,
                                                  local_work_sizze_12981, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12979 = get_wall_time();
                
                long time_diff_12980 = time_end_12979 - time_start_12978;
                
                if (detail_timing) {
                    scan1_kernel_11445total_runtime += time_diff_12980;
                    scan1_kernel_11445runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan1_kernel_11445", (int) time_diff_12980);
                }
            }
        }
        
        struct memblock_device mem_11887;
        
        mem_11887.references = NULL;
        memblock_alloc_device(&mem_11887, bytes_11879);
        
        int64_t bytes_11882 = 4 * binop_x_11880;
        struct memblock_local mem_11884;
        
        mem_11884.references = NULL;
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11486, 0, bytes_11882,
                                      NULL));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11486, 1,
                                      sizeof(num_groups_11259),
                                      &num_groups_11259));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11486, 2,
                                      sizeof(mem_11881.mem), &mem_11881.mem));
        OPENCL_SUCCEED(clSetKernelArg(scan2_kernel_11486, 3,
                                      sizeof(mem_11887.mem), &mem_11887.mem));
        if (1 * num_groups_11259 != 0) {
            const size_t global_work_sizze_12982[1] = {num_groups_11259};
            const size_t local_work_sizze_12986[1] = {num_groups_11259};
            int64_t time_start_12983, time_end_12984;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "scan2_kernel_11486");
                fprintf(stderr, "%zu", global_work_sizze_12982[0]);
                fprintf(stderr, "].\n");
                time_start_12983 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  scan2_kernel_11486, 1, NULL,
                                                  global_work_sizze_12982,
                                                  local_work_sizze_12986, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12984 = get_wall_time();
                
                long time_diff_12985 = time_end_12984 - time_start_12983;
                
                if (detail_timing) {
                    scan2_kernel_11486total_runtime += time_diff_12985;
                    scan2_kernel_11486runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "scan2_kernel_11486", (int) time_diff_12985);
                }
            }
        }
        
        int32_t num_threads_11511 = w_div_group_sizze_11257 * group_sizze_10391;
        struct memblock_device mem_11890;
        
        mem_11890.references = NULL;
        memblock_alloc_device(&mem_11890, bytes_11840);
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11512, 0, sizeof(arg_8695),
                                      &arg_8695));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11512, 1, sizeof(y_11453),
                                      &y_11453));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11512, 2,
                                      sizeof(mem_11842.mem), &mem_11842.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11512, 3,
                                      sizeof(mem_11887.mem), &mem_11887.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11512, 4,
                                      sizeof(mem_11890.mem), &mem_11890.mem));
        if (1 * (w_div_group_sizze_11257 * group_sizze_10391) != 0) {
            const size_t global_work_sizze_12987[1] = {w_div_group_sizze_11257 *
                         group_sizze_10391};
            const size_t local_work_sizze_12991[1] = {group_sizze_10391};
            int64_t time_start_12988, time_end_12989;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_11512");
                fprintf(stderr, "%zu", global_work_sizze_12987[0]);
                fprintf(stderr, "].\n");
                time_start_12988 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  map_kernel_11512, 1, NULL,
                                                  global_work_sizze_12987,
                                                  local_work_sizze_12991, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12989 = get_wall_time();
                
                long time_diff_12990 = time_end_12989 - time_start_12988;
                
                if (detail_timing) {
                    map_kernel_11512total_runtime += time_diff_12990;
                    map_kernel_11512runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_11512", (int) time_diff_12990);
                }
            }
        }
        
        int32_t last_index_8938 = arg_8695 - 1;
        char is_empty_8939 = arg_8695 == 0;
        int32_t partition_sizze_8940;
        
        if (is_empty_8939) {
            partition_sizze_8940 = 0;
        } else {
            int32_t read_res_12992;
            
            OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue, mem_11890.mem,
                                               CL_TRUE, last_index_8938 * 4,
                                               sizeof(int32_t), &read_res_12992,
                                               0, NULL, NULL));
            
            int32_t last_offset_8941 = read_res_12992;
            
            partition_sizze_8940 = last_offset_8941;
        }
        
        int64_t binop_x_11892 = sext_i32_i64(partition_sizze_8940);
        int64_t bytes_11891 = binop_x_11892 * 4;
        struct memblock_device mem_11893;
        
        mem_11893.references = NULL;
        memblock_alloc_device(&mem_11893, bytes_11891);
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11522, 0, sizeof(arg_8695),
                                      &arg_8695));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11522, 1,
                                      sizeof(partition_sizze_8940),
                                      &partition_sizze_8940));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11522, 2,
                                      sizeof(mem_11845.mem), &mem_11845.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11522, 3,
                                      sizeof(mem_11850.mem), &mem_11850.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11522, 4,
                                      sizeof(mem_11890.mem), &mem_11890.mem));
        OPENCL_SUCCEED(clSetKernelArg(map_kernel_11522, 5,
                                      sizeof(mem_11893.mem), &mem_11893.mem));
        if (1 * (w_div_group_sizze_11257 * group_sizze_10391) != 0) {
            const size_t global_work_sizze_12993[1] = {w_div_group_sizze_11257 *
                         group_sizze_10391};
            const size_t local_work_sizze_12997[1] = {group_sizze_10391};
            int64_t time_start_12994, time_end_12995;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "map_kernel_11522");
                fprintf(stderr, "%zu", global_work_sizze_12993[0]);
                fprintf(stderr, "].\n");
                time_start_12994 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  map_kernel_11522, 1, NULL,
                                                  global_work_sizze_12993,
                                                  local_work_sizze_12997, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_12995 = get_wall_time();
                
                long time_diff_12996 = time_end_12995 - time_start_12994;
                
                if (detail_timing) {
                    map_kernel_11522total_runtime += time_diff_12996;
                    map_kernel_11522runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "map_kernel_11522", (int) time_diff_12996);
                }
            }
        }
        
        struct memblock_device mem_11896;
        
        mem_11896.references = NULL;
        memblock_alloc_device(&mem_11896, bytes_11891);
        
        int32_t group_sizze_12567;
        int32_t num_groups_12568;
        
        group_sizze_12567 = cl_group_size;
        num_groups_12568 = squot32(partition_sizze_8940 +
                                   sext_i32_i32(group_sizze_12567) - 1,
                                   sext_i32_i32(group_sizze_12567));
        OPENCL_SUCCEED(clSetKernelArg(kernel_replicate_12564, 0,
                                      sizeof(res_8691), &res_8691));
        OPENCL_SUCCEED(clSetKernelArg(kernel_replicate_12564, 1,
                                      sizeof(partition_sizze_8940),
                                      &partition_sizze_8940));
        OPENCL_SUCCEED(clSetKernelArg(kernel_replicate_12564, 2,
                                      sizeof(mem_11896.mem), &mem_11896.mem));
        if (1 * (num_groups_12568 * group_sizze_12567) != 0) {
            const size_t global_work_sizze_12998[1] = {num_groups_12568 *
                         group_sizze_12567};
            const size_t local_work_sizze_13002[1] = {group_sizze_12567};
            int64_t time_start_12999, time_end_13000;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "kernel_replicate_12564");
                fprintf(stderr, "%zu", global_work_sizze_12998[0]);
                fprintf(stderr, "].\n");
                time_start_12999 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  kernel_replicate_12564, 1,
                                                  NULL, global_work_sizze_12998,
                                                  local_work_sizze_13002, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_13000 = get_wall_time();
                
                long time_diff_13001 = time_end_13000 - time_start_12999;
                
                if (detail_timing) {
                    kernel_replicate_12564total_runtime += time_diff_13001;
                    kernel_replicate_12564runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "kernel_replicate_12564", (int) time_diff_13001);
                }
            }
        }
        
        struct memblock_device mem_11899;
        
        mem_11899.references = NULL;
        memblock_alloc_device(&mem_11899, bytes_11891);
        
        int32_t group_sizze_12572;
        int32_t num_groups_12573;
        
        group_sizze_12572 = cl_group_size;
        num_groups_12573 = squot32(partition_sizze_8940 +
                                   sext_i32_i32(group_sizze_12572) - 1,
                                   sext_i32_i32(group_sizze_12572));
        OPENCL_SUCCEED(clSetKernelArg(kernel_replicate_12569, 0,
                                      sizeof(prime_8379), &prime_8379));
        OPENCL_SUCCEED(clSetKernelArg(kernel_replicate_12569, 1,
                                      sizeof(partition_sizze_8940),
                                      &partition_sizze_8940));
        OPENCL_SUCCEED(clSetKernelArg(kernel_replicate_12569, 2,
                                      sizeof(mem_11899.mem), &mem_11899.mem));
        if (1 * (num_groups_12573 * group_sizze_12572) != 0) {
            const size_t global_work_sizze_13003[1] = {num_groups_12573 *
                         group_sizze_12572};
            const size_t local_work_sizze_13007[1] = {group_sizze_12572};
            int64_t time_start_13004, time_end_13005;
            
            if (debugging) {
                fprintf(stderr, "Launching %s with global work size [",
                        "kernel_replicate_12569");
                fprintf(stderr, "%zu", global_work_sizze_13003[0]);
                fprintf(stderr, "].\n");
                time_start_13004 = get_wall_time();
            }
            OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue,
                                                  kernel_replicate_12569, 1,
                                                  NULL, global_work_sizze_13003,
                                                  local_work_sizze_13007, 0,
                                                  NULL, NULL));
            if (debugging) {
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
                time_end_13005 = get_wall_time();
                
                long time_diff_13006 = time_end_13005 - time_start_13004;
                
                if (detail_timing) {
                    kernel_replicate_12569total_runtime += time_diff_13006;
                    kernel_replicate_12569runs++;
                    fprintf(stderr, "kernel %s runtime: %ldus\n",
                            "kernel_replicate_12569", (int) time_diff_13006);
                }
            }
        }
        
        int32_t conc_tmp_8953 = sizze_8686 + partition_sizze_8940;
        int64_t binop_x_11901 = sext_i32_i64(conc_tmp_8953);
        int64_t bytes_11900 = binop_x_11901 * 4;
        struct memblock_device mem_11902;
        
        mem_11902.references = NULL;
        memblock_alloc_device(&mem_11902, bytes_11900);
        
        int32_t tmp_offs_12574 = 0;
        
        if (sizze_8686 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                               polys_mem_11835.mem,
                                               mem_11902.mem, 0,
                                               tmp_offs_12574 * 4, sizze_8686 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12574 += sizze_8686;
        if (partition_sizze_8940 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11896.mem,
                                               mem_11902.mem, 0,
                                               tmp_offs_12574 * 4,
                                               partition_sizze_8940 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12574 += partition_sizze_8940;
        
        struct memblock_device mem_11905;
        
        mem_11905.references = NULL;
        memblock_alloc_device(&mem_11905, bytes_11900);
        
        int32_t tmp_offs_12575 = 0;
        
        if (sizze_8686 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                               polys_mem_11837.mem,
                                               mem_11905.mem, 0,
                                               tmp_offs_12575 * 4, sizze_8686 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12575 += sizze_8686;
        if (partition_sizze_8940 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11899.mem,
                                               mem_11905.mem, 0,
                                               tmp_offs_12575 * 4,
                                               partition_sizze_8940 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12575 += partition_sizze_8940;
        
        struct memblock_device mem_11908;
        
        mem_11908.references = NULL;
        memblock_alloc_device(&mem_11908, bytes_11900);
        
        int32_t tmp_offs_12576 = 0;
        
        if (sizze_8686 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue,
                                               polys_mem_11839.mem,
                                               mem_11908.mem, 0,
                                               tmp_offs_12576 * 4, sizze_8686 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12576 += sizze_8686;
        if (partition_sizze_8940 * sizeof(int32_t) > 0) {
            OPENCL_SUCCEED(clEnqueueCopyBuffer(fut_cl_queue, mem_11893.mem,
                                               mem_11908.mem, 0,
                                               tmp_offs_12576 * 4,
                                               partition_sizze_8940 *
                                               sizeof(int32_t), 0, NULL, NULL));
            if (debugging)
                OPENCL_SUCCEED(clFinish(fut_cl_queue));
        }
        tmp_offs_12576 += partition_sizze_8940;
        
        int64_t polys_mem_sizze_tmp_12508 = bytes_11900;
        int64_t polys_mem_sizze_tmp_12509 = bytes_11900;
        int64_t polys_mem_sizze_tmp_12510 = bytes_11900;
        struct memblock_device polys_mem_tmp_12511;
        
        polys_mem_tmp_12511.references = NULL;
        memblock_set_device(&polys_mem_tmp_12511, &mem_11902);
        
        struct memblock_device polys_mem_tmp_12512;
        
        polys_mem_tmp_12512.references = NULL;
        memblock_set_device(&polys_mem_tmp_12512, &mem_11905);
        
        struct memblock_device polys_mem_tmp_12513;
        
        polys_mem_tmp_12513.references = NULL;
        memblock_set_device(&polys_mem_tmp_12513, &mem_11908);
        
        int32_t sizze_tmp_12514;
        
        sizze_tmp_12514 = conc_tmp_8953;
        polys_mem_sizze_11834 = polys_mem_sizze_tmp_12508;
        polys_mem_sizze_11836 = polys_mem_sizze_tmp_12509;
        polys_mem_sizze_11838 = polys_mem_sizze_tmp_12510;
        memblock_set_device(&polys_mem_11835, &polys_mem_tmp_12511);
        memblock_set_device(&polys_mem_11837, &polys_mem_tmp_12512);
        memblock_set_device(&polys_mem_11839, &polys_mem_tmp_12513);
        sizze_8686 = sizze_tmp_12514;
        memblock_unref_device(&mem_11842);
        memblock_unref_device(&mem_11845);
        memblock_unref_device(&mem_11847);
        memblock_unref_device(&mem_11850);
        memblock_unref_device(&mem_11881);
        memblock_unref_device(&mem_11853);
        memblock_unref_device(&mem_11856);
        memblock_unref_device(&mem_11859);
        memblock_unref_device(&mem_11862);
        memblock_unref_device(&mem_11865);
        memblock_unref_device(&mem_11868);
        memblock_unref_device(&mem_11871);
        memblock_unref_device(&mem_11874);
        memblock_unref_device(&mem_11887);
        memblock_unref_local(&mem_11884);
        memblock_unref_device(&mem_11890);
        memblock_unref_device(&mem_11893);
        memblock_unref_device(&mem_11896);
        memblock_unref_device(&mem_11899);
        memblock_unref_device(&mem_11902);
        memblock_unref_device(&mem_11905);
        memblock_unref_device(&mem_11908);
        memblock_unref_device(&polys_mem_tmp_12511);
        memblock_unref_device(&polys_mem_tmp_12512);
        memblock_unref_device(&polys_mem_tmp_12513);
    }
    memblock_set_device(&res_mem_11910, &polys_mem_11835);
    sizze_8682 = sizze_8686;
    res_mem_sizze_11909 = polys_mem_sizze_11834;
    memblock_set_device(&res_mem_11912, &polys_mem_11837);
    sizze_8682 = sizze_8686;
    res_mem_sizze_11911 = polys_mem_sizze_11836;
    memblock_set_device(&res_mem_11914, &polys_mem_11839);
    sizze_8682 = sizze_8686;
    res_mem_sizze_11913 = polys_mem_sizze_11838;
    
    int32_t arg_8957 = maxDegree_8380 + 1;
    int32_t x_11553 = sizze_8682 + y_10393;
    int32_t num_groups_11554 = squot32(x_11553, group_sizze_10391);
    int32_t num_threads_11555 = num_groups_11554 * group_sizze_10391;
    int64_t binop_x_11916 = sext_i32_i64(sizze_8682);
    int64_t bytes_11915 = binop_x_11916 * 4;
    struct memblock_device mem_11917;
    
    mem_11917.references = NULL;
    memblock_alloc_device(&mem_11917, bytes_11915);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11556, 0, sizeof(sizze_8682),
                                  &sizze_8682));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11556, 1, sizeof(arg_8957),
                                  &arg_8957));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11556, 2,
                                  sizeof(res_mem_11910.mem),
                                  &res_mem_11910.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11556, 3, sizeof(mem_11917.mem),
                                  &mem_11917.mem));
    if (1 * (num_groups_11554 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_13008[1] = {num_groups_11554 *
                     group_sizze_10391};
        const size_t local_work_sizze_13012[1] = {group_sizze_10391};
        int64_t time_start_13009, time_end_13010;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_11556");
            fprintf(stderr, "%zu", global_work_sizze_13008[0]);
            fprintf(stderr, "].\n");
            time_start_13009 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_11556, 1,
                                              NULL, global_work_sizze_13008,
                                              local_work_sizze_13012, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_13010 = get_wall_time();
            
            long time_diff_13011 = time_end_13010 - time_start_13009;
            
            if (detail_timing) {
                map_kernel_11556total_runtime += time_diff_13011;
                map_kernel_11556runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_11556", (int) time_diff_13011);
            }
        }
    }
    
    int32_t nesting_sizze_11528 = arg_8957 * sizze_8682;
    int32_t x_11532 = nesting_sizze_11528 + y_10393;
    int32_t num_groups_11533 = squot32(x_11532, group_sizze_10391);
    int32_t num_threads_11534 = num_groups_11533 * group_sizze_10391;
    int32_t convop_x_11919 = sizze_8682 * arg_8957;
    int64_t binop_x_11920 = sext_i32_i64(convop_x_11919);
    int64_t bytes_11918 = binop_x_11920 * 4;
    struct memblock_device mem_11921;
    
    mem_11921.references = NULL;
    memblock_alloc_device(&mem_11921, bytes_11918);
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11535, 0, sizeof(sizze_8682),
                                  &sizze_8682));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11535, 1, sizeof(arg_8957),
                                  &arg_8957));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11535, 2,
                                  sizeof(res_mem_11912.mem),
                                  &res_mem_11912.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11535, 3,
                                  sizeof(res_mem_11914.mem),
                                  &res_mem_11914.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11535, 4, sizeof(mem_11917.mem),
                                  &mem_11917.mem));
    OPENCL_SUCCEED(clSetKernelArg(map_kernel_11535, 5, sizeof(mem_11921.mem),
                                  &mem_11921.mem));
    if (1 * (num_groups_11533 * group_sizze_10391) != 0) {
        const size_t global_work_sizze_13013[1] = {num_groups_11533 *
                     group_sizze_10391};
        const size_t local_work_sizze_13017[1] = {group_sizze_10391};
        int64_t time_start_13014, time_end_13015;
        
        if (debugging) {
            fprintf(stderr, "Launching %s with global work size [",
                    "map_kernel_11535");
            fprintf(stderr, "%zu", global_work_sizze_13013[0]);
            fprintf(stderr, "].\n");
            time_start_13014 = get_wall_time();
        }
        OPENCL_SUCCEED(clEnqueueNDRangeKernel(fut_cl_queue, map_kernel_11535, 1,
                                              NULL, global_work_sizze_13013,
                                              local_work_sizze_13017, 0, NULL,
                                              NULL));
        if (debugging) {
            OPENCL_SUCCEED(clFinish(fut_cl_queue));
            time_end_13015 = get_wall_time();
            
            long time_diff_13016 = time_end_13015 - time_start_13014;
            
            if (detail_timing) {
                map_kernel_11535total_runtime += time_diff_13016;
                map_kernel_11535runs++;
                fprintf(stderr, "kernel %s runtime: %ldus\n",
                        "map_kernel_11535", (int) time_diff_13016);
            }
        }
    }
    memblock_set_device(&out_mem_12320, &mem_11921);
    out_arrsizze_12322 = sizze_8682;
    out_arrsizze_12323 = arg_8957;
    out_memsizze_12321 = bytes_11918;
    
    struct tuple_int32_t_device_mem_int32_t_int32_t retval_12850;
    
    retval_12850.elem_0 = out_memsizze_12321;
    retval_12850.elem_1.references = NULL;
    memblock_set_device(&retval_12850.elem_1, &out_mem_12320);
    retval_12850.elem_2 = out_arrsizze_12322;
    retval_12850.elem_3 = out_arrsizze_12323;
    memblock_unref_device(&out_mem_12320);
    memblock_unref_device(&mem_11614);
    memblock_unref_device(&mem_11617);
    memblock_unref_device(&mem_11623);
    memblock_unref_local(&mem_11620);
    memblock_unref_device(&mem_11629);
    memblock_unref_local(&mem_11626);
    memblock_unref_device(&mem_11632);
    memblock_unref_device(&mem_11635);
    memblock_unref_device(&mem_11638);
    memblock_unref_device(&mem_11641);
    memblock_unref_device(&mem_11644);
    memblock_unref_device(&mem_11647);
    memblock_unref_device(&mem_11653);
    memblock_unref_local(&mem_11650);
    memblock_unref_device(&mem_11659);
    memblock_unref_local(&mem_11656);
    memblock_unref_device(&mem_11662);
    memblock_unref_device(&mem_11665);
    memblock_unref_device(&mem_11668);
    memblock_unref_device(&mem_11671);
    memblock_unref_device(&mem_11674);
    memblock_unref_device(&mem_11677);
    memblock_unref_device(&mem_11683);
    memblock_unref_local(&mem_11680);
    memblock_unref_device(&mem_11689);
    memblock_unref_local(&mem_11686);
    memblock_unref_device(&mem_11692);
    memblock_unref_device(&mem_11695);
    memblock_unref_device(&mem_11698);
    memblock_unref_device(&mem_11701);
    memblock_unref_device(&mem_11704);
    memblock_unref_device(&mem_11707);
    memblock_unref_device(&mem_11713);
    memblock_unref_local(&mem_11710);
    memblock_unref_device(&mem_11719);
    memblock_unref_local(&mem_11716);
    memblock_unref_device(&mem_11722);
    memblock_unref_device(&mem_11725);
    memblock_unref_device(&mem_11728);
    memblock_unref_device(&mem_11731);
    memblock_unref_device(&mem_11734);
    memblock_unref_device(&mem_11737);
    memblock_unref_device(&mem_11746);
    memblock_unref_device(&mem_11740);
    memblock_unref_local(&mem_11743);
    memblock_unref_device(&mem_11752);
    memblock_unref_local(&mem_11749);
    memblock_unref_device(&mem_11755);
    memblock_unref_device(&mem_11758);
    memblock_unref_device(&mem_11761);
    memblock_unref_device(&mem_11764);
    memblock_unref_device(&mem_11767);
    memblock_unref_device(&mem_11770);
    memblock_unref_device(&mem_11779);
    memblock_unref_device(&mem_11773);
    memblock_unref_local(&mem_11776);
    memblock_unref_device(&mem_11785);
    memblock_unref_local(&mem_11782);
    memblock_unref_device(&mem_11788);
    memblock_unref_device(&mem_11791);
    memblock_unref_device(&mem_11794);
    memblock_unref_device(&mem_11797);
    memblock_unref_device(&mem_11800);
    memblock_unref_device(&mem_11803);
    memblock_unref_device(&mem_11806);
    memblock_unref_device(&mem_11809);
    memblock_unref_device(&mem_11812);
    memblock_unref_device(&mem_11815);
    memblock_unref_device(&res_mem_11829);
    memblock_unref_device(&res_mem_11831);
    memblock_unref_device(&res_mem_11833);
    memblock_unref_local(&mem_11878);
    memblock_unref_device(&res_mem_11910);
    memblock_unref_device(&res_mem_11912);
    memblock_unref_device(&res_mem_11914);
    memblock_unref_device(&polys_mem_11835);
    memblock_unref_device(&polys_mem_11837);
    memblock_unref_device(&polys_mem_11839);
    memblock_unref_device(&mem_11917);
    memblock_unref_device(&mem_11921);
    return retval_12850;
}
static FILE *runtime_file;
static int perform_warmup = 0;
static int num_runs = 1;
static const char *entry_point = "main";
int parse_options(int argc, char *const argv[])
{
    int ch;
    static struct option long_options[] = {{"write-runtime-to",
                                            required_argument, NULL, 1},
                                           {"runs", required_argument, NULL, 2},
                                           {"memory-reporting", no_argument,
                                            NULL, 3}, {"entry-point",
                                                       required_argument, NULL,
                                                       4}, {"binary-output",
                                                            no_argument, NULL,
                                                            5}, {"platform",
                                                                 required_argument,
                                                                 NULL, 6},
                                           {"device", required_argument, NULL,
                                            7}, {"synchronous", no_argument,
                                                 NULL, 8}, {"group-size",
                                                            required_argument,
                                                            NULL, 9},
                                           {"num-groups", required_argument,
                                            NULL, 10}, {"dump-opencl",
                                                        required_argument, NULL,
                                                        11}, {"load-opencl",
                                                              required_argument,
                                                              NULL, 12}, {0, 0,
                                                                          0,
                                                                          0}};
    
    while ((ch = getopt_long(argc, argv, ":t:r:me:bp:d:s", long_options,
                             NULL)) != -1) {
        if (ch == 1 || ch == 't') {
            runtime_file = fopen(optarg, "w");
            if (runtime_file == NULL)
                panic(1, "Cannot open %s: %s\n", optarg, strerror(errno));
        }
        if (ch == 2 || ch == 'r') {
            num_runs = atoi(optarg);
            perform_warmup = 1;
            if (num_runs <= 0)
                panic(1, "Need a positive number of runs, not %s\n", optarg);
        }
        if (ch == 3 || ch == 'm')
            detail_memory = 1;
        if (ch == 4 || ch == 'e')
            entry_point = optarg;
        if (ch == 5 || ch == 'b')
            binary_output = 1;
        if (ch == 6 || ch == 'p')
            set_preferred_platform(optarg);
        if (ch == 7 || ch == 'd')
            set_preferred_device(optarg);
        if (ch == 8 || ch == 's')
            cl_debug = debugging = 1;
        if (ch == 9)
            cl_group_size = atoi(optarg);
        if (ch == 10)
            cl_num_groups = atoi(optarg);
        if (ch == 11)
            cl_dump_program_to = optarg;
        if (ch == 12)
            cl_load_program_from = optarg;
        if (ch == ':')
            panic(-1, "Missing argument for option %s\n", argv[optind - 1]);
        if (ch == '?')
            panic(-1, "Unknown option %s\n", argv[optind - 1]);
    }
    return optind;
}
void entry_generatePolynomials()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t degree_7928;
    int32_t prime_7929;
    struct tuple_int32_t_device_mem_int32_t_int32_t_device_mem_int32_t_device_mem
    main_ret_13018;
    
    if (read_scalar(&i32, &degree_7928) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &prime_7929) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t out_memsizze_11965;
    struct memblock out_mem_11964;
    
    out_mem_11964.references = NULL;
    
    int32_t out_arrsizze_11966;
    int32_t out_memsizze_11968;
    struct memblock out_mem_11967;
    
    out_mem_11967.references = NULL;
    
    int32_t out_memsizze_11970;
    struct memblock out_mem_11969;
    
    out_mem_11969.references = NULL;
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_13018 = futhark_generatePolynomials(degree_7928, prime_7929);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&main_ret_13018.elem_1);
        memblock_unref_device(&main_ret_13018.elem_4);
        memblock_unref_device(&main_ret_13018.elem_6);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_13018 = futhark_generatePolynomials(degree_7928, prime_7929);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&main_ret_13018.elem_1);
            memblock_unref_device(&main_ret_13018.elem_4);
            memblock_unref_device(&main_ret_13018.elem_6);
        }
    }
    out_memsizze_11965 = main_ret_13018.elem_0;
    memblock_alloc(&out_mem_11964, main_ret_13018.elem_1.size);
    if (main_ret_13018.elem_1.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13018.elem_1.mem, CL_TRUE,
                                           0, main_ret_13018.elem_1.size,
                                           out_mem_11964.mem + 0, 0, NULL,
                                           NULL));
    out_arrsizze_11966 = main_ret_13018.elem_2;
    out_memsizze_11968 = main_ret_13018.elem_3;
    memblock_alloc(&out_mem_11967, main_ret_13018.elem_4.size);
    if (main_ret_13018.elem_4.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13018.elem_4.mem, CL_TRUE,
                                           0, main_ret_13018.elem_4.size,
                                           out_mem_11967.mem + 0, 0, NULL,
                                           NULL));
    out_memsizze_11970 = main_ret_13018.elem_5;
    memblock_alloc(&out_mem_11969, main_ret_13018.elem_6.size);
    if (main_ret_13018.elem_6.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13018.elem_6.mem, CL_TRUE,
                                           0, main_ret_13018.elem_6.size,
                                           out_mem_11969.mem + 0, 0, NULL,
                                           NULL));
    printf("#<opaque %s>", "[](i32, i32, i32)");
    printf("\n");
    memblock_unref_device(&main_ret_13018.elem_1);
    memblock_unref_device(&main_ret_13018.elem_4);
    memblock_unref_device(&main_ret_13018.elem_6);
}
void entry_isZeroable()
{
    int64_t t_start, t_end;
    int time_runs;
    int64_t pol_mem_sizze_11612;
    struct memblock pol_mem_11613;
    
    pol_mem_11613.references = NULL;
    memblock_alloc(&pol_mem_11613, 0);
    
    int32_t sizze_7966;
    int32_t p_7968;
    char main_ret_13019;
    
    {
        int64_t shape[1];
        
        errno = 0;
        if (read_array(&i32, (void **) &pol_mem_11613.mem, shape, 1) != 0)
            panic(1, "Failed reading input of type %s%s (errno: %s).\n", "[]",
                  i32.type_name, strerror(errno));
        sizze_7966 = shape[0];
        pol_mem_sizze_11612 = sizeof(int32_t) * shape[0];
        pol_mem_11613.size = pol_mem_sizze_11612;
    }
    if (read_scalar(&i32, &p_7968) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    char scalar_out_12000;
    
    if (perform_warmup) {
        time_runs = 0;
        
        struct memblock_device pol_mem_copy_13020;
        
        pol_mem_copy_13020.references = NULL;
        memblock_alloc_device(&pol_mem_copy_13020, pol_mem_11613.size);
        if (pol_mem_11613.size > 0)
            OPENCL_SUCCEED(clEnqueueWriteBuffer(fut_cl_queue,
                                                pol_mem_copy_13020.mem, CL_TRUE,
                                                0, pol_mem_11613.size,
                                                pol_mem_11613.mem + 0, 0, NULL,
                                                NULL));
        t_start = get_wall_time();
        main_ret_13019 = futhark_isZZeroable(pol_mem_sizze_11612,
                                             pol_mem_copy_13020, sizze_7966,
                                             p_7968);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&pol_mem_copy_13020);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        
        struct memblock_device pol_mem_copy_13020;
        
        pol_mem_copy_13020.references = NULL;
        memblock_alloc_device(&pol_mem_copy_13020, pol_mem_11613.size);
        if (pol_mem_11613.size > 0)
            OPENCL_SUCCEED(clEnqueueWriteBuffer(fut_cl_queue,
                                                pol_mem_copy_13020.mem, CL_TRUE,
                                                0, pol_mem_11613.size,
                                                pol_mem_11613.mem + 0, 0, NULL,
                                                NULL));
        t_start = get_wall_time();
        main_ret_13019 = futhark_isZZeroable(pol_mem_sizze_11612,
                                             pol_mem_copy_13020, sizze_7966,
                                             p_7968);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&pol_mem_copy_13020);
        }
    }
    memblock_unref(&pol_mem_11613);
    scalar_out_12000 = main_ret_13019;
    write_scalar(stdout, binary_output, &bool, &scalar_out_12000);
    printf("\n");
}
void entry_translate()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t padding_8004;
    int32_t d_8005;
    int32_t p_8006;
    int32_t x_8007;
    struct tuple_int32_t_device_mem_int32_t main_ret_13021;
    
    if (read_scalar(&i32, &padding_8004) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    
    int32_t out_memsizze_12012;
    struct memblock out_mem_12011;
    
    out_mem_12011.references = NULL;
    
    int32_t out_arrsizze_12013;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_13021 = futhark_translate(padding_8004, d_8005, p_8006,
                                           x_8007);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&main_ret_13021.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_13021 = futhark_translate(padding_8004, d_8005, p_8006,
                                           x_8007);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&main_ret_13021.elem_1);
        }
    }
    out_memsizze_12012 = main_ret_13021.elem_0;
    memblock_alloc(&out_mem_12011, main_ret_13021.elem_1.size);
    if (main_ret_13021.elem_1.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13021.elem_1.mem, CL_TRUE,
                                           0, main_ret_13021.elem_1.size,
                                           out_mem_12011.mem + 0, 0, NULL,
                                           NULL));
    out_arrsizze_12013 = main_ret_13021.elem_2;
    {
        int64_t shape[] = {out_arrsizze_12013};
        
        write_array(stdout, binary_output, &i32, out_mem_12011.mem, shape, 1);
    }
    printf("\n");
    memblock_unref_device(&main_ret_13021.elem_1);
}
void entry_generatePolynomialsTest()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t degree_8025;
    int32_t prime_8026;
    struct tuple_int32_t_device_mem_int32_t_int32_t main_ret_13022;
    
    if (read_scalar(&i32, &degree_8025) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &prime_8026) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t out_memsizze_12018;
    struct memblock out_mem_12017;
    
    out_mem_12017.references = NULL;
    
    int32_t out_arrsizze_12019;
    int32_t out_arrsizze_12020;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_13022 = futhark_generatePolynomialsTest(degree_8025,
                                                         prime_8026);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&main_ret_13022.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_13022 = futhark_generatePolynomialsTest(degree_8025,
                                                         prime_8026);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&main_ret_13022.elem_1);
        }
    }
    out_memsizze_12018 = main_ret_13022.elem_0;
    memblock_alloc(&out_mem_12017, main_ret_13022.elem_1.size);
    if (main_ret_13022.elem_1.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13022.elem_1.mem, CL_TRUE,
                                           0, main_ret_13022.elem_1.size,
                                           out_mem_12017.mem + 0, 0, NULL,
                                           NULL));
    out_arrsizze_12019 = main_ret_13022.elem_2;
    out_arrsizze_12020 = main_ret_13022.elem_3;
    {
        int64_t shape[] = {out_arrsizze_12019, out_arrsizze_12020};
        
        write_array(stdout, binary_output, &i32, out_mem_12017.mem, shape, 2);
    }
    printf("\n");
    memblock_unref_device(&main_ret_13022.elem_1);
}
void entry_translateTestTight()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_8068;
    int32_t p_8069;
    int32_t x_8070;
    struct tuple_int32_t_device_mem_int32_t main_ret_13023;
    
    if (read_scalar(&i32, &d_8068) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &p_8069) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &x_8070) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t out_memsizze_12055;
    struct memblock out_mem_12054;
    
    out_mem_12054.references = NULL;
    
    int32_t out_arrsizze_12056;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_13023 = futhark_translateTestTight(d_8068, p_8069, x_8070);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&main_ret_13023.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_13023 = futhark_translateTestTight(d_8068, p_8069, x_8070);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&main_ret_13023.elem_1);
        }
    }
    out_memsizze_12055 = main_ret_13023.elem_0;
    memblock_alloc(&out_mem_12054, main_ret_13023.elem_1.size);
    if (main_ret_13023.elem_1.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13023.elem_1.mem, CL_TRUE,
                                           0, main_ret_13023.elem_1.size,
                                           out_mem_12054.mem + 0, 0, NULL,
                                           NULL));
    out_arrsizze_12056 = main_ret_13023.elem_2;
    {
        int64_t shape[] = {out_arrsizze_12056};
        
        write_array(stdout, binary_output, &i32, out_mem_12054.mem, shape, 1);
    }
    printf("\n");
    memblock_unref_device(&main_ret_13023.elem_1);
}
void entry_translateTestLoose()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t d_8087;
    int32_t p_8088;
    int32_t x_8089;
    struct tuple_int32_t_device_mem_int32_t main_ret_13024;
    
    if (read_scalar(&i32, &d_8087) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &p_8088) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &x_8089) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t out_memsizze_12061;
    struct memblock out_mem_12060;
    
    out_mem_12060.references = NULL;
    
    int32_t out_arrsizze_12062;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_13024 = futhark_translateTestLoose(d_8087, p_8088, x_8089);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&main_ret_13024.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_13024 = futhark_translateTestLoose(d_8087, p_8088, x_8089);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&main_ret_13024.elem_1);
        }
    }
    out_memsizze_12061 = main_ret_13024.elem_0;
    memblock_alloc(&out_mem_12060, main_ret_13024.elem_1.size);
    if (main_ret_13024.elem_1.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13024.elem_1.mem, CL_TRUE,
                                           0, main_ret_13024.elem_1.size,
                                           out_mem_12060.mem + 0, 0, NULL,
                                           NULL));
    out_arrsizze_12062 = main_ret_13024.elem_2;
    {
        int64_t shape[] = {out_arrsizze_12062};
        
        write_array(stdout, binary_output, &i32, out_mem_12060.mem, shape, 1);
    }
    printf("\n");
    memblock_unref_device(&main_ret_13024.elem_1);
}
void entry_isIrreducible()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t bb_8107;
    int32_t bb_8108;
    int32_t bb_8109;
    int32_t aa_8110;
    int32_t aa_8111;
    int32_t aa_8112;
    int32_t prime_8113;
    int32_t padding_8114;
    char main_ret_13025;
    
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    panic(1, "Cannot read value of type %s\n", "(i32, i32, i32)");
    if (read_scalar(&i32, &prime_8113) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &padding_8114) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    char scalar_out_12066;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_13025 = futhark_isIrreducible(bb_8107, bb_8108, bb_8109,
                                               aa_8110, aa_8111, aa_8112,
                                               prime_8113, padding_8114);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_13025 = futhark_isIrreducible(bb_8107, bb_8108, bb_8109,
                                               aa_8110, aa_8111, aa_8112,
                                               prime_8113, padding_8114);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) { }
    }
    scalar_out_12066 = main_ret_13025;
    write_scalar(stdout, binary_output, &bool, &scalar_out_12066);
    printf("\n");
}
void entry_isIrreducibleTest()
{
    int64_t t_start, t_end;
    int time_runs;
    int64_t b_mem_sizze_11612;
    int64_t a_mem_sizze_11614;
    struct memblock b_mem_11613;
    
    b_mem_11613.references = NULL;
    memblock_alloc(&b_mem_11613, 0);
    
    struct memblock a_mem_11615;
    
    a_mem_11615.references = NULL;
    memblock_alloc(&a_mem_11615, 0);
    
    int32_t sizze_8235;
    int32_t sizze_8236;
    int32_t prime_8239;
    int32_t padding_8240;
    char main_ret_13026;
    
    {
        int64_t shape[1];
        
        errno = 0;
        if (read_array(&i32, (void **) &b_mem_11613.mem, shape, 1) != 0)
            panic(1, "Failed reading input of type %s%s (errno: %s).\n", "[]",
                  i32.type_name, strerror(errno));
        sizze_8235 = shape[0];
        b_mem_sizze_11612 = sizeof(int32_t) * shape[0];
        b_mem_11613.size = b_mem_sizze_11612;
    }
    {
        int64_t shape[1];
        
        errno = 0;
        if (read_array(&i32, (void **) &a_mem_11615.mem, shape, 1) != 0)
            panic(1, "Failed reading input of type %s%s (errno: %s).\n", "[]",
                  i32.type_name, strerror(errno));
        sizze_8236 = shape[0];
        a_mem_sizze_11614 = sizeof(int32_t) * shape[0];
        a_mem_11615.size = a_mem_sizze_11614;
    }
    if (read_scalar(&i32, &prime_8239) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &padding_8240) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    char scalar_out_12193;
    
    if (perform_warmup) {
        time_runs = 0;
        
        struct memblock_device b_mem_copy_13027;
        
        b_mem_copy_13027.references = NULL;
        memblock_alloc_device(&b_mem_copy_13027, b_mem_11613.size);
        if (b_mem_11613.size > 0)
            OPENCL_SUCCEED(clEnqueueWriteBuffer(fut_cl_queue,
                                                b_mem_copy_13027.mem, CL_TRUE,
                                                0, b_mem_11613.size,
                                                b_mem_11613.mem + 0, 0, NULL,
                                                NULL));
        
        struct memblock_device a_mem_copy_13028;
        
        a_mem_copy_13028.references = NULL;
        memblock_alloc_device(&a_mem_copy_13028, a_mem_11615.size);
        if (a_mem_11615.size > 0)
            OPENCL_SUCCEED(clEnqueueWriteBuffer(fut_cl_queue,
                                                a_mem_copy_13028.mem, CL_TRUE,
                                                0, a_mem_11615.size,
                                                a_mem_11615.mem + 0, 0, NULL,
                                                NULL));
        t_start = get_wall_time();
        main_ret_13026 = futhark_isIrreducibleTest(b_mem_sizze_11612,
                                                   a_mem_sizze_11614,
                                                   b_mem_copy_13027,
                                                   a_mem_copy_13028, sizze_8235,
                                                   sizze_8236, prime_8239,
                                                   padding_8240);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&b_mem_copy_13027);
        memblock_unref_device(&a_mem_copy_13028);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        
        struct memblock_device b_mem_copy_13027;
        
        b_mem_copy_13027.references = NULL;
        memblock_alloc_device(&b_mem_copy_13027, b_mem_11613.size);
        if (b_mem_11613.size > 0)
            OPENCL_SUCCEED(clEnqueueWriteBuffer(fut_cl_queue,
                                                b_mem_copy_13027.mem, CL_TRUE,
                                                0, b_mem_11613.size,
                                                b_mem_11613.mem + 0, 0, NULL,
                                                NULL));
        
        struct memblock_device a_mem_copy_13028;
        
        a_mem_copy_13028.references = NULL;
        memblock_alloc_device(&a_mem_copy_13028, a_mem_11615.size);
        if (a_mem_11615.size > 0)
            OPENCL_SUCCEED(clEnqueueWriteBuffer(fut_cl_queue,
                                                a_mem_copy_13028.mem, CL_TRUE,
                                                0, a_mem_11615.size,
                                                a_mem_11615.mem + 0, 0, NULL,
                                                NULL));
        t_start = get_wall_time();
        main_ret_13026 = futhark_isIrreducibleTest(b_mem_sizze_11612,
                                                   a_mem_sizze_11614,
                                                   b_mem_copy_13027,
                                                   a_mem_copy_13028, sizze_8235,
                                                   sizze_8236, prime_8239,
                                                   padding_8240);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&b_mem_copy_13027);
            memblock_unref_device(&a_mem_copy_13028);
        }
    }
    memblock_unref(&b_mem_11613);
    memblock_unref(&a_mem_11615);
    scalar_out_12193 = main_ret_13026;
    write_scalar(stdout, binary_output, &bool, &scalar_out_12193);
    printf("\n");
}
void entry_main()
{
    int64_t t_start, t_end;
    int time_runs;
    int32_t prime_8379;
    int32_t maxDegree_8380;
    struct tuple_int32_t_device_mem_int32_t_int32_t main_ret_13029;
    
    if (read_scalar(&i32, &prime_8379) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    if (read_scalar(&i32, &maxDegree_8380) != 0)
        panic(1, "Error when reading input of type %s (errno: %s).\n",
              i32.type_name, strerror(errno));
    
    int32_t out_memsizze_12321;
    struct memblock out_mem_12320;
    
    out_mem_12320.references = NULL;
    
    int32_t out_arrsizze_12322;
    int32_t out_arrsizze_12323;
    
    if (perform_warmup) {
        time_runs = 0;
        t_start = get_wall_time();
        main_ret_13029 = futhark_main(prime_8379, maxDegree_8380);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        memblock_unref_device(&main_ret_13029.elem_1);
    }
    time_runs = 1;
    /* Proper run. */
    for (int run = 0; run < num_runs; run++) {
        if (run == num_runs - 1)
            detail_timing = 1;
        t_start = get_wall_time();
        main_ret_13029 = futhark_main(prime_8379, maxDegree_8380);
        OPENCL_SUCCEED(clFinish(fut_cl_queue));
        t_end = get_wall_time();
        
        long elapsed_usec = t_end - t_start;
        
        if (time_runs && runtime_file != NULL)
            fprintf(runtime_file, "%ld\n", elapsed_usec);
        if (run < num_runs - 1) {
            memblock_unref_device(&main_ret_13029.elem_1);
        }
    }
    out_memsizze_12321 = main_ret_13029.elem_0;
    memblock_alloc(&out_mem_12320, main_ret_13029.elem_1.size);
    if (main_ret_13029.elem_1.size > 0)
        OPENCL_SUCCEED(clEnqueueReadBuffer(fut_cl_queue,
                                           main_ret_13029.elem_1.mem, CL_TRUE,
                                           0, main_ret_13029.elem_1.size,
                                           out_mem_12320.mem + 0, 0, NULL,
                                           NULL));
    out_arrsizze_12322 = main_ret_13029.elem_2;
    out_arrsizze_12323 = main_ret_13029.elem_3;
    {
        int64_t shape[] = {out_arrsizze_12322, out_arrsizze_12323};
        
        write_array(stdout, binary_output, &i32, out_mem_12320.mem, shape, 2);
    }
    printf("\n");
    memblock_unref_device(&main_ret_13029.elem_1);
}
typedef void entry_point_fun();
struct entry_point_entry {
    const char *name;
    entry_point_fun *fun;
} ;
int main(int argc, char **argv)
{
    fut_progname = argv[0];
    
    struct entry_point_entry entry_points[] = {{.name ="generatePolynomials",
                                                .fun =
                                                entry_generatePolynomials},
                                               {.name ="isZeroable", .fun =
                                                entry_isZeroable}, {.name =
                                                                    "translate",
                                                                    .fun =
                                                                    entry_translate},
                                               {.name =
                                                "generatePolynomialsTest",
                                                .fun =
                                                entry_generatePolynomialsTest},
                                               {.name ="translateTestTight",
                                                .fun =entry_translateTestTight},
                                               {.name ="translateTestLoose",
                                                .fun =entry_translateTestLoose},
                                               {.name ="isIrreducible", .fun =
                                                entry_isIrreducible}, {.name =
                                                                       "isIrreducibleTest",
                                                                       .fun =
                                                                       entry_isIrreducibleTest},
                                               {.name ="main", .fun =
                                                entry_main}};
    int parsed_options = parse_options(argc, argv);
    
    argc -= parsed_options;
    argv += parsed_options;
    setup_opencl_and_load_kernels();
    
    int num_entry_points = sizeof(entry_points) / sizeof(entry_points[0]);
    entry_point_fun *entry_point_fun = NULL;
    
    for (int i = 0; i < num_entry_points; i++) {
        if (strcmp(entry_points[i].name, entry_point) == 0) {
            entry_point_fun = entry_points[i].fun;
            break;
        }
    }
    if (entry_point_fun == NULL) {
        fprintf(stderr,
                "No entry point '%s'.  Select another with --entry-point.  Options are:\n",
                entry_point);
        for (int i = 0; i < num_entry_points; i++)
            fprintf(stderr, "%s\n", entry_points[i].name);
        return 1;
    }
    entry_point_fun();
    if (runtime_file != NULL)
        fclose(runtime_file);
    
    int total_runtime = 0;
    int total_runs = 0;
    
    if (debugging) {
        fprintf(stderr,
                "Kernel chunked_reduce_kernel_10338            executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                chunked_reduce_kernel_10338runs,
                (long) chunked_reduce_kernel_10338total_runtime /
                (chunked_reduce_kernel_10338runs !=
                 0 ? chunked_reduce_kernel_10338runs : 1),
                (long) chunked_reduce_kernel_10338total_runtime);
        total_runtime += chunked_reduce_kernel_10338total_runtime;
        total_runs += chunked_reduce_kernel_10338runs;
        fprintf(stderr,
                "Kernel chunked_reduce_kernel_9116             executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                chunked_reduce_kernel_9116runs,
                (long) chunked_reduce_kernel_9116total_runtime /
                (chunked_reduce_kernel_9116runs !=
                 0 ? chunked_reduce_kernel_9116runs : 1),
                (long) chunked_reduce_kernel_9116total_runtime);
        total_runtime += chunked_reduce_kernel_9116total_runtime;
        total_runs += chunked_reduce_kernel_9116runs;
        fprintf(stderr,
                "Kernel chunked_reduce_kernel_9818             executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                chunked_reduce_kernel_9818runs,
                (long) chunked_reduce_kernel_9818total_runtime /
                (chunked_reduce_kernel_9818runs !=
                 0 ? chunked_reduce_kernel_9818runs : 1),
                (long) chunked_reduce_kernel_9818total_runtime);
        total_runtime += chunked_reduce_kernel_9818total_runtime;
        total_runs += chunked_reduce_kernel_9818runs;
        fprintf(stderr,
                "Kernel fut_kernel_map_transpose_i32           executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                fut_kernel_map_transpose_i32runs,
                (long) fut_kernel_map_transpose_i32total_runtime /
                (fut_kernel_map_transpose_i32runs !=
                 0 ? fut_kernel_map_transpose_i32runs : 1),
                (long) fut_kernel_map_transpose_i32total_runtime);
        total_runtime += fut_kernel_map_transpose_i32total_runtime;
        total_runs += fut_kernel_map_transpose_i32runs;
        fprintf(stderr,
                "Kernel fut_kernel_map_transpose_lowheight_i32 executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                fut_kernel_map_transpose_lowheight_i32runs,
                (long) fut_kernel_map_transpose_lowheight_i32total_runtime /
                (fut_kernel_map_transpose_lowheight_i32runs !=
                 0 ? fut_kernel_map_transpose_lowheight_i32runs : 1),
                (long) fut_kernel_map_transpose_lowheight_i32total_runtime);
        total_runtime += fut_kernel_map_transpose_lowheight_i32total_runtime;
        total_runs += fut_kernel_map_transpose_lowheight_i32runs;
        fprintf(stderr,
                "Kernel fut_kernel_map_transpose_lowwidth_i32  executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                fut_kernel_map_transpose_lowwidth_i32runs,
                (long) fut_kernel_map_transpose_lowwidth_i32total_runtime /
                (fut_kernel_map_transpose_lowwidth_i32runs !=
                 0 ? fut_kernel_map_transpose_lowwidth_i32runs : 1),
                (long) fut_kernel_map_transpose_lowwidth_i32total_runtime);
        total_runtime += fut_kernel_map_transpose_lowwidth_i32total_runtime;
        total_runs += fut_kernel_map_transpose_lowwidth_i32runs;
        fprintf(stderr,
                "Kernel kernel_replicate_12564                 executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                kernel_replicate_12564runs,
                (long) kernel_replicate_12564total_runtime /
                (kernel_replicate_12564runs !=
                 0 ? kernel_replicate_12564runs : 1),
                (long) kernel_replicate_12564total_runtime);
        total_runtime += kernel_replicate_12564total_runtime;
        total_runs += kernel_replicate_12564runs;
        fprintf(stderr,
                "Kernel kernel_replicate_12569                 executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                kernel_replicate_12569runs,
                (long) kernel_replicate_12569total_runtime /
                (kernel_replicate_12569runs !=
                 0 ? kernel_replicate_12569runs : 1),
                (long) kernel_replicate_12569total_runtime);
        total_runtime += kernel_replicate_12569total_runtime;
        total_runs += kernel_replicate_12569runs;
        fprintf(stderr,
                "Kernel map_kernel_10129                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10129runs, (long) map_kernel_10129total_runtime /
                (map_kernel_10129runs != 0 ? map_kernel_10129runs : 1),
                (long) map_kernel_10129total_runtime);
        total_runtime += map_kernel_10129total_runtime;
        total_runs += map_kernel_10129runs;
        fprintf(stderr,
                "Kernel map_kernel_10220                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10220runs, (long) map_kernel_10220total_runtime /
                (map_kernel_10220runs != 0 ? map_kernel_10220runs : 1),
                (long) map_kernel_10220total_runtime);
        total_runtime += map_kernel_10220total_runtime;
        total_runs += map_kernel_10220runs;
        fprintf(stderr,
                "Kernel map_kernel_10310                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10310runs, (long) map_kernel_10310total_runtime /
                (map_kernel_10310runs != 0 ? map_kernel_10310runs : 1),
                (long) map_kernel_10310total_runtime);
        total_runtime += map_kernel_10310total_runtime;
        total_runs += map_kernel_10310runs;
        fprintf(stderr,
                "Kernel map_kernel_10380                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10380runs, (long) map_kernel_10380total_runtime /
                (map_kernel_10380runs != 0 ? map_kernel_10380runs : 1),
                (long) map_kernel_10380total_runtime);
        total_runtime += map_kernel_10380total_runtime;
        total_runs += map_kernel_10380runs;
        fprintf(stderr,
                "Kernel map_kernel_10484                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10484runs, (long) map_kernel_10484total_runtime /
                (map_kernel_10484runs != 0 ? map_kernel_10484runs : 1),
                (long) map_kernel_10484total_runtime);
        total_runtime += map_kernel_10484total_runtime;
        total_runs += map_kernel_10484runs;
        fprintf(stderr,
                "Kernel map_kernel_10494                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10494runs, (long) map_kernel_10494total_runtime /
                (map_kernel_10494runs != 0 ? map_kernel_10494runs : 1),
                (long) map_kernel_10494total_runtime);
        total_runtime += map_kernel_10494total_runtime;
        total_runs += map_kernel_10494runs;
        fprintf(stderr,
                "Kernel map_kernel_10590                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10590runs, (long) map_kernel_10590total_runtime /
                (map_kernel_10590runs != 0 ? map_kernel_10590runs : 1),
                (long) map_kernel_10590total_runtime);
        total_runtime += map_kernel_10590total_runtime;
        total_runs += map_kernel_10590runs;
        fprintf(stderr,
                "Kernel map_kernel_10600                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10600runs, (long) map_kernel_10600total_runtime /
                (map_kernel_10600runs != 0 ? map_kernel_10600runs : 1),
                (long) map_kernel_10600total_runtime);
        total_runtime += map_kernel_10600total_runtime;
        total_runs += map_kernel_10600runs;
        fprintf(stderr,
                "Kernel map_kernel_10696                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10696runs, (long) map_kernel_10696total_runtime /
                (map_kernel_10696runs != 0 ? map_kernel_10696runs : 1),
                (long) map_kernel_10696total_runtime);
        total_runtime += map_kernel_10696total_runtime;
        total_runs += map_kernel_10696runs;
        fprintf(stderr,
                "Kernel map_kernel_10706                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10706runs, (long) map_kernel_10706total_runtime /
                (map_kernel_10706runs != 0 ? map_kernel_10706runs : 1),
                (long) map_kernel_10706total_runtime);
        total_runtime += map_kernel_10706total_runtime;
        total_runs += map_kernel_10706runs;
        fprintf(stderr,
                "Kernel map_kernel_10802                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10802runs, (long) map_kernel_10802total_runtime /
                (map_kernel_10802runs != 0 ? map_kernel_10802runs : 1),
                (long) map_kernel_10802total_runtime);
        total_runtime += map_kernel_10802total_runtime;
        total_runs += map_kernel_10802runs;
        fprintf(stderr,
                "Kernel map_kernel_10812                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10812runs, (long) map_kernel_10812total_runtime /
                (map_kernel_10812runs != 0 ? map_kernel_10812runs : 1),
                (long) map_kernel_10812total_runtime);
        total_runtime += map_kernel_10812total_runtime;
        total_runs += map_kernel_10812runs;
        fprintf(stderr,
                "Kernel map_kernel_10985                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10985runs, (long) map_kernel_10985total_runtime /
                (map_kernel_10985runs != 0 ? map_kernel_10985runs : 1),
                (long) map_kernel_10985total_runtime);
        total_runtime += map_kernel_10985total_runtime;
        total_runs += map_kernel_10985runs;
        fprintf(stderr,
                "Kernel map_kernel_10995                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_10995runs, (long) map_kernel_10995total_runtime /
                (map_kernel_10995runs != 0 ? map_kernel_10995runs : 1),
                (long) map_kernel_10995total_runtime);
        total_runtime += map_kernel_10995total_runtime;
        total_runs += map_kernel_10995runs;
        fprintf(stderr,
                "Kernel map_kernel_11168                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_11168runs, (long) map_kernel_11168total_runtime /
                (map_kernel_11168runs != 0 ? map_kernel_11168runs : 1),
                (long) map_kernel_11168total_runtime);
        total_runtime += map_kernel_11168total_runtime;
        total_runs += map_kernel_11168runs;
        fprintf(stderr,
                "Kernel map_kernel_11178                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_11178runs, (long) map_kernel_11178total_runtime /
                (map_kernel_11178runs != 0 ? map_kernel_11178runs : 1),
                (long) map_kernel_11178total_runtime);
        total_runtime += map_kernel_11178total_runtime;
        total_runs += map_kernel_11178runs;
        fprintf(stderr,
                "Kernel map_kernel_11512                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_11512runs, (long) map_kernel_11512total_runtime /
                (map_kernel_11512runs != 0 ? map_kernel_11512runs : 1),
                (long) map_kernel_11512total_runtime);
        total_runtime += map_kernel_11512total_runtime;
        total_runs += map_kernel_11512runs;
        fprintf(stderr,
                "Kernel map_kernel_11522                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_11522runs, (long) map_kernel_11522total_runtime /
                (map_kernel_11522runs != 0 ? map_kernel_11522runs : 1),
                (long) map_kernel_11522total_runtime);
        total_runtime += map_kernel_11522total_runtime;
        total_runs += map_kernel_11522runs;
        fprintf(stderr,
                "Kernel map_kernel_11535                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_11535runs, (long) map_kernel_11535total_runtime /
                (map_kernel_11535runs != 0 ? map_kernel_11535runs : 1),
                (long) map_kernel_11535total_runtime);
        total_runtime += map_kernel_11535total_runtime;
        total_runs += map_kernel_11535runs;
        fprintf(stderr,
                "Kernel map_kernel_11556                       executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_11556runs, (long) map_kernel_11556total_runtime /
                (map_kernel_11556runs != 0 ? map_kernel_11556runs : 1),
                (long) map_kernel_11556total_runtime);
        total_runtime += map_kernel_11556total_runtime;
        total_runs += map_kernel_11556runs;
        fprintf(stderr,
                "Kernel map_kernel_9072                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9072runs, (long) map_kernel_9072total_runtime /
                (map_kernel_9072runs != 0 ? map_kernel_9072runs : 1),
                (long) map_kernel_9072total_runtime);
        total_runtime += map_kernel_9072total_runtime;
        total_runs += map_kernel_9072runs;
        fprintf(stderr,
                "Kernel map_kernel_9082                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9082runs, (long) map_kernel_9082total_runtime /
                (map_kernel_9082runs != 0 ? map_kernel_9082runs : 1),
                (long) map_kernel_9082total_runtime);
        total_runtime += map_kernel_9082total_runtime;
        total_runs += map_kernel_9082runs;
        fprintf(stderr,
                "Kernel map_kernel_9184                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9184runs, (long) map_kernel_9184total_runtime /
                (map_kernel_9184runs != 0 ? map_kernel_9184runs : 1),
                (long) map_kernel_9184total_runtime);
        total_runtime += map_kernel_9184total_runtime;
        total_runs += map_kernel_9184runs;
        fprintf(stderr,
                "Kernel map_kernel_9288                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9288runs, (long) map_kernel_9288total_runtime /
                (map_kernel_9288runs != 0 ? map_kernel_9288runs : 1),
                (long) map_kernel_9288total_runtime);
        total_runtime += map_kernel_9288total_runtime;
        total_runs += map_kernel_9288runs;
        fprintf(stderr,
                "Kernel map_kernel_9298                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9298runs, (long) map_kernel_9298total_runtime /
                (map_kernel_9298runs != 0 ? map_kernel_9298runs : 1),
                (long) map_kernel_9298total_runtime);
        total_runtime += map_kernel_9298total_runtime;
        total_runs += map_kernel_9298runs;
        fprintf(stderr,
                "Kernel map_kernel_9308                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9308runs, (long) map_kernel_9308total_runtime /
                (map_kernel_9308runs != 0 ? map_kernel_9308runs : 1),
                (long) map_kernel_9308total_runtime);
        total_runtime += map_kernel_9308total_runtime;
        total_runs += map_kernel_9308runs;
        fprintf(stderr,
                "Kernel map_kernel_9322                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9322runs, (long) map_kernel_9322total_runtime /
                (map_kernel_9322runs != 0 ? map_kernel_9322runs : 1),
                (long) map_kernel_9322total_runtime);
        total_runtime += map_kernel_9322total_runtime;
        total_runs += map_kernel_9322runs;
        fprintf(stderr,
                "Kernel map_kernel_9340                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9340runs, (long) map_kernel_9340total_runtime /
                (map_kernel_9340runs != 0 ? map_kernel_9340runs : 1),
                (long) map_kernel_9340total_runtime);
        total_runtime += map_kernel_9340total_runtime;
        total_runs += map_kernel_9340runs;
        fprintf(stderr,
                "Kernel map_kernel_9460                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9460runs, (long) map_kernel_9460total_runtime /
                (map_kernel_9460runs != 0 ? map_kernel_9460runs : 1),
                (long) map_kernel_9460total_runtime);
        total_runtime += map_kernel_9460total_runtime;
        total_runs += map_kernel_9460runs;
        fprintf(stderr,
                "Kernel map_kernel_9609                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9609runs, (long) map_kernel_9609total_runtime /
                (map_kernel_9609runs != 0 ? map_kernel_9609runs : 1),
                (long) map_kernel_9609total_runtime);
        total_runtime += map_kernel_9609total_runtime;
        total_runs += map_kernel_9609runs;
        fprintf(stderr,
                "Kernel map_kernel_9700                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9700runs, (long) map_kernel_9700total_runtime /
                (map_kernel_9700runs != 0 ? map_kernel_9700runs : 1),
                (long) map_kernel_9700total_runtime);
        total_runtime += map_kernel_9700total_runtime;
        total_runs += map_kernel_9700runs;
        fprintf(stderr,
                "Kernel map_kernel_9790                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9790runs, (long) map_kernel_9790total_runtime /
                (map_kernel_9790runs != 0 ? map_kernel_9790runs : 1),
                (long) map_kernel_9790total_runtime);
        total_runtime += map_kernel_9790total_runtime;
        total_runs += map_kernel_9790runs;
        fprintf(stderr,
                "Kernel map_kernel_9860                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9860runs, (long) map_kernel_9860total_runtime /
                (map_kernel_9860runs != 0 ? map_kernel_9860runs : 1),
                (long) map_kernel_9860total_runtime);
        total_runtime += map_kernel_9860total_runtime;
        total_runs += map_kernel_9860runs;
        fprintf(stderr,
                "Kernel map_kernel_9980                        executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                map_kernel_9980runs, (long) map_kernel_9980total_runtime /
                (map_kernel_9980runs != 0 ? map_kernel_9980runs : 1),
                (long) map_kernel_9980total_runtime);
        total_runtime += map_kernel_9980total_runtime;
        total_runs += map_kernel_9980runs;
        fprintf(stderr,
                "Kernel reduce_kernel_10365                    executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                reduce_kernel_10365runs,
                (long) reduce_kernel_10365total_runtime /
                (reduce_kernel_10365runs != 0 ? reduce_kernel_10365runs : 1),
                (long) reduce_kernel_10365total_runtime);
        total_runtime += reduce_kernel_10365total_runtime;
        total_runs += reduce_kernel_10365runs;
        fprintf(stderr,
                "Kernel reduce_kernel_9169                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                reduce_kernel_9169runs, (long) reduce_kernel_9169total_runtime /
                (reduce_kernel_9169runs != 0 ? reduce_kernel_9169runs : 1),
                (long) reduce_kernel_9169total_runtime);
        total_runtime += reduce_kernel_9169total_runtime;
        total_runs += reduce_kernel_9169runs;
        fprintf(stderr,
                "Kernel reduce_kernel_9845                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                reduce_kernel_9845runs, (long) reduce_kernel_9845total_runtime /
                (reduce_kernel_9845runs != 0 ? reduce_kernel_9845runs : 1),
                (long) reduce_kernel_9845total_runtime);
        total_runtime += reduce_kernel_9845total_runtime;
        total_runs += reduce_kernel_9845runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10040                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10040runs, (long) scan1_kernel_10040total_runtime /
                (scan1_kernel_10040runs != 0 ? scan1_kernel_10040runs : 1),
                (long) scan1_kernel_10040total_runtime);
        total_runtime += scan1_kernel_10040total_runtime;
        total_runs += scan1_kernel_10040runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10162                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10162runs, (long) scan1_kernel_10162total_runtime /
                (scan1_kernel_10162runs != 0 ? scan1_kernel_10162runs : 1),
                (long) scan1_kernel_10162total_runtime);
        total_runtime += scan1_kernel_10162total_runtime;
        total_runs += scan1_kernel_10162runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10252                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10252runs, (long) scan1_kernel_10252total_runtime /
                (scan1_kernel_10252runs != 0 ? scan1_kernel_10252runs : 1),
                (long) scan1_kernel_10252total_runtime);
        total_runtime += scan1_kernel_10252total_runtime;
        total_runs += scan1_kernel_10252runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10423                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10423runs, (long) scan1_kernel_10423total_runtime /
                (scan1_kernel_10423runs != 0 ? scan1_kernel_10423runs : 1),
                (long) scan1_kernel_10423total_runtime);
        total_runtime += scan1_kernel_10423total_runtime;
        total_runs += scan1_kernel_10423runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10529                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10529runs, (long) scan1_kernel_10529total_runtime /
                (scan1_kernel_10529runs != 0 ? scan1_kernel_10529runs : 1),
                (long) scan1_kernel_10529total_runtime);
        total_runtime += scan1_kernel_10529total_runtime;
        total_runs += scan1_kernel_10529runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10635                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10635runs, (long) scan1_kernel_10635total_runtime /
                (scan1_kernel_10635runs != 0 ? scan1_kernel_10635runs : 1),
                (long) scan1_kernel_10635total_runtime);
        total_runtime += scan1_kernel_10635total_runtime;
        total_runs += scan1_kernel_10635runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10741                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10741runs, (long) scan1_kernel_10741total_runtime /
                (scan1_kernel_10741runs != 0 ? scan1_kernel_10741runs : 1),
                (long) scan1_kernel_10741total_runtime);
        total_runtime += scan1_kernel_10741total_runtime;
        total_runs += scan1_kernel_10741runs;
        fprintf(stderr,
                "Kernel scan1_kernel_10922                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_10922runs, (long) scan1_kernel_10922total_runtime /
                (scan1_kernel_10922runs != 0 ? scan1_kernel_10922runs : 1),
                (long) scan1_kernel_10922total_runtime);
        total_runtime += scan1_kernel_10922total_runtime;
        total_runs += scan1_kernel_10922runs;
        fprintf(stderr,
                "Kernel scan1_kernel_11105                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_11105runs, (long) scan1_kernel_11105total_runtime /
                (scan1_kernel_11105runs != 0 ? scan1_kernel_11105runs : 1),
                (long) scan1_kernel_11105total_runtime);
        total_runtime += scan1_kernel_11105total_runtime;
        total_runs += scan1_kernel_11105runs;
        fprintf(stderr,
                "Kernel scan1_kernel_11445                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_11445runs, (long) scan1_kernel_11445total_runtime /
                (scan1_kernel_11445runs != 0 ? scan1_kernel_11445runs : 1),
                (long) scan1_kernel_11445total_runtime);
        total_runtime += scan1_kernel_11445total_runtime;
        total_runs += scan1_kernel_11445runs;
        fprintf(stderr,
                "Kernel scan1_kernel_9011                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_9011runs, (long) scan1_kernel_9011total_runtime /
                (scan1_kernel_9011runs != 0 ? scan1_kernel_9011runs : 1),
                (long) scan1_kernel_9011total_runtime);
        total_runtime += scan1_kernel_9011total_runtime;
        total_runs += scan1_kernel_9011runs;
        fprintf(stderr,
                "Kernel scan1_kernel_9227                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_9227runs, (long) scan1_kernel_9227total_runtime /
                (scan1_kernel_9227runs != 0 ? scan1_kernel_9227runs : 1),
                (long) scan1_kernel_9227total_runtime);
        total_runtime += scan1_kernel_9227total_runtime;
        total_runs += scan1_kernel_9227runs;
        fprintf(stderr,
                "Kernel scan1_kernel_9396                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_9396runs, (long) scan1_kernel_9396total_runtime /
                (scan1_kernel_9396runs != 0 ? scan1_kernel_9396runs : 1),
                (long) scan1_kernel_9396total_runtime);
        total_runtime += scan1_kernel_9396total_runtime;
        total_runs += scan1_kernel_9396runs;
        fprintf(stderr,
                "Kernel scan1_kernel_9520                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_9520runs, (long) scan1_kernel_9520total_runtime /
                (scan1_kernel_9520runs != 0 ? scan1_kernel_9520runs : 1),
                (long) scan1_kernel_9520total_runtime);
        total_runtime += scan1_kernel_9520total_runtime;
        total_runs += scan1_kernel_9520runs;
        fprintf(stderr,
                "Kernel scan1_kernel_9642                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_9642runs, (long) scan1_kernel_9642total_runtime /
                (scan1_kernel_9642runs != 0 ? scan1_kernel_9642runs : 1),
                (long) scan1_kernel_9642total_runtime);
        total_runtime += scan1_kernel_9642total_runtime;
        total_runs += scan1_kernel_9642runs;
        fprintf(stderr,
                "Kernel scan1_kernel_9732                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_9732runs, (long) scan1_kernel_9732total_runtime /
                (scan1_kernel_9732runs != 0 ? scan1_kernel_9732runs : 1),
                (long) scan1_kernel_9732total_runtime);
        total_runtime += scan1_kernel_9732total_runtime;
        total_runs += scan1_kernel_9732runs;
        fprintf(stderr,
                "Kernel scan1_kernel_9916                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan1_kernel_9916runs, (long) scan1_kernel_9916total_runtime /
                (scan1_kernel_9916runs != 0 ? scan1_kernel_9916runs : 1),
                (long) scan1_kernel_9916total_runtime);
        total_runtime += scan1_kernel_9916total_runtime;
        total_runs += scan1_kernel_9916runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10093                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10093runs, (long) scan2_kernel_10093total_runtime /
                (scan2_kernel_10093runs != 0 ? scan2_kernel_10093runs : 1),
                (long) scan2_kernel_10093total_runtime);
        total_runtime += scan2_kernel_10093total_runtime;
        total_runs += scan2_kernel_10093runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10194                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10194runs, (long) scan2_kernel_10194total_runtime /
                (scan2_kernel_10194runs != 0 ? scan2_kernel_10194runs : 1),
                (long) scan2_kernel_10194total_runtime);
        total_runtime += scan2_kernel_10194total_runtime;
        total_runs += scan2_kernel_10194runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10284                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10284runs, (long) scan2_kernel_10284total_runtime /
                (scan2_kernel_10284runs != 0 ? scan2_kernel_10284runs : 1),
                (long) scan2_kernel_10284total_runtime);
        total_runtime += scan2_kernel_10284total_runtime;
        total_runs += scan2_kernel_10284runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10458                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10458runs, (long) scan2_kernel_10458total_runtime /
                (scan2_kernel_10458runs != 0 ? scan2_kernel_10458runs : 1),
                (long) scan2_kernel_10458total_runtime);
        total_runtime += scan2_kernel_10458total_runtime;
        total_runs += scan2_kernel_10458runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10564                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10564runs, (long) scan2_kernel_10564total_runtime /
                (scan2_kernel_10564runs != 0 ? scan2_kernel_10564runs : 1),
                (long) scan2_kernel_10564total_runtime);
        total_runtime += scan2_kernel_10564total_runtime;
        total_runs += scan2_kernel_10564runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10670                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10670runs, (long) scan2_kernel_10670total_runtime /
                (scan2_kernel_10670runs != 0 ? scan2_kernel_10670runs : 1),
                (long) scan2_kernel_10670total_runtime);
        total_runtime += scan2_kernel_10670total_runtime;
        total_runs += scan2_kernel_10670runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10776                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10776runs, (long) scan2_kernel_10776total_runtime /
                (scan2_kernel_10776runs != 0 ? scan2_kernel_10776runs : 1),
                (long) scan2_kernel_10776total_runtime);
        total_runtime += scan2_kernel_10776total_runtime;
        total_runs += scan2_kernel_10776runs;
        fprintf(stderr,
                "Kernel scan2_kernel_10959                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_10959runs, (long) scan2_kernel_10959total_runtime /
                (scan2_kernel_10959runs != 0 ? scan2_kernel_10959runs : 1),
                (long) scan2_kernel_10959total_runtime);
        total_runtime += scan2_kernel_10959total_runtime;
        total_runs += scan2_kernel_10959runs;
        fprintf(stderr,
                "Kernel scan2_kernel_11142                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_11142runs, (long) scan2_kernel_11142total_runtime /
                (scan2_kernel_11142runs != 0 ? scan2_kernel_11142runs : 1),
                (long) scan2_kernel_11142total_runtime);
        total_runtime += scan2_kernel_11142total_runtime;
        total_runs += scan2_kernel_11142runs;
        fprintf(stderr,
                "Kernel scan2_kernel_11486                     executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_11486runs, (long) scan2_kernel_11486total_runtime /
                (scan2_kernel_11486runs != 0 ? scan2_kernel_11486runs : 1),
                (long) scan2_kernel_11486total_runtime);
        total_runtime += scan2_kernel_11486total_runtime;
        total_runs += scan2_kernel_11486runs;
        fprintf(stderr,
                "Kernel scan2_kernel_9046                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_9046runs, (long) scan2_kernel_9046total_runtime /
                (scan2_kernel_9046runs != 0 ? scan2_kernel_9046runs : 1),
                (long) scan2_kernel_9046total_runtime);
        total_runtime += scan2_kernel_9046total_runtime;
        total_runs += scan2_kernel_9046runs;
        fprintf(stderr,
                "Kernel scan2_kernel_9262                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_9262runs, (long) scan2_kernel_9262total_runtime /
                (scan2_kernel_9262runs != 0 ? scan2_kernel_9262runs : 1),
                (long) scan2_kernel_9262total_runtime);
        total_runtime += scan2_kernel_9262total_runtime;
        total_runs += scan2_kernel_9262runs;
        fprintf(stderr,
                "Kernel scan2_kernel_9434                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_9434runs, (long) scan2_kernel_9434total_runtime /
                (scan2_kernel_9434runs != 0 ? scan2_kernel_9434runs : 1),
                (long) scan2_kernel_9434total_runtime);
        total_runtime += scan2_kernel_9434total_runtime;
        total_runs += scan2_kernel_9434runs;
        fprintf(stderr,
                "Kernel scan2_kernel_9573                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_9573runs, (long) scan2_kernel_9573total_runtime /
                (scan2_kernel_9573runs != 0 ? scan2_kernel_9573runs : 1),
                (long) scan2_kernel_9573total_runtime);
        total_runtime += scan2_kernel_9573total_runtime;
        total_runs += scan2_kernel_9573runs;
        fprintf(stderr,
                "Kernel scan2_kernel_9674                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_9674runs, (long) scan2_kernel_9674total_runtime /
                (scan2_kernel_9674runs != 0 ? scan2_kernel_9674runs : 1),
                (long) scan2_kernel_9674total_runtime);
        total_runtime += scan2_kernel_9674total_runtime;
        total_runs += scan2_kernel_9674runs;
        fprintf(stderr,
                "Kernel scan2_kernel_9764                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_9764runs, (long) scan2_kernel_9764total_runtime /
                (scan2_kernel_9764runs != 0 ? scan2_kernel_9764runs : 1),
                (long) scan2_kernel_9764total_runtime);
        total_runtime += scan2_kernel_9764total_runtime;
        total_runs += scan2_kernel_9764runs;
        fprintf(stderr,
                "Kernel scan2_kernel_9954                      executed %6d times, with average runtime: %6ldus\tand total runtime: %6ldus\n",
                scan2_kernel_9954runs, (long) scan2_kernel_9954total_runtime /
                (scan2_kernel_9954runs != 0 ? scan2_kernel_9954runs : 1),
                (long) scan2_kernel_9954total_runtime);
        total_runtime += scan2_kernel_9954total_runtime;
        total_runs += scan2_kernel_9954runs;
    }
    if (debugging)
        fprintf(stderr, "Ran %d kernels with cumulative runtime: %6ldus\n",
                total_runs, total_runtime);
    if (detail_memory) {
        fprintf(stderr, "Peak memory usage for space 'device': %ld bytes.\n",
                peak_mem_usage_device);
        fprintf(stderr, "Peak memory usage for space 'local': %ld bytes.\n",
                peak_mem_usage_local);
        fprintf(stderr, "Peak memory usage for default space: %ld bytes.\n",
                peak_mem_usage_default);
    }
    return 0;
}
