-- Tests
-- ==
-- entry: generatePolynomialsTest
-- compiled input { 2 2 } output { [ [2,2,4], [2,2,5], [2,2,6], [2,2,7] ] }
-- compiled input { 3 2 } output { [ [3,2,8], [3,2,9], [3,2,10], [3,2,11], [3,2,12], [3,2,13], [3,2,14], [3,2,15] ] }

entry generatePolynomials (degree: i32) (prime: i32) : [](i32,i32,i32) =
  let polsTmp = map (\x -> (degree, prime, x)) (iota (prime ** (degree + 1)))
  in filter (\(_, _, x) -> x >= prime ** degree) polsTmp


entry generatePolynomialsTest (degree: i32) (prime: i32) : [][]i32 =
    let pols = generatePolynomials degree prime
    in map (\(d,p,i) -> [d,p,i]) pols
