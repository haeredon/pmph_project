-- Tests
-- ==
-- entry: isIrreducibleTest
-- compiled input { [2,3,9] [1,3,3] 3 3 } output { false }
-- compiled input { [2,3,9] [1,3,4] 3 3 } output { true }
-- compiled input { [2,5,245] [2,5,145] 5 3 } output { false }
-- compiled input { [2,5,246] [2,5,145] 5 3 } output { true }
-- compiled input { [3,5,2460] [2,5,1450] 5 4 } output { true }




import "translate"


entry isIrreducible (bb: (i32,i32,i32)) (aa: (i32,i32,i32)) (prime : i32) (padding: i32): bool
 =
    let a = translate padding aa
    let b = translate padding bb
    let aIndexTmp = scan (+) 0 a
    let aIndex = length (filter (\x -> x == 0) aIndexTmp)
    let max = if #2 bb == #2 aa then padding else 0

    let remain = unsafe loop (b, a, _) = (b, a, true) for i < max do
        let bIndexTmp = scan (+) 0 b
        let bIndex = length (filter (\x -> x == 0) bIndexTmp)

        let bIsLessDegree = bIndex > aIndex
        let isZero = reduce (+) 0 b == 0

        let dif = aIndex - bIndex
        let res = rotate dif a

        let bDividend = if isZero then 0 else b[bIndex]
        let resultTmp = bDividend / a[aIndex]
        let result = if bIsLessDegree then 0 else resultTmp

        let end = map(\i ->
            let ss = res[i] * result
            in (b[i] - ss) % prime
        ) (iota padding)

        in
        (end, a, (bIsLessDegree && !isZero) || (bIndex <= aIndex && result == 0))
    in
    #3 remain


entry isIrreducibleTest (b: []i32) (a: []i32) (prime : i32) (padding: i32): bool =
    isIrreducible (b[0], b[1], b[2]) (a[0], a[1], a[2]) prime padding

