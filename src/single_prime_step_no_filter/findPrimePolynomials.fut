-- Tests
-- ==
-- compiled input { 2 0 } output { [ [1] ] }
-- compiled input { 2 1 } output { [ [1,0],[1,1] ] }
-- compiled input { 2 2 } output { [ [0,1,0],[0,1,1],[1,1,1] ] }
-- compiled input { 2 3 } output { [ [0,0,1,0],[0,0,1,1],[0,1,1,1],[1,0,1,1],[1,1,0,1] ] }
-- compiled input { 2 4 } output { [ [0,0,0,1,0],[0,0,0,1,1],[0,0,1,1,1],[0,1,0,1,1],[0,1,1,0,1],[1,0,0,1,1],[1,1,0,0,1],[1,1,1,1,1] ] }

import "futlib/array"
import "generatePolynomials"
import "isZeroable"
import "translate"
import "isIrreducible"

let main (prime : i32) (maxDegree: i32) : [][]i32 =
  let genPoly0 = generatePolynomials 0 prime
  let genPoly1 = generatePolynomials 1 prime
  let genPoly2 = generatePolynomials 2 prime
  let genPoly3 = generatePolynomials 3 prime
  let isZero2  = filter (\p -> !(isZeroable (translate 3 p) prime)) genPoly2
  let isZero3  = filter (\p -> !(isZeroable (translate 4 p) prime)) genPoly3
  let resultSecondThird = if maxDegree == 2 then concat genPoly1 isZero2
                          else if maxDegree == 1 then genPoly1
                          else if maxDegree == 0 then genPoly0
                          else concat genPoly1 isZero2 isZero3

  let resultPolys = loop (polys) = (resultSecondThird) for i < maxDegree-3 do
    let degree = i+4
    let allPols = map (+(prime**degree)) (iota (prime ** (degree + 1) - prime ** degree))
    let padding = degree + 1
    let polIrreducibleTmp = map (\i ->
        let isIrreducePolys = map (\x -> isIrreducible (degree, prime, i) x prime padding) polys
        in (i, reduce (&&) true isIrreducePolys)
    ) allPols
    let tmp = filter (\(_, isIrre) -> isIrre) polIrreducibleTmp
    let polIrreducible = map (\(i,_) -> (degree, prime, i)) tmp
  in (concat polys polIrreducible)

  in map (\p -> translate (maxDegree + 1) p) resultPolys
