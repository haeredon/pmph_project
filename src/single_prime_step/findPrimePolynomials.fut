-- Tests                                                                                         
-- ==                                                                                            
-- compiled input { 2 0 } output { [ [1] ] }                                                     
-- compiled input { 2 1 } output { [ [1,0],[1,1] ] }                                             
-- compiled input { 2 2 } output { [ [0,1,0],[0,1,1],[1,1,1] ] }                                 
-- compiled input { 2 3 } output { [ [0,0,1,0],[0,0,1,1],[0,1,1,1],[1,0,1,1],[1,1,0,1] ] }       
-- compiled input { 2 4 } output { [ [0,0,0,1,0],[0,0,0,1,1],[0,0,1,1,1],[0,1,0,1,1],[0,1,1,0,1],[1,0,0,1,1],[1,1,0,0,1],[1,1,1,1,1] ] }

--import "generatePrimes"
import "futlib/array"
import "generatePolynomials"
import "isZeroable"
import "translate"
import "isIrreducible"

let main (n : i32) (maxDegree: i32) : [][]i32 =
  let (primes) = [n]
  let size = length primes

  let resultPolys = loop resultPolysAcc = empty((i32,i32,i32)) for i < size do
      let prime    = unsafe primes[i]
      let genPoly0 = generatePolynomials 0 prime
      let genPoly1 = generatePolynomials 1 prime
      let genPoly2 = generatePolynomials 2 prime
      let genPoly3 = generatePolynomials 3 prime
      let isZero2  = filter (\p -> !(isZeroable (translate 3 p) prime)) genPoly2
      let isZero3  = filter (\p -> !(isZeroable (translate 4 p) prime)) genPoly3
      let resultSecondThird = if maxDegree == 2 then concat genPoly1 isZero2
                              else if maxDegree == 1 then genPoly1
                              else if maxDegree == 0 then genPoly0
                              else concat isZero2 isZero3

      let resultPolysCurrent = loop (polys) = (resultSecondThird) for i < maxDegree-3 do
        let degree = i+4
        let allPols = generatePolynomials degree prime
        let padding = degree + 1
        let polIrreducible = filter (\p ->
            let isIrreducePolys = map (\x -> isIrreducible p x prime padding) polys
            in reduce (&&) true isIrreducePolys
        ) allPols
        in (concat polys polIrreducible)
      in concat resultPolysCurrent resultPolysAcc

  in map (\p -> translate (maxDegree + 1) p) resultPolys
