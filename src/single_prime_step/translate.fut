-- Tests if the translat efunction handles polynomials
-- without padding
-- ==
-- entry: translateTestTight                                                                     
-- compiled input { 1 2 0  } output { [0,0] }                                                    
-- compiled input { 1 2 1  } output { [0,1] }                                                    
-- compiled input { 1 2 2  } output { [1,0] }                                                    
-- compiled input { 1 2 3  } output { [1,1] }                                                    
-- compiled input { 1 2 4  } output { [0,0] }                                                    
-- compiled input { 1 2 5  } output { [0,1] }                                                    
-- compiled input { 2 5 0  } output { [0,0,0] }                                                  
-- compiled input { 2 5 1  } output { [0,0,1] }                                                  
-- compiled input { 2 5 2  } output { [0,0,2] }                                                  
-- compiled input { 2 5 3  } output { [0,0,3] }                                                  
-- compiled input { 2 5 4  } output { [0,0,4] }                                                  
-- compiled input { 2 5 5  } output { [0,1,0] }                                                  
-- compiled input { 2 5 6  } output { [0,1,1] }                                                  
-- compiled input { 2 5 7  } output { [0,1,2] }                                                  
-- compiled input { 2 5 8  } output { [0,1,3] }                                                  
-- compiled input { 2 5 9  } output { [0,1,4] }                                                  
-- compiled input { 2 5 10 } output { [0,2,0] }                                                  
-- compiled input { 2 5 11 } output { [0,2,1] }                                                  
-- compiled input { 2 5 12 } output { [0,2,2] }                                                  
-- compiled input { 2 5 13 } output { [0,2,3] }                                                  
-- compiled input { 2 5 14 } output { [0,2,4] }                                                  
-- compiled input { 2 5 15 } output { [0,3,0] }

-- Tests if the translate function handles polynomials                                           
-- with padding                                                                                  
-- ==                                                                                            
-- entry: translateTestLoose                                                                     
-- compiled input { 1 2 0  } output { [0,0] }                                                    
-- compiled input { 1 2 1  } output { [0,1] }                                                    
-- compiled input { 1 2 2  } output { [1,0] }                                                    
-- compiled input { 1 2 3  } output { [1,1] }                                                    
-- compiled input { 1 2 4  } output { [0,0] }                                                    
-- compiled input { 1 2 5  } output { [0,1] }                                                    
-- compiled input { 2 5 0  } output { [0,0,0,0] }                                                
-- compiled input { 2 5 1  } output { [0,0,0,1] }                                                
-- compiled input { 2 5 2  } output { [0,0,0,2] }                                                
-- compiled input { 2 5 3  } output { [0,0,0,3] }                                                
-- compiled input { 2 5 4  } output { [0,0,0,4] }                                                
-- compiled input { 2 5 5  } output { [0,0,1,0] }                                                
-- compiled input { 2 5 6  } output { [0,0,1,1] }                                                
-- compiled input { 2 5 7  } output { [0,0,1,2] }                                                
-- compiled input { 2 5 8  } output { [0,0,1,3] }                                                
-- compiled input { 2 5 9  } output { [0,0,1,4] }                                                
-- compiled input { 2 5 10 } output { [0,0,2,0] }                                                
-- compiled input { 2 5 11 } output { [0,0,2,1] }                                                
-- compiled input { 2 5 12 } output { [0,0,2,2] }                                                
-- compiled input { 2 5 13 } output { [0,0,2,3] }                                                
-- compiled input { 2 5 14 } output { [0,0,2,4] }                                                
-- compiled input { 2 5 15 } output { [0,0,3,0] }

-- Translate a polynomial tuple into a array polynomial
entry translate (padding: i32) (d: i32, p: i32, x: i32) : [padding]i32 =
  map (\i ->
    if i < padding - d - 1 then 0
    else (x  / (p ** (padding - i - 1))) % p)
  (iota padding)

-- Following 2 functions are implemented to get around the problem with                          
-- tuples not being supported properly in Futharks testing environment                           
entry translateTestTight (d: i32, p: i32, x: i32) : []i32 =                                      
    translate (d + 1) (d: i32, p: i32, x: i32)                                                   
                                                                                                 
entry translateTestLoose (d: i32, p: i32, x: i32) : []i32 =                                      
    translate (d * 2) (d: i32, p: i32, x: i32)    

--let main (n:i32) (i:i32) : []i32 = translate n (2,2,i)

