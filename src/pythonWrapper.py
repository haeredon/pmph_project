#!/usr/bin/env python

import subprocess
import ast


def runForField(prime, maxDegree, currentDegree=0, currentIrreducibles=""):
    args = ("./findPrimePolynomials")
    popen = subprocess.Popen(args, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    if (currentDegree == 0 and currentIrreducibles == ""):
        # Default start in field
        output = popen.communicate(input=str(prime) + " " + str(maxDegree))
        popen.wait()
        return output
    else:
        #with f as open("currentIrreducibles", "r"):
        #    irreds = f.read()
        #    output = popen.communicate(input=str(prime) + " " + str(maxDegree) + " " + str(currentDegree) + " " + irreds)
       #     popen.wait()
            return output
        # Resume for some degree & irreds
    
def translate(poly):
    (degree, prime, index) = poly
    array = range(degree+1)
    for i in array:
        array[i] = (index / (prime**(i - 1))) % prime
    return array

def toTuple(conwayPoly):
    poly   = conwayPoly
    prime  = int(poly.pop(0))
    degree = int(poly.pop(0))
    poly   = poly[0]
    return (prime, degree, map(int, poly))
    

# input: list of generated inputs (gIrreds)
#        list of conway irreducibles (cIrreds)
# output: (#hits, #misses). Ideally, #hits == maxDegree and #misses == 0
def test(gIrreds, cIrreds, prime, maxDegree):
    # Setup test environment
    conwayTuples = []
    generaPrimes = gIrreds
    hits = []
    miss = []
    extra= []
    # Convert from array to tuple form
    for poly in cIrreds:
        curTuple = toTuple(poly)
        if curTuple[0] == prime and curTuple[1] <= maxDegree:
            conwayTuples.append(curTuple)
    print conwayTuples
    # Perform tests
    for poly in conwayTuples:
        print poly
        processed = 0
        hit = False
        for prime in generaPrimes:
            processed += 1
            if len(prime) -1 != poly[1]:
                if hit:
                    break
                else:
                    print "Didn't find", poly[2]
                    miss.append(poly[2])
                    break
            elif (poly[2] == prime):
                hits.append(prime)
                hit = True
            else:
                print poly[2], "?=", prime
                extra.append(prime)
        print processed
        generaPrimes = generaPrimes[processed-1:]
    return (hits, miss, extra)

# Sanitizes the Conway file provided
def parseConways(fileName):
    lines = '['
    with open(fileName, 'r') as f:
         f.next()
         for line in f: 
             lines += line
    return ast.literal_eval(lines[:-2])[:-1]

def removeLeading(items):
    counter = 0
    tmp = map(int, items)
    for item in tmp:
        if item != 0:
            break
        else:
            counter += 1
    return tmp[counter:]
        
# Sanitizes i32 input to python-friendly lists
def parseFuthark(fileName):
    lines = ""
    with open(fileName, 'r') as f:
        for line in f:
            lines += line.translate(None, 'i32')
    temp = ast.literal_eval(lines)
    return map(removeLeading, temp)

# Test suite for various sub-functions
def testAll():
    tests = ['isZeroable.fut', 'translate.fut', 'isIrreducible.fut']
    allPassed = True
    for test in tests:
        allPassed = allPassed and testFunction(test)
    return allPassed

def testFunction(function):
    args = "futhark-test " + function
    popen = subprocess.Popen(args, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
    res = popen.wait()
    if int(res) != 0:
        print "Error in function " + function
    return int(res) == 0

if __name__ == '__main__':
    tmp = True
    while tmp:
        rawInp = raw_input(">")
        string = rawInp.lower().split()
        files  = rawInp.split()
        if string[0] == 'test':
            conway = files[1]
            irreds = files[2]
            conwayTuples = parseConways(conway)
            irredsTuples = parseFuthark(irreds)
            print test(irredsTuples, conwayTuples, int(string[3]), int(string[4]))
        elif string[0] == 'testFuncts':
            testAll()
        elif string[0] == '-h' or string[0] == '--help':
            print ("How to use:\nGenerate all prime polynomials from degree 2 to n," + 
                   " in field prime: prime n\nGenerate all prime polynomials from degree c " + 
                   "n, using file as irreducibles: prime c n file")
        elif len(string) == 2:
            print runForField(int(string[0]), int(string[1]))    
            tmp = False
        elif len(string) == 4:
            print runForField(int(string[0]), int(string[2]), int(string[1]), string[3])
            tmp = False
        else:
            print "Can't understand your input - type -h or --help for manual"
