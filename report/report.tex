\documentclass[10pt,a4paper,english]{article}
%% Indlæs ofte brugte pakker
\usepackage{amssymb}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{amsmath}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\rhead{\today}
\rfoot{\thepage}

\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb,graphicx}
\usepackage{xcolor}
\usepackage{listings}

\newtheorem{theorem}{Theorem}[section]

\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{CStyle}{
  backgroundcolor=\color{backgroundColour},   
  commentstyle=\color{mGreen},
  keywordstyle=\color{magenta},
  numberstyle=\tiny\color{mGray},
  stringstyle=\color{mPurple},
  basicstyle=\footnotesize,
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                    
  keepspaces=true,                 
  numbers=left,                    
  numbersep=5pt,                  
  showspaces=false,                
  showstringspaces=false,
  showtabs=false,                  
  tabsize=2,
  language=C
}

%% Titel og forfatter
\title{Programming Massively Parallel Hardware\\
       The Prime Polynomial Problem}
\author{Arinbj\"{o}rn Brandsson\\Lasse Petersen}

%% Start dokumentet
\begin{document}

%% Vis titel
\maketitle

\section{Abstract}
In this project, we examine how to parallelize over GPUs prime polynomial calculations 
restricted in a prime field. We present 4 different versions, showing our 
progress from start to finish, examining what we do differently each time. In 
the end, we find that performing long division of polynomials using array 
representation is faster than using our custom tuple representation.

\section{Problem Specification}
The purpose of this project is to find find polynomials without any linear 
factors within polynomial rings $Z_p[x]$, where p is a prime. These polynomials 
are also called conway polynomials. 

All polynomials of degree 1 are irreducible because there exists no linear 
factors which can reproduce a 1st-degree polynomial within $Z_p$.
%@TODO 
According to theorem~\ref{theorem} formulated in the project slides\footnote{Polynomials 
and Prime Numbers, Andreas Aabrandt}.

\begin{theorem}{}
\label{theorem}
For any polynomial $f\in Z_p[x]$ of degree 2 or 3, it holds that $f$ is reducible 
if and only if there exists an element $a\in Z_p$ such that $f(a) = 0$ in $Z_p[x]$
\end{theorem}

This gives a general method for finding an irreducible polynomial of degree 2 and 
3. It is more complicated to find irreducible polynomials of degree 4 and higher. 
A way to do this is to perform polynomial division on each candidate polynomial 
with each of the as-of-yet found irreducible polynomials. If exactly zero of the 
irreducible polynomials can be divided evenly into the candidate, then the 
candidate polynomial is irreducible too.

The general algorithm to find these polynomials is as follows (it has been 
changed through several versions discussed later in this report):

\begin{enumerate}
\item Generate a list of primes
\item For each prime construct all polynomials of degree 1, 2 and 3
\item Extract all zeroable polynomials of degree 2 and 3 out of the constructed 
ones. The extracted polynomials along with all constructed polynomials of 
degree 1 are irreducible
\item Construct all polynomials of degree 4 and divide each of them 
with each of the irreducible polynomials found so far. If the polynomial 
of degree 4 isn't evenly divisble by any of the irreducible polynomials 
add it to the list of irreducible polynomials
\item Repeat step 4 for degree one higher than the one in step 4.
\item When required degree is reached terminate and return the list of irreducible 
polynomials  
\end{enumerate}

\section{Parallelizable Properties of the Problem}
The prime polynomial problem has several aspects which makes it a candidate 
for parallelization. We are able to parallelize over primes, as every ring 
are independent of one another. We are also able to parallelize over the 
division of polynomials, for the same reason as previosuly mentioned - each 
division of the candidate polynomial is independent of other divisions. 

There are also parts of the problem which can't be parallelized, and therefore 
must be executed sequentially. The actual division of two polynomials is 
loop dependent on the previous iteration. We are also unable to parallelize 
over the degree of polynomials, as degree n+1 is dependent on the irreducible 
polynomials of degree n.

Based on these properties, we decided to parallelize over the list of candidate 
polynomials, as the candidate polynomial is the fastest-growing list in the 
program (if we have a prime field p, and a degree d, the size of the candidate 
polynomials is $(p-1)*p^d$). Over this, we also parallelize over the length 
of irreducible polynomials when dividing into a candidate, since division of 
polynomials is the most computation-heavy part of the program. As an example, 
consider we want to find 7th-degree irreducible polynomials in prime field 11. 
At this point, we have found 4950 2-6th degree irreducible prolynomials, which 
we need to divide into every candidate 7th-degree polynomial. Using the previous 
equation, we need to perform $((11-1)*11^7) + ((11-1)*11^7)*4950$, which 
approaches 1 quadrillion operations. Since we know that polynomial division is not 
a single calculation, since it is run sequentially, the actual number of operations 
are significantly larger. 

Since each prime field is unrelated to one another, we initially parallelized over 
the prime fields, from 2 up to a maxima. However this resulted in a more complex 
program, as well as memory limitation (explained in section~\ref{all-par}). 
Because of this, we decided to give the 
user the option to parallelize over a list of fields, since we are dealing with such large 
sets of polynomials that it makes more sense to focus on each prime by itself - 
however it is possible for the user to parallelize over the program, thereby 
parallelizing prime computations.

\section{General Structure of the Program and Version 0}
This section details the general structure of each version of the program. 
While some details in the implementation changes for each new version, the 
general structure and dependencies remains the same. This details the 
pre-implementation structure, and it changes as we develops further.

\subsection{findPrimePolynomials}
%TODO: Reference theorem in Problem Specification
Below is the pseudocode for our first draft of findPrimePolynomials:
\begin{lstlisting}[style=CStyle]
main n maxDegree:
  primes = generatePrimes n // Generates primes up to n
  for (i = 0; i < sizeOf primes; i++):
    prime = primes[i]
    genPoly2 = generatePolynomials 2 prime // Generate degree 2 polynomials
    genPoly3 = generatePolynomials 3 prime // Generate degree 3 polynomials
    irreducibles = irreducibles ++ (isZeroable on genPoly2 and genPoly3) // According to theorem 3.1 in Problem Specification
  for (i = 4; i < maxDegree; i++):
    genPoly = generatePolynomials i prime for each prime in primes
    irreducibles = irreducibles ++ (all irreducibles of genPoly / irreducibles of same field)
  return irreducibles
\end{lstlisting}
%@TODO: Write something clever about findPrimePolynomials

\subsection{isIrreducible}
Below is described the general algorithm for division of polynomials:
\begin{lstlisting}[style=CStyle]
isIrreducible dividend divisor:
  remains = dividend
  while (remains != 0 AND degree(dividend) >= degree(divisor)): // Sequential Loop
    t = leadingTerm(remains) / leadingTerm(divisor)
    remains = remains - t*divisor
  return (0 != remains)
\end{lstlisting}
%@TODO: Say something clever
\subsection{isZeroable}
This function implements the test for irreducible polynomials in the theorem 
for degree 2 and 3. It is a rather straightforward implementation, as it 
just tests for all values from 0 to its prime whether any of them equals 0.

\subsection{generatePolynomials}
In our first version of generatePolynomials, we generated polynomials in array form, 
as it is possible to represent polynomials by their coefficients. The higher 
the index of the coefficient, the lower its degree is. For example polynomial, 
the polynomial $x^2 + 2$ can be represented by the array $[1, 0, 2]$. 
\begin{lstlisting}[style=CStyle]
generatePolynomials prime dd:
  generate array of arrays of all permutations of polynomials in degree d in field prime
\end{lstlisting}

\section{First Version}
\label{all-par}
\subsection{Limitations of Previous Version}
\subsubsection{Polynomial Representation}
When we generated the polynomials at the start, the array representation of 
polynomials takes up a lot of space. To combat this, we created a polynomial 
dataype, which is of form (degree, prime, index).

The first two attributes specify the shape of the polynomial (the degree 
and the field of the polynomial), while the index specifies which 
'number' the polynomial is in its permutation. This is possible because each 
polynomial is contained in a field and is therefore finite, which can 
essentially be expressed in much the same way we are able to express 
numbers in binary.  

For example, consider the two polynomials $x^2 + 1$ and $x^2 +x $, both of 
degree 2 and in field 2. The first polynomial (we'll call it p1) is represented 
by the tuple (2, 2, 5) and the second polynomial (p2) is represented by the 
tuple (2, 2, 6).  The ordering of the polynomials is strictly ascending 
(so there's an observable pattern), and so, adding 1 to the index of p1 will 
overflow the least significant degree, and give us p2.

Using the above observation, we implemented a translate function, which 
takes a polynomial tuple, and returns its array form.

By expressing polynomials in this way, we solve the memory problem, since a 
3-tuple is smaller than the arrays we will have to work with. For example, 
when we reach degree 32 with prime 2, we generate more than 4 billion candidate 
polynomials, each of which generates an array of length 33, which is a lot of 
memory when they are in array form.

It also 
solves a problem we encountered with variable array sizes (since Futhark does 
not support variable array sizes in an array). 
\subsubsection{Padding}
Futhark's compiler optimization relies on information the compiler receives 
on compile time. Because of this, having variably-sized arrays in a function 
can lead to poor optimizations. To combat this, we added padding to the 
isIrreducible function, such that when we expand the two polynomials 
to arrays, using our translate function, they are of the same size. This helps 
the compiler to do loop distribution and privatize variables. While this 
does use more memory, because isIrreducible is parallelized, it is within each 
kernel the memory is used, which is acceptable.

\subsubsection{Tiling}
In figure~\ref{tiling}, found in the file findPrimePolynomials.fut, is an 
excerpt of the most computationally-intensive part of our program, where 
for each candidate polynomial, we divide it by all of our irreducible polynomials 
found so far. Since we are in a double loop, where the variable polys is of 
constant size, it is possible to perform tiling in Futhark, which allows for 
memory coalescing, greatly enhancing the speed of the program.
\begin{figure}
\begin{lstlisting}[style=CStyle]
let polIrreducibleTmp = map (\i ->                                                           
  let isIrreducePolys = map (\x -> isIrreducible (degree, prime, i) x prime padding) polys 
  in (i, reduce (&&) true isIrreducePolys)                                                 
) allPols  
\end{lstlisting}
\caption{Excerpt of code in findPrimePolynomials - this is where most of the 
         work is performed}
\label{tiling}
\end{figure}

%Polynomials of different degree takes up diferrent amounts of space in memory. 
%Since the program iterates over polynomials of increasing degree, each iteration 
%involves arrays of different sizes. Futhark doesn't support dynamic arrays 
%because this renders it unable to create optimized kernels where the data sent 
%to the gpu is known before hand. To fix this problem all arrays have been 
%padded with zeros to make room the the highest degree calculated.

\section{Second Version}
\subsection{Limitations of Previous Version}
\subsubsection{Prime Parallelization}
When running the previous version, we discovered that by trying to calculate 
each field's prime polynomials in parallel lead to massive memory usage, 
even in low degrees. This was because of Futhark's principle of having 
statically sized arrays, where an array of arrays must contain equally-sized 
arrays. If we wanted to bypass this when generating candidate polynomials, 
we would have to pad each array of a specific field according to the largest 
field. This meant that the amount of candidate polynomials for all fields 
would be equal to $length(primes) * (maxPrime - 1) * maxPrime^d$. So 
going back to a previous example, the amount of candidate polynomials for 
fields up to 11 of degree 7 is close to 1 billion, compared to close to 200 
million if we only calculated for field 11. However the careful observer would 
note that since it is a constant memory increase, the next degree would render the 
consant insignificant due to the exponential increase in polynomials generated. 

But there is another benefit to not parallelizing the primes. By not parallelizing 
the primes, the program gets less complex, and we can calculate irreducible polynomials 
faster for single primes. If parallelization over the primes is needed, the 
developer can run the program in parallel.


\section{Third Version}
\subsection{Limitations of Previous Version}
\subsubsection{Generating Polynomials}
We can further reduce the amount of memory used by never generating the 
polynomials, and instead pass an array of the candidate indexes, since 
we know in each loop what our current degree and prime is.

\subsubsection{Moved filter}
We wanted to check whether the Futhark compiler would be able to better 
flatten our code if we were to move the filter function outside our map. 
Unfortunately we didn't lead to any speedups, suggesting we have to perform 
the flattening by hand.

\section{Fourth Version}
\subsection{Limitations of Previous Version}
\subsubsection{Array Calculations}
For our previous versions, we have focused on using arrays to divide the 
polynomials. However allocating memory for polynomials is slow and memory-intensive, 
and is done every single time we need to divide a candidate polynomial by an 
irreducible polynomial. In an effort to minimize this overhead, we figured 
out a way to perform the divisions using only the polynomial tuples, instead of 
the converted arrays, such that we never need to expand the polynomials. 
Unfortunately, as we'll see in section~\ref{tests}, this did not lead to a 
speedup. Despite GPUs being capable of performing computationally-heavy 
calculations at a greater speed than a CPU, we can't be sure whether our calculations 
runs faster than array allocation and accesses. 

\section{Work and Depth}
\subsection{Depth}
Depth is the amount of sequential steps performed when running the program. 
Ideally, this value is minimized such that greater parallelism is exploited. 
For version 3 of our program, we start by looking at the depth of the 
function findPrimePolynomials. Here we run a sequential loop $max-3 $ iterations 
(where max is the maximal degree we go to). The reason we subtract 3 is because 
we run from degree 4 and up. In the loop, we have another sequential loop in 
isIrreducible, so the depth currently is 
$$D(findPrimePolynomials) = (max - 3)*D(isIrreducible)$$
Continuing in isIrreducible, we run a sequential loop for $d + 1$ times, where 
$d$ is the current degree. This gives us a depth in isIrreducible of 
$$D(isIrreducible) = d + 1$$
Plugging in our new depth value, we get a depth of 
$$D(findPrimePolynomials) = (max - 3)*(d + 1)$$

This can be rewritten with a sum function:
$$D(findPrimePolynomials) = \sum_{d=4}^{max} (d + 1) = 
\sum_{d=4}^{max} d + \sum_{d=4}^{max} 1 = \sum_{d=4}^{max}(d) + max - 4 \approx 
\frac{max*(max+1)}{2} + max-4$$
Which gives us our depth.

\subsection{Work}
For the work, we only look at the work generated by the loop inside 
findPrimePolynomials, as all other work is insignificant compared to the work below. 

When calculating the work, we need to map over all of the newly generated 
polynomials, which amounts to $p^(d+1)-p^d$ polynomials. For each of these 
polynomials, we must run isIrreducible over $len(polys)$ times, where 
$polys$ is the amount of currently-found irreducible polynomials. We then 
reduce over these to test whether any of them could be divided into the 
candidate polynomial, giving us $(p^{d+1} - p^d)*                                    
((len(polys)*(degree+1)^2$. We then filter over the newly-reducible polynomials, 
which is work time of $len(newlyReducible)$. Finally we do this $max-3$ times. 
This gives us work of:

$$W(findPrimePolynomials) = \sum_{d=4}^{max} (p^{d+1} - p^d)*
((len(polys)*(degree+1)^2+len(isIrreducible)))$$

\section{Benchmarking}
\label{tests}
% Prime: 11, degree: 6
% Version 2 runtime: 51.55 sec 32 kernels
% Version 3 runtime: 51.02 sec 42 kernels
% Version 4 runtime: 56.15 sec 34 kernels
The benchmark times for the different versions are checked by running 
findPrimePolynomials with the option $-t$, which gives us the result in 
microseconds. We tested the versions in the primefield 11, up to and including 
degree 5 (so we test all generated polynomials for degrees 4 and 5). We 
run all the tests on APL's gpu servers, specifically gpu01. For version 1, 
since we parallelized over all primes up to prime 11, we eventually ran out 
of memory, so version 1 does not have a runtime.

\begin{table}[h]
\center
\begin{tabular}{| l | c |}
\hline
Version & Time (Seconds)\\\hline
1       & Out of Memory\\\hline
2       & 51.55        \\\hline
3       & 51.02        \\\hline
4       & 56.15        \\\hline
4 (sequential) & 4057.80\\\hline
\end{tabular}
\caption{Runtimes of Different Versions, with prime=11 and degree=5}
\end{table}

As expected, version 1 ran out of memory, due to it needing to pad according to 
the highest degree for each prime, which lead to massive memory usage. However 
more surprisingly, version 2 and 3 runs slightly faster than our version 4, 
despite using costly array operations. This may be because that while we are using 
cheaper operations when calculating solely through indexes instead of using 
arrays, the amount of times we have to use these cheaper operations causes 
slowdown.

As expected however, we have a speedup when running the program in parallel 
compared to when running it sequentially at a magnitude of approximately 
79.5x. 

Besides this, to run some standard tests, run the bash script runTests.sh 
with the command {\tt sh runTests.sh}.

\section{Conclusion}
For this project, we present a way to, in parallel, calculate prime polynomials, 
as well as four different versions of the same implementation. We found that 
using a tuple representation to represent each polynomial solved some memory 
problems, as a 3-tuple is less costly than a majority of arrays. We also found 
that version 3, which calculates irreducible polynomials in array representation, 
runs faster than version 4, which performs polynomial division directly from 
the tuple representation. Logic dictates that version 4 should be the fastest 
version, since we don't perform array operations and allocations, however at 
this point in time we are not able to explain why that is so, except that 
doing index calculations may be very costly.

\end{document}
